//var __onBeforeAppLoadSuccess;
$(document).ready(function(){
    // if(!__onBeforeAppLoadSuccess){
        // __onBeforeAppLoadSuccess=function(onSuccessHandler){
            // onSuccessHandler({});
        // }
    // }
    // __onBeforeAppLoadSuccess(function(customobect){
        // logStatus(customobect, LOG_DEBUG);
        // _initPageTemplate();
        // $(window).bind('hashchange',function(){
            // _initPageTemplate();
        // });
    // })
});
// *** [SK] comments by shanethatech  ***//
//*** Get current url path ***//
function getPathHashData(){
    logStatus("Calling getPathHashData", LOG_COMMON_FUNCTIONS);
    //logStatus("getValidHashPath",LOG_FUNCTIONS);
    try {
        var _hashdata	=	{};
        var _hash	=	$(location).attr('hash');
        var _path	=	(_hash.split("#/")[1]) ? _hash.split("#/")[1]:'';
        _path	=	_path.split("?");
        _hashdata.path	=	_path[0];
        var astr		=	_path[1] ? _path[1]:'';
        _hashdata.args	=	{};
        if(astr!='') {
            var _astr1		=	astr.split("&");
            if(_astr1.length >0){
                for(var kval in _astr1){
                    if(_astr1.hasOwnProperty(kval)) {
                        var _kval = _astr1[kval].split("=");
                        _hashdata.args[_kval[0]] = _kval[1];
                    }
                }
            }

        }
        return _hashdata;
    } catch (e) {
        //ShowExceptionMessage("getValidHashPath", e);
    }
}
function _initPageTemplate(){

    logStatus("Calling initPageTemplate", LOG_COMMON_FUNCTIONS);
    try{
        var template =	getPathHashData();
        var path	=	(template.path!='') ? template.path:'home';
        showValidIcons(path);
        loadView(path,function(){
            //setfootermenu();
            UpdatePhotoOnFooter();
            _onPageLoadHandler();

        });
    } catch (e) {
        ShowExceptionMessage("initPageTemplate", e);
    }
}
function loadView(path,onPathLoadSuccess){
    logStatus("Calling loadView", LOG_COMMON_FUNCTIONS);
    $(".page_content").load(PATH_VIEWS+path+".html",function(){
        if(onPathLoadSuccess){
            onPathLoadSuccess();
        }
    });
}
function rtrim(_char,_string){
    logStatus("Calling rtrim", LOG_COMMON_FUNCTIONS);
    if (_string.charAt(_string.length - _char.length) == _char) {
      _string = _string.substr(0, _string.length - _char.length);
    }
    return _string;
}
/*
function base64_encode(data) {
    logStatus("Base64_encode", LOG_COMMON_FUNCTIONS);
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var i = 0, ac = 0, tmp_arr = [];
    if (!data) {
        return data;
    }
    data = unescape(encodeURIComponent(data));
    do {
        // pack three octets into four hexets
        var o1 = data.charCodeAt(i++);
        var o2 = data.charCodeAt(i++);
        var o3 = data.charCodeAt(i++);
        var bits = o1 << 16 | o2 << 8 | o3;
        var h1 = bits >> 18 & 0x3f;
        var h2 = bits >> 12 & 0x3f;
        var h3 = bits >> 6 & 0x3f;
        var h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);
    var enc = tmp_arr.join('');
    var r = data.length % 3;
    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}
function base64_decode(data) {
    logStatus(" Calling Base64_encode", LOG_COMMON_FUNCTIONS);
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var i = 0, ac = 0, tmp_arr = [];
    if (!data) {
        return data;
    }
    data += '';
    do {
        // unpack four hexets into three octets using index points in b64
        var h1 = b64.indexOf(data.charAt(i++));
        var h2 = b64.indexOf(data.charAt(i++));
        var h3 = b64.indexOf(data.charAt(i++));
        var h4 = b64.indexOf(data.charAt(i++));

        var bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        var o1 = bits >> 16 & 0xff;
        var o2 = bits >> 8 & 0xff;
        var o3 = bits & 0xff;
        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    var dec = tmp_arr.join('');
    return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
}*/
function _movePageTo(page,args){
    logStatus("Calling MovePageTo", LOG_COMMON_FUNCTIONS);
    var _page	=	(page && page!='') ? page:'home';
    var _argstr	=	'';
    if( args && ( !$.isEmptyObject(args) ) ){
        _argstr	=	'?';
        $.each(args,function(key,value){
            _argstr+=key+'='+value+'&';//encodeURIComponent()
        });
        _argstr	=	rTrim(_argstr,'&');
    }
    window.location.href	=	'#/'+_page+_argstr;
}  

function _setCacheArray(key,array){
    logStatus("Calling SetCacheArray", LOG_COMMON_FUNCTIONS);
    localStorage.setItem(key,JSON.stringify(array));
}
function _getCacheArray(key){
    logStatus("Calling GetCacheArray", LOG_COMMON_FUNCTIONS);
    var data	=	localStorage.getItem(key);
    return (data) ? JSON.parse(data):{};
}
function _setCacheValue(key,value){
    logStatus("Calling SetCacheValue", LOG_COMMON_FUNCTIONS);
    localStorage.setItem(key,value);
}
function _getCacheValue(key){
    logStatus(" Calling GetCacheValue", LOG_COMMON_FUNCTIONS);
    var value	=	localStorage.getItem(key);
    return (value) ? value:'';
}
function is_page(page){
    logStatus("Calling Is_page", LOG_COMMON_FUNCTIONS);
    var pageinfo	=	getPathHashData();
    return (page==pageinfo.path);
}
function _close_popup(){
    logStatus("Calling Close_popup", LOG_COMMON_FUNCTIONS);
    $(".popup").remove();
    $(".popupmask").remove();
}
function _popup(message,obj,success){
    logStatus("Calling Popup", LOG_COMMON_FUNCTIONS);
    var _obj	=	(obj) ? obj:{};
    var customClassMask		=	"";
    var closeButton		=	"";
    if(_obj.closeOnOuterClick){
        customClassMask	=	'close_onouterclick';
    }
    if(_obj.close){
        closeButton	='<button class="close selectbtn" onclick="_close_popup()">close</button>';
    }
    var HTML	=	'<div class="popupmask '+customClassMask+'" onclick=""></div><div class="popup hh">' + closeButton;
    HTML	+=message;
    HTML	+='</div>';
    $('body').append(HTML);
    if(success){
        success();
    }
}
function _isNumber(evt) {

    logStatus("Calling IsNumber", LOG_COMMON_FUNCTIONS);
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return !(charCode > 31 && (charCode < 46 || charCode > 57));
		
}
function _isValidPercent(elem){
    var $elem	=	$(elem);
    return ( ($elem.val()<=100) && ($elem.val()>=0));
}
function app_tempvars(key,value){
    logStatus("Calling App_tempvars", LOG_COMMON_FUNCTIONS);
    var $key	=	$("#" + key);
    if(value){
        var HTML	=	"";
        if(typeof($key.val())!='undefined'){
            $key.val(value);
        } else {
            HTML	=	'<input type="hidden" id="' + key + '" value="' + value + '" />';
            $('body').append(HTML);
        }
    } else {
        return $key.val();
    }
}
function getValidImageUrl(url,returnURL,_default) {
    var __default	=	(_default && _default!='') ? _default:'images/no_image.png';
    $("<img>", {
        src: url,
        error: function() { 
            returnURL(__default);
        },
        load: function() { 
            returnURL(url);
        }
    });
}

function showValidIcons(path){
    logStatus("Calling showValidIcons", LOG_FUNCTIONS);
    try{
        var $clsshanethethatech = $('.clsshanethethatech');
        var $logoright = $('.logo_right');
        var $footerbordermenu = $('.footerbordermenu');
        var $footerusermenu = $('.footerusermenu');
        $clsshanethethatech.show();
        if(path=='chestpress') {
            $logoright.hide();
            $footerusermenu.hide();
            $footerbordermenu.hide();
            $clsshanethethatech.hide();
        }else if(path=='login'){
            $logoright.hide();
            $footerbordermenu.show();
            $footerusermenu.hide();
            $clsshanethethatech.hide();
        } else {
            $footerbordermenu.hide();
            $logoright.show();
            $footerusermenu.show();
        }
    } catch (e) {
        ShowExceptionMessage("showValidIcons", e);
    }
}