var __onBeforeAppLoadSuccess;
function getToken(){
	
	logStatus("Get Token Device ID == "+device.uuid, LOG_EVENTS);
	
	var formData = new FormData();
	formData.append("device_id", device.uuid);
	formData.append("secret_key", TOKEN_KEY); 
	
	
	$.ajax({
	  url: base_url+'/backoffice/service/generate_token.php',
	  data: formData,
	  processData: false,
	  contentType: false,
	  type: 'POST',
	  success: function(data){
		//alert(data);
		
		var resultJson = JSON.parse(data);
		GEN_TOKEN = resultJson.token;
		logStatus("Get Token RESULT == "+resultJson.token, LOG_EVENTS);
		if(!__onBeforeAppLoadSuccess){
			__onBeforeAppLoadSuccess=function(onSuccessHandler){
				onSuccessHandler({});
			}
		}
		__onBeforeAppLoadSuccess(function(customobect){
			logStatus(customobect, LOG_DEBUG);
			_initPageTemplate();
			$(window).bind('hashchange',function(){
				_initPageTemplate();
			});
		})
		
	  }
	});
}
