/******************************************************************************************/
/********************************* COMMON Layer - Application Common - Start ******************/
/******************************************************************************************/
var IS_APP_PAUSED = 0;
function ApplicationReady() {
    logStatus("ApplicationReady", LOG_COMMON_FUNCTIONS);
    try{    
        IS_APP_PAUSED = 0;
        _IS_SYNC_PROGRESS = 1;
        InitializeCybexmachine();
    } catch (e) {
        ShowExceptionMessage("ApplicationReady", e); 
    }
}
 
/* Events */
function ExecutePause() {
    logStatus("ExecutePause", LOG_COMMON_FUNCTIONS);
    try{
        //xx IS_APP_PAUSED = 1;
    } catch (e) {
        ShowExceptionMessage("onResume", e);
    }
    // Handle the pause event
}
 
function ExecuteResume() {
    logStatus("ExecuteResume", LOG_COMMON_FUNCTIONS);
    try{
        IS_APP_PAUSED = 0;
        //XX getServerdata();
    } catch (e) {
        stopprogress();
        ShowExceptionMessage("ExecuteResume", e);
    }
    // Handle the resume event
}

function ExecuteOnline() {
    logStatus("ExecuteOnline", LOG_COMMON_FUNCTIONS);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteOnline", e);
    }
    // Handle the online event
}

function ExecuteOffline() {
    logStatus("ExecuteOffline", LOG_COMMON_FUNCTIONS);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteOffline", e);
    }
    // Handle the offline event
}

//Only on Andriod
function ExecuteBackKeyDown() {
    logStatus("ExecuteBackKeyDown", LOG_COMMON_FUNCTIONS);
    try{
        /**
         * @param app.backHistory() This is app backHistory function
         */
        if(GLOBAL_DIALOG){
            if(GLOBAL_DIALOG.close){
                GLOBAL_DIALOG.close();
            }
        }
        navigator.app.backHistory();
        /*
        ShowAlertConfirmMessage("Are you sure you want to EXIT the app?", function(selbtn){
            if (selbtn == 2){
                if (_ISIN_PROGRESS == 1){
                    stopprogress();
                    setTimeout(function(){
                        IS_APP_PAUSED = 1;
                        navigator.app.exitApp();
                        }, 2000);
                }else{
                    IS_APP_PAUSED = 1;
                    navigator.app.exitApp();                    
                }                
            }  
        }, 'Cancel,OK');
        */
    } catch (e) {
        ShowExceptionMessage("ExecuteBackKeyDown", e);
    }
    // Handle the back button    
}

function ExecuteBatteryCritical(info) {
    logStatus("ExecuteBatteryCritical", LOG_COMMON_FUNCTIONS);
    logStatus(info, LOG_DEBUG);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryCritical", e);
    }
    // Handle the battery critical event
    //alert("Battery Level Critical " + info.level + "%\nRecharge Soon!"); 
}

function ExecuteBatteryLow(info) {
    logStatus("ExecuteBatteryLow", LOG_COMMON_FUNCTIONS);
    logStatus(info, LOG_DEBUG);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryLow", e);
    }
    // Handle the battery low event
    //alert("Battery Level Low " + info.level + "%"); 
}

function ExecuteBatteryStatus(info) {
    logStatus("ExecuteBatteryStatus", LOG_COMMON_FUNCTIONS);
    logStatus(info, LOG_DEBUG);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteBatteryStatus", e);
    }
    // Handle the online event
    //console.log("Level: " + info.level + " isPlugged: " + info.isPlugged); 
}

function ExecuteMenuKeyDown() {
    logStatus("ExecuteMenuKeyDown", LOG_COMMON_FUNCTIONS);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteMenuKeyDown", e);
    }
    // Handle the back button
}

function ExecuteSearchKeyDown() {
    logStatus("ExecuteSearchKeyDown", LOG_COMMON_FUNCTIONS);
    try{
    } catch (e) {
        ShowExceptionMessage("ExecuteSearchKeyDown", e);
    }
    // Handle the search button
}

function getValidStringFromHTMLCode(value){
    if (value){
        return value.replace(/amp;/g, "").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'").replace(/<br>/g, "\n");            
    }
    return "";
}
function escapeSqlString(strInputString){
    return strInputString.replace(/'/g, "\\'");
}
function getSQLSafeString(value){
    if (value){
        var escaped1 = getValidStringFromHTMLCode(value);
        return escaped1.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }else{
        return "";
    }
}
function formatHHMMSSFromSec(sec){
    var sec_num = parseInt(sec, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}