/******************************************************************************************/
/************************** COMMON.JS VERSION 2.1 23/02/2013 ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Global Events  - Start *********************************/
/******************************************************************************************/

/* Setting up the Device ready event */
document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("pause", onPause, false);
document.addEventListener("resume", onResume, false);
document.addEventListener("online", onOnline, false);
document.addEventListener("offline", onOffline, false);
window.addEventListener("batterycritical", onBatteryCritical, false);
window.addEventListener("batterylow", onBatteryLow, false); 
window.addEventListener("batterystatus", onBatteryStatus, false);
document.addEventListener("menubutton", onMenuKeyDown, false);
//document.addEventListener("searchbutton", onSearchKeyDown, false);
function onDeviceReady() {
    //XX ShowSplashScreen();
	getToken();
    logStatus("Calling OnDeviceReady", LOG_EVENTS);
    try {
        CreateApplicationDirectories();
        _IS_DEVICE_READY_PROGRESS = 1;
        onPlatformReady();
        InitializeDBObjects();
        //document.addEventListener("backbutton", onBackKeyDown, false);
        ApplicationReady(); //Call to Application Level Function
		 window.FirebasePlugin.getToken(function(token) {
            if(token!='' && token!=null){
            get_device_token(token);
            }
        }, function(error) {
            console.error(error);  
        }); 
        window.FirebasePlugin.onTokenRefresh(function(token) {
            if(token!=''){
            get_device_token(token);
            }
        }, function(error) {
            console.error(error);
        }); 
		  if(device.platform=='iOS'){
		   window.FirebasePlugin.grantPermission();
		   window.FirebasePlugin.hasPermission(function(data){
			console.log("push notification" + data.isEnabled);
			  });
		  }
		  window.FirebasePlugin.onNotificationOpen(function(notification) {
		   console.log(notification);
		  }, function(error) {
		   console.error(error);
		  });
    } catch (e) {
        ShowExceptionMessage("OndeviceReady", e);
    }
}
function get_device_token(token){
    $('#device_token').val(token);
}
function onPause() {
    logStatus("Excuting onPause", LOG_EVENTS);
     try{
        ExecutePause();
    } catch (e) {
        ShowExceptionMessage("onPause", e);
    }
    // Handle the pause event
}

function onResume() {
    logStatus("Excuting onResume", LOG_EVENTS);
    try{
        ExecuteResume();
    } catch (e) {
        ShowExceptionMessage("onResume", e);
    }
    // Handle the resume event

}

function onOnline() {
    logStatus("Excuting onOnline", LOG_EVENTS);
    try{
        ExecuteOnline();
    } catch (e) {
        ShowExceptionMessage("onOnline", e);
    }
    // Handle the online event

}

function onOffline() {
    logStatus("Excuting onOffline", LOG_EVENTS);
    try{
        ExecuteOffline();
    } catch (e) {
        ShowExceptionMessage("onOffline", e);
    }
    // Handle the offline event

}

//Only on Andriod
function onBackKeyDown() {
    logStatus("Excuting onBatteryCritical", LOG_EVENTS);
    try{
        ExecuteBackKeyDown();
    } catch (e) {
        ShowExceptionMessage("onBackKeyDown", e);
    }
    // Handle the back button    
    

}

function onBatteryCritical(info) {
    logStatus("Excuting onBatteryCritical", LOG_EVENTS);
    try{
        logStatus(info, LOG_DEBUG);
        ExecuteBatteryCritical();
    } catch (e) {
        ShowExceptionMessage("onBatteryCritical", e);
    }
    // Handle the battery critical event
    //alert("Battery Level Critical " + info.level + "%\nRecharge Soon!"); 

}

function onBatteryLow(info) {
    logStatus("Excuting onBatteryLow", LOG_EVENTS);
    try{
        logStatus(info, LOG_DEBUG);
        ExecuteBatteryLow();
    } catch (e) {
        ShowExceptionMessage("onBatteryLow", e);
    }
    // Handle the battery low event
    //alert("Battery Level Low " + info.level + "%"); 

}

function onBatteryStatus(info) {
    /**
     * @param info.level This is level
     * @param info.isPlugged This is isPlugged
     */
    logStatus("Excuting onBatteryStatus", LOG_EVENTS);
    try{
        ExecuteBatteryStatus();
    } catch (e) {
        ShowExceptionMessage("onBatteryStatus", e);
    }
    // Handle the online event
    console.log("Level: " + info.level + " isPlugged: " + info.isPlugged); 
}

function onMenuKeyDown() {
    logStatus("Excuting onMenuKeyDown", LOG_EVENTS);
    try{
        ExecuteMenuKeyDown();
    } catch (e) {
        ShowExceptionMessage("onMenuKeyDown", e);
    }
    // Handle the back button
}

function onSearchKeyDown() {
    logStatus("Excuting onSearchKeyDown", LOG_EVENTS);
    try{
        ExecuteSearchKeyDown();
    } catch (e) {
        ShowExceptionMessage("onSearchKeyDown", e);
    }
    // Handle the search button
}
/******************************************************************************************/
/********************************* Global Events  - End *********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Functions  - Start ************************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Translations - Start ***********************************/
/******************************************************************************************/
function getTraslatedString(nodestr) {
    logStatus("Excuting getTraslatedString", LOG_COMMON_FUNCTIONS);
    try {
        if (!localStorage.curLangCode) {
            localStorage.curLangCode = "en";
           // localStorage.curLangId = getSettingsLangidfromlandcode(localStorage.curLangCode);
        }

        var labelCurval;
        if (!__LangXmlData) {
            if ((!localStorage.translations) || (localStorage.translations == null)) {
                localStorage.translations = __LangXmlDatastr_Default;
            }
            var parser=new DOMParser();
            __LangXmlData = parser.parseFromString(localStorage.translations,"text/xml"); //$.parseXML(localStorage.translations);
        }

        $(__LangXmlData).find(localStorage.curLangCode).find("trans").each(function () {
            if ($(this).attr("id") == nodestr) {
                labelCurval = $(this).text();
            }
        });
        return labelCurval;
    } catch (e) {
        ShowExceptionMessage("getTraslatedString",e);
    }
}

function getTraslatedText(nodestr, destinationId) {
    logStatus("Excuting getTraslatedText", LOG_COMMON_FUNCTIONS);
    try {

        var labelCurval = getTraslatedString(nodestr);

        $("#" + destinationId).html(labelCurval)
    } catch (e) {
        ShowExceptionMessage("getTraslatedText", e);
    }
}
/******************************************************************************************/
/********************************* Translations  - End ************************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Alert Message  - Start *********************************/
/******************************************************************************************/
var _ISIN_PROGRESS = 0;
function startprogress(title, msg){
    logStatus("Excuting startprogress", LOG_COMMON_FUNCTIONS);
    try {
       /* var durationOptions = {
                minDuration: 2
            };*/
        if (_ISIN_PROGRESS == 1){
            stopprogress();
            setTimeout(function(){
                _ISIN_PROGRESS = 1;            
                NotificationActivityStart(title, msg);                
            },700);
            //XX },1500)
        }else{
            _ISIN_PROGRESS = 1;            
            NotificationActivityStart(title, msg);
        }
        
    }catch(e){
         ShowExceptionMessage("startprogress", e);
    }
}

function stopprogress(){ 
    logStatus("Excuting stopprogress", LOG_COMMON_FUNCTIONS);
    try {
        window.setTimeout(function() {
            NotificationActivityStop();
            _ISIN_PROGRESS = 0;            
        }, 200);    
        //XX }, 1000);
        //NotificationActivityStop(); 
    }catch(e){
         ShowExceptionMessage("stopprogress", e);
    }
}

function showAlert(message) {
    logStatus("Excuting showAlert", LOG_COMMON_FUNCTIONS);
    try {
        NotificationAlert(message);
    }catch(e){
         ShowExceptionMessage("showAlert", e);
    }
}

function alertDismissed() {
    logStatus("Excuting alertDismissed", LOG_COMMON_FUNCTIONS);
}

function ShowAlertMessage(strMsg){
    logStatus("Excuting ShowAlertMessage", LOG_COMMON_FUNCTIONS);
    try {
        setTimeout(function () {
                   showAlert(strMsg);
                   }, 0);
    }catch(e){
         ShowExceptionMessage("ShowAlertMessage", e);
    }
}

function ShowAlertConfirmMessage(strMsg, callBackFunction, buttons) {
    logStatus("Excuting ShowAlertConfirmMessage", LOG_COMMON_FUNCTIONS);
    try {
        setTimeout(function() {
            NotificationConfirmMessage(
                    strMsg,  // message
                    callBackFunction,         // callback
                    buttons                 // buttonName
                    );
                   }, 0);
    }catch(e){
         ShowExceptionMessage("ShowAlertConfirmMessage", e);
    }
}

function ShowTransAlertMessage(strMsg){
    logStatus("Excuting ShowTransAlertMessage", LOG_COMMON_FUNCTIONS);
    try {
        var messageTrans = getTraslatedString(strMsg);
        ShowAlertMessage(messageTrans);
    }catch(e){
         ShowExceptionMessage("ShowTransAlertMessage", e);
    }
}

/**
 * @return {string}
 */
function FormatExceptionMessage(Module, exception) {
    return "Exception: On " + Module + " " + exception;
}

function ShowExceptionMessage(Module, exception) {
    logStatus("Excuting ShowExceptionMessage", LOG_COMMON_FUNCTIONS);
    try {
        var Message = FormatExceptionMessage(Module, exception);
        if (SHOW_EXCEPTION_ALERT == 1) {
            ShowAlertMessage(Message);
        } else {
            LogMessage(Message);
        }
    }catch(e){
         ShowExceptionMessage("ShowExceptionMessage", e);
    }
}

/******************************************************************************************/
/********************************* Alert Message  - End   *********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Log Status  - Start ************************************/
/******************************************************************************************/
function LogMessage(strMsglog) {
    //if(!IS_WEBAPP ) {
        console.log(strMsglog);
    //}
}

function logStatus(strMsglog, loglevel) {

    try{
        if (!loglevel){
            loglevel = LOG_DEBUG;
        }
        if (loglevel <= APPLICATION_LOG_LEVEL) {

            LogMessage(strMsglog);
        }
    }catch(e){
         ShowExceptionMessage("logStatus", e);
    }
}

/******************************************************************************************/
/********************************* Log Status  - End **************************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Date and Time - Start **********************************/
/******************************************************************************************/
/* Updates Current system date and time to the Global Variable */
function updateDateAndTime() {
    logStatus("Excuting updateDateAndTime", LOG_COMMON_FUNCTIONS);
    try{
        _todaydate = getValidDateAndTime();
    }catch(e){
         ShowExceptionMessage("updateDateAndTime", e);
    }
}

function getfourdigitsYear(number) {
    logStatus("Excuting updateDateAndTime", LOG_COMMON_FUNCTIONS);
    try{
        return (number < 1000) ? number + 1900 : number;
    }catch(e){
         ShowExceptionMessage("getfourdigitsYear", e);
    }
}

function getValidDateAndTime() {
    logStatus("Excuting getValidDateAndTime", LOG_COMMON_FUNCTIONS);
    try{
        var months =['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds();
        var timeStr = "" + hours;
        timeStr += ((minutes < 10) ? ":0" : ":") + minutes;
        timeStr += ((seconds < 10) ? ":0" : ":") + seconds;

        var today = (getfourdigitsYear(now.getYear())) + "-" + months[now.getMonth()] + "-" + now.getDate();
        return today + " " + timeStr;
    }catch(e){
         ShowExceptionMessage("getValidDateAndTime", e);
    }
}

function getPlainDateandTime(){
    logStatus("Excuting getPlainDateandTime", LOG_COMMON_FUNCTIONS);
    try{
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
        return d.getFullYear() + '' +
        ((''+month).length<2 ? '0' : '') + month + '' +
        ((''+day).length<2 ? '0' : '') + day + '' +
        ((''+hour).length<2 ? '0' :'') + hour + '' +
        ((''+minute).length<2 ? '0' :'') + minute + '' +
        ((''+second).length<2 ? '0' :'') + second;
    }catch(e){
         ShowExceptionMessage("getPlainDateandTime", e);
    }
}

function getPlainDate(){
    logStatus("Excuting getPlainDate", LOG_COMMON_FUNCTIONS);
    try{
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        return  d.getFullYear() + '' +
        ((''+month).length<2 ? '0' : '') + month + '' +
        ((''+day).length<2 ? '0' : '') + day;
    }catch(e){
         ShowExceptionMessage("getPlainDate", e);
    }
}

function hour2mins(timestr) {
    logStatus("Excuting hour2mins", LOG_COMMON_FUNCTIONS);
    try{
        var spltd = timestr.split(":");
        if (spltd.length > 2) {
            return parseInt(parseInt(spltd[0] * 60) + parseInt(spltd[1]));
        } else {
            return false;
        }
    }catch(e){
         ShowExceptionMessage("hour2mins", e);
    }
}

function mins2hour(instr) {
    logStatus("Excuting mins2hour", LOG_COMMON_FUNCTIONS);
    try{
        var hourstr = parseInt(instr / 60);
        if (hourstr.toString().length == 1) {
            hourstr = "0" + (hourstr + '');
        }
        var minstr = parseInt(instr % 60);
        if (minstr.toString().length == 1) {
            minstr = "0" + (minstr + '');
        }
        return hourstr + ':' + minstr;
    }catch(e){
         ShowExceptionMessage("mins2hour", e);
    }
}
/*
function timediff(t1, t2) {
    logStatus("Excuting timediff", LOG_COMMON_FUNCTIONS);
    try{
        var t1 = hour2mins(t1);
        var t2 = hour2mins(t2);
        var ret = mins2hour(parseInt(t2 - t1));
        if (t2 < t1) {
            ret = mins2hour(parseInt(parseInt(t2 + 1440) - t1));
        }
        return ret;
    }catch(e){
         ShowExceptionMessage("timediff", e);
    }
}
*/
function getTwodigittime(timestr){
    logStatus("Excuting getTwodigittime", LOG_COMMON_FUNCTIONS);
    try{
        var spltd = timestr.split(":");
        var timeplain = '';
        for (var i = 0; i < spltd.length; i++) {
            var timeinc = spltd[i];
            timeplain = timeplain + ((''+timeinc).length<2 ? '0' : '') + timeinc + '';
        }
        return timeplain;
    }catch(e){
         ShowExceptionMessage("getTwodigittime", e);
    }
}

function getOnlyHoursAndMinutes(time){
    var timepart = time.split(":");
    if (timepart.length > 1){
        return timepart[0]+":"+timepart[1];
    }
    return "";
}

function getTimeFromDateAndTime(datetime){
    var datepart = datetime.split(" ");
    if (datepart.length > 1){
        return datepart[1];        
    }
    return "";
}

function getDateFromDBDateAndTime(datetime){
    var datepart = datetime.split(" ");
    if (datepart.length > 1){
        var datepartarray = datepart[0].split("-");
        return datepartarray[2] + "/" + datepartarray[1] + "/" + datepartarray[0];        
    }
    return "";
}

function convert24TimeTo12Time(time){
    var hms = time.split(':');
    if (hms.length > 0){
        var h = +hms[0],
        suffix = (h < 12) ? 'am' : 'pm';
        hms[0] = h % 12 || 12;        
        return hms.join(':') + suffix            
    }
}

/******************************************************************************************/
/********************************* Date and Time - End **********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* String Functions-Start *********************************/
/******************************************************************************************/
function addSlash(path){
    var lastChar = path.substr(-1); // Selects the last character
    if (lastChar != '/') {         // If the last character is not a slash
       path = path + '/';            // Append a slash to it.
    }
    return path;
}

function getFileNameFromPath(fullPath){
    return fullPath.replace(/^.*[\\\/]/, '');
}
/******************************************************************************************/
/********************************* String Functions -End **********************************/
/******************************************************************************************/


/******************************************************************************************/
/********************************* Work Around Func - Start **********************************/
/******************************************************************************************/
/**
 * @return {boolean}
 */
function CanIExecute(){
    var current = new Date().getTime();
    var delta = current - _LASTCLICKTIME;
    window._LASTCLICKTIME = current;
    if (delta >= 500) {
        console.log("true event");
        //ShowAlertMessage("true " + delta);
        return true;
    } else {
        console.log("false event");
        //ShowAlertMessage("false " + delta);
        return false;
    }
}

/******************************************************************************************/
/********************************* Work Around - End **********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Validation Func - Start **********************************/
/******************************************************************************************/
/**
 * @return {boolean}
 */
function IsEmailValid(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

/**
 * @return {boolean}
 */
function IsPhoneValid(phone) {
    var regex = /^[0-9-+()]+$/;
    return regex.test(phone);
}

function ReplaceAllString(givenstr, searchstr, replacestr){
    var regexp = new RegExp(searchstr,"g");
    return  givenstr.replace(regexp,replacestr);
}

function replaceQuoats(str){
    str = StripNullValue(str);
    var replacedstr = str.replace(/\"/g,'""');
    replacedstr = replacedstr.replace(/\'/g,"\\'");
    return replacedstr;
}

function utf8_decode(str) {
    var output = "";
    var i = 0;
    var c = 0;
    //var c1 = 0;
    var c2 = 0;
    while ( i < str.length ) {
      c = str.charCodeAt(i);
      if (c < 128) {
        output += String.fromCharCode(c);
        i++;
      }
      else if((c > 191) && (c < 224)) {
        c2 = str.charCodeAt(i+1);
        output += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = str.charCodeAt(i+1);
        var c3 = str.charCodeAt(i+2);
        output += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
    }
    return output;
}
/******************************************************************************************/
/********************************* Validation Func - End **********************************/
/******************************************************************************************/
function getDateTimeByFormat(format,byDate){

    var _Date	=	(byDate) ? byDate:new Date();
    var day		=	_Date.getDate();

    if(day.toString().split('').length==1){
        day	=	'0' + day;
    }
    format	=	format.replace('d',day);

    var month	=	_Date.getMonth();
    var mm	=	(month+1);
    if(mm.toString().split('').length==1){
        mm	=	'0' + mm;
    }
    format	=	format.replace('m',mm);

    var fullyear	=	_Date.getFullYear();
    format	=	format.replace('Y',fullyear);

    var hours	=	_Date.getHours();
    if(hours.toString().split('').length==1){
        hours	=	'0' + hours;
    }
    format	=	format.replace('H',hours);
    var minutes	=	_Date.getMinutes();
    if(minutes.toString().split('').length==1){
        minutes	=	'0' + minutes;
    }
    format	=	format.replace('i',minutes);
    var seconds	=	_Date.getSeconds();
    if(seconds.toString().split('').length==1){
        seconds	=	'0' + seconds;
    }
    format	=	format.replace('s',seconds);
    return format;
}
function AddProcessingLoader(obj){
    logStatus("AddProcessingLoader",LOG_FUNCTIONS);
    try {
        LAST_LOADER_ID = obj;
        if (LAST_LOADER_ID){
            $(LAST_LOADER_ID).addClass('clsprocessing');
            $(LAST_LOADER_ID).attr("disabled", "disabled");
        }
    }catch(e){
        ShowExceptionMessage("AddProcessingLoader", e);
    }
}

function RemoveProcessingLoader(obj){
    logStatus("RemoveProcessingLoader",LOG_FUNCTIONS);
    try {
        var removeobj = LAST_LOADER_ID;
        if (!obj){
            removeobj = obj;
        }
        if (removeobj){
            $(removeobj).removeClass('clsprocessing');
            $(removeobj).removeAttr("disabled");
        }
    }catch(e){
        ShowExceptionMessage("RemoveProcessingLoader", e);
    }

}
function getSplittedTextsBasedonLimits(teststring, Maxlimit){
    var charlength = teststring.length;
   // var currlen = 0;
    var balancetxt = teststring;
    var retrunvalue = "";
    //alert("teststring: " + teststring);
    while (balancetxt != ""){
        var subtexts = getWholeWord(balancetxt, Maxlimit);
        //alert("subtexts: " + subtexts);
        balancetxt = trim(balancetxt.substr(subtexts.length, charlength));
        if (retrunvalue != ""){
          var  subtextslen = subtexts.indexOf("\n");
            if (subtextslen <= 0){
                retrunvalue = retrunvalue + "\n" + subtexts;
            }else{
                retrunvalue = retrunvalue + subtexts;
            }
        }else{
            retrunvalue = subtexts;
        }
        //alert("retrunvalue " + retrunvalue);
        //alert("balancetxt: "+ balancetxt);
    }   
    return retrunvalue;
}
function trim(txt){
    return txt.replace(/^\s+|\s+$/g, "");
}
/*
function rTrim(text,token){
    return text.substring(0,text.lastIndexOf(token));
}*/
function rTrim(text,token){
    token = !token ? ' \\s\u00A0' : (token + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
    var re = new RegExp('[' + token + ']+$', 'g');
    return (text + '').replace(re, '');
}
function lTrim(text,token){
    token = !token ? ' \\s\u00A0' : (token + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    var re = new RegExp('^[' + token + ']+', 'g');
    return (text + '').replace(re, '');
}
function numPad(number, pad) {
    var N = Math.pow(10, pad);
    return number < N ? String(N + number).slice(-pad) : number;
}
/*
function getFullVersionDetail(){
    return APP_VERSION + "." + _DBVERSION;
}
*/
function _goBackOnSave(pages,onBackSuccess){
    logStatus("_goBackOnSave", LOG_FUNCTIONS);
    try {
        window.history.go(-(pages));
        if(onBackSuccess){
            onBackSuccess()
        }
    } catch(e){
        alert(JSON.stringify(e));
    }

    //window.history.back(-(pages));
    //XX window.history.go(-(pages));
    /*setTimeout(function(){
        window.history.go(-(pages));
    }, 50);*/
}
function _forceToChangePage(href){
    logStatus("_forceToChangePage > to #" + href, LOG_FUNCTIONS);
    try {
        window.location.href = "#" + href;
    } catch(e){
        ShowExceptionMessage("_forceToChangePage : " + href, e);
    }
}
function base64_encode(data) {
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var i = 0, ac = 0, tmp_arr = [];
    if (!data) {
        return data;
    }
    data = unescape(encodeURIComponent(data));
    do {
        // pack three octets into four hexets
        var o1 = data.charCodeAt(i++);
        var o2 = data.charCodeAt(i++);
        var o3 = data.charCodeAt(i++);
        var bits = o1 << 16 | o2 << 8 | o3;
        var h1 = bits >> 18 & 0x3f;
        var h2 = bits >> 12 & 0x3f;
        var h3 = bits >> 6 & 0x3f;
        var h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);
    var enc = tmp_arr.join('');
    var r = data.length % 3;
    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}
function base64_decode(data) {
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var i = 0, ac = 0,tmp_arr = [];
    if (!data) {
        return data;
    }
    data += '';
    do {
        // unpack four hexets into three octets using index points in b64
        var h1 = b64.indexOf(data.charAt(i++));
        var h2 = b64.indexOf(data.charAt(i++));
        var h3 = b64.indexOf(data.charAt(i++));
        var h4 = b64.indexOf(data.charAt(i++));

        var bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        var o1 = bits >> 16 & 0xff;
        var o2 = bits >> 8 & 0xff;
        var o3 = bits & 0xff;
        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    var dec = tmp_arr.join('');
    return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
}
/*
function FormatedDate(date, type){
    var yyyy= date.getFullYear().toString();
    var mm 	= (date.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = date.getDate().toString();
    var h	=	date.getHours().toString();
    var i	=	date.getMinutes().toString();
    var s	=	date.getSeconds().toString();
    var formatedDate	=	'';
    switch(type){
        case FORMAT_ONLY_DATE:
            formatedDate	=	( yyyy + "-" + ( mm[1] ? mm:"0" + mm[0] ) + "-" + ( dd[1] ? dd: "0" + dd[0] ) );
            break;
        case FORMAT_ONLY_TIME:
            formatedDate	=	(( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        case FORMAT_DATE_TIME:
            formatedDate	=	(yyyy + "-" + ( mm[1] ? mm:"0" + mm[0] ) + "-" + ( dd[1] ? dd: "0" + dd[0] ) + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        case FORMAT_SLASH_DATE_DMY_HIS:
            formatedDate	=	(( dd[1] ? dd: "0" + dd[0] ) + "/" + ( mm[1] ? mm:"0" + mm[0] ) + "/" + yyyy + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
            break;
        default:
            formatedDate	=	(yyyy + "-" + ( mm[1] ? mm:"0" + mm[0] ) + "-" + ( dd[1] ? dd: "0" + dd[0] ) + " " + ( h[1] ? h : "0" + h[0] ) + ":" + ( i[1] ? i : '0' + i[0] ) + ":" + ( s[1] ? s : '0' + s[0]));
    }
    return formatedDate;
}
*/
function onlyNos(e, t) {
    logStatus(t, LOG_DEBUG);
    try {
        var charCode='';
        if (window.event) {
             charCode = window.event.keyCode;
        } else if (e) {
             charCode = e.which;
        } else {
            return true;
        }
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46));

    } catch (e) {
        ShowExceptionMessage("onlyNos",e);
    }
}
function getItemInArrayByKeyValue(key, value ,searchArray,mode){
    logStatus("getIndexInArrayByKeyVal",LOG_FUNCTIONS);
    try {
        var _customResult	=	[];
        for (var index in searchArray){
            if(searchArray.hasOwnProperty(index)){
                if(searchArray[index][key] && searchArray[index][key]==value){
                    if(mode && mode==true){
                        _customResult.push(searchArray[index]);
                    } else{
                        return searchArray[index];
                    }
                }
            }
        }
        return _customResult;
    }catch(e){
        ShowExceptionMessage("getIndexInArrayByKeyVal", e);
    }
}
function getKeyItemInArrayByKeyValue(getKey, whereKey, value ,searchArray){
    logStatus("getKeyItemArrayByKeyValue",LOG_FUNCTIONS);
    try {
        for (var index in searchArray){
            if(searchArray.hasOwnProperty(index)) {
                if (searchArray[index][whereKey] == value) {
                    return (searchArray[index][getKey]) ? searchArray[index][getKey] : null;
                }
            }
        }
    }catch(e){
        ShowExceptionMessage("getKeyItemArrayByKeyValue", e);
    }

}
function removeItemFromArrayByKeyValue(key, value, searchArray){
    logStatus("removeItemFromArrayByKeyValue",LOG_FUNCTIONS);
    try {
        var _TempArray	=	[];
        for (var index in searchArray){
            if(searchArray.hasOwnProperty(index)) {
                if (searchArray[index][key] != value) {
                    _TempArray.push(searchArray[index]);
                }
            }
        }
        return _TempArray;
    }catch(e){
        ShowExceptionMessage("removeItemFromArrayByKeyValue", e);
    }
}
function getTotalFromObjectValues(_Object){
    var _total= 0;
    for( var elem in _Object ) {
        if(_Object.hasOwnProperty ( elem ) ) {
            var _amount	=	((_Object[elem]) && _Object[elem]!="" ) ? parseFloat( _Object[elem] ) : 0;
            _total +=	_amount;
        }
    }
    return _total;
}

/*
function GetCurrentOnlyDate(){
    return FormatedDate(new Date(),FORMAT_ONLY_DATE);
}
*/

function isInArray(key, value, array){
    for(var index in array){
        if(array.hasOwnProperty(index)) {
            var row = array[index];
            for (var k in row) {
                if(row.hasOwnProperty(k)) {
                    if (k == key && row[k] == value) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}
function is_array(mixed_var) {
    var ini,
    _getFuncName = function(fn) {
        var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
        if (!name) {
            return '(Anonymous)';
        }
        return name[1];
    };
    var _isArray = function(mixed_var) {
        // return Object.prototype.toString.call(mixed_var) === '[object Array]';
        // The above works, but let's do the even more stringent approach: (since Object.prototype.toString could be overridden)
        // Null, Not an object, no length property so couldn't be an Array (or String)
        if (!mixed_var || typeof mixed_var !== 'object' || typeof mixed_var.length !== 'number') {
            return false;
        }
        var len = mixed_var.length;
        mixed_var[mixed_var.length] = 'bogus';
        // The only way I can think of to get around this (or where there would be trouble) would be to have an object defined
        // with a custom "length" getter which changed behavior on each call (or a setter to mess up the following below) or a custom
        // setter for numeric properties, but even that would need to listen for specific indexes; but there should be no false negatives
        // and such a false positive would need to rely on later JavaScript innovations like __defineSetter__
        if (len !== mixed_var.length) { // We know it's an array since length auto-changed with the addition of a
            // numeric property at its length end, so safely get rid of our bogus element
            mixed_var.length -= 1;
            return true;
        }
        // Get rid of the property we added onto a non-array object; only possible
        // side-effect is if the user adds back the property later, it will iterate
        // this property in the older order placement in IE (an order which should not
        // be depended on anyways)
        delete mixed_var[mixed_var.length];
        return false;
    };
    if (!mixed_var || typeof mixed_var !== 'object') {
        return false;
    }
    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    /**
     * @param ini.local_value This is local value
     */
    ini = this.php_js.ini['phpjs.objectsAsArrays'];
    return _isArray(mixed_var) ||
        // Allow returning true unless user has called
        // ini_set('phpjs.objectsAsArrays', 0) to disallow objects as arrays
        ((!ini || ( // if it's not set to 0 and it's not 'off', check for objects as arrays
            (parseInt(ini.local_value, 10) !== 0 && (!ini.local_value.toLowerCase
                || ini.local_value.toLowerCase() !=='off')))) && (Object.prototype.toString.call(mixed_var) === '[object Object]'
                && _getFuncName(mixed_var.constructor) ==='Object' // Most likely a literal and intended as assoc. array
        ));
}
function getObject(data){
    return data;
}
function stripslashes(_string){
    return (_string + '').replace(/\\(.?)/g, function(s, n1) {
        switch (n1) {
            case '\\':
                return '\\';
            case '0':
                return '\u0000';
            case '':
                return '';
            default:
                return n1;
        }
    });
}
function addslashes(str) {
    return (str + '')
        .replace(/[\\"']/g, '\\$&')
        .replace(/\u0000/g, '\\0');
}
function getValidJsonObjectByString(jsonString){
    logStatus("getValidJsonObjectByString",LOG_FUNCTIONS);
    try {
        var jsonobj	=	{};
        if ((jsonString) && jsonString!="") {
        //	var jsonStringnew = "";
            var jsonStringnew = jsonString.replace(/""/g, '\\""');
            jsonStringnew = jsonStringnew.replace(/\\\\"/g, '\\"');
            jsonobj = JSON.parse(jsonStringnew);
        }
    }catch(e){
        ShowExceptionMessage("getValidJsonObjectByString",e);
    }
    return jsonobj;
}
function utf8_encode(argString) {
    if (argString === null || typeof argString === 'undefined') {
        return '';
    }
    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = '',start, end;
    start = end = 0;
    var stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;
        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
        } else if ((c1 & 0xF800) != 0xD800) {
            enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
        } else { // surrogate pairs
            if ((c1 & 0xFC00) != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if ((c2 & 0xFC00) != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode((c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
    return utftext;
}
/*
function utf8_decode(str_data) {
    var tmp_arr = [],i = 0,ac = 0,c1 = 0,c2 = 0,c3 = 0,c4 = 0;

    str_data += '';
    while (i < str_data.length) {
        c1 = str_data.charCodeAt(i);
        if (c1 <= 191) {
            tmp_arr[ac++] = String.fromCharCode(c1);
            i++;
        } else if (c1 <= 223) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
            i += 2;
        } else if (c1 <= 239) {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        } else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            c4 = str_data.charCodeAt(i + 3);
            c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
            c1 -= 0x10000;
            tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
            tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
            i += 4;
        }
    }
    return tmp_arr.join('');
}*/
function searchInArray(objectarray,key,LIKE){
    var results	=	[];
    $.each(objectarray,function(i,rowdata){
        /*
        if(rowdata[key].split('').indexOf(LIKE)!=-1){
            results.push(rowdata);
        }*/
        if(rowdata[key].search(LIKE)>-1){
            results.push(rowdata);
        }
    });
    return results;
}
function setValidUTF8_encode(_string){
    return _string.match('!!u') ? _string:utf8_encode(_string);
}
function setValidUTF8_decode(_string){
    //return _string.match('!!u')? utf8_decode(_string):_string;
    return _string.match('!!u') ? decodeURIComponent(escape(_string)):_string;
}
function setArrayIn_cache(key,arrayVal){
    localStorage.setItem(key,JSON.stringify(arrayVal))
}
function getArrayFrom_cache(key){
    return	JSON.parse(localStorage.getItem(key));
}
function setValIn_cache(key,val){
    localStorage.setItem(key,val);
}
function getValFrom_cache(key){
    return localStorage.getItem(key);
}
function getCurrentTime(format){
    var _Date	=	new Date();
    var hours	=	_Date.getHours();
    if(hours.toString().split('').length==1){
        hours	=	'0' + hours;
    }
    format	=	format.replace('H',hours);
    var minutes	=	_Date.getMinutes();
    if(minutes.toString().split('').length==1){
        minutes	=	'0' + minutes;
    }
    format	=	format.replace('i',minutes);
    var seconds	=	_Date.getSeconds();
    if(seconds.toString().split('').length==1){
        seconds	=	'0' + seconds;
    }
    format	=	format.replace('s',seconds);
    return format;
}
/******************************************************************************************/
/********************************* Functions  - End ***************************************/
/******************************************************************************************/
