/******************************************************************************************/
/************************** DBLAYER.JS VERSION 2.1 23/02/2013 ******************************/
/******************************************************************************************/
var databaseconnection;
/**
 * @return {string}
 */
function GetValidDBName() {
    logStatus("Excuting GetValidDBName", LOG_COMMON_FUNCTIONS);
    return APPLICATION_NAME.replace(" ", "") + "Database";
}

function OpenDBConnection() {
    logStatus("Excuting OpenDBConnection", LOG_COMMON_FUNCTIONS);
     try {
        var DBName = GetValidDBName();
        databaseconnection = window.openDatabase(APPLICATION_NAME, "1.0", DBName, 20000000);
    } catch (e) {
        ShowExceptionMessage("OpenDBConnection", e);
    }
}

function ExecuteWithSucessTransation(TargetExecuteQuery, TargetSucessQuery) {
    logStatus("Excuting ExecuteWithSucessTransation", LOG_COMMON_FUNCTIONS);
    try {
        OpenDBConnection();
        databaseconnection.transaction(TargetExecuteQuery, DBGeneralError, TargetSucessQuery);
    } catch (e) {
        ShowExceptionMessage("ExecuteWithSucessTransation", e);
    }
}

function ExecuteTransation(TargetExecuteQuery) {
    logStatus("Excuting ExecuteTransation", LOG_COMMON_FUNCTIONS);
    try {
        OpenDBConnection();
        databaseconnection.transaction(TargetExecuteQuery, DBGeneralError, DBGeneralSucess);
    } catch (e) {
        ShowExceptionMessage("ExecuteTransation",e);
    }
}

function DBGeneralError(error) {
    logStatus("Excuting DBGeneralError", LOG_COMMON_FUNCTIONS);
    ShowExceptionMessage("DBGeneralError", "Error Code " + error.code + " Message: " + error.message);
}

function DBGeneralTransError(trans, error) {
    logStatus("Excuting DBGeneralTransError", LOG_COMMON_FUNCTIONS);
    logStatus(trans, LOG_DEBUG);
    DBGeneralError(error);
}

function DBSyncTransError(trans, error) {
    logStatus("Excuting DBSyncTransError", LOG_COMMON_FUNCTIONS);
    logStatus(trans, LOG_DEBUG);
     try {
        _isSyncError = 1;
        DBGeneralError(error);
    } catch (e) {
        ShowExceptionMessage("DBSyncTransError",e);
    }
}

function DBGeneralSucess() {
    logStatus("Excuting DBGeneralSucess", LOG_COMMON_FUNCTIONS);
}

function ProcessQuery(QueryStr, SucessFunction) {
    logStatus("Excuting ProcessQuery", LOG_COMMON_FUNCTIONS);
    try {
        if ((databaseconnection) && (databaseconnection != undefined)){
            databaseconnection.transaction(function (transaction) {
                transaction.executeSql(QueryStr, [], SucessFunction, DBGeneralTransError);
            });            
        }
    } catch (e) {
        ShowExceptionMessage("ProcessQuery", e);
    }
}
function ProcessQueryWithFailureHandler(QueryStr, SucessFunction, FailureFunction){
    logStatus("Excuting ProcessQueryWithFailureHandler", LOG_COMMON_FUNCTIONS);
    try {
        if ((databaseconnection) && (databaseconnection != undefined)){
            databaseconnection.transaction(function (transaction) {
                transaction.executeSql(QueryStr, [], SucessFunction, FailureFunction);
            });            
        }
    } catch (e) {
        ShowExceptionMessage("ProcessQueryWithFailureHandler", e);
    }

}
function ProcessJustQuery(QueryStr) {
     logStatus("Excuting ProcessJustQuery", LOG_COMMON_FUNCTIONS);
     try {
        ProcessQuery(QueryStr, DBGeneralSucess);
     } catch (e) {
        ShowExceptionMessage("ProcessQuery", e);
    }
}

function ProcessBatchQuery(QueryStr, SucessFunction) {
    logStatus("Excuting ProcessBatchQuery", LOG_COMMON_FUNCTIONS);
    try {
        if (databaseconnection){
        }else{
            OpenDBConnection();               
        }
        databaseconnection.transaction(function (transaction) {
            //transaction.executeSql(QueryStr, [], SucessFunction, function(trans, error){DBGeneralTransError(trans, error);alert(QueryStr);});
            var transarray = QueryStr.split(");");
            for (var i = 0; i < transarray.length; i++) {
                if (transarray[i] != ""){
                    var qry = transarray[i] + ")";
                    transaction.executeSql(qry, [], SucessFunction, DBGeneralTransError);                    
                }
            }
        });
    } catch (e) {
        ShowExceptionMessage("ProcessBatchQuery", e);
    }
}

function ProcessJustBatchQuery(QueryStr) {
    ProcessBatchQuery(QueryStr, DBGeneralSucess);
}

function ProcessQueryWithParam(QueryStr, SucessFunction, Param){
     logStatus("Excuting ProcessQueryWithParam", LOG_COMMON_FUNCTIONS);
    try {
        ProcessQuery(QueryStr, function (activetrans, results){
            SucessFunction(activetrans, results, Param);
         }
        )
    } catch (e) {
        ShowExceptionMessage("ProcessQueryWithParam", e);
    }
}


/**
 * @return {string}
 */
function StripNullValue(DBValue){
    //logStatus("Excuting StripNullValue", LOG_COMMON_FUNCTIONS);
    try {
        if (!DBValue){
            return "";
        }
        else if (DBValue != "null") {
        } else {
            return "";
        }
        return DBValue;
    } catch (e) {
        ShowExceptionMessage("StripNullValue", e);
    }
}

/**
 * @return {number}
 */
function StripNullIntValue(DBValue){
    if (!DBValue){
        return 0;
    }
    else if (DBValue == "null"){
        return 0;
    }
    return DBValue; 
}

/* General DB */

function InitializeDBObjects() {
    logStatus("Excuting InitializeDBObjects", LOG_COMMON_FUNCTIONS);
    try { 
        LoadOneTimeVariablesFromDB();
    } catch (e) {
        ShowExceptionMessage("InitializeDBObjects", e);
    }
}

function LoadOneTimeVariablesFromDB() {
     logStatus("Excuting LoadOneTimeVariablesFromDB", LOG_COMMON_FUNCTIONS);
     try {
        ExecuteTransation(CreateTable); //CreateTable need to be implemented in dblayer_app.js
    } catch (e) {
        ShowExceptionMessage("LoadOneTimeVariablesFromDB", e);
    }    
}