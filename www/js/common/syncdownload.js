/******************************************************************************************/
/************************** SYNCDOWNLOAD.JS VERSION 2.0 14/02/2013 ******************************/
/******************************************************************************************/

var __Sync_DownloadArray =[];
var _CurrentDownloadingFileIndx = -1;
var IsProcessDownload=true;
var EachDownloadSuccessHandler = null;
var EachDownloadFailureHandler = null;

var DownloadSuccessHandler = null;
var DownloadFailureHandler = null;

var DOWNLOAD_STATUS_PENDING = -1;
var DOWNLOAD_STATUS_SUCCESS = 1;
var DOWNLOAD_STATUS_ERROR = 2;

function DownloadFileProperties(dlsourceFilePath, dldestFilePath, dldownloadStatus, dlerror, refid, isdownload){
    logStatus("Excuting DownloadFileProperties", LOG_COMMON_FUNCTIONS);
    try {
        if (!isdownload){
            this._dlsourceFilePath =  dlsourceFilePath;
        }else{
            this._dlsourceFilePath =  dlsourceFilePath.replace(/ /g,"%20");
        }
        this._dldestFileName = dldestFilePath.substr(dldestFilePath.lastIndexOf("/") + 1);
        this._dldestFilePath = dldestFilePath;
        this._dldownloadStatus = dldownloadStatus;
        this._dlerror = dlerror;
        this._refid = refid;
        this._isisdownload = isdownload;
     } catch (e) {
        ShowExceptionMessage("DownloadFileProperties",e);
    }
}

function AddDownloadFile(sourceFilePath, destFilePath, refid, isdownload)
{
    logStatus("Excuting AddDownloadFile", LOG_COMMON_FUNCTIONS);
    try {
        var obj = new DownloadFileProperties(sourceFilePath, destFilePath, DOWNLOAD_STATUS_PENDING, '', refid, isdownload);
        __Sync_DownloadArray.push(obj);
    } catch (e) {
        ShowExceptionMessage("AddDownloadFile",e);
    }
}

function ResetDownloadArray()
{
    logStatus("Excuting ResetDownloadArray", LOG_COMMON_FUNCTIONS);
    __Sync_DownloadArray = [];
}

/**
 * @return {number}
 */
function GetNextFileNameIndex(){
    logStatus("Excuting GetNextFileNameIndex", LOG_COMMON_FUNCTIONS);
    try {
        for (var i = 0; i <= __Sync_DownloadArray.length-1; i++) {
            if (__Sync_DownloadArray[i]._dldownloadStatus == DOWNLOAD_STATUS_PENDING){
                return i;
            }
        }
        return -1;
    } catch (e) {
        ShowExceptionMessage("GetNextFileNameIndex",e);
    }
}

/**
 * @return {number}
 */
function CheckAndErrorDownload(){
    logStatus("Excuting CheckAndErrorDownload", LOG_COMMON_FUNCTIONS);
    try {
        _LastSyncStatus = -1;
        for (var i = 0; i <= __Sync_DownloadArray.length-1; i++) {
            if (__Sync_DownloadArray[i]._dldownloadStatus == DOWNLOAD_STATUS_ERROR){
                return i;
            }
        }
        return -1;
    } catch (e) {
        ShowExceptionMessage("CheckAndErrorDownload",e);
    }
}

function ResetErrorDownloads(){
    logStatus("Excuting ResetErrorDownloads", LOG_COMMON_FUNCTIONS);
    try {
        for (var i = 0; i <= __Sync_DownloadArray.length-1; i++) {
            if (__Sync_DownloadArray[i]._dldownloadStatus == DOWNLOAD_STATUS_ERROR){
                __Sync_DownloadArray[i]._dldownloadStatus = DOWNLOAD_STATUS_PENDING;
            }
        }
    } catch (e) {
        ShowExceptionMessage("ResetErrorDownloads",e);
    }
}


function GetCurrentRefId()
{
    logStatus("Excuting GetCurrentRefId", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            return __Sync_DownloadArray[_CurrentDownloadingFileIndx]._refid;
        }
    } catch (e) {
        ShowExceptionMessage("GetCurrentRefId",e);
    }
}


/**
 * @return {boolean}
 */
function GetCurrentisDownload(defaultidx)
{
    logStatus("Excuting GetCurrentisDownload", LOG_COMMON_FUNCTIONS);
    try {
        if (defaultidx < 0){
            defaultidx = _CurrentDownloadingFileIndx;
        }
        if (defaultidx >= 0) {
            return __Sync_DownloadArray[defaultidx]._isisdownload;
        }
        return false;
    } catch (e) {
        ShowExceptionMessage("GetCurrentisDownload",e);
        return false;
    }
}

function GetCurrentSourceFilePath()
{
    logStatus("Excuting GetCurrentSourceFilePath", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            return __Sync_DownloadArray[_CurrentDownloadingFileIndx]._dlsourceFilePath;
        }
    } catch (e) {
        ShowExceptionMessage("GetCurrentSourceFilePath",e);
    }
}

function GetCurrentDestFilePath()
{
    logStatus("Excuting GetCurrentDestFilePath", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            return __Sync_DownloadArray[_CurrentDownloadingFileIndx]._dldestFilePath;
        }
    } catch (e) {
        ShowExceptionMessage("GetCurrentDestFilePath",e);
    }
}

/**
 * @return {string}
 */
function GetCurrentSourceFileName()
{
    logStatus("Excuting GetCurrentSourceFileName", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            var filePath = GetCurrentSourceFilePath();
            return filePath.substr(filePath.lastIndexOf("/") + 1);
        }
    } catch (e) {
        ShowExceptionMessage("GetCurrentSourceFileName",e);
    }
}

function GetCurrentDestFileName()
{
    logStatus("Excuting GetCurrentDestFileName", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            return __Sync_DownloadArray[_CurrentDownloadingFileIndx]._dldestFileName;
        }
    } catch (e) {
        ShowExceptionMessage("GetCurrentDestFileName",e);
    }
}

function SetCurrentDownloadStatus(Status)
{
    logStatus("Excuting SetCurrentDownloadStatus", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            __Sync_DownloadArray[_CurrentDownloadingFileIndx]._dldownloadStatus = Status;
        }
    } catch (e) {
        ShowExceptionMessage("SetCurrentDownloadStatus",e);
    }
}

function SetCurrentDownloadError(Error)
{
    logStatus("Excuting SetCurrentDownloadError", LOG_COMMON_FUNCTIONS);
    try {
        if (_CurrentDownloadingFileIndx >= 0) {
            __Sync_DownloadArray[_CurrentDownloadingFileIndx]._dlerror = Error;
        }
    } catch (e) {
        ShowExceptionMessage("SetCurrentDownloadError",e);
    }
}


function onDownloadSucess() {
    //logSyncroFileStatus(MSG_SYNC_PROGRESS_DOWNLOAD_SUCESS_TAG, GetCurrentDestFileName());
    logStatus("Excuting onDownloadSucess", LOG_COMMON_FUNCTIONS);
    try {
        var refid = GetCurrentRefId();
        SetCurrentDownloadStatus(DOWNLOAD_STATUS_SUCCESS);
        if (EachDownloadSuccessHandler){
            EachDownloadSuccessHandler(refid);
        }
        StartDownload();
    } catch (e) {
        ShowExceptionMessage("onDownloadSucess",e);
    }
}

function onDownloadFailer(e) {
    //logSyncroFileStatus(MSG_SYNC_PROGRESS_DOWNLOAD_ERROR_TAG, GetCurrentDestFileName());
    logStatus("Excuting onDownloadFailer", LOG_COMMON_FUNCTIONS);
    try {
        var refid = GetCurrentRefId();
        _LastSyncStatus = SYNC_STATUS_ERROR;
        ShowExceptionMessage("onDownloadFailer", JSON.stringify(e));
        //alert(refid + "=" + JSON.stringify(e));
        SetCurrentDownloadStatus(DOWNLOAD_STATUS_ERROR);
        SetCurrentDownloadError(JSON.stringify(e));
        if (EachDownloadFailureHandler){
            EachDownloadFailureHandler(refid, JSON.stringify(e));
        }
        StartDownload();
    } catch (e) {
        ShowExceptionMessage("onDownloadFailer",e);
    }
}

function StartDownload()
{
    
    try {
        _CurrentDownloadingFileIndx = GetNextFileNameIndex();
        var isdownload='';
        if (_CurrentDownloadingFileIndx >= 0)
        {
             isdownload = GetCurrentisDownload(-1);
            if (isdownload){
                logStatus("Start Downloading..", LOG_DEBUG);
            }else{
                logStatus("Start Uploading..", LOG_DEBUG);
            }
            var SrcFile = GetCurrentSourceFilePath();
            var DestFile = GetCurrentDestFilePath();
            //logSyncroFileStatus(MSG_SYNC_PROGRESS_DOWNLOAD_FILE_TAG, ShortStatus +'...');
            var filetransfer = new FileTransfer();
            if (isdownload){
                var DestFileName = GetCurrentDestFileName();
              //  var ShortStatus = DestFileName.substring(0, 25);
                logStatus('Downloading File:' + DestFileName + '...', LOG_DEBUG);
                filetransfer.download(SrcFile, DestFile, onDownloadSucess, onDownloadFailer);
            }else{
                var SourceFileName = SrcFile.substr(SrcFile.lastIndexOf("/") + 1);
                logStatus('Uploading File:' + SourceFileName + '...', LOG_DEBUG);
                var options = new FileUploadOptions();
                options.fileKey="file";
                options.fileName= SourceFileName;
                options.mimeType="image/jpeg";

                var params = [];
                params.value1 = "test";
                params.value2 = "param";

                options.params = params;
                options.headers = {
                   Connection: "close"
                };	     
                //alert(SrcFile+","+SourceFileName+","+DestFile);
                options.chunkedMode = false;
                filetransfer.upload(SrcFile, DestFile, onDownloadSucess, onDownloadFailer, options);
            }
        }
        else {
            var ErrorIdx = CheckAndErrorDownload();
            if (ErrorIdx >= 0)
            {
                 isdownload = GetCurrentisDownload(ErrorIdx);
                if (isdownload){
                    ShowAlertConfirmMessage("There were some error downloading files. Do you want retry Downloading them?", onDownloadErrorConfirmation, 'Retry, Cancel');
                }else{
                    ShowAlertConfirmMessage("There were some error uploading files. Do you want retry Uploading them?", onDownloadErrorConfirmation, 'Retry, Cancel');	                
                }
            }
            else
            {
                ResetDownloadArray();
                //logSyncroStatus(MSG_SYNC_PROGRESS_DOWNLOAD_COMP_TAG);
                if (DownloadSuccessHandler){
                    DownloadSuccessHandler();
                }
                //SyncNextDataClientProcess(5);
            }
        }
    } catch (e) {
        ShowExceptionMessage("StartDownload",e);
    }
}

function onDownloadErrorConfirmation(button){
     logStatus("Excuting onDownloadErrorConfirmation", LOG_DEBUG);
     try {
        if (button == 1)
        {
            ResetErrorDownloads();
            StartDownload();
        }else{
            ResetDownloadArray();
            //logSyncroStatus(MSG_SYNC_PROGRESS_DOWNLOAD_COMP_TAG);
            _LastSyncStatus = SYNC_STATUS_ERROR;
            if (DownloadFailureHandler){
                DownloadFailureHandler();
            }
            //SyncNextDataClientProcess(6);
        }
    } catch (e) {
        ShowExceptionMessage("onDownloadErrorConfirmation",e);
    }
}


/*
DownloadSuccessHandler = function(){
alert("Upload done");
}

DownloadFailureHandler = function(){
alert("Upload Failure");
}

EachDownloadSuccessHandler =function(refid){

}

EachDownloadFailureHandler =function(refid, JSONerror){

}

*/