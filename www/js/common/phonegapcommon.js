/******************************************************************************************/
/************************** PHONEGAPCOMMON.JS VERSION 2.1 23/02/2013 ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* General  - Start ***********************************/
/******************************************************************************************/
function onPlatformReady(){
   // _DEVICE_ID = device.uuid;
      if(device.uuid == null){
	 _DEVICE_ID = "qwrettyyrtyrtur_coachmmmmm";
	 }
	 else{
	 _DEVICE_ID = device.uuid;
	 }
    //_DEVICE_ID = device.uuid;
	 console.log('====>>>','HELLO ASHIT == '+_DEVICE_ID);
	 
}

function HideSplashScreen(){
    logStatus("Excuting HideSplashScreen", LOG_COMMON_FUNCTIONS);
    cordova.exec(null, null, "SplashScreen", "hide", []);
}

function ShowSplashScreen(){
    logStatus("Excuting ShowSplashScreen", LOG_COMMON_FUNCTIONS);
    cordova.exec(null, null, "SplashScreen", "show", []);
}


function ExitApplication() {
    logStatus("ExitApplication", LOG_COMMON_FUNCTIONS);
    try{
        if(GLOBAL_DIALOG){
            if(GLOBAL_DIALOG.close){
                GLOBAL_DIALOG.close();
            }
        }
        /**
         * @param navigator.app This is navigator app
         * @param navigator.app.exitApp() This is navigator app exitApp function
         */
        ShowAlertConfirmMessage("Are you sure you want to EXIT the app?", function(selbtn){
            if (selbtn == 2){
                if (_ISIN_PROGRESS == 1){
                    setTimeout(function(){
                        IS_APP_PAUSED = 1;
                        navigator.app.exitApp();
                        }, 2000);
                }else{
                    IS_APP_PAUSED = 1;
                    navigator.app.exitApp();                    
                }                
            }  
        }, 'Cancel,OK');
    } catch (e) {
        ShowExceptionMessage("ExitApplication", e);
    }
}
/******************************************************************************************/
/********************************* General  - Start ***********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* File Utils  - Start ***********************************/
/******************************************************************************************/
function getFullpath(curDirIndex, fileName) {
    logStatus("Excuting getFullpath", LOG_COMMON_FUNCTIONS);
    try {
        var fullreturnpath = "";
        var curDir = "";
        switch (curDirIndex) {
            case DIR_GENERAL_INDEX:
                curDir = DIR_GENERAL;
                break;
        }
        if (fileName) {
            fullreturnpath = APPLICATION_DIR + curDir + fileName;
        } else {
            fullreturnpath = APPLICATION_DIR + curDir;
        }
        return addSlash(ROOT_PATH) + fullreturnpath;
    } catch (e) {
        ShowExceptionMessage("getFullpath", e);
    }
}

function CreateApplicationDirectories() {
    logStatus("Excuting CreateApplicationDirectories", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param LocalFileSystem.PERSISTENT This is Local File System PERSISTENT

         */
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onDirectorySucess, onDirectoryFail);
    } catch (e) {
        ShowExceptionMessage("CreateApplicationDirectories", e);
    }
}

function onDirectoryFail(error) {
    logStatus("Excuting onDirectoryFail", LOG_COMMON_FUNCTIONS);
    logStatus(error, LOG_DEBUG);
    try {
        //XX ShowExceptionMessage("onDirectoryFail", error.code);
    } catch (e) {
        ShowExceptionMessage("onDirectoryFail", e);
    }
    
}
function onDirectorySucess(filesystem) {
    logStatus("Excuting onDirectorySucess", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param root.fullPath This is  fullPath
         * @param root.getDirectory This is  getDirectory function
         */
        ROOT_PATH = filesystem.root.fullPath;
        localStorage.fullpath = ROOT_PATH;
        filesystem.root.getDirectory(APPLICATION_DIR, {
            create: true
        }, onApplicationDirectorySucess, onDirectoryFail);
    } catch (e) {
        ShowExceptionMessage("onDirectorySucess", e);
    }
}

function onApplicationDirectorySucess(entry) {
    logStatus("Excuting onApplicationDirectorySucess", LOG_COMMON_FUNCTIONS);
    try {
        entry.getDirectory(DIR_GENERAL, {
            create: true
        }, onGeneralDirectorySucess, onDirectoryFail);
    } catch (e) {
        ShowExceptionMessage("onApplicationDirectorySucess", e);
    }
}

function onGeneralDirectorySucess(entry) {
    logStatus(entry, LOG_DEBUG);
    logStatus("Excuting onGeneralDirectorySucess", LOG_COMMON_FUNCTIONS);
}

/******************************************************************************************/
/********************************* File Utils  - End ***********************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Notification Objects  - Start *********************************/
/******************************************************************************************/
function NotificationAlert(message){
    logStatus("Excuting NotificationAlert", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param navigator.notification This is notification
         */
        if (navigator.notification){
            navigator.notification.alert(
                    message,  // message
                    alertDismissed,         // callback
                    APPLICATION_NAME,            // title
                    'OK'                  // buttonName
                    );
        }
    } catch (e) {
        ShowExceptionMessage("NotificationAlert", e);
    }
}

function NotificationActivityStart(title, msg){
    logStatus("Excuting NotificationActivityStart", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param notification.activityStart() This is activityStart function
         */
        NotificationActivityStop();
        window.setTimeout(function() {
            //alert(1);
            navigator.notification.activityStart(title, msg); 
        }, 500);    
    } catch (e) {
        ShowExceptionMessage("NotificationActivityStart", e);
    }
}

function NotificationActivityStop(){ 
    logStatus("Excuting NotificationActivityStop", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param notification.activityStop() This is activityStop function
         */
        window.setTimeout(function() {
            navigator.notification.activityStop();
        }, 100);
    } catch (e) {
        ShowExceptionMessage("NotificationActivityStop", e);
    }
}

function NotificationConfirmMessage(strMsg, callBackFunction, buttons){
    logStatus("Excuting NotificationConfirmMessage", LOG_COMMON_FUNCTIONS);
    try {
        navigator.notification.confirm(
                strMsg,  // message
                callBackFunction,         // callback
                APPLICATION_NAME,            // title
                buttons                 // buttonName
                );
    } catch (e) {
        ShowExceptionMessage("NotificationActivityStop", e);
    }
}
/******************************************************************************************/
/********************************* Notification Objects  - End *********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Barcode Scanning  - Start *********************************/
/******************************************************************************************/
function StartBarcodeScan(SuccessHandler, FailureHandler){
    //ShowToastMessage("seems to work", _TOAST_LONG);
    window.plugins.barcodeScanner.scan( function(result) {
        if (SuccessHandler){
            SuccessHandler(result)
        }
        /*
        alert("We got a barcode\n" +
                  "Result: " + result.text + "\n" +
                  "Format: " + result.format + "\n" +
                  "Cancelled: " + result.cancelled);
        */
    }, function(error) {
        if (FailureHandler){
            FailureHandler(error);
        }
        //alert("Scanning failed: " + error);
    }
    );    
}
/******************************************************************************************/
/********************************* Barcode Scanning  - End *********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Geo Location  - Start *********************************/
/******************************************************************************************/
function getCurrentGeoPosition(SuccessHandler, FailureHandler, options){
    navigator.geolocation.getCurrentPosition(SuccessHandler, FailureHandler, options);
}

function WatchGeoPosition(SuccessHandler, FailureHandler, options){
    return navigator.geolocation.watchPosition(SuccessHandler, FailureHandler, options);
}

function ClearWatchGeoPosition(watchgeoid){
    navigator.geolocation.clearWatch(watchgeoid);
}
/******************************************************************************************/
/********************************* Geo Location  - End *********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* Camera  - Start *********************************/
/******************************************************************************************/
function getCurrentCapturedImage(SuccessHandler, FailureHandler, options){
   if (!SuccessHandler){
       SuccessHandler = function(imageData){
           
       }
   }
   if (!FailureHandler){
       FailureHandler = function(message){
           
       }
   }
    navigator.camera.getPicture( SuccessHandler, FailureHandler, options);
}

var retries = 0;
function UploadJpgImageToServer(fileURI, hosturl, successfunction, failfunction, getdata, postdata) {
    var win = function (r) {
        //clearCache();
        retries = 0;
        if (successfunction){
            if (r.response){
                successfunction(r.response);
            }else{
                successfunction(r);
            }
        }
    };
 
    var fail = function (error) {
        if (retries == 0) {
            retries ++;
            setTimeout(function() {
                UploadJpgImageToServer(fileURI, hosturl, successfunction, failfunction);
            }, 1000)
        } else {
            retries = 0;
            //clearCache();
            if (failfunction){
                failfunction(error);
            }
        }
    };

    hosturl	= AppendURLWithGetData(hosturl, getdata);
    var options = new FileUploadOptions();
    options.fileKey = "image";
    options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    options.params = {}; // if we need to send parameters to the server request
    if (postdata){
        $.each(postdata, function(k, v) {
            options.params[k] = v;
        });
    }
    options.headers = {
       Connection: "close"
    };	     
    options.chunkedMode = false;	                
    var ft = new FileTransfer();
    ft.upload(fileURI, encodeURI(hosturl), win, fail, options);
}
/******************************************************************************************/
/********************************* Camera  - End *********************************/
/******************************************************************************************/
/******************************************************************************************/
/********************************* HR BLE Bluetooth  - Start *********************************/
/******************************************************************************************/
var heartRate = {
    service: '180d',
    measurement: '2a37'
};

function ScanHR_BLEDevice(){
    logStatus("Excuting ScanHR_BLEDevice", LOG_COMMON_FUNCTIONS);
    ble.scan([heartRate.service], 5, ON_SCAN_BLEDevice, ON_SCAN_BLEDevice_Fail);
}

function isHR_Connected(deviceid, successhandler, failurehandler){
    logStatus("Excuting isHR_Connected", LOG_COMMON_FUNCTIONS);
    ble.isConnected(deviceid, successhandler, failurehandler);
}


function ON_SCAN_BLEDevice(peripheral){
    logStatus("Excuting ON_SCAN_BLEDevice", LOG_COMMON_FUNCTIONS);
    // this is demo code, assume there is only one heart rate monitor
    console.log("Found " + JSON.stringify(peripheral));
    //var foundHeartRateMonitor = true;
    if (OnHearRateDeviceScanSuccess){
        if (!peripheral.length){
            peripheral = [peripheral];
        }
        OnHearRateDeviceScanSuccess(peripheral);
    }
    //XX BLEDeviceConnect(peripheral);
}

function ON_SCAN_BLEDevice_Fail(){
    logStatus("Excuting ON_SCAN_BLEDevice_Fail", LOG_COMMON_FUNCTIONS);
    ShowAlertMessage("BLE Scan Failed");
}

function BLEDeviceConnect(peripheralid){
    logStatus("Excuting BLEDeviceConnect", LOG_COMMON_FUNCTIONS);
    //ble.connect(peripheral.id, BLEDeviceOnConnect, BLEDeviceOnDisConnect);
    ble.connect(peripheralid, BLEDeviceOnConnect, BLEDeviceOnDisConnect);
}

function BLEDeviceOnConnect(peripheral) {
    logStatus("Excuting BLEDeviceConnect", LOG_COMMON_FUNCTIONS);
    if (OnHeartRateConnectedCallBack){
        OnHeartRateConnectedCallBack(peripheral);
    }
}

function BLEDeviceOnDisConnect(reason) {
    logStatus("Excuting BLEDeviceOnDisConnect", LOG_COMMON_FUNCTIONS);
    if (OnHeartRateDisconnectCallBack){
        OnHeartRateDisconnectCallBack(reason);
    }
}   

function StartBLENotificationProcess(peripheralid, successhandler, errorhandler){
    logStatus("Excuting StartBLENotificationProcess", LOG_COMMON_FUNCTIONS);
    /**
     * @param  ble.startNotification This is start Notification
     */
    if (!successhandler){
        successhandler = BLEDeviceOnData;
    }
    if (!errorhandler){
        errorhandler = BLEDeviceOnError;
    }
    ble.startNotification(peripheralid, heartRate.service, heartRate.measurement, successhandler, errorhandler);	
}

function StopBLENotificationProcess(peripheralid, successhandler, errorhandler){
    logStatus("Excuting StopBLENotificationProcess", LOG_COMMON_FUNCTIONS);
    /**
     * @param ble
     * @param ble.stopNotification
     */
    if (!successhandler){
        successhandler = BLEDeviceOnData;
    }
    if (!errorhandler){
        errorhandler = BLEDeviceOnError;
    }
    ble.stopNotification(peripheralid, heartRate.service, heartRate.measurement, successhandler, errorhandler);	
}


function BLEDeviceOnData(buffer) {
    logStatus("Excuting BLEDeviceOnData", LOG_COMMON_FUNCTIONS);
        // assuming heart rate measurement is Uint8 format, real code should check the flags
        // See the characteristic specs http://goo.gl/N7S5ZS
        var data = new Uint8Array(buffer);
        var curdatetime = new Date();
        if (_CurrTime == 0){
            _CurrTime = curdatetime;
        }
        var hrtime = Math.round((curdatetime - _CurrTime)/1000);
        if (THRChart.ChartHRData_FULL[THRChart.ChartHRData_FULL.length-1] != hrtime){        	
            var graphdata = {
                x: hrtime/60,
                    y: data[1]
                };
            if (hrtime <= _WARM_UP_SECOND){
                THRChart.ChartHRData_WARMUP.push(graphdata);
            }
            else if (hrtime > 80 /*XX For Testing Cool Down */){
                //Just to get the continuation in the graph add the last same to  _TRAIN as well
                if (THRChart.ChartHRData_COOLDWN.length == 0){
                    THRChart.ChartHRData_COOLDWN.push(THRChart.ChartHRData_TEST[THRChart.ChartHRData_TEST.length-1]);
                }
                THRChart.ChartHRData_COOLDWN.push(graphdata)
            }else{
                //Just to get the continuation in the graph add the last same to  _TRAIN as well
                if (THRChart.ChartHRData_TEST.length == 0){
                    THRChart.ChartHRData_TEST.push(THRChart.ChartHRData_WARMUP[THRChart.ChartHRData_WARMUP.length-1]);
                }
                THRChart.ChartHRData_TEST.push(graphdata)
            }
            THRChart.ChartHRData_FULL.push(graphdata);

            //Need to refresh the Chart Here
        }
    }

function BLEDeviceOnError(reason) {
        alert("There was an error " + reason);
}
/******************************************************************************************/
/********************************* HR BLE Bluetooth  - End *********************************/
/******************************************************************************************/
