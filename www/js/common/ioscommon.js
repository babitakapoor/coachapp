/******************************************************************************************/
/************************** IOSCOMMON.JS VERSION 2.0 14/02/2013 ******************************/
/******************************************************************************************/
_TOAST_LONG = 1;
_TOAST_SHORT = 2;

function ShowToastMessage(msg, toastlen){
    logStatus(toastlen, LOG_DEBUG);
    logStatus(msg, LOG_DEBUG);
    try {
        logStatus("Calling ShowToastMessage", LOG_COMMON_FUNCTIONS);        
    } catch (e) {
        ShowExceptionMessage("ShowToastMessage", e);
    }
}

function ShowKeyBoard(){
    try {
        logStatus("Calling ShowKeyBoard", LOG_COMMON_FUNCTIONS);    	
    } catch (e) {
        ShowExceptionMessage("ShowKeyBoard", e);
    }
}

function HideKeyBoard(){
    try {
        logStatus("Calling HideKeyBoard", LOG_COMMON_FUNCTIONS);        
    } catch (e) {
        ShowExceptionMessage("HideKeyBoard", e);
    }
}