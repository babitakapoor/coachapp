/******************************************************************************************/
/************************** ANDROIDCOMMON.JS VERSION 2.0 14/02/2013 ******************************/
/******************************************************************************************/
_TOAST_LONG = 1;
_TOAST_SHORT = 2;

function ShowToastMessage(msg, toastlen){
     logStatus("Calling ShowToastMessage", LOG_COMMON_FUNCTIONS);
    try {
        /**
         * @param window.plugins.toast
         * @param window.plugins.toast.showLongCenter
         * @param window.plugins.toast.showShortCenter
         */

        if (toastlen == _TOAST_LONG){
            //XX window.MyToast.lng(msg);
            window.plugins.toast.showLongCenter(msg, function(){}, function(){});
        }else{
            //XX window.MyToast.srt(msg);
            window.plugins.toast.showShortCenter(msg, function(){}, function(){});
        }
    } catch (e) {
       ShowExceptionMessage("ShowToastMessage", e);
   }  
}

function PlayBeep(count){
    /**
     * @param  window.MyBeep.beep
     * @param  window.MyBeep.showKeyBoard
     * @param  window.MyBeep.hideKeyBoard
     */
    window.MyBeep.beep(count);        
}

function ShowKeyBoard(){
    logStatus("Calling ShowKeyBoard", LOG_COMMON_FUNCTIONS);
    try {
        window.KeyBoard.showKeyBoard();
    } catch (e) {
        ShowExceptionMessage("ShowKeyBoard", e);
    }   
}

function HideKeyBoard(){
    logStatus("Calling HideKeyBoard", LOG_COMMON_FUNCTIONS);
    try {
        window.KeyBoard.hideKeyBoard();
    } catch (e) {
        ShowExceptionMessage("HideKeyBoard", e);
    }
}