
/* Cybex Cardio Methods - Start */
var EQUIPMENT_FAMILY_NAME = {
    "-1": "Not Connected",
    "1": "Treadmill",
    "2": "ArcTrainer",
    "3": "Bike",
    "0": "Unknown"
};

var EQUIPMENT_STATE = {
    "1": "Active",
    "2": "Bootloader",
    "3": "Dormant",
    "4": "Error",
    "5": "Going Active",
    "6": "Leaving Active",
    "7": "Paused",
    "8": "Power Down",
    "9": "Power Up",
    "10": "Review",
    "11": "Toolbox",
    "12": "Wake Up",
    "13": "CSAFE Wake Up",
    "0": "Unknown"
};

var WORKOUT_STATE = {
    "1": "Started",
    "2": "Paused",
    "3": "Stopped",
    "4": "Unknown"
};

var _CurrentWATT = 0;
var _CurrentRPM = 40;
var _CurrentMachineName = 'Bike';
var _CurrentFamily = 3;
function InitializeCybexmachine(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler();
    }
}

function getCurrentTargetWatt(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler(_CurrentWATT);
    }
}

function setCurrentTargetWatt(watt) {
    _CurrentWATT = watt;
}

function getCurrentTargetRPM(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler(_CurrentRPM);
    }
}

function setMachinetoConstantPowerMode(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler();
    }
}

function getConnectedMachineName(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler(_CurrentMachineName);
    }
}

function getConnectedMachineFamily(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler(_CurrentFamily);
    }
}

function startWorkout(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler();
    }
}

function stopWorkout(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler();
    }
}

function coolWorkout(SuccessHandler) {
    if (SuccessHandler) {
        SuccessHandler();
    }
}

function Elapsedtimecallback(time) {
    logMessage('Elapsedtimecallback:' + time)
}


function EquipmentStateChangeCallback(state) {
    logMessage(state);
}

function WorkoutStateChangeCallback(state) {
    logMessage(state);
}

function updateMatrixdataCallback(matrixdata) {
    logMessage(matrixdata);
    if (CallBackFromCardioMachineMatrixdata){
        CallBackFromCardioMachineMatrixdata(matrixdata);
    }
    /*{elapsedtime: '%@', calories: '%d', caloriesPerHour: '%d', mets: '%.1f', watts: '%d', averageWatts: '%d', distanceInmiles: '%.2f', bpm: '%d', peakBpm: '%d', secondsInHeartRateZone: '%d', speedInMil: '%.1f', avgspeedInmiles: '%.1f', paceInSec: '%d:%02d', avgPaceInSec: '%d:%02d', currentStridesPerMinute: '%d', averageStridesPerMinute: '%d', currentTargetSpm: '%d', currentCrankRPM: '%d', averageCrankRPM: '%d'}
     */
}

function logMessage(msg){

}
/* Cybex Cardio Methods - End */