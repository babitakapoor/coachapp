var _doWebLoad;
var LAST_DIS_ID = "";
var LAST_LOADER_ID = "";
var FIELDTYPE = '';
var LAST_DIS_TXT = "";
var LANGUAGE	=	{};
var LOGIN_INFO	=	{};
var USER		=	{};
var EXT         =  ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
var HeartRateChart	=	{};
var GLOBAL_DIALOG;

/*Agility Level*/
var AGILITY_LEVEL;
$(document).ready(function(){
    $(".main_left").hide();
		  setInterval(function(){
				 if(is_login()){
				  var loginInfo = getCurrentLoginInfo();
				//////save user devie type app version///////////
						  checkAppversion(loginInfo.user.user_id);
				 }
				 else{
				  console.log("data not saved");
				 }
		},100000);
    $(".tickouter li").click(function(){
        $(".tickouter li").removeClass("tick");
        if($(this).hasClass("tick")){
            $(this).removeClass("tick");
        }else{
            $(this).addClass("tick");
        }
    });
    /*var doubleLabels = [
                "<i>-2</i><span>I hated it</span>",
                "<i>-1</i><span>I was displeased</span>",
                "<i>0</i><span>I have no feelings</span>",
                "<i>+1</i><span>I liked it</span>",
                "<i>+2</i><span>I am easily excited</span>"];*/
    setfootermenu();
    /* * / setCaptchaContent(".recapchabox",function(){

        /*On load Captcha Success* /

    });	*/
    $(".accordion-holder > .acc-row h2,.accordion-holder > .acc-row h3").click(function() {
        $(this).toggleClass('current-sec');
        $(this).next('.acc-content').slideToggle(250);
        $(this).find('.icon').toggleClass('icon-up');
    });
    $(window).resize(function(){
        if($(".page_content .showsideimage").html()) {
            maincontent_leftimage();
        }else{
            maincontent_leff_none();
        }
    });
});
/*MK - Added Hours,minutes with dropdown-*/
$(document).delegate("#lsthours,#lstminutes","change",function(){
    logStatus("Calling Event#lsthours.Change", LOG_FUNCTIONS);
    try {
        var hr	=	$("#lsthours").val();
        var mn	=	$("#lstminutes").val();
        $("#timehrmn").val(hr+":"+mn);
    } catch(e) {
        ShowExceptionMessage("Event#lsthours.Change", e);
    }
});
function HandleServiceFailure(){
    EnableLastProcessBtn();
}

$(document).click(function(e){

    if ((!$(e.target).closest('.logo_right .login').length) && (!$(e.target).closest('.logmenu').length)){
        $('.logmenu').hide();
    }
    if ((!$(e.target).closest('.logo_right .menu').length) && (!$(e.target).closest('.supmenu').length)){
        $('.supmenu').hide();
    }
    /*
   if(($(e.target).closest('.logmenu').length != 0) && ($(e.target).closest('.logo_right').length != 0)) return false;
   $('.logmenu').hide();
   */
});

/*
var $tabscnt2= $("#clsanalysis_tabs_cnt .tabs_cnt2");
var $tabscnt1= $("#clsanalysis_tabs_cnt .tabs_cnt1");
$(document).delegate(".tabschart1","click",function(){
    $(".clsanalysis_tabs_hdr span").removeClass("atve_tabs");
    $(this).addClass("atve_tabs");

    $tabscnt2.hide();
    $tabscnt1.show();
});

$(document).delegate(".tabschart2","click",function(){
    $(".clsanalysis_tabs_hdr span").removeClass("atve_tabs");
    $(this).addClass("atve_tabs");
    $tabscnt1.hide();
    $tabscnt2.show();
});
*/
$(document).delegate("#idbuttonlogin","click",function(){
    logStatus("Calling Event#idbuttonlogin.click", LOG_FUNCTIONS);
    try {
        var $loginmessage= $('.loginmessage');
        $loginmessage.find('label').html('');
        var  $loginid= $("#loginid");
        var  $loginpswd= $("#loginpswd");
        var username 	=	$loginid.val();
        var password	=	$loginpswd.val();
        if($loginid.val()==''){
            $loginid.parent().find('label').html(GetLanguageText(PLEASE_ENTER_USERNMAE));
            return;
        }
        if($loginpswd.val()==''){
            $loginpswd.parent().find('label').html(GetLanguageText(PLEASE_ENTER_PASSWORD));
            return;
        }
        $('.errortext').html('');		
        //var user=	getCurrentUser();
        if(validateSteps('#loginform1')) {
            DisableProcessBtn("#idbuttonlogin", false);
            doLoginLogoutUser(username,password,function(response){
                DisableProcessBtn("#idbuttonlogin", true,GetLanguageText(BTN_SUBMIT));
                if(response.status_code==1){
                    updateAppLogin(response);
					var userid  = response.user.user_id;
                    var device_type = device.platform;
                    var DEVICE_ID = $('#device_token').val();
						if(device_type == 'iOS' || device_type == 'Android'){
                    updateDeviceId(userid,DEVICE_ID,device_type);
						}
                } else {
                    $loginmessage.find('label').html(GetLanguageText(LOGIN_USERNAMEPASSWORD_INCORRECT));
                }
            });
        } else {
            $loginmessage.find('label').html(GetLanguageText(USERNAMEPASSWORD_INCORRECT));
        }
    } catch(e) {
        ShowExceptionMessage("Event#idbuttonlogin.click", e);
    }
});
$(document).delegate(".close_onouterclick","click",function(){
    logStatus("Calling close_onouterclick event",LOG_FUNCTIONS);
    try {
        _close_popup();
    } catch(e){
        ShowExceptionMessage("close_onouterclick click event", e);
    }
});
$(document).delegate(".logmenu li,.supmenu li","click",function(){
    logStatus("Calling logmenu click event",LOG_FUNCTIONS);
    try{
        $(this).parent('ul').hide();
    } catch(e){
        ShowExceptionMessage("logmenu click event", e);
    }
});


$(document).delegate('input[type="checkbox"]',"change",function(){
    logStatus("Calling type checkbox event",LOG_FUNCTIONS);
    try{
        var $this	=	$(this);
        var $assoc	=	$this.parent('span');
        if($this.is(":checked")){
            $assoc.addClass("checkbtn");
        } else {
            $assoc.removeClass("checkbtn");
        }
    } catch(e){
        ShowExceptionMessage("input type checkbox event", e);
    }
});
$(document).delegate('input[type="radio"]',"change",function(){
    logStatus("Calling input radio click event",LOG_FUNCTIONS);
    try{
        var $this	=	$(this);
        var $assoc	=	$this.parent('span');
        if($this.is(":checked")){
            $assoc.addClass("radiobtn");
        } else {
            $assoc.removeClass("radiobtn");
        }
    } catch(e){
        ShowExceptionMessage("input radio click event", e);
    }
});
$(document).delegate('.radiobox',"click",function(){
    logStatus("Calling radiobox click event",LOG_FUNCTIONS);
    try{
        var $this	=	$(this);
        var $assoc	=	$this.find('input[type="radio"]');
        var _class  =	$assoc.attr("class");
        if($assoc.is(":checked")){
                // Ed 20160210 radio button
                if(!$this.hasClass('radiobtn')){
            $assoc.removeAttr("checked");
                    $this.addClass("radiobtn");
                }
        } else {
            $assoc.attr("checked","true");
                $this.removeClass("radiobtn");
        }

        if(_class && _class!="") {
            $("." + _class).trigger("change");
        }

    } catch(e){
        ShowExceptionMessage("radiobox click event", e);
    }
});
$(document).delegate('.checkouter',"click",function(){
    logStatus("Calling checkouter click event",LOG_FUNCTIONS);
    try{
        var $this	=	$(this);
        var $assoc	=	$this.find('input[type="checkbox"]');
        var _class	=	$assoc.attr("class");
        var _name	=	$assoc.attr("name");
        var _hrefvalue	=	$assoc.attr("hrefvalue");
        var $hrefvalue	=	$("input[hrefvalue="+_hrefvalue+"]");
        var $machinedetails	=	$(".machinedetails");
        var $name	=	$("input[name="+_name+"]");
        if($assoc.is(":checked")){
            $assoc.removeAttr("checked");
            $hrefvalue.attr("checked",true);
            $hrefvalue.parent().addClass("checkbtn");
        } else {
            $assoc.attr("checked",true);
            $machinedetails.removeAttr("checked",false);
            $machinedetails.parent().removeClass("checkbtn");
            $name.attr("checked",true);
            $name.parent().addClass("checkbtn");
        }
        $("." + _class).trigger("change");
    } catch(e){
        ShowExceptionMessage("checkouter click event", e);
    }
});
/*$(document).delegate('#checkouter',"click",function(){
    logStatus("Calling checkouter click event",LOG_FUNCTIONS);
    try{
        var $this	=	$(this);
        var $assoc	=	$this.find('input[type="checkbox"]');
        var _class	=	$assoc.attr("class");
        if($assoc.is(":checked")){
            $assoc.removeAttr("checked");
            //$this.addClass("radiobtn");
        } else {
            $assoc.attr("checked","true");
            //$this.removeClass("radiobtn");
        }
        $("." + _class).trigger("change");
    } catch(e){
        ShowExceptionMessage("checkouter click event", e);
    }
});*/
$(document).on('change','#init_club',function(){
    logStatus("Calling init_club details",LOG_FUNCTIONS);
    try{
        var coach_club = $("#init_club").val();
        app_tempvars('club_selected',coach_club);
        storeselectedclubid(coach_club);
    } catch(e){
        ShowExceptionMessage("init_club click event", e);
    }
});
$(document).delegate(".tickouter li","click",function(){
    logStatus("Calling tickouter",LOG_FUNCTIONS);
    try{

        //$(".tickouter li").removeClass("tick");
        if($(this).hasClass("tick")){
            $(this).removeClass("tick");
        }else{
            $(".tickouter li").removeClass("tick");
            $(this).addClass("tick");
        }

        // alert("1");

        // $(".tickouter li").removeClass("tick");
        // $(this).addClass("tick");
    } catch(e){
        ShowExceptionMessage("tickouter click", e);
    }
});

$(document).delegate(".hidePg","click",function(){
    logStatus("Calling hidePg",LOG_FUNCTIONS);
    try{
        $(".choosephoto_popup").hide();
        //popup_bg(false);
    } catch(e){
        ShowExceptionMessage("hidePg", e);
    }
});
// *** [SK] comments by shanethatech ***//
// *** Show toggle on header login *** //
function showTogglePop(popmenu,hidemenu){
        logStatus("Calling showTogglePop", LOG_FUNCTIONS);
        try{
            $(hidemenu).hide();
            var $elem=	$(popmenu);//(popmenu == 'logmenu') ? $('.logmenu') : $('.supmenu');
            if($elem.is(":visible")){
                $elem.hide();
            } else{
                $elem.show();
                //$('.hideclass').show();
            }
        } catch(e) {
        ShowExceptionMessage("showTogglePop", e);
    }
}

// *** [SK] comments by shanethatech ***//
// *** Shown Page history ***//
function goBack(){
    logStatus("Calling goBack", LOG_FUNCTIONS);
    try {
        window.history.go(-1);
    } catch(e) {
        ShowExceptionMessage("goBack", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Verify weather user is login are not ***//
function is_login(){
    logStatus("Calling is_login", LOG_FUNCTIONS);
    try {
        if($.isEmptyObject(USER) && $.isEmptyObject(LOGIN_INFO)){
            USER		=	getCurrentUser();
            LOGIN_INFO	=	getCurrentLoginInfo();
            TranslateGenericText();
        }
        return !!( (!$.isEmptyObject(USER)) && (!$.isEmptyObject(LOGIN_INFO)));
    } catch(e) {
        ShowExceptionMessage("is_login", e);
    }
}
//*** [SK] comments by shanethatech ***//
//*** Application Logout, Then redirect to login page ***//
function signout(){
    logStatus("Calling signout", LOG_FUNCTIONS);
    try {
        localStorage.clear();
        LOGIN_INFO	=	{};
        USER		=	{};
        _movePageTo('login');
    } catch(e) {
        ShowExceptionMessage("signout", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Giving page access permission for including footer based on user login details ***//
function _onPageLoadHandler(){
    logStatus("Calling _onPageLoadHandler", LOG_FUNCTIONS);
    try{
        if(is_login() && is_page('login')){
            _movePageTo('home');
        } else if(!is_login() && !is_page('login') && !is_page('forgotpasword')){
            _movePageTo('login');
        }
        /* PK Added Login Valid icons * /
        if(is_page('login')){
        $(".logo_right").hide();
        $(".clsshanethethatech").hide();
        }else{
            $(".logo_right").show();
            $(".clsshanethethatech").show();
        }
        /**/
        /*MK Fixes Quick From Edison's code*/
        $(".double-label-slider").slider({
            max: 2,
            min: -2,
            value: 0,
            animate: 400,
            slide: function( event, ui ) {
                var $sliderelem		=	$(event.target);
                var $clsslidervalue= $( ".clsslidervalue"+$sliderelem.attr("sliderid"));
                if ($sliderelem.attr("sliderid")){
                    $clsslidervalue.html(ui.value);
                    $clsslidervalue.val(ui.value);
                    if(doFlexibilityCalculation) {
                        doFlexibilityCalculation();
                    }
                }
            }
        });
        /* MK Content Width on the - Landshcape & Portrait mode*/
        if(isSideBarAVailable()) {
            maincontent_leftimage();
        } else {
            maincontent_leff_none();
        }
        /* MK Content Width on the - Landshcape & Portrait mode - Ends*/
    } catch(e) {
        ShowExceptionMessage("_onPageLoadHandler", e);
    }
}

/*Added By Ashit to skip Manage Member Detail*/
function movebodycomptionskip(mid,testid,clubid){
    logStatus("Calling movebodycomption",LOG_FUNCTIONS);
    try {
        var  $bodycomptostionuserdata = mid;//$('#bodycomptostionuserdata');
        //var mid		= $bodycomptostionuserdata.val();
        var testid 	= testid;//$('#idbodycomptiontestdata').val();
        var clubid 	= clubid;//$('#clubid').val();
       
        if(!clubid){
            var urldata	=	getPathHashData();
            clubid	=	(urldata.args.clubid && urldata.args.clubid!='undefined') ? urldata.args.clubid:0;
        }
        var clubdetail	=  base64_decode(clubid);
        var _memberId	=	+$bodycomptostionuserdata.val();
        /*SN changed 20160215 - default bodycompostion test value display*/
        _movePageTo('manage-bodycomposition',{mid:base64_encode(_memberId),testid:base64_encode(testid),clubid:base64_encode(clubdetail),tab:'bodycomposition',newTest:1});
    } catch(e) {
        ShowExceptionMessage("movebodycomption", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** redirect appropriate page on select Grid list ***//
function moveSelectMemberPage(mid,testid,clubid,page,tab){
    logStatus("Calling moveSelectMemberPage",LOG_FUNCTIONS);
    try {
        _close_popup();
        var args	=	{};
        var _testid	=	(testid) ? testid:0;
        args.mid	=	base64_encode(mid);
        args.testid	=	base64_encode(""+_testid);
        args.clubid	=	base64_encode(clubid);
        if(tab){
            args.tab	=	tab;
        }
        /**
         * @param response.signature This is signature data
         * @param response.isclubmedicalinfo This is isclubmedicalinfo
         */
        /*SN Edited - Allow only for Members who completed Registeration */
        if(page=='manage-cardiotest3'){
            _getClientInformationByID(mid,function(response){
                if(response.signature=='' && response.isclubmedicalinfo==1){
                    ShowAlertMessage(GetLanguageText(EXCEPTION_CLIENT_PROFILE_INCOMPLETE));

                }
                else{
                     //Added to skip Member Detail Page
                    //_movePageTo(page,args);
               _movePageTo('manage-bodycomposition',{mid:base64_encode(mid),testid:base64_encode(testid),clubid:base64_encode(clubid),tab:'bodycomposition',newTest:1});
                                      
                }
            });
        }else{
            
            _movePageTo(page,args);
        }

        /*
        if (page == 'manage-cardiotest3'){
            ShowAlertMessage("In Progress");
        }else{
            _movePageTo(page,args);
        }
        */
    } catch(e) {
        ShowExceptionMessage("moveSelectMemberPage", e);
    }
}

/* 
 * Comments: To handle all members related ajax request.
  

 * author SK Shanethatech * 

 * Created Date  : Jan-21-2016 
 
*/

function redirectopageto(page,tab){
    logStatus("Calling redirectopageto",LOG_FUNCTIONS);
    try {
        var args = {};
        var urldata	=	getPathHashData();
        args.mid = $('#mid').val();
        args.testid = $('#testid').val();
        args.testid =	(urldata.args.testid && urldata.args.testid!='undefined') ? urldata.args.testid:base64_encode(""+0);
        var clubid 	=	(urldata.args.clubid && urldata.args.clubid!='undefined') ? base64_decode(urldata.args.clubid):0;
        args.clubid =base64_encode(clubid);
        if(tab){
            args.tab	=	tab;
        }
        _movePageTo(page,args);
    } catch(e) {
        ShowExceptionMessage("redirectopageto", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Validating input fields***//
function validateSteps(parentSelector,errordisplay){
        logStatus("Calling validateSteps", LOG_FUNCTIONS);
        logStatus(errordisplay, LOG_DEBUG);
    try {

        var _nonempty	=	jQuery(parentSelector + " .req-nonempty");
        var _none_zero	=	jQuery(parentSelector + " .req-nonzero");
        var _email		=	jQuery(parentSelector + " .req-email");
        var _emptyselect=	jQuery(parentSelector + " .req-emptyselect");
        var _inrangecheck=	jQuery(parentSelector + " .req-inrange");
        //var _ukpostcode	=	jQuery(parentSelector + " .req-ukpostcode");
        var _novalequal	=	jQuery(parentSelector + " .req-novalequal");
        var errormsg="";
        var htmlcnt='';
        var noError		=	true;
        _nonempty.each(function(){
            var $this=	jQuery(this);
            if(jQuery.trim($this.val())==''){
                noError	=	false;
                errormsg= $this.attr("validate-fail");
                htmlcnt = errormsg;
                if(errormsg){
                        $(".info_icon").css("display","inline-block");
                }
                //var elem = $this.parent().find('label').html(htmlcnt);
                $this.css({"border":"1px solid red"});
                $this.keyup(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.next('label').html('');
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    //$this.parent().find('span').removeClass('info_icon');
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();

                    //$this.parent().find('label').html(htmlcnt);
                });
            } else {
                $this.css({"border":"1px solid #000"});
                // $this.parent().find('span').hide();
                // $this.parent().find('.error_info').hide();
            }
        });
        _email.each(function(){
            var $this	=	jQuery(this);
            if(!_validateEmail($this.val())){
                noError	=	false;
                errormsg= $this.attr("validate-fail");
                htmlcnt = errormsg;
                if(errormsg){
                    $(".info_icon").css("display","inline-block");
                    $this.after(htmlcnt);
                }
                $this.css({"border":"1px solid red"});
                $this.keyup(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
        _emptyselect.each(function(){
            var $this	=	jQuery(this);
            noError		=	false;
            errormsg+= $this.attr("validate-fail");
            var html='<div>'+errormsg+'</div>';
            $this.after(html);
            if((!$this.find("option:selected").text()) || ($this.find("option:selected").text()=="")){
                $this.css({"border":"1px solid red"});
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.parent().find('.info_icon').hide();
                    $this.parent().find('.error_info').hide();
                });
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
        _inrangecheck.each(function(){
                          var $this	=	jQuery(this);
                          noError		=	false;
                          errormsg+= $this.attr("validate-fail");
                          var html='<div>'+errormsg+'</div>';
                           if(errormsg){
                           $(".info_icon").css("display","inline-block");
                           $this.after(html);
                           }
                           var minVal =$this.find("min").text();
                           var maxVal=$this.find("max").text();
                          if($this.val() > max || $this.val() < min)
                           {
                          $this.css({"border":"1px solid red"});
                          $this.change(function(){
                                       $this.css({"border":"1px solid #000"});
                                       $this.parent().find('.info_icon').hide();
                                       $this.parent().find('.error_info').hide();
                                       });
                          } else{
                          $this.css({"border":"1px solid #000"});
                          }
                          });
        _none_zero.each(function(){
            var $this	=	jQuery(this);
            if($this.val()=='0'){
                noError	=	false;
                //var html='';
                var errormsgzero= $this.attr("validate-fail");
                //htmlcnt=errormsgzero;

                if(errormsgzero){
                    $(".info_icon").css("display","inline-block");
                }
                //var elem = $this.parent().find('span').addClass('info_icon');
                //$this.next().html('<span class="info_icon"></span>');
                //var elem = $this.parent().find('label').html(errormsgzero);
                //$this.parent().find("label").before('<span class="info_icon"></span>');
                $this.css({"border":"1px solid red"});
                $this.change(function(){
                    $this.css({"border":"1px solid #000"});
                    $this.next('label').html('');
                    //$this.parent().find(".info_icon").css("display","none");
                    $this.parent().find('span').removeClass('info_icon');
                });
            } else{
                $this.css({"border":"1px solid #000"});
            }
        });
        /*if(_ukpostcode) {
            _ukpostcode.each(function(){
                var $this=	jQuery(this);
                if(!validateUKPostCode($this.val())){
                    noError	=	false;
                    $this.css({"border":"1px solid red"});
                    $this.siblings('.cval-lbl-error').html(GetLanguageText(MSG_VALID_UK_POSTCODE));
                } else{
                    $this.siblings('.cval-lbl-error').html("");
                }

            });
        }*/
        _novalequal.each(function(){
            var $this=	jQuery(this);
            alert($this.val());
            //var ne_value	=	$this.attr("novalequal") ? $this.attr("novalequal"):-1;

        });
        return noError;
    } catch (e) {
        //ShowExceptionMessage("validateSteps", e);
    }
}

function _validateEmail(email) {
    logStatus("Calling _validateEmail",LOG_FUNCTIONS);
    try {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    } catch(e) {
        ShowExceptionMessage("_validateEmail", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Shown proper formated date ***//
function dateFormat(datetime){

    var t = datetime.split(/[- :]/);
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    var date = d.getDate();
    date = date < 10 ? "0"+date : date;

    var mon = d.getMonth()+1;
    mon = mon < 10 ? "0"+mon : mon;

    var year = d.getFullYear();

    return (date+"/"+mon+"/"+year);
}
// *** [SK] comments by shanethatech ***//
// *** Login details bind to cookies ***//
function updateAppLogin(data){
    logStatus("Calling updateAppLogin",LOG_FUNCTIONS);
    try{
        var isCheckRemindMe	=	$("#is_remindme").is(":checked");
        LOGIN_INFO	=	data;
        USER		=	data.user;
        USER_LANG	=	data.user.r_language_id;
        if(isCheckRemindMe){
            _setCacheArray('user',data.user);
            _setCacheArray('login_info',data);
        }
        UpdatePhotoOnFooter();
        var user_reqpage=	$(".tickouter .tick .selected_page");
        var logintopage	=	'home';
        if(user_reqpage.val()==USER_SELECT_CARDIOTEST){
            logintopage	='cardio-test';
        } else if(user_reqpage.val()==USER_SELECT_STRENGTHTEST){
            logintopage	='chestpress';
        }
        /**
         * @param loginInfo.user_session This is user detail session
         * @param user_session.associatedClubs This is associated Clubs
         * @param userclubs[0].r_club_id This is  Club id
         */
        var loginInfo	=	getCurrentLoginInfo();

        var userclubs	=	loginInfo.user_session.associatedClubs;

        if(userclubs.length==1) {
            if(userclubs[0].r_club_id) {
                storeselectedclubid(userclubs[0].r_club_id);
                loadDeviceSettings(function(response){
                    if($.isEmptyObject(response)){
                        _saveDeviceData();
                    }else{
                        _setCacheArray('devicesettings',response);
                    }
                });
            }
        }
        if(userclubs.length > 1){
            _popup('<div class="popuptitle locationclass">'+GetLanguageText(LBL_SELECT_LOCATION)+'</div><div class="field_in_box">'+
                '<select id="init_club">' +
                    setOption_AssociatedClubs() +
                '</select>'+
                '</div><div class="pupopfooter">'+
                '<button class="btn" onclick="ChooseCurrentClub(\''+logintopage+'\');">'+GetLanguageText(BTN_GO)+'</button></div>',{close:false});
        } else {
            profileIncomplete(logintopage);
        }
    } catch(e) {
        ShowExceptionMessage("updateAppLogin", e);
    }
}
/*MK Moved to COMMON JS to make the selected clubs*/
function setOption_AssociatedClubs(_element){
    logStatus("Calling setOption_AssociatedClubs", LOG_FUNCTIONS);
    try {
        var loginInfo	=	getCurrentLoginInfo();
        var userclubs	=	loginInfo.user_session.associatedClubs;
		console.log('hello');
		console.log(userclubs);
        var HTML		=	"";
        if(userclubs){
            /**
             * @param index.r_club_id This is club id
             * @param index.club_name This is club name
             */
            $.each(userclubs,function(key,index){
                var clubid	=	getselectedclubid();
                var selected=	'';
                if(clubid) {
                    selected=	(clubid && clubid==index.r_club_id) ? 'selected':'';
                }
                HTML +='<option value='+index.r_club_id+' '+selected+'>'+index.club_name+'</option>';
            });
        }
        if(_element){
            $(_element).html(HTML);
        } else {
            return HTML;
        }
    } catch(e) {
        ShowExceptionMessage("setOption_AssociatedClubs", e);
    }
}
function ChooseCurrentClub(redirecpage){
    logStatus("Calling ChooseCurrentClub",LOG_FUNCTIONS);
    try {
        storeselectedclubid($("#init_club").val());
        _close_popup();
        profileIncomplete(redirecpage);
    } catch(e) {
        ShowExceptionMessage("ChooseCurrentClub", e);
    }
}
function UpdatePhotoOnFooter(){
    logStatus("Calling UpdatePhotoOnFooter",LOG_FUNCTIONS);
    try {
        var data = getCurrentLoginInfo();
        if(!$.isEmptyObject(data) && !$.isEmptyObject(data.user)){
         _getUserImage('getUserImage',data.user.user_id,function(response){
            var filename = (response && response!='') ? response.split('.').pop():'';
             if(jQuery.inArray(filename, EXT) !== -1){
                //$('#userprofiledata').attr('src',PROFILEPATH+response);
                if(response!=''){
                    getValidImageUrl(PROFILEPATH + response,function(url){
                        $('#userprofile').attr('src',url);
                        $('#userprofiledata').attr('src',url);
                    });
                }
             }
         });
         }
     } catch(e) {
        ShowExceptionMessage("UpdatePhotoOnFooter", e);
    }
}

// *** [SK] comments by shanethatech ***//
// *** If profile is incompleted, I will re-direct to profile information page ***//
function profileIncomplete(redirectpage){
    logStatus("Calling profileIncomplete", LOG_FUNCTIONS);
    try {
        /**
         * @param data.security_questions This is security questions
         */
        var data = getCurrentUser();
        var incompleteprofile = !!( (!data) || (!data.security_questions) || (data.security_questions == "") ) ;
        if(incompleteprofile){
            redirectpage = 'edit-profile';
            var POPUP_HTML	= '<div class="field_in_box">'+GetLanguageText(TEXT_INCOMPLETE_PROFILE)+'<button class="selectbtn"  onclick="_close_popup();_movePageTo(\'' + redirectpage + '\');" >'+GetLanguageText(BTN_GO)+'</button></div>';
                _popup(POPUP_HTML,{close:false});
        } else{
            _movePageTo(redirectpage);
        }
    } catch(e) {
        ShowExceptionMessage("profileIncomplete", e);
    }
}
/*
function Onprofileincompletedetails(button){
     logStatus("Excuting Onprofileincompletedetails", LOG_DEBUG);
     try{
         if (button == 1)
        {
            _movePageTo('edit-profile');
        }
     } catch (e) {
        ShowExceptionMessage("Onprofileincompletedetails",e);
    }
}
*/
// *** [SK] comments by shanethatech ***//
// *** Binding page Titles ***//
function _setPageTitle(title){
    logStatus("Calling _setPageTitle", LOG_FUNCTIONS);
    try {
        $('#pagetitle').html(title);
    } catch(e) {
        ShowExceptionMessage("_setPageTitle", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Set footer menu based on login page access details ***//
function setfootermenu(){
    logStatus("Calling setfootermenu", LOG_FUNCTIONS);
    try {
        var $footerbordermenu= $('.footerbordermenu');
        var $footerusermenu= $('.footerusermenu');
        $footerbordermenu.hide();
        $footerusermenu.show();
        if(is_page('login') || is_page('forgotpasword')){
            $footerusermenu.hide();
            $footerbordermenu.show();
        }
        UpdatePhotoOnFooter();
    } catch(e) {
        ShowExceptionMessage("setfootermenu", e);
    }
}


// *** [SK] comments by shanethatech ***//
// ***Get current login details stored in cookies ***//
function getCurrentUser(){	
    logStatus("Calling getCurrentUser", LOG_FUNCTIONS);
    try {
        if(!$.isEmptyObject(USER)){
            return USER;
        }
        USER	=	_getCacheArray('user');
        return USER;
    } catch(e) {
        ShowExceptionMessage("getCurrentUser", e);
    }
}

// *** [MD] comments by shanethatech ***//
// ***get the current logged in userid ***//
function getCurrentLoggedinUserId(){
    logStatus("Calling getCurrentLoggedinUserId", LOG_FUNCTIONS);
    try {
        var userobj = getCurrentUser();
        if ((userobj)&&(userobj.user_id)){
            return userobj.user_id;
        }
        return 0;
    } catch(e) {
        ShowExceptionMessage("getCurrentLoggedinUserId", e);
    }
}

// *** [SK] comments by shanethatech ***//
// ***Get entire login details of curent user stored in cookies ***//
function getCurrentLoginInfo(){	
    logStatus("Calling getCurrentLoginInfo", LOG_FUNCTIONS);
    try {
        if(!$.isEmptyObject(LOGIN_INFO)){
            return LOGIN_INFO;
        }
        LOGIN_INFO	=	_getCacheArray('login_info');
        getCurrentUser();
        return LOGIN_INFO;
    } catch(e) {
        ShowExceptionMessage("getCurrentLoginInfo", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** UPdate current login data information ***//
function updateCurrentLoginInfo(){
    logStatus("Calling updateCurrentLoginInfo", LOG_FUNCTIONS);
    try {
        /* Already Written code*/
        /* MK commented
         * Mis Replaced Correct code*/
        if(!$.isEmptyObject(LOGIN_INFO)){
            _setCacheArray('login_info', LOGIN_INFO);
            _setCacheArray('user',LOGIN_INFO.user);
        }
        /**/
    } catch(e) {
            ShowExceptionMessage("updateCurrentLoginInfo", e);
    }
}
function cardiotest(){
    logStatus("Calling cardiotest",LOG_FUNCTIONS);
    try {
        //var urldata	=	getPathHashData();
        //var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        //var testid	=	(urldata.args.testid) ? base64_decode(urldata.args.testid) :0;
        //var clubid	=	getselectedclubid();
        /*_movePageTo('manage-cardiotest1',{mid:base64_encode(memberId),testid:base64_encode(testid),clubid:base64_encode(clubid)});*/
        //_movePageTo('cardio-test');
        //Update On Automatic Appearance Temporarily We use this code
        _movePageTo('manage-cardiotest4');//,{member:base64_encode(val)});
    } catch(e) {
        ShowExceptionMessage("cardiotest", e);
    }
}
function storeselectedclubid(clubid){
    logStatus("Calling storeselectedclubid",LOG_FUNCTIONS);
    try {
        //_setCacheArray('club',clubid);
        _setCacheValue('club',clubid);
    } catch(e) {
        ShowExceptionMessage("storeselectedclubid", e);
    }
}
function getselectedclubid(){
    logStatus("Calling getselectedclubid",LOG_FUNCTIONS);
    try {
        // _getCacheArray('club');
        return _getCacheValue('club');
    } catch(e) {
        ShowExceptionMessage("getselectedclubid", e);
    }
}
// *** [MD] comments by shanethatech ***//
// *** Updates the test option with the HTML tag and updates the given control ***//
function PopulateTestOptions(controlid, jsondata){
    logStatus("Calling PopulateTestOptions",LOG_FUNCTIONS);

    try {
        var htmlcnt = '';
        if (jsondata){
            var JSONOBJ = null;
            try{

                JSONOBJ = JSON.parse(jsondata);
            }catch(e){
            }
            /**
             * @param value.test_id This is test id
             * @param value.test_name This is test name
             */
            if (JSONOBJ){
                $.each(JSONOBJ,function(key,value){
                    htmlcnt += "<option value='"+value.test_id+"'>"+value.test_name+"</option>";
                })
            }
        }
        $(controlid).html(htmlcnt);
    } catch(e) {
        ShowExceptionMessage("PopulateTestOptions", e);
    }
}
// *** [MD] comments by shanethatech ***//
// *** Updates the test option with the HTML tag and updates the given control ***//
function PopulateTestLevels(controlid, jsondata){
    logStatus("Calling PopulateTestLevels",LOG_FUNCTIONS);
    try {
        var htmlcnt = '';
        if (jsondata){

            var JSONOBJ = null;
            try{
                JSONOBJ = JSON.parse(jsondata);

            }catch(e){
            }
            if (JSONOBJ){
                $.each(JSONOBJ,function(key,value){

/* 					if(key=='E' || key=='F')
					{
						return;
					} */
                    var lvl = $('#tst_lvl_val').val();
                    if($('#tst_lvl_val').val() != '')
                    {
                        var selected = (key==lvl) ?'selected':'';
                    }
                    else{
                         var selected = (key=="C") ?'selected':'';
                    }
                    /*SN added testlevel selected 20160210*/
                   
                    htmlcnt += "<option value='"+key+"' "+selected+">"+key + " = " +value+" Watt </option>";
					 })
            }
        }
        $(controlid).html(htmlcnt);
    } catch(e) {
        ShowExceptionMessage("PopulateTestLevels", e);
    }
}
$(document).delegate("#idcardiotestweight","blur",function(){
    logStatus("Calling Event.Click.idcardiotestweight", LOG_FUNCTIONS);
    try {
        LoadTestLevelList($('#idcardiotestweight').val(), function(response){
            PopulateTestLevels('.cdbdcmppersontestlevel', response);
            var selectedLevel =	app_tempvars('cardiotest_level');
            if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
                $("#testlevel").val(selectedLevel);
            }
        });
    } catch(e) {
        ShowExceptionMessage("idcardiotestweight click event", e);
    }
});
 

  // *** [SK] comments by shanethatech ***//
// *** Start New test data ***//
/*
 function getNewTestdata(mid,testid,clubid,onsuccessnewtestdataHandler){
      logStatus("Calling getNewTestdata", LOG_FUNCTIONS);
    try {
        //var responsedata = "";
        _getUserImage('getUserImage',mid,function(response){
         var filename = response.split('.').pop();
            if(jQuery.inArray(filename,EXT) !== -1){
                getValidImageUrl(PROFILEPATH + response,function(url){
                    $('#userprofile').attr('src',url);
                });
            }
        });
        /**
         * @param response.fitness_level This is fitness level
         * /
        getEditMember(mid,testid,clubid,function(response){
            $('.memberName').html(response.last_name);
            $('.user_weight').html(response.weight);
            $('.curntfitnesslevel').html(response.fitness_level);
            $('#bodycomptostionuserdata').attr('value',response.user_id);
        });
        if(onsuccessnewtestdataHandler){
            onsuccessnewtestdataHandler();
        }
    } catch(e) {
        ShowExceptionMessage("getNewTestdata", e);
    }
 }
*/
/*
function profilepop(){
    logStatus("Calling profilepop",LOG_FUNCTIONS);
    try {
         var POPUP_HTML = '<form id="upload_formdata"  name="upload"  method="post" enctype="multipart/form-data"><div class="choosephoto_popup">'+
                    '<div class="choosephotocnt">'+
                    '<h2 class="capturephotoopt" selectopt="0">'+
                '<span class="galleryicon" ></span>'+GetLanguageText(TXT_GALLERY)+'<input type="file" onchange="uploadImage()" id="uploadBtndata" name="profileImage"/></h2>'+
                '<h2 id="idcapturephoto" class="capturephotoopt" selectopt="1">'+
                '<span class="photoicon" ></span>'+GetLanguageText(TXT_CAMERA)+'</h2></div></div></form>'+
                '<span class="hidePg"></span>';
        _popup(POPUP_HTML,{close:false, closeOnOuterClick:true});
    } catch(e) {
        ShowExceptionMessage("profilepop", e);
    }
}*/
/* function displayImage(){
     logStatus("Calling Display picture", LOG_FUNCTIONS);
    var imgName = uploadBtn.files[0].name;
    var Extension = imgName.substring(imgName.lastIndexOf('.') + 1).toLowerCase();
    if ($.inArray(Extension, EXT) == -1) {
            /* If images extenssion is not avilible*/
    /*	 return false;
    }
    if (uploadBtn.files && uploadBtn.files[0]) {
        ShowToastMessage(JSON.stringify(uploadBtn.files[0]), _TOAST_LONG);
    }
 } */

 
 function DisableProcessBtn(id, enable, text,isHtmlType){
     logStatus("Calling DisableProcessBtn",LOG_FUNCTIONS);
     try {
        /*MK Added - To OverWirte the Custom Text*/
        if (!text){
            text = GetLanguageText(TXT_LOAD_WAITING_MSG);//"Please wait...";
        }
        if (enable){
            $(id).removeAttr("disabled");
        }else{
            LAST_DIS_ID = id;
            FIELDTYPE = '';
            LAST_DIS_TXT = $(id).val();
            $(id).attr("disabled", "disabled");
        }
        if(isHtmlType){
            FIELDTYPE = 'html';
            $(id).html(text);
            return;
        }
        $(id).val(text);
    } catch(e) {
        ShowExceptionMessage("DisableProcessBtn", e);
    }
 }
 
 function EnableLastProcessBtn(){
     logStatus("Calling EnableLastProcessBtn",LOG_FUNCTIONS);
     try {
        if (LAST_DIS_ID != ''){
            if ($(LAST_DIS_ID)){
                $(LAST_DIS_ID).removeAttr("disabled");
                if (FIELDTYPE == 'html'){
                    $(LAST_DIS_ID).html(LAST_DIS_TXT);
                }else{
                    $(LAST_DIS_ID).val(LAST_DIS_TXT);
                }

            }
        }
    } catch(e) {
        ShowExceptionMessage("EnableLastProcessBtn", e);
    }
 }


/*
function bdcmpflexslider(val){
    logStatus("Calling bdcmpflexslider",LOG_FUNCTIONS);
    try {
    //	var $self = $(this);
    //	alert($self.attr('sliderid'));
        var data = $( ".clsslidervalue"+val).attr("sliderid").html();
    } catch(e) {
            ShowExceptionMessage("bdcmpflexslider", e);
        }
}

*/

// *** [MD] show standard ajax message ***//
function showstandardajaxmessage(response, successhandler){
    logStatus("Calling showstandardajaxmessage", LOG_FUNCTIONS);
    if(typeof response !='object'){
        try{
            response = JSON.parse(response);
        }catch(e){
        }
    }
    /**
     * @param response.status_message This is status message
     */
    if ((response)&&(response.status)) {
        if (response.status == AJAX_STATUS_TXT_SUCCESS){
            ShowToastMessage(response.status_message, _TOAST_LONG);
            if (successhandler){
                successhandler();
            }
        }else{
            ShowAlertMessage(response.status_message);
        }
    }
}
function _(el){
    logStatus("_",LOG_FUNCTIONS);
    try {
        return document.getElementById(el);
    } catch (e) {
        ShowExceptionMessage("_", e);
    }
}
/* 
 * Comments: To Handel Page Custom Alert message
 * author SK Shanethatech * 
 * Created Date  : Jan-29-2014 */
/*
function customAlertMessage(msg,trueHandler,falseHandler){
    logStatus("customAlertMessage",LOG_FUNCTIONS);
    try {
        var HTML ='<div class="mask-screen"></div>'+
                    '<div class="mask-message">'+
                        '<div>'+
                            '<p>' + msg + '</p>'+
                        '</div>'+
                        '<div class="popupinner_panel_btnbx">'+
                            '<button class="selectbtn" id="clickyes">OK</button>'+
                            '<button class="selectbtn" id="clickno">No</button>'+
                        '</div>'+
                '</div>';
            $('body').append(HTML);
            $("#clickyes").on("click",function(){
                trueHandler();
            });
            $("#clickno").on("click",function(){
                $('.mask-screen').remove();
                $('.mask-message').remove();
                if(falseHandler){
                    falseHandler();
                }
            });
    } catch (e) {
        ShowExceptionMessage("customAlertMessage", e);
    }
}
*/
/*MK Added Get Date for type date input*/
function _getDateForInput(date){
    logStatus("_getDateForInput",LOG_FUNCTIONS);
    try {
        var d = (date) ? new Date(date):new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        return  '' + d.getFullYear() + '-'  + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;
        /*MK to be used later
        var _date	=	new Date();
        return _date.toDateInputValue();*/
    } catch (e) {
        ShowExceptionMessage("_getDateForInput", e);
    }
}
/*MK - Added Web Page*/
function IsWebLoad(){
    return (typeof(_doWebLoad) != 'undefined');

}
/*MK Quick Fix - Need to change later*/
$(document).delegate(".forgot",'click',function(){	
    logStatus("Calling forgot click event", LOG_FUNCTIONS);
    try {
    var emailplaceholder = GetLanguageText(TXT_ENTER_EMAIL);
    var POPUP_HTML	= '<div class="popuptitle transforgotpaswd">'+GetLanguageText(TEXT_FORGOTPASSWORD)+'</div><div class="field_in_box"><input type="email" id="forgetemail" class="frgemail " placeholder="'+emailplaceholder+'" ><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="forgetpswrdred()">'+GetLanguageText(LBL_NEXT)+'</button><button class="selectbtn" onclick="_close_popup()">'+GetLanguageText(LBL_CLOSE)+'</button></div></div>';
    _popup(POPUP_HTML,{close:false});
    } catch(e) {
        ShowExceptionMessage("forgot", e);
    }
});
/*SN added highchart graph load 20160206*/
function graphOnload(titleGraph, bottomCaption, totalDatas, yAxisValue,tab,popUpId) {
    logStatus("graphOnload",LOG_FUNCTIONS);
    logStatus(yAxisValue,LOG_DEBUG);
    logStatus(titleGraph,LOG_DEBUG);
    try {
        var popUp ='';
        if (popUpId == undefined) {
             popUp = 'weightFat';
        } else {
             popUp = 'largePopUp';
        }
        if(tab=='bodycomposition'){
        $('#' + popUp).highcharts({
            chart: {
                type: 'column'
            },
            title: {
            text: GetLanguageText(TXT_BODY_COMPOSITION_GRAPH)
            },
            xAxis: {
                categories: bottomCaption,
                crosshair: true
            },
            yAxis: {
                min: 0,
                /*max:200,*/
                gridLineDashStyle: 'longdash',
                gridLineColor: '#0095ac',
                title: { text: GetLanguageText(TXT_EVOLUTION_LIST) }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: totalDatas
        });
        }
        if(tab=='cardio'){
            $('#' + popUp).highcharts({
                chart: {
                    type: 'spline'
                },
                title: {
                    text: GetLanguageText(TXT_CARDIO_GRAPH)
                },
                xAxis: {
                    type: 'datetime'
                },
            yAxis: {
                title: {
                    text: GetLanguageText(TXT_TEST_LEVEL_WATT)
                },
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                        from: 0.3,
                        to: 1.5,
                        color: 'rgba(68, 170, 213, 0.1)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Light breeze
                        from: 1.5,
                        to: 3.3,
                        color: 'rgba(0, 0, 0, 0)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Gentle breeze
                        from: 3.3,
                        to: 5.5,
                        color: 'rgba(68, 170, 213, 0.1)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Moderate breeze
                        from: 5.5,
                        to: 8,
                        color: 'rgba(0, 0, 0, 0)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Fresh breeze
                        from: 8,
                        to: 11,
                        color: 'rgba(68, 170, 213, 0.1)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Strong breeze
                        from: 11,
                        to: 14,
                        color: 'rgba(0, 0, 0, 0)',
                        label: {
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // High wind
                        from: 14,
                        to: 15,
                        color: 'rgba(68, 170, 213, 0.1)',
                        label: {
                            style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            plotOptions: {
                spline: {
                    lineWidth: 4,
                    states: {
                        hover: {
                            lineWidth: 5
                        }
                    },
                    marker: {
                        enabled: false
                    },
                    pointInterval: 3600000, // one hour
                    pointStart: Date.UTC(2015, 4, 31, 0, 0, 0)
                }
            },
            series: [{
                data: [0.2, 0.8, 0.8, 0.8, 1, 1.3, 1.5, 2.9, 1.9, 2.6, 1.6, 3, 4, 3.6, 4.5, 4.2, 4.5, 4.5, 4, 3.1, 2.7, 4, 2.7, 2.3, 2.3, 4.1, 7.7, 7.1, 5.6, 6.1, 5.8, 8.6, 7.2, 9, 10.9, 11.5, 11.6, 11.1, 12, 12.3, 10.7, 9.4, 9.8, 9.6, 9.8, 9.5, 8.5, 7.4, 7.6]
            }],
            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        });
        }
    }catch (e) {
        ShowExceptionMessage("graphOnload", e);
    }
}
/*MK Added To check Whether Form is CHANGED Or NOT*/
function _isFormDataChanged(parent){
    var $parent		=	$(parent);
    var $trackinp	=	$parent.find('.trackIPChanges');
    var flag		=	false;
    $trackinp.each(function(i,row){
        var $each	=	$(row);
        if($each.attr('datavalold')!=$each.val()){
            flag	=	true;
            return false;
        }
    });
    return flag;
}
/*Test Type ID From Test Type String*/
function getValidTestTypeID(currentTab){
    logStatus("Calling getValidTestTypeID", LOG_FUNCTIONS);
    try{
        var TestTypeId	=	0;
        switch(currentTab){
            case 'bodycomposition':
                TestTypeId	=	TEST_TYPE_BODYCOMP;
                break;
            case 'flexibility':
                TestTypeId	=	TEST_TYPE_FLEXIBLITY;
                break;
            case 'cardio':
                TestTypeId	=	TEST_TYPE_CARDIO;
                break;
        }
        return TestTypeId;
    } catch(e){
        ShowExceptionMessage("getValidTestTypeID", e);
    }
}
/*MK Added Optimized Chart - 16 Feb 2016*/
function setCustomChart(memberID,selector,isReturn){
    logStatus("Calling setCustomChart", LOG_FUNCTIONS);
    try{
        var _customChart	=	new Highcharts.Chart({
            chart: {
                renderTo:selector
            },
            xAxis: {
                tickInterval: 1,
                minPadding: 0,
                maxPadding: 0
            }
        });
        if(isReturn){
            return _customChart;
        } else {
            if(!HeartRateChart[memberID]){
                HeartRateChart[memberID]	=	_customChart;
            }
        }
    } catch(e){
        ShowExceptionMessage("setCustomChart", e);
    }
}
function __onBeforeAppLoadSuccess(onSuccessHandler){
    /*Init Before the Fwork load*/
    loadLanguage(function(response){
        LANGUAGE	=	response;
        /*End Initialization*/
        if(onSuccessHandler){
            onSuccessHandler()
        }
    });
}
/*PK Added - TRanslation*/

 function TranslateGenericText(){
     TranslateText(".nextcls", TEXTTRANS, LBL_NEXT);
     TranslateText(".transsave", VALTRANS,BTN_SAVE );
     TranslateText(".transselect", TEXTTRANS, LBL_SELECT);
     TranslateText(".transsearch", PHTRANS, TEX_SEARCH);
     TranslateText(".transsearch", TEXTTRANS, TEX_SEARCH);
     TranslateText(".refresh_btn",TEXTTRANS, PH_REFRESH_BTN);
     TranslateText(".savecls", TEXTTRANS, BTN_SAVE);
     TranslateText(".transemail", TEXTTRANS, PH_EMAIL);
     TranslateText(".transaction", TEXTTRANS, LBL_ACTION);
     TranslateText(".transgender", TEXTTRANS, LBL_GENDER);
     TranslateText(".transfirstname", TEXTTRANS, PH_FIRST_NAME);
     TranslateText(".translastname", TEXTTRANS, PH_LAST_NAME);
     TranslateText(".transmemberid", TEXTTRANS, LBL_MEMBER_ID);
     TranslateText(".transpersonalid", TEXTTRANS, LBL_PERSONAL_ID);
     TranslateText(".transmanagemembersmenu", TEXTTRANS, MENU_MANAGE_MEMBERS);
     TranslateText(".transcardiomenu", TEXTTRANS, BTN_CARDIO);
     TranslateText(".transstrengthmenu", TEXTTRANS, LBL_STRENGTH);
     TranslateText(".transhealthcheckmenu", TEXTTRANS, MENU_HEALTH_CHECK);
     TranslateText(".transreportsmenu", TEXTTRANS, LBL_REPORTS);
     TranslateText(".transmessagesmenu", TEXTTRANS, MENU_MESSAGE);
     TranslateText(".transagendacallentermenu", TEXTTRANS, MENU_AGENDA_CALLENTER);
     TranslateText(".transsettingmenu", TEXTTRANS, MENU_SETTINGS);
     TranslateText(".transswitchusermenu", TEXTTRANS, MENU_SWITCH_USER);
     TranslateText(".transexitmenu", TEXTTRANS, TXT_EXIT);
     TranslateText(".transgallery", TEXTTRANS, TXT_GALLERY);
     TranslateText(".transcamera", TEXTTRANS, TXT_CAMERA);
     TranslateText(".transtests", TEXTTRANS, TXT_TESTS);
     TranslateText(".newmachinesexe", TEXTTRANS, TXT_NEW_MACHINES_EXERCISSES_PROGRAM);
     TranslateText(".transpoint", TEXTTRANS, TXT_POINTS);
     TranslateText(".transcalendar", TEXTTRANS, PAGE_TTL_MANAGE_CALENTER);
     TranslateText(".transcontact", TEXTTRANS, TXT_CONTACT);
     TranslateText(".transstrengthover", TEXTTRANS, TXT_STRENGTH_OVERVIEW);
     TranslateText(".transcoach", TEXTTRANS, LBL_COACH);
     TranslateText(".transclubname", TEXTTRANS, TXT_CLUB_NAME);
     TranslateText(".transagetestcls", TEXTTRANS, LBL_AGE_TEST);
     TranslateText(".transweighttestcls", TEXTTRANS, LBL_WEIGHT_TEST);
     TranslateText(".transtestlevelcls", TEXTTRANS, LBL_TEST_LEVEL);
     TranslateText(".transtestoption", TEXTTRANS, LBL_TEST_OPTION);
     TranslateText(".transtestdatecls", TEXTTRANS, LBL_TEST_DATE);
     TranslateText(".transtimecls", TEXTTRANS, TXT_TIME);
     TranslateText(".transtestcls", TEXTTRANS, TXT_TEST);
     TranslateText(".addmember", TEXTTRANS, TXT_ADD_MEMBER);
     TranslateText(".transleft", TEXTTRANS, TXT_LEFT);
     TranslateText(".transright", TEXTTRANS, TXT_RIGHT);
     TranslateText(".transcardiotest", TEXTTRANS, PAGE_TTL_CARDIOTEST);
     TranslateText(".transstrengthtest", TEXTTRANS, LBL_STRENGTHTEST);
     TranslateText(".transnumber", TEXTTRANS, PH_NUMBER);
     TranslateText(".translist", TEXTTRANS, BTN_LIST);
     TranslateText(".transedit", TEXTTRANS, BTN_EDIT);
     TranslateText(".transadd", TEXTTRANS, BTN_ADD);
     TranslateText(".transdelete", TEXTTRANS, BTN_DETETE);
     TranslateText(".transsave", TEXTTRANS,BTN_SAVE );
     TranslateText(".transtestequipment", TEXTTRANS, LBL_TEST_EQUIPMENT);
}

function TranslateText(transobj, fieldtype, transkey){
     var $transobj= $(transobj);
     if (fieldtype == HTMLTRANS){
         $transobj.html(GetLanguageText(transkey));
    }else if (fieldtype == VALTRANS){
         $transobj.val(GetLanguageText(transkey));
     }else if (fieldtype == TEXTTRANS){
        $transobj.text(GetLanguageText(transkey));
     }else {
        $transobj.attr('placeholder',GetLanguageText(transkey));
     }
 }

/*MK Added-  Loadin Hours Minutes  - DROP DOWN*/
function getMinutesListOption(selector){
    logStatus("Calling getMinutesListOption", LOG_FUNCTIONS);
    try{
        var HTML	=	'<option value="00"class="transmin">'+GetLanguageText(PH_MIN)+'</option>';
        for(var i=10;i<=60;){
            var minutes	=	( (i.toString().split('').length<2) ? '0'+i:i);
            HTML+=	'<option value=' + minutes + '>'  + minutes + '</option>';
            i=i+10;
        }
        $(selector).html(HTML);
    } catch(e){
        ShowExceptionMessage("getMinutesListOption", e);
    }
}
function getHoursListOption(selector){
    logStatus("Calling getHoursListOption", LOG_FUNCTIONS);
    try{
        var HTML	=	'<option value="00">'+GetLanguageText(LBL_HR)+'</option>';
        for(var i=0;i<24;i++){
            var hours	=	( (i.toString().split('').length<2) ? '0'+i:i);
            HTML+=	'<option value='+ hours + '>'  + hours + '</option>';
        }
        $(selector).html(HTML);
    } catch(e){
        ShowExceptionMessage("getHoursListOption", e);
    }
}
/*MK Added - Range Slider*/
/*
function incInput(MaxVal,step,selector,onChangeVal){
    logStatus("incInput",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector    =   "#rangerinput";
        }
        var $elem       =   $(selector);
        var spanname = "c_"+$elem.attr('id');
        var spannamebtm = "d_"+$elem.attr('id');
		var spannamebtmf = "e_"+$elem.attr('id');
		$('#'+spannamebtmf).css('display','none');
        // var spannamebtm1 = "e_"+$elem.attr('id');

        var incval  =   $.trim($elem.html());
        var nevalue =   (parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            
            $elem.html(nevalue);
            sym = nesymbolconvert(nevalue);
           syymtxt = nesymboltext(nevalue,spanname);

            $('#'+spanname).text(sym[0]);
            $('#'+spannamebtm).text(syymtxt);
           // $('#'+spannamebtm1).text(sym[2]);
           
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInput", e);
    }
}
    function decInput(MinVal,step,selector,onChangeVal){
    logStatus("decInput",LOG_FUNCTIONS);
    try {
        if(!selector){

            selector    =   "#rangerinput";
        }
        var $elem       =   $(selector);
       
        var spanname = "c_"+$elem.attr('id');
        var spannamebtm = "d_"+$elem.attr('id');
		 var spannamebtmf = "e_"+$elem.attr('id');
       
		
		$('#'+spannamebtmf).css('display','none');
       // var spannamebtm1 = "e_"+$elem.attr('id');
        var decval  =   $.trim($elem.html());
        var newvalue=   parseInt(decval)-step;
       
        
        if(newvalue>=MinVal){
            $elem.html(newvalue);

            sym = nesymbolconvert(newvalue);
            syymtxt = nesymboltext(newvalue,spanname);

            $('#'+spanname).text(sym[0]);
            
            $('#'+spannamebtm).text(syymtxt);

             //$('#'+spannamebtm1).text(sym[2]);
               
             if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInput", e);
    }
}
*/
function GetLanguageText(Key){	
    var Lang = USER_LANG;
    if((LANGUAGE[Lang]) && LANGUAGE[Lang][Key]){
        return LANGUAGE[Lang][Key];
    }
    return LANGUAGE[DEF_USER_LANG][Key];
}
function GetUrlArg(key,isEncoded,defValue){
    logStatus("GetUrlArg",LOG_FUNCTIONS);
    try {
        var urldata	=	getPathHashData();
        if(urldata.args && urldata.args[key]){
            return isEncoded ? base64_decode(urldata.args[key]) : urldata.args[key];
        }
        if(defValue){
            return defValue;
        }
        return '';
    } catch(e){
        ShowExceptionMessage("GetUrlArg", e);
    }
}
/*SN Added -20160312 Age By DOB*/
function getAgeByDOB(date) {
    logStatus("getAgeByDOB",LOG_FUNCTIONS);
    try {
        var today = new Date();
        var birthDate = new Date(date);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }
        return isNaN(age) ? 'N/A':age;
    } catch(e){
        ShowExceptionMessage("getAgeByDOB", e);
    }
}
function getAgeBetweenDates(dob, date) {
    logStatus("getAgeBetweenDates",LOG_FUNCTIONS);
    try {
        var birthDate = new Date(dob);
        var today = new Date(date.split(" ")[0]);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }
        return isNaN(age) ? 'N/A':age;
    } catch(e){
        ShowExceptionMessage("getAgeBetweenDates", e);
    }
}

function getLogingPlaceholder(){
    return '<img src="images/loading.gif" alt="Loading.."/>';
}

function maincontent_leftimage(){
    var $mainleft= $(".main_left");
    var $pagecontent= $(".page_content");
    $pagecontent.removeClass("centerwidth");
    $mainleft.show();
    var header_height = $(".header").outerHeight();
    var footer_height = $("footer").outerHeight();
    var window_height = $(window).height();
    var maincontent_leftimage = (window_height - footer_height - header_height  );
    $mainleft.css({
        "margin-top" : header_height,
        "margin-bottom" : footer_height,
        "height": maincontent_leftimage
    });
    $(".main_left img").css({
        "height": maincontent_leftimage
    });
    var main_left_width = $mainleft.width();
    var window_width = $(window).width();

    var page_content_width = (window_width - main_left_width - 20);
    $pagecontent.css({
        "width": page_content_width,
        "height": maincontent_leftimage-5,
        "margin-top" : header_height
        // "overflow" : "auto"
    });
}

function maincontent_leff_none(){
    var  $pagecontent= $('.page_content');
    var header_height = $(".header").outerHeight();
    var footer_height = $("footer").outerHeight();
    var window_height = $(window).height();
    var maincontent_leftimage = (window_height - footer_height - header_height  );
    $(".main_left").hide();

    $pagecontent.addClass("centerwidth");
    $pagecontent.css({
        "height": maincontent_leftimage-5,
        "margin-top" : header_height
        // "overflow" : "auto"
    });
}



function isSideBarAVailable(){
    return $(".main_left").is(":visible");
}

function CallBackMotionFromStrengthSensor(data){
    if (UpdateStrengthSensorParam){
        UpdateStrengthSensorParam(data);
    }
}
function updateDeviceData(){
    loadDeviceSettings(function(response){
        _setCacheArray('devicesettings',response);
    });
}
/*MK Added Task No.
* Flexibility Calculation */
function doFlexibilityCalculation(){
    logStatus("Calling doFlexibilityCalculation", LOG_FUNCTIONS);
    try{
        /* Fixed - Flex Muscle Point variation*/
        var FlexiblityMuscPoint	=	[-1.5,-1.0,1.0,1.5,2.0];
        /* Fixed - Flex Level Limitation {Level:Limit}*/
        var LevelObject			=	{1:1.99,2:3.99,3:6.49,4:8.99,5:10.99,6:12.99};
        /* Fixed - Flex Result Based Level {Level:Result String} */
        var LevelString			=	{1:'Bad',2:'Average',3:'Good',4:'Very Good',5:'Hypermobile'};
        var TotalMusclePointVal	=	0;
        $(".clsbdcmpflextestparamer").each(function(i,elem){
            var $elem	=	$(elem);
            var sliderid	=	$elem.attr("sliderid");
            if(sliderid) {
                var eachSliderVal	=	$( ".clsslidervalue" + sliderid).html();
                /*Flexibility Calculation - MusclePoint= MusclePoint + Variation[Selected Slider Value] */
                var muscleIndex		=	parseInt($.trim(eachSliderVal)) + 2;
                TotalMusclePointVal	=	TotalMusclePointVal + parseFloat(FlexiblityMuscPoint[muscleIndex]);
            }
        });
        var flex_level	=	getFlexLevel(LevelObject,TotalMusclePointVal);
        var flex_string	=	getFlexString(LevelString,flex_level);
        $("#flexLevel").val(flex_level);
        $("#flexLevelString").val(flex_string);
        $("#flexCategory").val(TotalMusclePointVal);
    } catch(e){
         ShowExceptionMessage("doFlexibilityCalculation", e);
    }
}
function getFlexLevel(levelarray,muscle_point){
    logStatus("Calling getFlexLevel", LOG_FUNCTIONS);
    try{
        var flexlevel	=	7;	// Default Level
        $.each(levelarray,function(level,val){
            flexlevel	=	level;
            if(muscle_point<val){
                return false;
            }
        });
        return flexlevel;
    } catch(e){
        ShowExceptionMessage("getFlexLevel", e);
    }
}
function getFlexString(levelString,flex_level){
    logStatus("Calling getFlexString", LOG_FUNCTIONS);
    try{
        if(levelString[flex_level]){
            return levelString[flex_level];
        }
        return 'Bad'; // Default Result
    } catch(e){
        ShowExceptionMessage("getFlexString", e);
    }
}
/*-------------------------------  28-02-17 --------------------------------------------*/

/*-------------------------------  Calfs Left--------------------------------------------*/

function incInputCalfsl(MaxVal,step,selector,onChangeVal){
    logStatus("incInputCalfsl",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
		
		if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Calfs_l_new').html('0');
				$('#Calfs_l_text').html('26° – 35°');
			}
			if(nevalue==1){
				$('#Calfs_l_new').html('+');
				$('#Calfs_l_text').html('36° – 45°');
			}
			if(nevalue==2){
				$('#Calfs_l_new').html('++');
				$('#Calfs_l_text').html('>46°');
				
			}
			if(nevalue==-1){
				$('#Calfs_l_new').html('-');
				$('#Calfs_l_text').html('16° – 25°');
			}
			if(nevalue==-2){
				$('#Calfs_l_new').html('--');
				$('#Calfs_l_text').html('<15°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputCalfsl", e);
    }
}
function decInputCalfsl(MinVal,step,selector,onChangeVal){
    logStatus("decInputCalfsl",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
			
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Calfs_l_new').html('0');
				$('#Calfs_l_text').html('26° – 35°');
			}
			if(newvalue==1){
				$('#Calfs_l_new').html('+');
				$('#Calfs_l_text').html('36° – 45°');
			}
			if(newvalue==2){
				$('#Calfs_l_new').html('++');
				$('#Calfs_l_text').html('>46°');
			}
			if(newvalue==-1){
				$('#Calfs_l_new').html('-');
				$('#Calfs_l_text').html('16° – 25°');
			}
			if(newvalue==-2){
				$('#Calfs_l_new').html('--');
				$('#Calfs_l_text').html('<15°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputCalfsl", e);
    }
}

/*-------------------------------  Calfs Left End--------------------------------------------*/

/*-------------------------------  Calfs Right Start--------------------------------------------*/

function incInputCalfsr(MaxVal,step,selector,onChangeVal){
    logStatus("incInputCalfsr",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
		if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Calfs_r_new').html('0');
				$('#Calfs_r_text').html('26° – 35°');
			}
			if(nevalue==1){
				$('#Calfs_r_new').html('+');
				$('#Calfs_r_text').html('36° – 45°');
			}
			if(nevalue==2){
				$('#Calfs_r_new').html('++');
				$('#Calfs_r_text').html('>46°');
			}
			if(nevalue==-1){
				$('#Calfs_r_new').html('-');
				$('#Calfs_r_text').html('16° – 25°');
			}
			if(nevalue==-2){
				$('#Calfs_r_new').html('--');
				$('#Calfs_r_text').html('<15°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputCalfsr", e);
    }
}
function decInputCalfsr(MinVal,step,selector,onChangeVal){
    logStatus("decInputCalfsr",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
			
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Calfs_r_new').html('0');
				$('#Calfs_r_text').html('26° – 35°');
			}
			if(newvalue==1){
				$('#Calfs_r_new').html('+');
				$('#Calfs_r_text').html('36° – 45°');
			}
			if(newvalue==2){
				$('#Calfs_r_new').html('++');
				$('#Calfs_r_text').html('>46°');
			}
			if(newvalue==-1){
				$('#Calfs_r_new').html('-');
				$('#Calfs_r_text').html('16° – 25°');
			}
			if(newvalue==-2){
				$('#Calfs_r_new').html('--');
				$('#Calfs_r_text').html('<15°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputCalfsr", e);
    }
}

/*-------------------------------  Calfs Right End--------------------------------------------*/

/*-------------------------------  Illipsoas Left Start--------------------------------------------*/

function incInputIllipsoasL(MaxVal,step,selector,onChangeVal){
    logStatus("incInputIllipsoasL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Illipsoas_l_new').html('0');
				$('#Illipsoas_l_text').html('Horizontaal');
			}
			if(nevalue==1){
				$('#Illipsoas_l_new').html('+');
				$('#Illipsoas_l_text').html('Beneden hor');
			}
			if(nevalue==2){
				$('#Illipsoas_l_new').html('++');
				$('#Illipsoas_l_text').html('Ver beneden hor');
			}
			if(nevalue==-1){
				$('#Illipsoas_l_new').html('-');
				$('#Illipsoas_l_text').html('Boven hor');
			}
			if(nevalue==-2){
				$('#Illipsoas_l_new').html('--');
				$('#Illipsoas_l_text').html('Ver boven hor');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputIllipsoasL", e);
    }
}
function decInputIllipsoasL(MinVal,step,selector,onChangeVal){
    logStatus("decInputIllipsoasL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Illipsoas_l_new').html('0');
				$('#Illipsoas_l_text').html('Horizontaal');
			}
			if(newvalue==1){
				$('#Illipsoas_l_new').html('+');
				$('#Illipsoas_l_text').html('Beneden hor');
			}
			if(newvalue==2){
				$('#Illipsoas_l_new').html('++');
				$('#Illipsoas_l_text').html('Ver beneden hor');
			}
			if(newvalue==-1){
				$('#Illipsoas_l_new').html('-');
				$('#Illipsoas_l_text').html('Boven hor');
			}
			if(newvalue==-2){
				$('#Illipsoas_l_new').html('--');
				$('#Illipsoas_l_text').html('Ver boven hor');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputIllipsoasL", e);
    }
}
/*-------------------------------  Illipsoas Left End--------------------------------------------*/
/*-------------------------------  Illipsoas Right Start--------------------------------------------*/

function incInputIllipsoasR(MaxVal,step,selector,onChangeVal){
    logStatus("incInputIllipsoasR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Illipsoas_r_new').html('0');
				$('#Illipsoas_r_text').html('Horizontaal');
			}
			if(nevalue==1){
				$('#Illipsoas_r_new').html('+');
				$('#Illipsoas_r_text').html('Beneden hor');
			}
			if(nevalue==2){
				$('#Illipsoas_r_new').html('++');
				$('#Illipsoas_r_text').html('Ver beneden hor');
			}
			if(nevalue==-1){
				$('#Illipsoas_r_new').html('-');
				$('#Illipsoas_r_text').html('Boven hor');
			}
			if(nevalue==-2){
				$('#Illipsoas_r_new').html('--');
				$('#Illipsoas_r_text').html('Ver boven hor');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputIllipsoasR", e);
    }
}
function decInputIllipsoasR(MinVal,step,selector,onChangeVal){
    logStatus("decInputIllipsoasR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Illipsoas_r_new').html('0');
				$('#Illipsoas_r_text').html('Horizontaal');
			}
			if(newvalue==1){
				$('#Illipsoas_r_new').html('+');
				$('#Illipsoas_r_text').html('Beneden hor');
			}
			if(newvalue==2){
				$('#Illipsoas_r_new').html('++');
				$('#Illipsoas_r_text').html('Ver beneden hor');
			}
			if(newvalue==-1){
				$('#Illipsoas_r_new').html('-');
				$('#Illipsoas_r_text').html('Boven hor');
			}
			if(newvalue==-2){
				$('#Illipsoas_r_new').html('--');
				$('#Illipsoas_r_text').html('Ver boven hor');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputIllipsoasR", e);
    }
}
/*-------------------------------  Illipsoas Right End--------------------------------------------*/
/*-------------------------------  Hamstrings Left Start --------------------------------------------*/

function incInputHamstringsL(MaxVal,step,selector,onChangeVal){
    logStatus("incInputHamstringsL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Hamstrings_l_new').html('0');
				$('#Hamstrings_l_text').html('85° - 95°');
			}
			if(nevalue==1){
				$('#Hamstrings_l_new').html('+');
				$('#Hamstrings_l_text').html('95° - 100°');
			}
			if(nevalue==2){
				$('#Hamstrings_l_new').html('++');
				$('#Hamstrings_l_text').html('>100°');
			}
			if(nevalue==-1){
				$('#Hamstrings_l_new').html('-');
				$('#Hamstrings_l_text').html('75° - 85°');
			}
			if(nevalue==-2){
				$('#Hamstrings_l_new').html('--');
				$('#Hamstrings_l_text').html('<75°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputHamstringsL", e);
    }
}
function decInputHamstringsL(MinVal,step,selector,onChangeVal){
    logStatus("decInputHamstringsL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Hamstrings_l_new').html('0');
				$('#Hamstrings_l_text').html('85° - 95°');
			}
			if(newvalue==1){
				$('#Hamstrings_l_new').html('+');
				$('#Hamstrings_l_text').html('95° - 100°');
			}
			if(newvalue==2){
				$('#Hamstrings_l_new').html('++');
				$('#Hamstrings_l_text').html('>100°');
			}
			if(newvalue==-1){
				$('#Hamstrings_l_new').html('-');
				$('#Hamstrings_l_text').html('75° - 85°');
			}
			if(newvalue==-2){
				$('#Hamstrings_l_new').html('--');
				$('#Hamstrings_l_text').html('<75°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputHamstringsL", e);
    }
}
/*-------------------------------  Hamstrings Left End --------------------------------------------*/

/*-------------------------------  Hamstrings Left Start --------------------------------------------*/

function incInputHamstringsR(MaxVal,step,selector,onChangeVal){
    logStatus("incInputHamstringsR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Hamstrings_r_new').html('0');
				$('#Hamstrings_r_text').html('85° - 95°');
			}
			if(nevalue==1){
				$('#Hamstrings_r_new').html('+');
				$('#Hamstrings_r_text').html('95° - 100°');
			}
			if(nevalue==2){
				$('#Hamstrings_r_new').html('++');
				$('#Hamstrings_r_text').html('>100°');
			}
			if(nevalue==-1){
				$('#Hamstrings_r_new').html('-');
				$('#Hamstrings_r_text').html('75° - 85°');
			}
			if(nevalue==-2){
				$('#Hamstrings_r_new').html('--');
				$('#Hamstrings_r_text').html('<75°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputHamstringsR", e);
    }
}
function decInputHamstringsR(MinVal,step,selector,onChangeVal){
    logStatus("decInputHamstringsR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Hamstrings_r_new').html('0');
				$('#Hamstrings_r_text').html('85° - 95°');
			}
			if(newvalue==1){
				$('#Hamstrings_r_new').html('+');
				$('#Hamstrings_r_text').html('95° - 100°');
			}
			if(newvalue==2){
				$('#Hamstrings_r_new').html('++');
				$('#Hamstrings_r_text').html('>100°');
			}
			if(newvalue==-1){
				$('#Hamstrings_r_new').html('-');
				$('#Hamstrings_r_text').html('75° - 85°');
			}
			if(newvalue==-2){
				$('#Hamstrings_r_new').html('--');
				$('#Hamstrings_r_text').html('<75°');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputHamstringsR", e);
    }
}
/*-------------------------------  Hamstrings Left End --------------------------------------------*/

/*-------------------------------  Breast Left Start --------------------------------------------*/
function incInputBreastL(MaxVal,step,selector,onChangeVal){
    logStatus("incInputBreastL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Breast_l_new').html('0');
				$('#Breast_l_text').html('Tegen de tafel');
			}
			if(nevalue==1){
				$('#Breast_l_new').html('+');
				$('#Breast_l_text').html('Onder de tafel');
			}
			if(nevalue==2){
				$('#Breast_l_new').html('++');
				$('#Breast_l_text').html('Ver onder de tafel');
			}
			if(nevalue==-1){
				$('#Breast_l_new').html('-');
				$('#Breast_l_text').html('Minder dan een vuist');
			}
			if(nevalue==-2){
				$('#Breast_l_new').html('--');
				$('#Breast_l_text').html('Meer dan een vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputBreastL", e);
    }
}
function decInputBreastL(MinVal,step,selector,onChangeVal){
    logStatus("decInputBreastL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Breast_l_new').html('0');
				$('#Breast_l_text').html('Tegen de tafel');
			}
			if(newvalue==1){
				$('#Breast_l_new').html('+');
				$('#Breast_l_text').html('Onder de tafel');
			}
			if(newvalue==2){
				$('#Breast_l_new').html('++');
				$('#Breast_l_text').html('Ver onder de tafel');
			}
			if(newvalue==-1){
				$('#Breast_l_new').html('-');
				$('#Breast_l_text').html('Minder dan een vuist');
			}
			if(newvalue==-2){
				$('#Breast_l_new').html('--');
				$('#Breast_l_text').html('Meer dan een vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputBreastL", e);
    }
}

/*-------------------------------  Brest Left End --------------------------------------------*/
/*-------------------------------  Breast Right Start --------------------------------------------*/
function incInputBreastR(MaxVal,step,selector,onChangeVal){
    logStatus("incInputBreastR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Breast_r_new').html('0');
				$('#Breast_r_text').html('Tegen de tafel');
			}
			if(nevalue==1){
				$('#Breast_r_new').html('+');
				$('#Breast_r_text').html('Onder de tafel');
			}
			if(nevalue==2){
				$('#Breast_r_new').html('++');
				$('#Breast_r_text').html('Ver onder de tafel');
			}
			if(nevalue==-1){
				$('#Breast_r_new').html('-');
				$('#Breast_r_text').html('Minder dan een vuist');
			}
			if(nevalue==-2){
				$('#Breast_r_new').html('--');
				$('#Breast_r_text').html('Meer dan een vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputBreastR", e);
    }
}
function decInputBreastR(MinVal,step,selector,onChangeVal){
    logStatus("decInputBreastR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Breast_r_new').html('0');
				$('#Breast_r_text').html('Tegen de tafel');
			}
			if(newvalue==1){
				$('#Breast_r_new').html('+');
				$('#Breast_r_text').html('Onder de tafel');
			}
			if(newvalue==2){
				$('#Breast_r_new').html('++');
				$('#Breast_r_text').html('Ver onder de tafel');
			}
			if(newvalue==-1){
				$('#Breast_r_new').html('-');
				$('#Breast_l_text').html('Minder dan een vuist');
			}
			if(newvalue==-2){
				$('#Breast_r_new').html('--');
				$('#Breast_r_text').html('Meer dan een vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputBreastR", e);
    }
}
/*-------------------------------  Brest Right End --------------------------------------------*/
/*-------------------------------  Quadriceps Left Start --------------------------------------------*/

function incInputQuadricepsL(MaxVal,step,selector,onChangeVal){
    logStatus("incInputQuadricepsL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Quadriceps_l_new').html('0');
				$('#Quadriceps_l_text').html('één vuist');
			}
			if(nevalue==1){
				$('#Quadriceps_l_new').html('+');
				$('#Quadriceps_l_text').html('tegen zitvlak');
			}
			if(nevalue==2){
				$('#Quadriceps_l_new').html('++');
				$('#Quadriceps_l_text').html('naast zitvlak');
			}
			if(nevalue==-1){
				$('#Quadriceps_l_new').html('-');
				$('#Quadriceps_l_text').html('boven één vuist');
			}
			if(nevalue==-2){
				$('#Quadriceps_l_new').html('--');
				$('#Quadriceps_l_text').html('ver boven één vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputQuadricepsL", e);
    }
}
function decInputQuadricepsL(MinVal,step,selector,onChangeVal){
    logStatus("decInputQuadricepsL",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Quadriceps_l_new').html('0');
				$('#Quadriceps_l_text').html('één vuist');
			}
			if(newvalue==1){
				$('#Quadriceps_l_new').html('+');
				$('#Quadriceps_l_text').html('tegen zitvlak');
			}
			if(newvalue==2){
				$('#Quadriceps_l_new').html('++');
				$('#Quadriceps_l_text').html('naast zitvlak');
			}
			if(newvalue==-1){
				$('#Quadriceps_l_new').html('-');
				$('#Quadriceps_l_text').html('boven één vuist');
			}
			if(newvalue==-2){
				$('#Quadriceps_l_new').html('--');
				$('#Quadriceps_l_text').html('ver boven één vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputQuadricepsL", e);
    }
}
/*-------------------------------  Quadriceps Left End --------------------------------------------*/

/*-------------------------------  Quadriceps Right Start --------------------------------------------*/

function incInputQuadricepsR(MaxVal,step,selector,onChangeVal){
    logStatus("incInputQuadricepsR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var incval	=	$.trim($elem.html());
        var nevalue	=	(parseInt(incval) + parseInt(step));
        if(nevalue<=MaxVal){
            $elem.html(nevalue);
			if(nevalue==0){
				$('#Quadriceps_r_new').html('0');
				$('#Quadriceps_r_text').html('één vuist');
			}
			if(nevalue==1){
				$('#Quadriceps_r_new').html('+');
				$('#Quadriceps_r_text').html('tegen zitvlak');
			}
			if(nevalue==2){
				$('#Quadriceps_r_new').html('++');
				$('#Quadriceps_r_text').html('naast zitvlak');
			}
			if(nevalue==-1){
				$('#Quadriceps_r_new').html('-');
				$('#Quadriceps_r_text').html('boven één vuist');
			}
			if(nevalue==-2){
				$('#Quadriceps_r_new').html('--');
				$('#Quadriceps_r_text').html('ver boven één vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("incInputQuadricepsL", e);
    }
}
function decInputQuadricepsR(MinVal,step,selector,onChangeVal){
    logStatus("decInputQuadricepsR",LOG_FUNCTIONS);
    try {
        if(!selector){
            selector	=	"#rangerinput";
        }
        var $elem		=	$(selector);
        var decval	=	$.trim($elem.html());
        var newvalue=	parseInt(decval)-step;
        if(newvalue>=MinVal){
            $elem.html(newvalue);
			if(newvalue==0){
				$('#Quadriceps_r_new').html('0');
				$('#Quadriceps_r_text').html('één vuist');
			}
			if(newvalue==1){
				$('#Quadriceps_r_new').html('+');
				$('#Quadriceps_r_text').html('tegen zitvlak');
			}
			if(newvalue==2){
				$('#Quadriceps_r_new').html('++');
				$('#Quadriceps_r_text').html('naast zitvlak');
			}
			if(newvalue==-1){
				$('#Quadriceps_r_new').html('-');
				$('#Quadriceps_r_text').html('boven één vuist');
			}
			if(newvalue==-2){
				$('#Quadriceps_r_new').html('--');
				$('#Quadriceps_r_text').html('ver boven één vuist');
			}
            if(onChangeVal){
                onChangeVal();
            }
        }
    } catch(e){
        ShowExceptionMessage("decInputQuadricepsR", e);
    }
}
/*-------------------------------  Quadriceps Right End --------------------------------------------*/

 $(document).delegate("#weight","blur",function(){
    logStatus("Calling Event.Click.idcardiotestweight", LOG_FUNCTIONS);
    try {
        LoadTestLevelList($('#weight').val(), function(response){
            PopulateTest_Levels('.cdbdcmppersontest_level', response);
            var selectedLevel = app_tempvars('cardiotest_newlevel');
            if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
                $("#testlevel").val(selectedLevel);
            }
        });
    } catch(e) {
        ShowExceptionMessage("idcardiotestweight click event", e);
    }
});
// ** Updates the test option with the HTML tag and updates the given control **//
function PopulateTest_Levels(controlid, jsondata){
    logStatus("Calling PopulateTestLevels",LOG_FUNCTIONS);
    try {
   var htmlcnt = '';
        if (jsondata){

            var JSONOBJ = null;
            try{
                JSONOBJ = JSON.parse(jsondata);

            }catch(e){
            }
            if (JSONOBJ){
                $.each(JSONOBJ,function(key,value){

                    var lvl = $('#tst_lvl_newval').val();
                    if($('#tst_lvl_newval').val() != '')
                    {
                        var selected = (key==lvl) ?'selected':'';
                    }
                    else{
                         var selected = (key=="C") ?'selected':'';
                    }
                    /*SN added testlevel selected 20160210*/
                   
                    htmlcnt += "<option value='"+key+"' "+selected+">"+key + "</option>";
      })
            }
        }
        $(controlid).html(htmlcnt);
    } catch(e) {
        ShowExceptionMessage("PopulateTestLevels", e);
    }
}
/*to update device id*/
function updateDeviceId(userid,DEVICE_ID,device_type){
     logStatus("Calling MindUpdateActivities", LOG_FUNCTIONS);
        try {
        var postdata = {}; 
         postdata.user_id   =   userid;
         postdata.device_id  =   DEVICE_ID;
          postdata.device_type  =   device_type;
         postdata.is_reporting=  base_url+"/reporting/index.php/voucherlist/addupdateDeviceIdforcoach";
         updatedeviceCoach(postdata,function(response){
                if(response.status==1){
              console.log("data saved");
                }
                else{
                console.log("data not saved");
                }
        });
    }
     catch(e) {
        ShowExceptionMessage("MindUpdateActivities", e);
    }
}
/**********************************/
function checkAppversion(user_id)
{
    
    var postdata= {};
    postdata.user_id = user_id;
    postdata.app_platform = device.platform;
    postdata.device_version = device.version;
    postdata.device_model = device.model;
    postdata.app_version = APP_VERSION;
    addAppVersion_type(postdata,function(response){
                      console.log(response.status_message);
                       });
}