/******************************************************************************************/
/********************************* Application Level  - Start **********************************/
/******************************************************************************************/
//var Num=1;
/*var GlobalActiveScope = null;
var GlobalFooterScope = null;
function DeviceLoadCompleted(){ 
    _IS_SYNC_PROGRESS = 0;
    CheckAndBlockScreenNotSyncronized(); 
    setTimeout(function() {stopprogress()}, 2000);
    setTimeout(function() {HideSplashScreen();}, 2000);
    if (GlobalActiveScope){
        if (GlobalActiveScope.refresh){
            GlobalActiveScope.refresh();        
        }
    }
    if (GlobalFooterScope){
        if (GlobalFooterScope.refresh){
            GlobalFooterScope.refresh();        
        }
    }
    //setUserData(FormatedDate(new Date(),FORMAT_ONLY_DATE)); //By Sankar
}
function SetActivePageLoadHandler(LoadHandler){
    _ACTIVEPAGEHANDLER = LoadHandler;
}
function InitApp(){
    logStatus("Execute InitApp ",LOG_FUNCTIONS);
    stopprogress();
    /*angular.element(document).ready(function() {
        angular.bootstrap(document, ["ImpulseApp"]);
        _ISAPPLOADED = true;
    });	* /
}*/
/*
function RefreshScroll(){
    myScroll2.scrollTo(0, 0, 1000);
    //alert($("#wrapper").height());
    setTimeout(function(){         
        myScroll.refresh();
        myScroll2.refresh();
        //alert(1);
    }, 1000)
}
*/
/*
function LoadCurrentPageData(){
    logStatus("LoadCurrentPageData", LOG_FUNCTIONS);    
    try{
        if (_ACTIVEPAGEHANDLER){
            //SetWrapperHeight();
            _ACTIVEPAGEHANDLER();
            //ApplyAppTheme();
            stopprogress();
        }else{
            //ExecuteMainCartContent();
        } 
    } catch (e) { 
        stopprogress();
        ShowExceptionMessage("LoadCurrentPageData", e); 
    }        
    
}
*/
/******************************************************************************************/
/********************************* Main Cart Screen  - Start ******************************/
/******************************************************************************************/
function CheckAndBlockScreenNotSyncronized(){

}

function getWholeWord(givestring, MaxLimit){
    var subtextslen = givestring.length;
    if (subtextslen > MaxLimit){  
        subtextslen = givestring.substr(0, MaxLimit).lastIndexOf("\n");
        if (subtextslen <= 0){
            subtextslen = givestring.substr(0, MaxLimit).lastIndexOf(" ");            
        }
        if (subtextslen <= 0){
            subtextslen = MaxLimit;
        }
    }
    return  givestring.substr(0, subtextslen);
}

$(document).delegate('.custom_checkbox','click',function(){
    var check	=	$(this).find('.check');
    var $this	=	$(this);
    if(check.is(':checked')){
        check.attr('checked',false);
        $this.removeClass("selected");
    } else {
        check.attr('checked',true);
        $this.addClass("selected");
    }
});

/*
function _autoPostDataOnTimer(){
    if (_initTimerAfterPost){
        clearTimeout(_initTimerAfterPost);
    }
    _initTimerAfterPost =	setTimeout(function(){
        if(_IsNotCancelledAutoTimer){
            _isNeedForceDataUpdate=false;
            //postDataForSynch();
        } else {
            clearInterval(_autoposttimer);
        }
    },AUTO_POST_TIMEER_DELAY * 1000 + 1800);
}
*/
function _showAutoPostTimer(onTimerCompletion){
    var _x	=	15;
    var _autoposttimer	=	setInterval(function(){
        $("#timer").html(_x);
        if(_x==0){
            clearInterval(_autoposttimer);
            if(onTimerCompletion){
                onTimerCompletion();
            }
        }
        _x--;
    },1000);
}
/*Function created and implemented by MK*/
__sortMe	=	function(originalObject,_sortHandler){
    try{
        var _objectAsArray	=	[];
        var _objectAsObject	=	{};
        $.each(originalObject,function(key,rows){
            _objectAsArray.push({data:rows,key:key})
        });
        if(_sortHandler){
            _objectAsArray.sort(_sortHandler);
        } else {
            _objectAsArray.sort();
        }
        for(var _iter=0;_iter<_objectAsArray.length;_iter++){
            _objectAsObject[_objectAsArray[_iter].key]	=	_objectAsArray[_iter].data;
        }
        return _objectAsObject;
    } catch(e){
        ShowExceptionMessage("Object.prototype.__sortMe", e);
    }
};
function _preventSourceElement($event,isDisable,handleOnSuccess){
    logStatus("_preventSourceElement " + isDisable,LOG_FUNCTIONS);
    try{
        var _isDisable	=	(isDisable) ? isDisable:false;
        var elem = $event.currentTarget || $event.srcElement;
        var $elem= jQuery(elem);
        if(_isDisable){
            $elem.attr("disabled",true);
        } else{
            $elem.removeAttr("disabled");
        }
        if(handleOnSuccess){
            handleOnSuccess();
        }
    } catch(e){
        ShowExceptionMessage("_preventSourceElement", e);
    }
}

/******************************************************************************************/
/********************************* Main Cart Screen  - End ******************************/
/******************************************************************************************/

/******************************************************************************************/
/********************************* Application Level  - End **********************************/
/******************************************************************************************/