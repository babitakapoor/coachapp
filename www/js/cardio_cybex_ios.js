
/* Cybex Cardio Methods - Start */
var EQUIPMENT_FAMILY_NAME = {
    "-1": "Not Connected",
    "1": "Treadmill",
    "2": "ArcTrainer",
    "3": "Bike",
    "0": "Unknown"
};

var EQUIPMENT_STATE = {
    "1": "Active",
    "2": "Bootloader",
    "3": "Dormant",
    "4": "Error",
    "5": "Going Active",
    "6": "Leaving Active",
    "7": "Paused",
    "8": "Power Down",
    "9": "Power Up",
    "10": "Review",
    "11": "Toolbox",
    "12": "Wake Up",
    "13": "CSAFE Wake Up",
    "0": "Unknown"
};

var WORKOUT_STATE = {
    "1": "Started",
    "2": "Paused",
    "3": "Stopped",
    "4": "Unknown"
};

function InitializeCybexmachine(SuccessHandler) {
    /**
     * @param window.plugins.cybex
     * @param window.plugins.cybex.initializecybex
     */
    window.plugins.cybex.initializecybex([10], function(msg) {
        if (SuccessHandler){
            SuccessHandler(msg);
        }
        logMessage(msg);
        //logMessage("Success");
    }, function() {
        logMessage("Failue");
    });
}

function getCurrentTargetWatt(SuccessHandler) {
    /**
     * @param window.plugins.cybex
     * @param window.plugins.cybex.getCurrentTargetWatt
     */
    window.plugins.cybex.getCurrentTargetWatt([], function(watt) {
        if (SuccessHandler) {
            SuccessHandler(watt);
        }
    }, function() {
        logMessage("Failue");
    });
}

function setCurrentTargetWatt(watt) {
    //var wattarr = [];
    //wattarr['WATT'] = parseInt(watt);
    /**
     * @param window.plugins.cybex
     * @param window.plugins.cybex.setCurrentTargetWatt
     */
    window.plugins.cybex.setCurrentTargetWatt(watt, function() {

        logMessage("setCurrentTargetWatt Success");
    }, function() {
        logMessage("Failue");
    });
}

function getCurrentTargetRPM(SuccessHandler) {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.getCurrentTargetRPM() This is cybex getCurrentTargetRPM function
     */
    window.plugins.cybex.getCurrentTargetRPM(function(rpm) {
        if (SuccessHandler) {
            SuccessHandler(rpm);
        }
    }, function() {
        logMessage("Failue");
    });
}

function setMachinetoConstantPowerMode(SuccessHandler) {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.setToConstantPowerMode() This is cybex setToConstantPowerMode function
     */
    window.plugins.cybex.setToConstantPowerMode(function() {
        if (SuccessHandler) {
            SuccessHandler();
        }
        logMessage("setMachinetoConstantPowerMode");
    }, function() {
        logMessage("Failue");
    });
}

function getConnectedMachineName(SuccessHandler) {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.getCurrentMachineName() This is cybex getCurrentMachineName function
     */
    window.plugins.cybex.getCurrentMachineName(function(machinename) {
        if (SuccessHandler) {
            SuccessHandler(machinename);
        }
    }, function() {
        logMessage("Failue");
    });
}

function getConnectedMachineFamily(SuccessHandler) {
    /**
     * @param window.plugins
     * @param window.plugins.cybex This is cybex
     * @param window.plugins.cybex.getCurrentMachineFamily() This is cybex getCurrentMachineFamily function
     */
    window.plugins.cybex.getCurrentMachineFamily(function(machinefamily) {
        if (SuccessHandler) {
            SuccessHandler(machinefamily);
        }
    }, function() {
        logMessage("Failue");
    });
}

function startWorkout(SuccessHandler) {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.startWorkout() This is cybex startWorkout
     */
    window.plugins.cybex.startWorkout(function() {
        if (SuccessHandler) {
            SuccessHandler();
        }
        logMessage("startWorkout");
    }, function() {
        logMessage("Failue");
    });
}

function stopWorkout() {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.stopworkout() This is cybex stopworkout function
     */
    window.plugins.cybex.stopworkout(function() {

        logMessage("stopworkout");
    }, function() {
        logMessage("Failue");
    });
}

function coolWorkout() {
    /**
     * @param  window.plugins
     * @param  window.plugins.cybex This is cybex
     * @param  window.plugins.cybex.cooldown() This is cybex cooldown function
     */
    window.plugins.cybex.cooldown(function() {

        logMessage("coolWorkout");
    }, function() {
        logMessage("Failue");
    });
}

function Elapsedtimecallback(time) {
    logMessage('Elapsedtimecallback:' + time)
}


function EquipmentStateChangeCallback(state) {
    logMessage('EquipmentStateChangeCallback ' + state);
}

function WorkoutStateChangeCallback(state) {
    logMessage('WorkoutStateChangeCallback ' + state);
}

function updateMatrixdataCallback(matrixdata) {
    logMessage('updateMatrixdataCallback' + matrixdata);
    var matrixdataobj = {};
    try{
        var mtrxdata = ReplaceAllString(matrixdata, '~', '"');
        //logMessage('updateMatrixdataCallback' + mtrxdata);
        matrixdataobj = JSON.parse(mtrxdata);
    }catch(e){
        matrixdataobj.error = e;
    }
    if (CallBackFromCardioMachineMatrixdata){
        CallBackFromCardioMachineMatrixdata(matrixdataobj);
    }/*{elapsedtime: '%@', calories: '%d', caloriesPerHour: '%d', mets: '%.1f', watts: '%d', averageWatts: '%d', distanceInmiles: '%.2f', bpm: '%d', peakBpm: '%d', secondsInHeartRateZone: '%d', speedInMil: '%.1f', avgspeedInmiles: '%.1f', paceInSec: '%d:%02d', avgPaceInSec: '%d:%02d', currentStridesPerMinute: '%d', averageStridesPerMinute: '%d', currentTargetSpm: '%d', currentCrankRPM: '%d', averageCrankRPM: '%d'}
     */
}

function EquipmentDidAttach() {
    logMessage('EquipmentDidAttach');
    if (OnEquipmentAttachSuccess){
        OnEquipmentAttachSuccess();
    }
}


function logMessage(msg){
    //$("#idcybexlog").append(msg+'<br>');
    //XX console.log("MsgFrom:" + msg);
}
/* Cybex Cardio Methods - End */