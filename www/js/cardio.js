var SPEED_LEVEL = {A:3, B:4, C:5, D:6, E:7, F:8};
var cardioobj = {
    _HRData: [],
    _VS_: 0,
    _AppearType_: _C_taTestData,
    _ZoomBg_: 0,
    _ZoomEnd_: 0,
    _ZoomFlag_: false,
    _Watt_Max: 0,
    _HR_Max: 0,
    _HR_Min: 0,
    _SM_Max: 0,
    _SM_Min: 0,
    _VertLines_: [],
    _IntervalVertLines_: [],
    _VArray_: [],
    _ANTI_: 0,
    _ANTI0_: 0,
    _bGraphAltered: false,
    _bPageChanged: false,
    _TempDefPoint: [],
    _DefPoint0: [],
    _TrainZone_: {},
    _TrimBold_: 0,
    _TrimJust_: 0,
    CardioCalc: function(HRData) {
        if (HRData) {
            this._HRData = HRData;
        }
        if (_ClientData_g.TestStatus_ < _C_tsToAnalyze || _ClientData_g.TestMachine_ == _C_tmManual) {
            return;
        }
        this.CalcHR();
        if (_ClientData_g.TestStatus_ == _C_tsToAnalyze) {
            this.CalcSMGPoints();
            this.SMGMaxMin();
            this.Treashold();
            this.AdjustDeflPoint();
            this.GetTrainZone(_ClientData_g.SPMed_.TrainType_);
        } else {
            this.GetSMGParams();
            this.CalcSMGPoints();
            this.SMGMaxMin();
            this.Treashold();
        }
        this.DefRegrLines();
    },
    CalcHR: function(AppearType) {
        if (AppearType) {
            this._AppearType_ = AppearType;
        }
        switch (_ClientData_g.TestMachine_) {
            case _C_tmCycle:
                this._VS_ = _C_StartSpeedBike[_ClientData_g.TestStart_ - 1] + 2;
                break;
            case _C_tmThreadmil:
                this._VS_ = _C_StartSpeedThreadmill[_ClientData_g.TestStart_ - 1] + 2;
                break;
            case _C_tmField, _C_tmSpecCycle:
                this._VS_ = 0;
                break;
            default:
                ;
        }
        this.HRMaxMin();
        this.SpeedAdaptLines(this._VS_, _ClientData_g.TestMachine_);
    },
    HRMaxMin: function() {
        var Start = 0;
        var Fin = _ClientData_g.CardioTest_.getHRCount();
        if (this._AppearType_ == _C_taTestData) {
            if (this._ZoomFlag_) {
                Start = this._ZoomBg_ + _ClientData_g.CardioTest_.DataBg_;
                Fin = this._ZoomEnd_ + _ClientData_g.CardioTest_.DataBg_;
            } else {
                Start = _ClientData_g.CardioTest_.DataBg_;
                Fin = _ClientData_g.CardioTest_.CoolDnBg_;
            }
        }
        var phr = _ClientData_g.CardioTest_.HRData_;
        this._HR_Max = phr[Start];
        this._HR_Min = phr[Start];
        for (var i = Start; i < Fin - 1; i++) {
            if (parseInt(phr[i]) >= this._HR_Max) {
                this._HR_Max = parseInt(phr[i]);
            } else {
                if (parseInt(phr[i]) <= this._HR_Min) {
                    this._HR_Min = parseInt(phr[i]);
                }
            }
        }
    },
    SpeedAdaptLines: function(startspeed, TestMachine) {
        this._VertLines_[0] = 0;
        var i = 0;
        var maxspeed = 0;
        var testtime = _ClientData_g.CardioTest_.getTestTime();
        if (TestMachine <= _C_tmThreadmil) {
            var intlength = 12;
            if (startspeed == 0) {
                return;
            }
            this._VArray_[0] = this._VS_;
            while (testtime > this._VertLines_[i]) {
                var verttimespeed = this._VertLines_[i] + intlength / startspeed;
                if (verttimespeed < testtime) {
                    this._VertLines_[i + 1] = this._VertLines_[i] + intlength / startspeed;
                    this._IntervalVertLines_[i] = intlength / startspeed;
                    startspeed = startspeed + 0.5;
                    this._VArray_[i] = this._VS_ + i * 0.5;
                    maxspeed = this._VArray_[i];
                } else {
                    break;
                }
                i = i + 1;
                if (i >= 100) {
                    break;
                }
            }
        }
        this._Watt_Max = maxspeed;
    },
    CalcSMGPoints: function() {
        var MaxSMGNum = this._VArray_.length;
        if (MaxSMGNum < 2) {
            return false;
        }
        var iMax1 = MaxSMGNum - 1;
        if (iMax1 >= 100) {
            iMax1 = 99;
        }
        for (var i = 0; i <= iMax1; i++) {
            var PointIndex1 = DelphiRound(this._VertLines_[i + 1] * _C_SampleRate_);
            var NUsrPoint = DelphiRound(this._IntervalVertLines_[i] * _C_SampleRate_ / 5);
            var SMGval = this.Avr(PointIndex1, NUsrPoint);
            var SMGdata = {
                X: this._VArray_[i],
                Y: SMGval
            };
            _ClientData_g.SPMed_.SPM_.push({
                SMG: SMGdata,
                Status: true
            });
        }
        return true;
		
    },
    Avr: function(NP, NUsr) {
        var Sum = 0;
        var IntNP = NP - NUsr + 1;
        for (var i = NP; i >= IntNP; i--) {
            Sum = Sum + parseFloat(_ClientData_g.CardioTest_.HRData_[i + _ClientData_g.CardioTest_.DataBg_]);
        }
        if (NUsr == 0) {
            NUsr = 1;
        }
        Avrg = DelphiRound(Sum / NUsr);
        if (Avrg != 0) {
            return Avrg;
        }
        return 0;
    },
    SMGMaxMin: function() {
        this._SM_Max = _ClientData_g.SPMed_.SPM_[0].SMG.Y;
        this._SM_Min = _ClientData_g.SPMed_.SPM_[0].SMG.Y;

        for (var ii = 0; ii <= _ClientData_g.SPMed_.SPM_.length - 1; ii++) {
            if (_ClientData_g.SPMed_.SPM_[ii].SMG.Y >= this._SM_Max) {
                this._SM_Max = _ClientData_g.SPMed_.SPM_[ii].SMG.Y;
            } else {
                if (_ClientData_g.SPMed_.SPM_[ii].SMG.Y <= this._SM_Min) {
                    this._SM_Min = _ClientData_g.SPMed_.SPM_[ii].SMG.Y;
                }
            }
        }
    },
    Treashold: function() {
        this.DefANT();
        this._ANTI_ = this._ANTI0_ + this._TrimBold_;
        if (_ClientData_g.TestStatus_ >= _C_tsAnalyzed) {
            return;
        }
        this.CalcRegressionLines();
    },
    DefANT: function() {
        var HR_93 = this._SM_Max * 0.93;
        var HR_90 = this._SM_Max * 0.9 - 0.2;
        this._ANTI_ = 0;
        var imx = _ClientData_g.SPMed_.SPM_.length - 1;
        for (var ii = imx; ii >= 0; ii--) {
            if (_ClientData_g.SPMed_.SPM_[ii].SMG.Y >= HR_90 && _ClientData_g.SPMed_.SPM_[ii].SMG.Y <= HR_93) {
                this._ANTI_ = ii;
                break;
            }
        }
        if (this._ANTI_ == 0) {
            var Nearest = 10000;
            var Delta = 0;
            this._ANTI_ = imx;
            for (var i = imx; i >= 0; i--) {
                Delta = this.Abs(_ClientData_g.SPMed_.SPM_[i].SMG.Y - HR_93);
                if (Delta < Nearest) {
                    Nearest = Delta;
                    this._ANTI_ = i;
                }
            }
        }
        this._ANTI0_ = this._ANTI_;
    },
    Sqrt: function(givendata) {
        return Math.sqrt(givendata);
    },
    Abs: function(givendata) {
        if (givendata < 0) {
            return givendata * -1;
        }
        return givendata;
    },
    CalcRegressionLines: function(aValue) {
        if (!aValue) {
            aValue = 0;
        }
        var count = 0;
        for (var i = 1; i <= this._ANTI_; i++) {
            if (_ClientData_g.SPMed_.SPM_[i - 1].Status) {
                count++;
            }
        }
        if (count > 1) {
            _ClientData_g.SPMed_.Regr1_ = this.Regression(1, this._ANTI_ + 1, this._ANTI_ + 1, 0);
        }
        count = 0;
        for (var i = this._ANTI_ + 2; i <= _ClientData_g.SPMed_.SPM_.length; i++) {
            if (_ClientData_g.SPMed_.SPM_[i - 1].Status) {
                count++;
            }
        }
        if (count < 2 || this._ANTI_ >= _ClientData_g.SPMed_.SPM_.length - 2) {
            _ClientData_g.SPMed_.Regr2_.B0 = 0;
            _ClientData_g.SPMed_.Regr2_.B1 = 0;
        } else {
            _ClientData_g.SPMed_.Regr2_ = this.Regression(this._ANTI_ + 1, _ClientData_g.SPMed_.SPM_.length, this._ANTI_ + 1, 0);
        }
        if (aValue != 1) {
            _ClientData_g.SPMed_._DeflPoint_ = this.SetCorrectPoint();
        } else {
            _ClientData_g.SPMed_._DeflPoint_ = this.SetCorrectPoint(this._ANTI_);
        }
    },
    NewCalcRegressionLines: function() {
        var count = 0;
        for (var i = 1; i <= this._ANTI_; i++) {
            if (_ClientData_g.SPMed_.SPM_[i - 1].Status) {
                count++;
            }
        }
        if (count > 1) {
            _ClientData_g.SPMed_.Regr1_ = this.Regression(1, this._ANTI_ + 1, this._ANTI_ + 1, 0);
        }
        count = 0;
        for (var i = this._ANTI_ + 2; i <= _ClientData_g.SPMed_.SPM_.length; i++) {
            if (_ClientData_g.SPMed_.SPM_[i - 1].Status) {
                count++;
            }
        }
        if (count < 2 || this._ANTI_ >= _ClientData_g.SPMed_.SPM_.length - 2) {
            _ClientData_g.SPMed_.Regr2_.B0 = 0;
            _ClientData_g.SPMed_.Regr2_.B1 = 0;
        } else {
            _ClientData_g.SPMed_.Regr2_ = this.Regression(this._ANTI_ + 1, _ClientData_g.SPMed_.SPM_.length, this._ANTI_ + 1, 0);
        }
    },
    SetCorrectPoint: function(aANT) {
        if (!aANT) {
            aANT = 0;
        }
        var DefPoint = {};
        if (aANT > 0) {
            if (this._bGraphAltered && this._bPageChanged) {
                DefPoint.X = this._TempDefPoint.X;
                DefPoint.Y = this._TempDefPoint.Y;
            } else {
                DefPoint.X = this._VArray_[aANT];
                DefPoint.Y = _ClientData_g.SPMed_.SPM_[aANT].SMG.Y;
                this._DefPoint0 = jQuery.extend(true, {}, DefPoint);
            }
            return DefPoint;
        }
        if (this._bGraphAltered && this._bPageChanged) {
            DefPoint.X = this._TempDefPoint.X;
            DefPoint.Y = this._TempDefPoint.Y;
        } else {
            DefPoint.X = this._VArray_[this._ANTI_];
            DefPoint.Y = _ClientData_g.SPMed_.SPM_[this._ANTI_].SMG.Y;
            this._TempDefPoint.Y = DefPoint.Y;
            this._TempDefPoint.X = DefPoint.X;
            this._DefPoint0 = jQuery.extend(true, {}, DefPoint);
        }
        return DefPoint;
    },
    Regression: function(Min, Max, iDefl, Weight) {
        var i, j, k, SumX, SumY, SumXY, SumX2, SumY2, SumXSumY, SqX, SqY, Znam, R1X, R1Y, B1, B0, R1, R, NPoints;
        var X = [];
        var Y = [];
        var Param = {};
        B1 = 0;
        SumX = 0;
        SumY = 0;
        SumXY = 0;
        SumX2 = 0;
        SumY2 = 0;
        NPoints = Max - Min + 1;
        k = 1;
        for (var i = Min; i <= Max; i++) {
            if (_ClientData_g.SPMed_.SPM_[i - 1].Status) {
                X[k] = _ClientData_g.SPMed_.SPM_[i - 1].SMG.X;
                Y[k] = _ClientData_g.SPMed_.SPM_[i - 1].SMG.Y;
                k++;
                if (iDefl = i) {
                    for (var j = 2; j <= Weight; j++) {
                        X[k] = _ClientData_g.SPMed_.SPM_[i - 1].SMG.X;
                        Y[k] = _ClientData_g.SPMed_.SPM_[i - 1].SMG.Y;
                        k++;
                        NPoints++;
                    }
                }
            } else {
                NPoints = NPoints - 1;
            }
        }
        for (var k = 1; k <= NPoints; k++) {
            SumX = SumX + X[k];
            SumY = SumY + Y[k];
            SumXY = SumXY + X[k] * Y[k];
            SumX2 = SumX2 + X[k] * X[k];
            SumY2 = SumY2 + Y[k] * Y[k];
        }
        SumXSumY = SumX * SumY;
        SqX = SumX * SumX;
        SqY = SumY * SumY;
        Znam = SqX - NPoints * SumX2;
        if (Znam != 0) {
            B1 = (SumXSumY - NPoints * SumXY) / Znam;
        }
        B0 = (SumY - B1 * SumX) / NPoints;
        R1X = this.Sqrt(this.Abs(SumX2 - SqX / NPoints));
        R1Y = this.Sqrt(this.Abs(SumY2 - SqY / NPoints));
        R1 = R1X * R1Y;
        if (R1 > 0.000001) {
            R = (SumXY - SumXSumY / NPoints) / R1;
        } else {
            R = 1;
        }
        Param.B0 = B0;
        Param.B1 = B1;
        Param.R = R;
        return Param;
    },
    AdjustDeflPoint: function() {
        var CrossY, Delta, NAnt;
        NAnt = this._ANTI0_;
        var HRCriteria_g = 0;
        for (var i = this._ANTI0_ + 2; i <= _ClientData_g.SPMed_.SPM_.length - 1; i++) {
            if (_ClientData_g.SPMed_.SPM_.length > 0) {
                CrossY = this.DefPulse1(_ClientData_g.SPMed_.SPM_[i - 1].SMG.X);
                Delta = CrossY - _ClientData_g.SPMed_.SPM_[i - 1].SMG.Y;
            }
            if (this.Abs(Delta) <= HRCriteria_g) {
                NAnt++;
            }
        }
        this._ANTI_ = NAnt;
        if (this._ANTI0_ != NAnt) {
            this._ANTI0_ = NAnt;
            this.CalcRegressionLines();
        }
    },
    DefPulse: function(Resistance) {
        var Result = DelphiRound(_ClientData_g.SPMed_.Regr1_.B1 * Resistance + _ClientData_g.SPMed_.Regr1_.B0);
        _ClientData_g.WriteRegFactor(_ClientData_g.SPMed_.Regr1_.B1, _ClientData_g.SPMed_.Regr1_.B0);
        return Result;
    },
    DefPulse1: function(Resistance) {
        var Result = _ClientData_g.SPMed_.Regr1_.B1 * Resistance + _ClientData_g.SPMed_.Regr1_.B0;
        _ClientData_g.WriteRegFactor(_ClientData_g.SPMed_.Regr1_.B1, _ClientData_g.SPMed_.Regr1_.B0);
        return Result;
    },
    GetTrainZone: function(TrainMode) {
        var ANT_90, ANT_85, ANT_70, ZoneMin, ZoneMax;
        ANT_70 = _ClientData_g.SPMed_._DeflPoint_.X * 0.7;
        ANT_85 = _ClientData_g.SPMed_._DeflPoint_.X * 0.85;
        ANT_90 = _ClientData_g.SPMed_._DeflPoint_.X * 0.9;
        ZoneMin = this.DefPulse(ANT_70);
        this._TrainZone_.Min = {};
        this._TrainZone_.Max = {};
        if (TrainMode = _C_Cond_Train) {
            ZoneMax = this.DefPulse(ANT_90);
        } else {
            ZoneMax = this.DefPulse(ANT_85);
        }
        this._TrainZone_.Min.X = ANT_70;
        this._TrainZone_.Min.Y = ZoneMin;
        if (TrainMode = _C_Cond_Train) {
            this._TrainZone_.Max.X = ANT_90;
        } else {
            this._TrainZone_.Max.X = ANT_85;
        }
        this._TrainZone_.Max.Y = ZoneMax;
    },
    GetSMGParams: function() {
        var MaxSMGNum = _ClientData_g.SPMed_.SPM_.length;
        if (MaxSMGNum > 0) {
            for (var i = 0; i <= MaxSMGNum; i++) {}
        }
    },
    DefRegrLines: function() {
        if (_ClientData_g.SPMed_.SPM_.length == 0) {
            return;
        }
        _ClientData_g.SPMed_.RLP1.X = this._VS_ - 0.5;
        _ClientData_g.SPMed_.RLP1.Y = DelphiRound(_ClientData_g.SPMed_.Regr1_.B1 * _ClientData_g.SPMed_.RLP1.X + _ClientData_g.SPMed_.Regr1_.B0);
        _ClientData_g.SPMed_.RLP2.X = _ClientData_g.SPMed_._DeflPoint_.X + 2;
        _ClientData_g.SPMed_.RLP2.Y = DelphiRound(_ClientData_g.SPMed_.Regr1_.B1 * _ClientData_g.SPMed_.RLP2.X + _ClientData_g.SPMed_.Regr1_.B0);
        _ClientData_g.SPMed_.RLP3.X = _ClientData_g.SPMed_._DeflPoint_.X - 1;
        _ClientData_g.SPMed_.RLP3.Y = DelphiRound(_ClientData_g.SPMed_.Regr2_.B1 * _ClientData_g.SPMed_.RLP3.X + _ClientData_g.SPMed_.Regr2_.B0);
        _ClientData_g.SPMed_.RLP4.X = this._VArray_[this._VArray_.length - 1];
        _ClientData_g.SPMed_.RLP4.Y = DelphiRound(_ClientData_g.SPMed_.Regr2_.B1 * _ClientData_g.SPMed_.RLP4.X + _ClientData_g.SPMed_.Regr2_.B0);
    },
    GetANT_HR: function(aANT) {
        var PX1 = 0;
        var PX2 = 0;
        var MaxSMGNum = _ClientData_g.SPMed_.SPM_.length;
        var ResultPoint = {
            X: 0,
            Y: 0
        };
        if (MaxSMGNum > 0) {
            var Defpoint = _ClientData_g.SPMed_._DeflPoint_;
            if (this._bGraphAltered && this._bPageChanged) {
                Defpoint = this._TempDefPoint;
            }
            for (var incloop = 0; incloop < MaxSMGNum; incloop++) {
                var nextspeed = this._VArray_[incloop + 1] ? this._VArray_[incloop + 1] : 0;
                if (Defpoint.X >= this._VArray_[incloop] && Defpoint.X <= nextspeed) {
                    PX1 = incloop + 1;
                    break;
                } else {
                    PX1 = MaxSMGNum - 1;
                }
            }
            if (this._VArray_[incloop + 1] - this._VArray_[incloop] != 0) {
                PX2 = (Defpoint.X - this._VArray_[incloop]) / (this._VArray_[incloop + 1] - this._VArray_[incloop]);
            } else {
                PX2 = 0;
            }
            var warmuptime = _ClientData_g.CardioTest_.getWarmUpTimeForAppearType(this._AppearType_);
            ResultPoint.X = this._VertLines_[PX1] + PX2 * (this._VertLines_[PX1 + 1] - this._VertLines_[PX1]) - this._ZoomBg_ / _C_SampleRate_ + warmuptime;
            ResultPoint.Y = Defpoint.Y;
        }
        return ResultPoint;
    },
    getFitnesslevel: function(successhander) {
        getFitnesslevelforAgeSexAndSpeed(_ClientData_g.age_, _ClientData_g.gender_, _ClientData_g.SPMed_._DeflPoint_.X, successhander);
    },
    get12WeeksTrainingPlan: function(fitlevel, successhander) {
        get12WeeksTrainingPlanforAgeSexAndFitlevel(_ClientData_g.age_, _ClientData_g.gender_, fitlevel, successhander);
    }
};

function DelphiRound(data) {
    var wholeno = parseInt(data);
    var deci = data % 1;
    if (wholeno % 2 == 0) {
        if (deci == 0.5) {
            return wholeno;
        }
    }
    return Math.round(data);
}

function getWattFromSpeed(speed, weight) {
    return Math.round(((speed * 2.98 + 4.25) * 0.9 * weight - 320) / 11.35, 0);
}

function getSpeedFromWatt(watt, weight) {
    var nweight = weight > 0 ? weight : 1;
    return ((watt * 11.35 + 320) / 0.9 / nweight - 4.25) / 2.98;
}

function getTimeForSpeed(speed) {
    return DelphiRound(200 / (speed * 1000) * 60 * 60);
}

function CalculateWATTDeflectionPointForActivity(aWattIant) {
    return DelphiRound(((aWattIant * 11.35 + 320) / 0.9 - 320) / 11.35);
}

function getLoadValueForSpeed(startspeed, testweight) {
    var endspeed = startspeed + 25;
    var loadobj = [];
    var starttime = 0;
    for (i = startspeed; i <= endspeed; i = i + 0.5) {
        var newload = getWattFromSpeed(i, testweight);
        if (i == startspeed) {
            var warmup1 = newload * 0.7;
            var warmuptimediff1 = 60;
            starttime += warmuptimediff1;
            loadobj.push({
                speed: i,
                time: starttime,
                timediff: warmuptimediff1,
                load: Math.round(warmup1),
                loadtype: 1
            });
            var warmup2 = newload * 0.8;
            var warmuptimediff2 = 60;
            starttime += warmuptimediff2;
            loadobj.push({
                speed: i,
                time: starttime,
                timediff: warmuptimediff2,
                load: Math.round(warmup2),
                loadtype: 1
            });
            var warmup3 = newload * 0.9;
            var warmuptimediff3 = 30;
            starttime += warmuptimediff3;
            loadobj.push({
                speed: i,
                time: starttime,
                timediff: warmuptimediff3,
                load: Math.round(warmup3),
                loadtype: 1
            });
        }
        var timediff = getTimeForSpeed(i);
        starttime += timediff;
        loadobj.push({
            speed: i,
            time: starttime,
            timediff: timediff,
            load: Math.round(newload),
            loadtype: 2
        });
    }
    return loadobj;
}

function getFitnesslevelforAgeSexAndSpeed(age, gender, speed, successhander) {
    if (do_service) {
        do_service({
            action: "getfitlevelforagesexspeed",
            p: "members",
            age: age,
            gender: gender,
            speed: speed
        }, {}, function(response) {
            if (response) {
                if (response.fitdata) {
                    if (successhander) {
                        successhander(response.fitdata);
                    }
                }
            }
        }, "json");
    } else {
        console.log("Service method could not be found");
    }
}

function get12WeeksTrainingPlanforAgeSexAndFitlevel(age, gender, fitlevel, successhander) {
    if (do_service) {
        do_service({
            action: "get12weekstraninigplanfor_age_sex_fitlevel",
            p: "members",
            age: age,
            gender: gender,
            fitlevel: fitlevel
        }, {}, function(response) {
            if (response) {
                if (response.fitpointdata) {
                    if (successhander) {
                        successhander(response.fitpointdata);
                    }
                }
            }
        }, "json");
    } else {
        console.log("Service method could not be found");
    }
}

function getpointsper_activity_for_testid_iantw(testid, IANTW, successhander) {
    if (do_service) {
        var IANTWval = CalculateWATTDeflectionPointForActivity(IANTW);
        do_service({
            action: "getpointsper_activity_for_testid_iantw",
            p: "members",
            userTestID: testid,
            wattdiflperactivty: IANTWval
        }, {}, function(response) {
            if (response) {
                if (response.creditperactivity) {
                    var creditobjarr = [];
                    $.each(response.creditperactivity, function(idx, creditobj) {
                        var min = creditobj.hrzone.min ? creditobj.hrzone.min : 0;
                        var max = creditobj.hrzone.max ? creditobj.hrzone.max : 0;
                        var reg1obj = creditobj.Regr1 ? JSON.parse(creditobj.Regr1) : {};
                        var B0 = reg1obj.B0 ? reg1obj.B0 : 0;
                        var B1 = reg1obj.B1 ? reg1obj.B1 : 0;
                        var weight = creditobj.weight ? creditobj.weight : 0;
                        var newmin = DelphiRound(B1 * (((min * 11.35 + 320) / (0.9 * weight) - 4.25) / 2.98) + B0);
                        var newmax = DelphiRound(B1 * (((max * 11.35 + 320) / (0.9 * weight) - 4.25) / 2.98) + B0);
                        if (newmax + newmin >= 10) {
                            if (newmax - newmin < 10) {
                                newmin = newmax - 10;
                            }
                        }
                        creditobj.hrzone.min = newmin;
                        creditobj.hrzone.max = newmax;
                        creditobj.Regr1 = "";
                        creditobjarr.push(creditobj);
                    });
                    if (successhander) {
                        successhander(creditobjarr);
                    }
                }
            }
        }, "json");
    } else {
        console.log("Service method could not be found");
    }
}