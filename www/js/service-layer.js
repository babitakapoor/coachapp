//*** [SK] Comments ***//
//*** validate coach/client login details ***//
function doLoginLogoutUser(username,password,onSucess){
    logStatus("Calling doLoginLogoutUser", LOG_FUNCTIONS);
    try{
        /*$('.errortext').html('');
        //var user=	getCurrentUser();
        if(validateSteps('#loginform1')) {
            DisableProcessBtn("#idbuttonlogin", false);*/
            //do_service({method:'authenticate','p':'login'},{username:username,password:password,usertype:APP_USERTYPE},function(response){,usertype:APP_USERTYPE
            do_service({method:'authenticate','p':'login'},{username:username,password:password},function(response){
				console.log('Hello New One Iswe');
				console.log(response);
				$('#coach_id').val(response.user.user_id);
                /*DisableProcessBtn("#idbuttonlogin", true,GetLanguageText(BTN_SUBMIT));
                if(response.status_code==1){
                    updateAppLogin(response);
                } else {
                    $('.loginmessage').find('label').html(GetLanguageText(LOGIN_USERNAMEPASSWORD_INCORRECT));
                }*/
                if(onSucess){
                    onSucess(response);
                }
            },'json');
        /*} else {
            $('.loginmessage').find('label').html(GetLanguageText(USERNAMEPASSWORD_INCORRECT));
        }*/
    } catch(e) {
        ShowExceptionMessage("doLoginLogoutUser", e);
    }
}
//*** [SK] Comments ***//
//** On successfull Login, Associated Club details display for dropdown ***//
function _appendClubName(){
    logStatus("Calling _appendClubName",LOG_FUNCTIONS);
    try{
        /*MK Changed club ID */
        var clubid =	getselectedclubid();
        $('#users_club').val(clubid);
    } catch(e) {
        ShowExceptionMessage("_appendClubName", e);
    }

}
//*** [SK] comments by shanethatech ***//
//*** Logout coach Application***//
/*
function doLogoutUser(){
    logStatus("Calling doLogoutUser",LOG_FUNCTIONS);
    try{
        do_service({method:'logout','p':'login'},{},function(response){
            _movePageTo('login');
        },'json');
    } catch(e) {
        ShowExceptionMessage("doLogoutUser", e);
    }
}
*/
/*Changes Date : 04-04-17*/
function getManageMembersData(action,_element,tab,pageindex,searchValue,searchType){
    logStatus("Calling getManageMembersData ", LOG_COMMON_FUNCTIONS);
    try{
         $('.tabsbox').on('click','span',function(){
             $('.tabsbox > span').removeClass("active");
             $(this).addClass("active");
         });
        var _data	=	{};
        var _datapost	=	{};
        var selectedClub= getselectedclubid();
        _data.action=	action;
        _data.p		=	'members';
		_data.searchValue=	searchValue;
		if(!searchType || searchType=="undefined"){
			_data.searchType="";
		}
		else{
			_data.searchType=searchType;
		}
		
        if(tab){
            _data.tab	=	tab;
        }
        _data.userid = getCurrentLoggedinUserId();
        if(pageindex){
            _datapost.page=pageindex;
        }
        if(selectedClub){
            _data.clubId=selectedClub;
        }
        do_service(_data,_datapost,function(respsonse){
            /*MK - SK Uncommented - Due to need of display the records in the member list - */
            $(_element).find('tbody').html(respsonse);
            //XX Commented for now Needs to be uncommented $(_element).find('tbody').html(respsonse);
            _appendClubName();
        });
    } catch(e) {
        ShowExceptionMessage("getManageMembersData", e);
    }
}
 /*End*/
/*SN added 20160129*/
function _getManageMemberTestData(action,_element,tab,pageindex,onSucess){
    logStatus("Calling _getManageMemberTestData", LOG_COMMON_FUNCTIONS);
    logStatus(_element, LOG_DEBUG);
    try{
        var _data	=	{};
        var _datapost	=	{};
        var selectedClub	=	$("#users_club").val();
        _data.action=	action;
        _data.p		=	'members';
        if(tab){
            _data.tab	=	tab;
        }
        _data.userid = getCurrentLoggedinUserId();
        if(pageindex){
            _datapost.page=pageindex;
        }
        if(selectedClub){
            _data.clubId=selectedClub;
        }
        do_service(_data,_datapost,function(response){
            if(onSucess){
                onSucess(response);
            }
        });
    }catch(e){
        ShowExceptionMessage("_getManageMemberTestData", e);
    }
}
/*SN added bodycompostion graph*/
function getManageBodycompositionGraph(action,_element,tab,pageindex,onSucess){
    logStatus("Calling getManageBodycompositionGraph", LOG_COMMON_FUNCTIONS);
    logStatus(_element, LOG_DEBUG);
    try{
        /* $('.tabsbox').on('click','span',function(){
             $('.tabsbox > span').removeClass("active");
             $(this).addClass("active");
         });*/
        var _data	=	{};
        var _datapost	=	{};
        var selectedClub	=	$("#users_club").val();
        _data.action=	action;
        _data.p		=	'members';
        if(tab){
            _data.tab	=	tab;
        }
        _data.userid = getCurrentLoggedinUserId();
        if(pageindex){
            _datapost.page=pageindex;
        }
        if(selectedClub){
            _data.clubId=selectedClub;
        }
        /**
         * @param   respsonse.dateGraph This is  date graph
         * @param   respsonse.arraySeries This is  array series
         * @param   respsonse.weightKgGraph This is  weight kg graph
         */
        do_service(_data,_datapost,function(respsonse){
            if(onSucess){
                onSucess(respsonse);
            }
            //$('#graphcnt').html(graphcnt);
        },'json');
    }catch(e){
        ShowExceptionMessage("getManageBodycompositionGraph", e);
    }
}

//*** [SK] comments by shanethatech ***//
//*** Display Serach Result by using first,last name , by gender, userid ***//
function _getSearchResult(action,_element,searchValue,userid,tabidx,filterby,onSucess){
    logStatus("Calling _getSearchResult", LOG_COMMON_FUNCTIONS);
    logStatus(_element, LOG_DEBUG);
    try {
        //var searchValue	=	$("#searchinputvalue").val();
		
        var clubid	 	=  getselectedclubid();
		
        //var filterby = $('#selecrfiltervalue').val();
        //var userid = getCurrentLoggedinUserId();
        //Need to set the post page index
        // var tabidx = '';
        // var $activetabobj = $(".clsmanagememtbs .clsmngmemtab.active");
        // if ($activetabobj){
            // tabidx = $activetabobj.attr("serchlist");
        // }
		
        do_service({action:action,p:'members',searchType:filterby,searchValue:searchValue,clubId:clubid,tab:tabidx,userid:userid},{},function(response){

            //$(_element).find('tbody').html(respsonse);
            if(onSucess){
                onSucess(response);
            }
        });
    } catch(e) {
        ShowExceptionMessage("_getSearchResult", e);
    }
}
/* search Tab for new machines */
function getSearchResultexercisedata(action,_element,clubid,userid,searchtype,tab,searchValue,onSucess){
    logStatus("Calling getSearchResultexercisedata", LOG_COMMON_FUNCTIONS);
    logStatus(userid, LOG_DEBUG);
    logStatus(_element, LOG_DEBUG);
    try {
        //var searchValue	=	$("#selecrfilterdatavalue").val();
        //var clubid	 	=  clubid;
        //var userid = getCurrentLoggedinUserId();
        //Need to set the post page for tab index
        var tabidx = (tab) ? tab:'manage_machine_list';
        do_service({action:action,p:'members',searchType:searchtype,searchValue:searchValue,clubId:clubid,tab:tabidx,userid:getCurrentLoggedinUserId()},{},function(response){
            //$(_element).find('tbody').html(respsonse);
            if(onSucess){
                onSucess(response);
            }
        });
    } catch(e) {
        ShowExceptionMessage("getSearchResultexercisedata", e);
    }
}
//*** [SK] comments by shanethatech ***//
//*** Get Details of select client Detail Information ***//
function getEditMember(mid,testid,clubid,onsuccessresponse){
    logStatus("Calling getEditMember Result", LOG_COMMON_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'testResultsListDetail';
        _data.p = 'members';
        _data.id = mid;
        _data.testId = testid;
        _data.authorizedClubId	=	clubid;
        do_service(_data,{},function(respsonse){
            if(onsuccessresponse){
                onsuccessresponse(respsonse);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("getEditMember Result", e);
    }
}
//**** [SK] comments *****//
//**** Forget password for coach app*****//
/*
function forgetpassword(){	
    logStatus("Calling forgetpassword Result", LOG_COMMON_FUNCTIONS);
    try {
        var _data = {};
        _data.action = 'sendForgotPasswordLink';
        _data.p = 'members';
        _data.email = $('#forgetemail').val();
        do_service(_data,{},function(respsonse){
            _close_popup();
        },'json');
    } catch(e) {
        ShowExceptionMessage("forgetpassword", e);
    }
}
*/
// *** [SK] comments by shanethatech ***//
// *** Start new test Details ***//
function newTestDetails(action,mid,teststatus,onsuccessresponse){
    logStatus("Calling newTestDetails", LOG_FUNCTIONS);
    logStatus(teststatus, LOG_DEBUG);
    try{
        var _data = {};
        _data.action 	= action;
        _data.p			= 'members';
        _data.id 		= mid;
        _data.newTest	= 1;
        do_service(_data,{},function(respsonse){
            if(onsuccessresponse){
                onsuccessresponse(respsonse);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("newTestDetails", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Fetch Profile details to diaply user profile ***//
function _getUserImage(action,mid,onsuccessimageresponse){
    logStatus("Calling _getUserImage Result", LOG_COMMON_FUNCTIONS);
    try {
        var _data = {};
        _data.action 	= action;
        _data.p			= 'members';
        _data.id 		= mid;
        do_service(_data,{},function(respsonse){
            if(onsuccessimageresponse){
                onsuccessimageresponse(respsonse);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("_getUserImage", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Update Coach profile data ***//
function updateSecurityQuestion(action,mid,question,datapost,onSuccess){
    logStatus("Calling updateSecurityQuestion Result", LOG_COMMON_FUNCTIONS);
    try {
        var _data = {};
        var _datapost = {};
        _data.action 	= action;
        _data.p			= 'userProfile';
        _data.id 		= mid;
        // var question	=	{};
        // $(".sq_ques").each(function(i,eachitem){
            // var idstr	=	$(eachitem).attr("id");
            // var answer	=	$(eachitem).val();
            // if($.trim(answer)!=""){
                // var _qid	=	idstr.split('sqQues_')[1];
                // question[_qid]=answer;
            // }
        // });
        /**
         * @param LOGIN_INFO.user This login user info detail
         */
        _datapost		=	datapost;
        _datapost.question		=	JSON.stringify(question); //Changed By Sankar
        //DisableProcessBtn("#saveProfieldata", false);
        do_service(_data,_datapost,function(response){
            if(onSuccess){
                onSuccess(response);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("updateSecurityQuestion", e);
    }
}
// *** [SK] comments by shanethatech ***//
// *** Fetech Security Questions from DB table ***//
function getsecurityQuestions(action,eid,onsuceessresponse){
    logStatus("Calling getsecurityQuestions Result", LOG_COMMON_FUNCTIONS);
    logStatus(eid, LOG_DEBUG);
    try {
        var _data = {};
        _data.method = action;
        _data.p 	 = 'login';
        //_data.email  =  eid;
        do_service(_data,{},function(respsonse){
            if(onsuceessresponse){
                onsuceessresponse(respsonse);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getsecurityQuestions", e);
    }

}
// *** [SK] comments by shanethatech ***//
//*** get Request password ***//
function sendPasswordrequest(action,email,question,onsuccessdatahandler){
    logStatus("Calling sendPasswordrequest Result", LOG_COMMON_FUNCTIONS);
    try {
        var _data= {};
        var _datapost = {};
        _data.method = action;
        _data.p = 'login';
        _data.email = email;
        _datapost.question		=	JSON.stringify(question);
        do_service(_data,_datapost,function(response){
            if(onsuccessdatahandler){
                onsuccessdatahandler(response);
            }
        },'json');
    }  catch (e) {
       ShowExceptionMessage("sendPasswordrequest", e);
   }  
}
/* 
 * Comments: To handle all Manage-Bodycomposition Data related ajax request. 
 * author SK Shanethatech * 
 * Created Date  : Sept-24-2014
 */
function postbodyCompositionTest(postdata,onsuccesresponsehandler){
    logStatus("Calling postbodyCompositionTest Result", LOG_COMMON_FUNCTIONS);
    try {
        var data = {};
        data.action = 'submitBodyCompositionTest';
        data.p = 'members';
        do_service(data,postdata,function(response){
            if(onsuccesresponsehandler){
                onsuccesresponsehandler(response);
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("postbodyCompositionTest", e);
    }
}

/* 
 * Comments: To handle Flexiblity Data related ajax request. 
 * author SK Shanethatech * 
 * Created Date  : 2016-01-25
 */
 
 function postFlexbilityTestdata(action,postdata,onsuccessflexibilityhandler){
     logStatus("Calling postFlexbilityTestdata Result", LOG_COMMON_FUNCTIONS);
    try {
        var data = {};
        data.action = action;
        data.p = 'members';
        do_service(data,postdata,function(response){
            if(onsuccessflexibilityhandler){
                onsuccessflexibilityhandler(response);
            }
        },'json');
    } catch (e){
        ShowExceptionMessage("postFlexbilityTestdata", e);
    }
 }
 
/* 
 * Comments: To handle Flexiblity Data related ajax request. 
 * author SK Shanethatech * 
 * Created Date  : Sept-24-2014
 */

/*
 function savecardiotestdata(action,userid,testid,testdate,dataelements,hiddenclub,newTest,onsuccesscardiohaandler){
     logStatus("Calling savecardiotestdata result",LOG_COMMON_FUNCTIONS);
     try {
         var _data = {};
         var _dataelements ={};
         _data.action = action;
         _data.p	  ="members";
         _data.clubId = hiddenclub;
         _data.newTest = newTest;
        _dataelements.clubId = getselectedclubid();
        _data.currentuserId = userid;
        _dataelements.loggedinUserId = getCurrentLoggedinUserId();
        _dataelements = dataelements;
        //_dataelements.newTest = 1;//Set user status to test to do
         do_service(_data,_dataelements,function(response){
             if(onsuccesscardiohaandler){
                 onsuccesscardiohaandler(response);
             }
         },'json');
     } catch(e){
        ShowExceptionMessage("savecardiotestdata", e);
     }
}
*/

function saveMindTestStarted(postdata,onsuccesresponsehandler){
 logStatus("Calling saveMindTestStarted",LOG_FUNCTIONS);
 try {
        var data = {};
        
		data.items = postdata.items;
        data.userId = postdata.userId;
		data.is_reporting = postdata.is_reporting;
        data.testId = postdata.testId;
        data.testdate = postdata.testdate;
	   //testService();
	   
        do_service(data,postdata,function(response){
            if(onsuccesresponsehandler){
                onsuccesresponsehandler(response);
            }
        },'json');
		
    } catch (e){
         ShowExceptionMessage("postMindTest", e);
    }

}

function saveStrenghtTestStarted(postdata,onsuccesresponsehandler){
 logStatus("Calling saveStrenghtTestStarted",LOG_FUNCTIONS);
 try {
        var data = {};
        data.items = postdata.items;
        data.userId = postdata.userId;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuccesresponsehandler){
                onsuccesresponsehandler(response);
            }
        },'json');
        
    } catch (e){
         ShowExceptionMessage("saveStrenghtTestStarted", e);
    }

}

/*---------24-11-16-------------*/


function showstrenghtDatanew(postdata,onsuceessresponse)
{
    logStatus("Calling showstrenghtDatanew",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("showstrenghtDatanew", e);
    }
}
/*-----------------------*/

/*---------29-11-16-------------

Show Training Schema
-------------------------------*/
function showtraingSchema(postdata,onsuceessresponse)
{
    logStatus("Calling showtraingSchema",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("showtraingSchema", e);
    }
}
/*-----------------------*/


/*Show Mind Data*/
function showMindDatanew(postdata,onsuceessresponse)
{
	logStatus("Calling showMindDatanew",LOG_FUNCTIONS);
	try {
        var data = {};
        data.user_id = postdata.user_id;
		data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("showMindDatanew", e);
    }
}

/*Show Check result*/
function showcheckresultdataa(postdata,onsuceessresponse)
{
    logStatus("Calling showcheckresultdata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("showcheckresultdata", e);
    }
}

/*Send Status of Mind Activation*/
function statusMindActivation(postdata,onsuceessresponse)
{
    logStatus("Calling statusMindActivation",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("statusMindActivation", e);
    }
}


/*Show Check result graph */
function showcheckresultdatagraps(postdata,onsuceessresponse)
{
    logStatus("Calling showcheckresultdatagraps",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("showcheckresultdatagraps", e);
    }
}



/*MK Added Save Cardio Test Data*/
function saveCardioTestStarted(memberId,testData,onsuccesscardiohaandler,clubId){
    logStatus("Calling saveCardioTestStarted",LOG_FUNCTIONS);
    try {
		
        var _data		=	{};
        var _testData	=	(testData) ? testData:{};
		
        _data.action	=	'updateTestResult';
        _data.p			=	'members';
        _data.clubId	=	clubId;
        _data.currentuserId 	= 	memberId;
        _testData.loggedinUserId=	getCurrentLoggedinUserId();
        _testData.testStatus	=	0;
        do_service(_data,_testData,function(response){
            if(onsuccesscardiohaandler){
                onsuccesscardiohaandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("saveCardioTestStarted", e);
    }
}
/*SN added profile image 20160123*/
function updateProfileImage(action,formdata,userid,onSuccessResponse){
    logStatus("Calling updateProfileImage ", LOG_FUNCTIONS);
    try{
        var _data = {};
        //var _datapost = {};
        _data.action = action;
        _data.p 	 = 'userProfile';
        _data.userid 	 = userid;
        /*if(machineid) {
            _data.machineid = machineid;
        }*/
        do_service_upload(_data,formdata,function(response){
            if(onSuccessResponse){
                onSuccessResponse(response);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("updateProfileImage", e);
    }
}
/* SK upload data through external url */
function uploadExternalUrldata(url,Onsuccessdatahandler){
    logStatus("Calling uploadExternalUrldata",LOG_FUNCTIONS);
    try{
        var _data = {};
        _data.action = "externalImageUrlData";
        _data.p 	 = "userProfile";
        _data.url 	 = url;
        do_service(_data,{},function(response){
            //console.log(JSON.stringify(response));
            if(Onsuccessdatahandler){
                Onsuccessdatahandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("uploadExternalUrldata",e);
    }
}
/*
function getBodycomptiontestmethods(clubid,onsuccesscardiohaandler){
     logStatus("Calling getBodycomptiontestmethods result",LOG_COMMON_FUNCTIONS);
    try {
        var  _data	=	{};
        _data.action=	'fetchTestMethodologies';
        _data.p 	=	'members';
        _data.id	=	clubid;
        //do_service(_data,_dateelements,function(response){
        do_service(_data,{},function(response){
            if(onsuccesscardiohaandler){
                onsuccesscardiohaandler(response);
            }
         });
    } catch (e){
        ShowExceptionMessage("getBodycomptiontestmethods", e);
    }
}
*/
/* Popuplate test option list in to the given control */

/*
function LoadTestOptionList(successhandler){
     logStatus("Calling LoadTestOptionList result",LOG_FUNCTIONS);
    try {
        var  _data = {};
        var _dateelements = {};
        _data.action = 'getTestList';
        _data.p = "test";
         do_service(_data,_dateelements,function(response){
                if (successhandler){
                    successhandler(response);
                }
         });
    } catch (e){
        ShowExceptionMessage("LoadTestOptionList", e);
    }
}


/* Popuplate test level list in to the given control */
function LoadTestLevelList(weight, successhandler){
     logStatus("Calling LoadTestLevelList result",LOG_FUNCTIONS);
    try {
        var  _data = {};
        var _dateelements = {};
        _data.action = 'getStartLevels';
        _data.p = "members";
        _dateelements.weight = weight;
         do_service(_data,_dateelements,function(response){
                if (successhandler){
                    successhandler(response);
                }
         });
    } catch (e){
        ShowExceptionMessage("LoadTestLevelList", e);
    }
}
function getTesttypes(clubid,action,OnsuccessHandler){
    logStatus("Calling getTesttypes",LOG_FUNCTIONS);
    try {
        var _data = {};
        _data.action = action;
        _data.p = "members";
        _data.clubId = clubid;
         do_service(_data,{},function(response){
             if(OnsuccessHandler){
                 OnsuccessHandler(response);
             }
         },'json');
    } catch(e) {
        ShowExceptionMessage("getTesttypes", e);
    }
}

 /*
 * Comments: To handle Strength  Test.
 * Added by SK Shanethatech * 
 * Created Date  : JAN-28-2016
 */
 /*Edited MK - No Club ID Passed - But Reffered in Server*/
//function strengthtest(typeval,userId,loguserId,weight,height,Onsucesshandler){
function strengthtest(postdata,Onsucesshandler){
    logStatus("Calling strengthtest",LOG_COMMON_FUNCTIONS);
    try{
		
        var _data={};
        _data.action = "saveStrengthTest";
        _data.p 	 = "members";
        /*_data.userId = userId;
        _data.testoptiontype = typeval;
        _data.coach_id	=	loguserId;
        _data.weight	=	weight;
        _data.height 	=	height;
        _data.clubID	=	getselectedclubid();*/
        do_service(_data,postdata,function(response){
            if(Onsucesshandler){
                Onsucesshandler(response);
            }
            //showstandardajaxmessage(response.status_message);
         },'json');

     } catch(e){
         ShowExceptionMessage("strengthtest", e);
     }
 }
/*Comments: To handle cardio test page client details on load.
* author SK Shanethatech * 
* Created Date  : FEB-02-2016
*/
function getClientsdata(postaData,Onsuccesscardiotesthandler,clubId,testDate){
    logStatus("Calling getClientsdata",LOG_FUNCTIONS);
    try{
        var _data	= {};
        //_data.action 	= 'Cardiotest';
        _data.action 	= 'CardiotestMembers';
        _data.p 		= "members";
        _data.clubId	= clubId;
        if(testDate){
            _data.testDate=	testDate;
        }
        do_service(_data,postaData,function(response){
            if(Onsuccesscardiotesthandler){
                Onsuccesscardiotesthandler(response);
            }
            //showstandardajaxmessage(response.status_message);
        },'json');
    } catch(e){
        ShowExceptionMessage("getClientsdata", e);
    }
}
/*MK Added To get client detail By Member ID (client) */
function _getClientInformationByID(memberID,onSuccessGetMemberDetail){
    logStatus("Calling _getClientInformationByID",LOG_FUNCTIONS);
    try{
         var _data={};
         _data.action	= 'getMemberDetailByID';
         _data.p		= 'members';
         _data.memberId	=	memberID;
         do_service(_data,{},function(response){
            if(onSuccessGetMemberDetail){
                onSuccessGetMemberDetail(response);
            }
         },'json');
    } catch(e){
        ShowExceptionMessage("_getClientInformationByID", e);
    }
}
function getListMachinesData(datapost,data,OnsuccessHandlerdata,actionID){
    logStatus("Calling getListMachinesData",LOG_FUNCTIONS);
    try{
        var _data 	=	(data) ? data:{};
        _data.action	=	'ListMachinesData';
        _data.p 	=	"members";
        _data.userid	=	getCurrentLoggedinUserId();
        _data.actionID	=	(actionID)? actionID : 0 ;
        do_service(_data,datapost,function(response){
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
        });
    } catch (e){
        ShowExceptionMessage("getListMachinesData", e);
    }
}
function getMachineData(clubid,OnsuccessHandlerdata){
    logStatus("Calling getMachineData",LOG_FUNCTIONS);
    try{
        var _data 	={};
        _data.action	=	'machinesData';
        _data.p 	=	"members";
        _data.userid	=	getCurrentLoggedinUserId();
        _data.clubid	=	clubid;
        do_service(_data,{},function(response){
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
        });
    } catch (e){
        ShowExceptionMessage("getMachineData", e);
    }
}
function getListDeviceData(OnsuccessHandlerdata){
    logStatus("Calling getListDeviceData",LOG_FUNCTIONS);
    try{
        var _data 	={};
        _data.action	=	'getListDeviceData';
        _data.p 	=	"members";
        _data.userid	=	getCurrentLoggedinUserId();
        _data.clubid	=	getselectedclubid();
        do_service(_data,{},function(response){
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
        });
    } catch (e){
        ShowExceptionMessage("getListDeviceData", e);
    }
}
function getDeviceById(deviceid,OnsuccessHandlerdata){
    logStatus("Calling getDeviceData",LOG_FUNCTIONS);
    try{
        var _data 	={};
        _data.action	=	'getDeviceById';
        _data.p 	=	"members";
        _data.userid	=	getCurrentLoggedinUserId();
        _data.deviceid	=	deviceid;
        _data.clubid	=	getselectedclubid();
        do_service(_data,{},function(response){
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
        },'json');
    } catch (e){
        ShowExceptionMessage("getDeviceData", e);
    }
}
function _saveDeviceData(machineid,OnsuccessHandlerdata){
    logStatus("Calling _saveDeviceData",LOG_FUNCTIONS);
    try{
        var _data 	={};
        _data.action	=	'saveDeviceData';
        _data.p 		=	"members";
        _data.machineid	=	(machineid) ?machineid:0;
        _data.userid	=	getCurrentLoggedinUserId();
        _data.clubid	=	getselectedclubid();
        do_service(_data,{},function(response){
            updateDeviceData();
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
        });
    } catch (e){
        ShowExceptionMessage("_saveDeviceData", e);
    }
}

function getLastTestResultdata(mid,testTypeId,OnsuccessHandlerdata){
    logStatus("Calling getLastTestResultdata",LOG_FUNCTIONS);
    try{
        var data ={};
        data.action	=	'getLastTestResultdata';
        data.p		=	'members';
        data.userId = mid;
        data.testType = testTypeId;
        do_service(data,{},function(response){
             if(OnsuccessHandlerdata){
                 OnsuccessHandlerdata(response);
             }
         },'json');
    } catch (e){
        ShowExceptionMessage("getLastTestResultdata", e);
    }
}
function getMemberTestTobeAnalysed(postData,handleOnSuccess){
    logStatus("Calling getMemberTestTobeAnalysed",LOG_FUNCTIONS);
    try {
        var data ={};
        data.action	=	'getMemberTestTobeAnalysed';
        data.p		=	'members';
        do_service(data,postData,function(response){
             if(handleOnSuccess){
                 handleOnSuccess(response);
             }
         },'json');
    } catch (e){
        ShowExceptionMessage("getMemberTestTobeAnalysed", e);
    }
}
function getMembersTestResultdataForClubandDate(clubid,date, statusdet, OnsuccessHandlerdata,MemberID, isActive){
    logStatus("Calling getMembersTestResultdataForClubandDate",LOG_FUNCTIONS);
    try{
        var data ={};
        data.action	=	'getMembersTestResultdataForClubandDate';
        data.p		=	'members';
        data.clubId = clubid;
        data.testDate = date;
        data.statusdet = statusdet;
		
        if(MemberID && MemberID!=0){
            data.userId	=	MemberID;
        }
		
        if(!isActive){
            data.isActive	=	'0,1'; //All
        }else{
            data.isActive = '0,1';
        }
//console.log("hello");
		//console.log(data.userId);
        do_service(data,{},function(response){
            if(OnsuccessHandlerdata){
                OnsuccessHandlerdata(response);
            }
         },'json');
    } catch (e){
        ShowExceptionMessage("getMembersTestResultdataForClubandDate", e);
    }
}

function gettypelistdetails(clubid,Onsuccesshanlder){
    logStatus("Calling getbrands",LOG_FUNCTIONS);
    try{
        var data 	= {};
        data.action 	= "getlistdetails";
        data.p		= "members";
        data.clubid 	=  clubid;
        do_service(data,{},function(response){
             if(Onsuccesshanlder){
                 Onsuccesshanlder(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("getbrands", e);
    }
}
/** Comments: To handle save new machine data. TASKID = '4307'
  * author SK Shanethatech * 
  * Created Date  : FEB-05-2016*/
function savenewMachinedata(data,Onsuccesshanlder){
    logStatus("Calling savenewMachinedata",LOG_FUNCTIONS);
    try{
        var _data 		= {};
        //var machinedetails 	= {};
        _data.action 		= "saveNewMachineDetails";
        _data.p		 	= "members";
        var machinedetails 		= data;
        do_service(_data,machinedetails,function(response){
            if(Onsuccesshanlder){
                Onsuccesshanlder(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("savenewMachinedata", e);
    }
}
function getclubnamedetails(action,Onsuccesshandler){
    logStatus("Calling savenewMachinedata",LOG_FUNCTIONS);
    try{
        var _data 		= {};
        //var machinedetails 	= {};
        _data.action 		= action;
        _data.p			="members";
        _data.clubid 		= getselectedclubid();
        do_service(_data,{},function(response){
             if(Onsuccesshandler){
                 Onsuccesshandler(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("savenewMachinedata", e);
    }
}
function getNewMachinedetailsbyeditid(editid,Onsuccesshandler){
    logStatus("Calling getNewMachinedetailsbyeditid",LOG_COMMON_FUNCTIONS);
    try{
        var _data =  {};
        _data.action = "getNewMachinedetailsbyeditid";
        _data.p 	 = "members";
        _data.editId = editid;
        do_service(_data,{},function(response){
             if(Onsuccesshandler){
                 Onsuccesshandler(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("getNewMachinedetailsbyeditid", e);
    }
}
/* delete new machien details from grid data*/
function deleteNewMachine(checkval,Onsuccesshndler){
    logStatus("Calling deleteeditednewmachine",LOG_FUNCTIONS);
    try{
        var _data 	= {};
        //var datapost	= {};
        _data.action 	= "deleteNewMachine";
        _data.p 	= "members";
        _data.items 	= checkval;
        do_service(_data,{},function(response){
            if(Onsuccesshndler){
                Onsuccesshndler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("deleteeditednewmachine",e);
    }
}
function deleteStrengthMachienItem(delvalue,OnsuccessHandler){
    logStatus("Calling deleteStrengthMachienItem",LOG_FUNCTIONS);
    try{
        var data 	= {};
        data.action 	= "deleteStrengthMachienItem";
        data.p	 	= "members";
        data.editID 	= delvalue;

        do_service(data,{},function(response){
            if(OnsuccessHandler){
                OnsuccessHandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("deleteStrengthMachienItem",e);
    }
}
/*SN added Get Client Personal info 20160307*/
function _getClientProfileInformationByID(memberID,onSuccess){
    logStatus("Calling _getClientProfileInformationByID",LOG_FUNCTIONS);
    try{
         var _data={};
         _data.action	= 'getClientDetailByID';
         _data.p		= 'members';
         _data.memberId	=	memberID;
         do_service(_data,{},function(response){
            if(onSuccess){
                onSuccess(response);
            }
         },'json');
    } catch(e){
        ShowExceptionMessage("_getClientProfileInformationByID", e);
    }
}
/*MK List Heartrate Devices*/
/*
function getHeartrateDevices(handleOnSuccess){
    logStatus("Calling getHeartrateDevices",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getHeartrateDevices";
        data.p	 	= "members";
        do_service(data,{},function(response){
            if(handleOnSuccess){
                handleOnSuccess(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getHeartrateDevices",e);
    }
}
*/
/*SNK Get all mapped device info*/
function getMappedDeviceInfo(handleOnSuccess){
    logStatus("Calling getMappedDeviceInfo",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getMappedDeviceInfo";
        data.p	 	= "members";
        do_service(data,{},function(response){
            if(handleOnSuccess){
                handleOnSuccess(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getMappedDeviceInfo",e);
    }
}

/*Update Heartrate Monitor Data*/
function _updateHeartRateOnServer(datapost,userTestID,user_id,onSuccessHandler){
	//alert(user_id);
    logStatus("Calling _updateHeartRateOnServer",LOG_FUNCTIONS);
    try{
        var data	=	{};
        data.action = "updateHeartRateOnTest";
        data.p	 	= "members";
        data.userTestID	 = userTestID;
		var user		=	getCurrentUser();
        data.userId		=	user_id;
        do_service(data,{data:JSON.stringify(datapost)},function(){
            if(onSuccessHandler){
                onSuccessHandler();
            }
        });
    } catch(e){
        ShowExceptionMessage("_updateHeartRateOnServer",e);
    }
}
/*
function getCardioTestDetailByID(userTestID,onSuccessResponse){
    logStatus("Calling getCardioTestDetailByID",LOG_FUNCTIONS);
    try{
        var data ={};
        data.action	=	'getCardioTestDetailByID';
        data.p		=	'members';
        data.userTestID = userTestID;
        do_service(data,{},function(response){
             if(onSuccessResponse){
                 onSuccessResponse(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("getCardioTestDetailByID",e);
    }
}
*/
/* SK- Changes New Machine Training Program by using program id */
function strengthProgramOverview(pid,Onsucceesshandler,page){
    logStatus("Calling strengthProgramOverview",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "strengthOverviewprogram";
        data.p 		= "members";
        data.actionID  	= (page)? page : 0;
        data.clubid  	= getselectedclubid();
        data.pid  		= pid;
        do_service(data,{},function(response){
             if(Onsucceesshandler){
                 Onsucceesshandler(response);
             }
         });
    } catch(e){
        ShowExceptionMessage("strengthProgramOverview",e);
    }
}

function strengthProgramTraining(tapvalue,Onsucceesshandler){
    logStatus("Calling strengthProgramTraining", LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "strengthProgramTraining";
        data.p 		= "members";
        data.tapvalue = tapvalue;
        do_service(data,{},function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        });
    } catch(e){
        ShowExceptionMessage("strengthProgramTraining",e);
    }
}
/*SK Get Program by week training */
function getWeekProgramTraining(tapvalue,OnsuccessHandler,flag,hrefid){
    logStatus("Calling _getMachienMappedetails", LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getWeekProgramTraining";
        data.p		= "members";
        data.type	= tapvalue;
        data.clubid	= getselectedclubid();
        data.flag 	= flag;
        data.hrefid = hrefid;
        do_service(data,{},function(response){
             if(OnsuccessHandler){
                 OnsuccessHandler(response);
             }
        });
    } catch (e){
        ShowExceptionMessage("getListMachinesData", e);
    }
}
function _getMachienMappedetails(mid,Onsucceesshandler){
    logStatus("Calling _getMachienMappedetails", LOG_FUNCTIONS);
    try{

        var data = {};
        data.action 	= 'getMachienMappedetails';
        data.p 			= 'members';
        data.userID 	= mid;
        data.clubid 	= getselectedclubid();
        do_service(data,{},function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getMachienMappedetails",e);
    }
}
function getMachienMappedetailsGrid(mid,Onsucceesshandler,selectweek){
    logStatus("Calling _getMachienMappedetails", LOG_FUNCTIONS);
    try{

        var data = {};
        data.action 	= 'getMachienMappedetailsGrid';
        data.p 			= 'strength';
        data.userID 	= mid;
        data.clubid 	= getselectedclubid();
        data.selectweek 	= selectweek;
        do_service(data,{},function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        });
    } catch(e){
        ShowExceptionMessage("_getMachienMappedetails",e);
    }
}

function _getStrengthProgramType(Onsucceesshandler){
    logStatus("Calling _getStrengthProgramType", LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getStengthProgramType";
        data.p		= "members";
        do_service(data,{},function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        });
    } catch(e){
        ShowExceptionMessage("_getStrengthProgramType",e);
    }
}

function _getWeekAssignProgramm(desc,selectid,OnsuccessTrueHandlerdata){
    logStatus("Calling _getWeekAssignProgramm event",LOG_FUNCTIONS);
    try{
        var data 	= {};
        var _post 	= {};
        data.action 		= "createWeekAssignProgram";
        data.p				= "members";
        data.description	= desc;
        _post.val			= selectid;
        _post.loggedUserID	= getCurrentLoggedinUserId();
        _post.strengthProgramId = '';
        _post.clubid			 = getselectedclubid();
        do_service(data,_post,function(response){
            if(OnsuccessTrueHandlerdata){
                OnsuccessTrueHandlerdata(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getWeekAssignProgramm",e);
    }
}

function _saveWeekprgmanually(data,OnsuccessTrueDataHandlerdata){
    logStatus("Calling _saveWeekprgmanually", LOG_FUNCTIONS);
    try{
        var _data 		= {};
        _data.action 	= "strengthProgramTrainingAddUpdate" ;
        _data.p			= "members";
        _data.strengthProgramTrainingId			= '';
        //_data.data 		= data;
        do_service(_data,data,function(response){
            if(OnsuccessTrueDataHandlerdata){
                OnsuccessTrueDataHandlerdata(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("_saveWeekprgmanually",e);
    }
}
/*
function getWeekExerciseActivityDetails(programid,Successhandler){
    logStatus("Calling getWeekExerciseActivityDetails",LOG_FUNCTIONS);
    try{
        var _data =  {};
        _data.action 	= "strengthProgramTraining";
        _data.p 		= "settings";
        _data.tapvalue 	= programid;
        do_service(_data,{},function(responedata){
                if(Successhandler){
                    Successhandler(responedata);
                }
        });
    } catch(e){
        ShowExceptionMessage("getWeekExerciseActivityDetails",e);
    }
}
*/
function _saveNewProgramStatus(postdata,Onsuccessdatahandler){
    logStatus("Calling _saveNewProgramStatus function",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "saveNewProgramStatus";
        data.p 		= "members";
        do_service(data,postdata,function(responedata){
                if(Onsuccessdatahandler){
                    Onsuccessdatahandler(responedata);
                }
        });
    } catch(e){
        ShowExceptionMessage("_saveNewProgramStatus",e);
    }
}
/*SN Added- Get Machine List By Club Id-20160316*/

function _getMachineByClubId(clubid,Onsuccessdatahandler){
    logStatus("Calling _getMachineByClubId function",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getMachineByClubId";
        data.p 		= "strength";
        data.clubid 		= clubid;
        do_service(data,{},function(responedata){
                if(Onsuccessdatahandler){
                    Onsuccessdatahandler(responedata);
                }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getMachineByClubId",e);
    }
}
function _getMachineByDefaultProgram(Onsuccessdatahandler){
    logStatus("Calling _getMachineByClubId function",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getMachineByDefaultProgram";
        data.p 		= "strength";
        do_service(data,{},function(responedata){
                if(Onsuccessdatahandler){
                    Onsuccessdatahandler(responedata);
                }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getMachineByClubId",e);
    }
}

/*
function _saveProgramWtMachine(postdata,Onsuccessdatahandler){
    logStatus("Calling _saveProgramWtMachine function",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "saveProgramWtMachine";
        data.p 		= "strength";
        do_service(data,postdata,function(responedata){
                if(Onsuccessdatahandler){
                    Onsuccessdatahandler(responedata);
                }
        });
    } catch(e){
        ShowExceptionMessage("_saveProgramWtMachine",e);
    }
}
*/
/* getting mapped machien by program id with checked option */
function _getMappedMachienbyProgramId(programid,memberId,Onsuccessdatatruhandler,flag){
    logStatus("Calling _getMappedMachienbyProgramId function",LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getMappedMachienbyProgramId";
        data.p 		= "members";
        data.pid 	= programid;
        data.userId = memberId;
        data.clubid	= getselectedclubid();
        data.loggeduserid 	= getCurrentLoggedinUserId();
        data.flag 	= flag;
        do_service(data,{},function(response){
                if(Onsuccessdatatruhandler){
                    Onsuccessdatatruhandler(response);
                }
        });
    } catch(e){
        ShowExceptionMessage("_getMappedMachienbyProgramId",e);
    }
}

function addOrRemoveNewMachineDetails(checkval,programid,OnsuccessTrueDataHandlerdata){
    logStatus("Calling addOrRemoveNewMachineDetails function",LOG_FUNCTIONS);
    try{
        var data = {};
        var postdata = {};
        data.action = 'addNewActivities';
        data.p 		= 'strength';
        data.pid 	= programid;
        postdata.strength_machine	=	JSON.stringify(checkval);
        do_service(data,postdata,function(response){
                if(OnsuccessTrueDataHandlerdata){
                    OnsuccessTrueDataHandlerdata(response);
                }
        },'json');
    } catch(e){
        ShowExceptionMessage("addOrRemoveNewMachineDetails",e);
    }
}

function _getweekdatelist(programid,memberid,countdata,OnsuccessTrueDatahandler){
    logStatus("Calling _getweekdatelist function",LOG_FUNCTIONS);
    try{
        var _data = {};
        _data.action = 'getweekdatelist';
        _data.p 		= 'members';
        _data.userid	= memberid;
        _data.pid 	= programid;
        _data.clubid = getselectedclubid();
        _data.week   = countdata;

        do_service(_data,{},function(response){
                if(OnsuccessTrueDatahandler){
                    OnsuccessTrueDatahandler(response);
                }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getweekdatelist",e);
    }
}
/* REmove machien from program id as well as cleint id */
function removeMachineformprogramid(checkval,programid,onTrueSuccessDataHandler){
    logStatus("Calling removeMachineformprogramid",LOG_FUNCTIONS);
    try{
        var _data 		= {};
        var postdata 	= {};
        _data.action 	= 'removeActivities';
        _data.p   	 	= 'strength';
        data.pid 		= programid;
        postdata.strength_machine	=	JSON.stringify(checkval);
        do_service(_data,postdata,function(response){
                if(onTrueSuccessDataHandler){
                    onTrueSuccessDataHandler(response);
                }
        });
    } catch (e){
        ShowExceptionMessage("removeMachineformprogramid",e);
    }
}
function getStrengthUserList(usrid,onTrueSuccessDataHandler){
    logStatus("Calling removeMachineformprogramid",LOG_FUNCTIONS);
    try{
        var _data 		= {};
        var postdata 	= {};
        _data.action 	= 'getStrengthUserList';
        _data.p   	 	= 'strength';
        _data.usrid 		= usrid;
        do_service(_data,postdata,function(response){
                if(onTrueSuccessDataHandler){
                    onTrueSuccessDataHandler(response);
                }
        },'json');
    } catch (e){
        ShowExceptionMessage("removeMachineformprogramid",e);
    }
}

function loadLanguage(Onsuccesshandler){
    logStatus("calling loadLanguage",LOG_COMMON_FUNCTIONS);
    try{
        var _data={};
        _data.action="getLanguageDetails";
        _data.p		 ="settings";
        do_service(_data,{},function(response){
             if(Onsuccesshandler){
                 Onsuccesshandler(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("loadLanguage", e);
    }
}
function loadDeviceSettings(Onsuccesshandler){
    logStatus("calling loadDeviceSettings",LOG_COMMON_FUNCTIONS);
    try{
        var _data={};
        var devicedata =  _getCacheArray('devicesettings');
        _data.action="getdevicesettings";
        _data.p		 ="settings";
        _data.clubId	=	getselectedclubid();
        _data.uniqueId	=	devicedata.device;
        //_data.UserId	=	getCurrentLoggedinUserId();
        do_service(_data,{},function(response){
             if(Onsuccesshandler){
                 Onsuccesshandler(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("loadDeviceSettings", e);
    }
}
/*SN added client personal info*/
function getCityListByZipCode(data,handleOnSuccess){
    logStatus("Calling getCityListByZipCode", LOG_FUNCTIONS);
    try {
        var _data	=	(data) ? data: {};
        _data.action = 'getCityListByZipCode';
        _data.p 	 = 'members';
        do_service(_data,{},function(response){
            if(handleOnSuccess) {
                handleOnSuccess(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getCityListByZipCode", e);
    }
}
function loadMemberOptionList(action,handleOnSuccess){
    logStatus("Calling loadMemberOptionList", LOG_FUNCTIONS);
    try {
        var _data	=	{};
        _data.action = action;
        /*action may be 'getActivityList','getProfessionList','getNationalityList'*/
        _data.p 	 = 'members';
        do_service(_data,{},function(response){
            if(action=='getActivityList'){
            }
            if(handleOnSuccess) {
                handleOnSuccess(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("loadMemberOptionList", e);
    }
}
function getlanguages(Onsuccesshandler){
    logStatus("Calling getlanguages Result", LOG_FUNCTIONS);
    try {
        var _data={};
        _data.action='getLanguagelist';
        _data.p		 ='settings';
        do_service(_data,{},function(response){
             if(Onsuccesshandler){
                 Onsuccesshandler(response);
             }
         },'json');
    } catch(e){
        ShowExceptionMessage("getlanguages", e);
    }
}
function MailAnalysisReportPDF(postdata,onSuccessMailSend){
    logStatus("Calling MailAnalysisReportPDF", LOG_FUNCTIONS);
    try {
        var getdata	=	{};
        getdata.action	=	'MailAnalysisReportPDF';
        getdata.p		=	'members';
       // getdata.userTestId	=	UserTestID;
        do_service(getdata,postdata,function(response){
            if(onSuccessMailSend){
                onSuccessMailSend(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("MailAnalysisReportPDF", e);
    }
}
/**/
function getTestSummaryByMember(postData,onSuccessHandler){
    logStatus("Calling getTestSummaryByMember", LOG_FUNCTIONS);
    try {
        var getData			=	{};
        getData.action	=	'getTestSummaryByMember';
        getData.p		=	'members';
        do_service(getData,postData,function(response){
            if(onSuccessHandler){
                onSuccessHandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getTestSummaryByMember", e);
    }
}
/**/
function getStrengthLevel(age,gender,memberId,onSuccessHandler){
    logStatus("Calling getTestSummaryByMember", LOG_FUNCTIONS);
    try {
        var getData			=	{};
        getData.action	=	'getStrengthLevel';
        getData.memage	=	age;
        getData.gender	=	gender;
        getData.memberId	=	memberId;
        getData.p		=	'members';
        do_service(getData,{},function(response){
            if(onSuccessHandler){
                onSuccessHandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("getTestSummaryByMember", e);
    }
}
/*MK Added Update Member Mapped to a device*/
function updateMappedMemberDevice(postData,onUpdateSuccess){
    logStatus("Calling updateMappedMemberDevice", LOG_FUNCTIONS);
    try {
        var getData		=	{};
        getData.action	=	'updateMappedMemberDevice';
        getData.p		=	'members';
        getData.clubId	=	getselectedclubid();
        getData.UserId	=	getCurrentLoggedinUserId();
        do_service(getData,postData,function(response){
            if(onUpdateSuccess){
                onUpdateSuccess(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("updateMappedMemberDevice", e);
    }
}

/*PK- update message(email)*/
function updateEmailInformation(data,Successhandler){
        logStatus("Calling updateEmailInformation", LOG_FUNCTIONS);
    try{
        var _data 		= {};
        _data.action 	= "InsertMessageEmail" ;
        _data.p			= "members";
        do_service(_data,data,function(response){
            if(Successhandler){
                Successhandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("updateEmailInformation",e);
    }
}
function _getMessageInfo(postdata,Onsucceesshandler){
    logStatus("Calling _getMessageInfo", LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getMessageDetails";
        data.p		= "members";
        do_service(data,postdata,function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getMessageInfo",e);
    }
}
/*Calendar Information*/
function SaveCalenderNote(postdata,Successhandler){
    logStatus("Calling SaveCalenderNote", LOG_FUNCTIONS);
    try{
        var getdata 		= {};
        getdata.action 	= "SaveCalenderNote";
        getdata.p		= "settings";
        var user		= getCurrentUser();
        postdata.userId	= user.user_id;
        do_service(getdata,postdata,function(response){
            if(Successhandler){
                Successhandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("SaveCalenderNote",e);
    }
}
function GetCalenderInfo(Onsucceesshandler){
    logStatus("Calling GetCalenderInfo", LOG_FUNCTIONS);
    try{
        var data = {};
        data.action = "getCalenderDetails";
        data.p		= "settings";
        data.clubId		=	getselectedclubid();
        var user		=	getCurrentUser();
        data.userId		=	user.user_id;
        do_service(data,{},function(response){
            if(Onsucceesshandler){
                Onsucceesshandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("GetCalenderInfo",e);
    }
}
function saveContactForm(data,onSuccessHandler){
    logStatus("Calling saveContactForm", LOG_FUNCTIONS);
    try {
        var getData		=	{};
        getData.action	=	'saveContactFormDetail';
        getData.p		=	'members';
        getData.clubId	=	getselectedclubid();
        getData.UserId	=	getCurrentLoggedinUserId();
        do_service(getData,data,function(response){
            if(onSuccessHandler){
                onSuccessHandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("saveContactForm", e);
    }
}

function getcoachlist(data,onsuccessresponse){
    logStatus("Calling getcoachlist", LOG_COMMON_FUNCTIONS);
    try {
        var is_reporting = base_url+'/reporting/index.php/Voucherlist/coachList';
        var _data = (data) ? data : {};
        _data.is_reporting =  is_reporting;
        do_service(_data,_data,function(respsonse){
            if(onsuccessresponse){
                onsuccessresponse(respsonse);
            }
        },'json');
    } catch(e) {
        ShowExceptionMessage("getcoachlist", e);
    }
}

function _getMachinePinPosition(machineid,maxweight,onSuccessHandler){
    logStatus("Calling _getMachinePinPosition", LOG_FUNCTIONS);
    try {
        var getData		=	{};
        getData.action	=	'getMachinePinPositionById';
        getData.p		=	'strength';
        getData.machineid	=	machineid;
        getData.maxweight	=	maxweight;
        //getData.clubId	=	getselectedclubid();
        // getData.UserId	=	getCurrentLoggedinUserId();
        do_service(getData,{},function(response){
            if(onSuccessHandler){
                onSuccessHandler(response);
            }
        },'json');
    } catch(e){
        ShowExceptionMessage("_getMachinePinPosition", e);
    }
}
function strength_machinelist(postdata,onsuceessresponse)
{
    
    logStatus("Calling strength_machinelist",LOG_FUNCTIONS);
    try {
        var data = {};
        data.Clubid  = postdata.Clubid ;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("strength_machinelist", e);
    }
}

function strength_Prog(postdata,onsuceessresponse)
    {
        //alert("helloo");
        logStatus("Calling strength_Prog",LOG_FUNCTIONS);
        try {
            var data = {};
           // data.clubid  = postdata.clubid;
           // console.log("clubidddd");
          //  console.log(data.clubid);

            data.type  = postdata.type;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
                if(onsuceessresponse){
                    
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("strength_Prog", e);
        }
    }
/*machineinformation page*/
	function machineoverview(postdata,onsuceessresponse)
    {
        //alert("helloo");
		
        logStatus("Calling machineoverview",LOG_FUNCTIONS);
        try {
            var data = {};
           
            data.user_id  = postdata.user_id;
			 data.week   =  postdata.week;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
				
                if(onsuceessresponse){
                   
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("machineoverview", e);
        }
    }
	/*machineinformation page*/
	function program(postdata,onsuceessresponse)
    {
        //alert("helloo");
        logStatus("Calling showweight",LOG_FUNCTIONS);
        try {
            var data = {};
           
            //data.user_id  = postdata.user_id;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
                if(onsuceessresponse){
                    
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("showweight", e);
        }
    }
	/*for weedata*/
	/*machineinformation page*/
	function program_week(postdata,onsuceessresponse)
    {
        //alert("helloo");
		
        logStatus("Calling showeekdata",LOG_FUNCTIONS);
        try {
            var data = {};
           
            data.r_strength_pgmid  = postdata.r_strength_pgmid;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
				
                if(onsuceessresponse){
                    
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("showeekdata", e);
        }
    }
	function userdetail(postdata,onsuceessresponse)
    {
        //alert("helloo");
		
        logStatus("Calling userdetail",LOG_FUNCTIONS);
        try {
            var data = {};
           
            data.user_id  = postdata.user_id;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
				
                if(onsuceessresponse){
                    
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("userdetail", e);
        }
    }
	/*****to save program_schedule*****/
	function save_program_schedule(postdata,onsuceessresponse)
    {
        //alert("helloo");
		
        logStatus("Calling userdetail",LOG_FUNCTIONS);
        try {
            var data = {};
           
            data.user_id  = postdata.user_id;
			data.program_type  = postdata.program_type;
			data.program_id  = postdata.program_id;
			data.action  = postdata.action;
			data.week_data  = postdata.week_data;
            data.is_reporting = postdata.is_reporting;
            do_service(data,postdata,function(response){
				
                if(onsuceessresponse){
                    
                    onsuceessresponse(response); 
                }
            },'json');
        } catch (e){
             ShowExceptionMessage("userdetail", e);
        }
    }
//---------------------------Show Graph for MOVE EAT AND MIND 
function getMovedata(postdata,onsuceessresponse)
{
    logStatus("Calling getMovedata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("getMovedata", e);
    }
}
//------------for updating mindtab
function updatemindstatusdata(postdata,onsuceessresponse)
{
    logStatus("Calling getMovedata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
		data.coach_id = postdata.coach_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("getMovedata", e);
    }
}
//------------for add manualdata
function savemandata(postdata,onsuceessresponse)
{
    logStatus("Calling savemandata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
		data.coach_id = postdata.coach_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("savemandata", e);
    }
}
//------------for add manualdata
function userweight(postdata,onsuceessresponse)
{
    logStatus("Calling getweightdata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("getweightdata", e);
    }
}
//update device
//------------
function updatedeviceCoach(postdata,onsuceessresponse)
{
    logStatus("Calling updatedevicedata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("updatedevicedata", e);
    }
}
//------------for add extracredit
function addextracreditdata(postdata,onsuceessresponse)
{
    logStatus("Calling getweightdata",LOG_FUNCTIONS);
    try {
        var data = {};
        data.user_id = postdata.user_id;
        data.is_reporting = postdata.is_reporting;
        do_service(data,postdata,function(response){
            if(onsuceessresponse){
                onsuceessresponse(response); 
            }
        },'json');
    } catch (e){
         ShowExceptionMessage("getweightdata", e);
    }
}
//update device type version
//------------
function addAppVersion_type(postdata,onsuceessresponse)
{
    logStatus("Calling addAppVersion_type",LOG_FUNCTIONS);
    try {
        var data = {};
        data.p = 'members';
        data.action = 'saveUserDevice';
        data.user_id = postdata.user_id;
        data.app_platform = postdata.app_platform;
        data.device_version = postdata.device_version;
        data.device_model = postdata.device_model;
        data.app_version = postdata.app_version;
        do_service(data,postdata,function(response){
                   if(onsuceessresponse){
                   onsuceessresponse(response);
                   }
                   },'json');
    } catch (e){
        ShowExceptionMessage("addAppVersion_type", e);
    }
}