// For Development Server
//=======================
//var BASEURL = 'http://movesmart.offshoresolutions.nl/movesmart_dev/backoffice/';
//var base_url = "http://movesmart.offshoresolutions.nl/movesmart_dev";
// For Acceptance Server
//=======================
var BASEURL = 'https://movesmart.offshoresolutions.nl/movesmart_acceptance/backoffice/';
var base_url = "https://movesmart.offshoresolutions.nl/movesmart_acceptance";

// For Live Server
//=======================
//var BASEURL = 'http://movesmart.offshoresolutions.nl/backoffice/';
//var base_url = "http://movesmart.offshoresolutions.nl";

//for localhost
//var BASEURL = 'http://movesmart/backoffice/';
//var base_url = "http://movesmart";

var PROFILEPATH	= BASEURL+'images/uploads/movesmart/profile/';
var MACHINE_PATH= BASEURL+'images/uploads/movesmart/machines/';
var URL_SERVICE	=	BASEURL+'service/servicecoach.php';

//var URL_SQ		=	BASEURL+'service/_sq.php';
//var URL_CAPTCHA	=	BASEURL+'service/captcha.php';

//var _load_value = {'A':3,'B':4,'C':5,'D':6,'E':7,'F':8};
var _load_value = {'A':1,'B':2,'C':3,'D':4,'E':5,'F':6};
/*Allows only coach user COACH*/
var APP_USERTYPE	=	1;
var APP_VERSION = '0.0.66';
/*Language*/
var USER_LANG	   =	1;
var DEF_USER_LANG	=	1;

/*translation variable types */
var HTMLTRANS = 1;
var VALTRANS  = 2;
var TEXTTRANS = 3;
var PHTRANS = 4;

var PROGRAMS_CIRCUTE_Type	=	2;
var PROGRAMS_MIXED_Type		=	3;

/*language traslation Key*/
var BTN_SUBMIT	                               = 1;
//var LBT_TABLE	                               = 2;
//var CLICK_TABLE	                               = 3;
var BTN_REMEMBER_ME                            = 5;
var LBL_FORGOT_PASSWORD                        = 6; 
var PH_USERNAME                                = 7;
var PAGE_TTL_LOGIN_ON                          = 8;
var PAGE_TTL_HOME                              = 9;
var PH_PASSWORD                                = 10;
var LBL_SELECT_LOCATION                        = 11;
var BTN_GO                                     = 12;
//var BTN_CONTINUE                               = 13;
var TEXT_INCOMPLETE_PROFILE                    = 14;
var PH_FIRST_NAME                              = 15;
var PH_LAST_NAME                               = 16;
var PH_EMAIL                                   = 17;
var PH_PHONE                                   = 18;
var TEXT_LINKED                                = 19;
var LBL_ADMINISTRATOR                          = 20;
var LBL_STRENGTH                               = 22;
var LBL_NUTRITION                              = 23;
var LBL_GROUP_MESSAGES                         = 24;
var LBL_All_CLIENTS                            = 25;
var TEX_RIGHT                                  = 26;
var TEX_GET_IMAGE                              = 27;
var TEX_ACTIVE                                 = 28;
var TEX_NON_ACTIVE                             = 29;
var PH_EXTRA_PARAMETERS                        = 30;
var BTN_SAVE                                   = 31;
//var PH_SEARCH                                = 32;
var BTN_MEMBER_LIST                            = 33;
var BTN_CARDIO_TEST                            = 34;
var BTN_BODY_COMP                              = 35;
var BTN_FLEXIBILITY_TEST                       = 36;
var BTN_REPORTS                                = 37;
var BTN_OVERTRAINING                           = 38;
var BTN_CANCEL                                 = 39;
var BTN_MACHINES                               = 40;
var BTN_EXERCISES                              = 41;
var BTN_ACTIVITIES                             = 42;
var BTN_DUMBELLS                               = 43;
//var PAGE_TTL_LOGOUT                            = 44;
var PAGE_TTL_MANAGE_MEMBER                     = 45;
var PAGE_TTL_MANAGE_TEST                       = 46;
var PAGE_TTL_MANAGE_REPORTS                    = 47;
var PAGE_TTL_MANAGE_NEWMACHINESEXERCISE        = 48;
var PAGE_TTL_MANAGE_POINT                      = 49;
var PAGE_TTL_MANAGE_CALENTER                   = 50;
var PAGE_TTL_MANAGE_MESSAGES                   = 51;
var PAGE_TTL_CONTACT                           = 52;
var TEX_SEARCH                                 = 55;
var BTN_TEST_PAGE                              = 56;
//var BTN_REPORT_PAGE                            = 57;
//var BTN_MESSAGES_PAGE                          = 58;
var PAGE_TTL_MANAGE_MEMBER_DETAILS             = 59;
var BTN_START_NEW_TEST                         = 60;
var PAGE_TTL_MANAGE_BODYCOMPOSITION            = 61;
var BTN_BODYCOMPOSITION                        = 62;
var BTN_FLEXIBILITY                            = 63;
var BTN_CARDIO                                 = 64;
//var BTN_STRENGTH                               = 65;
var LBL_TEST_DATE          	                   = 66;
var LBL_TEST_METHOD          			       = 67;
var LBL_GENDER                                 = 68;
var LBL_AGE          			               = 69;
var LBL_HEGHT          			              = 70;
var LBL_WEIGHT          			          = 71;
var LBL_BODY_FAT               		          = 72;
var LBL_BMI          		   	               = 73;
var LBL_BELLYGROWTH          	   	           = 74;
var LBL_FATFREEWEIGHT          	        	   = 75;
var LBL_PREOFESSION          		           = 76;
var LBL_ACTIVITY          		    	       = 77;
var LBL_SPORTS          		    	       = 78;
var LBL_LIFESTYLE          		    	       = 79;
var LBL_STRESS          		    	       = 80;
var LBL_LABORCHARGE_PER          		       = 81;
var LBL_SPORT_CHARGEE          		    	   = 82;
var LBL_LABORCHARGE          		           = 83;
var LBL_BASICMETABOLISM          	 	       = 84;
var LBL_SPORTS_SPECIFIC              	       = 85;
var LBL_ENERGY          		    	       = 86;
var LBL_NEXT          		    	           = 87;
var LBL_FREQWEEK          		    	       = 88;
var LBL_INTENSITY          		    	       = 89;
var LBL_AGE_TEST          		    	       = 90;
var LBL_WEIGHT_TEST          		    	   = 91;
var LBL_TEST_LEVEL          		    	   = 92;
var LBL_TEST_OPTION          		    	   = 93;
var LBL_COACH                		    	   = 94;
var BTN_NEXT_ARROW                		       = 95;
var BTN_SAVE_START_TEST                	       = 96;
var PAGE_TTL_CARDIOTEST   					   = 97;
var TEX_WELCOME_STRONG	     				   = 98;
var PH_ENTER_CLIENT_CODE	     	     	   = 99;
var PH_SEARCH_CLIENT	     	     	       = 100;
var PH_REFRESH_BTN   	     	     	       = 101;
var LBL_CODE_LABEL                             = 102;
var BTN_GOOD                                   = 103;	
var BTN_FLEX_LEVE                              = 104;	
var BTN_FLEX_CAT                               = 105;	
var BTN_CALFS                                  = 106;	
var BTN_ILLIPSOAS                              = 107;	
var BTN_HAMSTRINGS                             = 108;	
var BTN_BREAST                                 = 109;	
var BTN_QUADRICEPS                             = 110;	
var BTN_REMARKS                                = 111;	
var TEXT_FORGOTPASSWORD                        = 112;	
var LBL_CLOSE                                  = 113;
var LBL_SEND                                   = 114;
var PAGE_TTL_FORGOTPASSWORD                    = 115;
var EXCP_UNDER_CONSTUCTION_PAGE                = 117;
//var EXCP_INVALID_CAPTCHA_ERROR                 = 118;
var EXCP_INVALID_CLICK_EVENT_MSG               = 119;
var EXCP_NO_MEMBER_MAPPED                      = 120;
var EXCP_DEL_COFRM_MSG                         = 121;
//var CONFIRM_SUBMIT_DUPLICATETEST               = 122;
var LBL_STRENGTH_WEEK						   = 122;
var PAGE_TTL_STRENGTH_PROGRAM_OVERVIEW 		   = 123;
//var LBL_NAME								   = 124;
var LBL_IMAGE_URL						       = 125;
var LBL_MOVEI_URL						       = 126;
var LBL_CLEAR      						       = 127;
var TXT_EXIT      						   	   = 128;
var LBL_OUT_OF_ORDER      				       = 129;
var LBL_USE_MIX_TRAINING_ALLOW      	       = 130;
var LBL_USE_CIRCUIT_TRAINING_ALLOW		       = 131;
var LBL_USE_FREE_TRAINING_ALLOW                = 132;
var LBL_MOVESMART_CERTIFIET     		       = 133;
var LBL_ONLY_ALLOWED_WITH_MACHINES		       = 134;
var LBL_ENTER_URL						       = 135;
var PH_ENTER_URL						       = 136;
var LBL_SELECT								   = 137;
var LBL_MEMBER_ID							   = 138;
var LBL_PERSONAL_ID							   = 139;
//var TXT_MALE							   	   = 140;
//var TXT_FEMALE							       = 141;
var LBL_CLUB_ORG							   = 142;
//var LBL_EMAIL								   = 143;
var LBL_ACTION								   = 144;
var LBL_CURRENT_TEST						   = 145;
var LBL_ANALYSED							   = 146;
var LBL_REPORTS								   = 147;
var PAGE_TTL_SETTINGS					       = 148;
//var PAGE_TTL_STRENGTH						   = 149;
var PAGE_TTL_HEALTHCHECK			           = 150;
var LBL_CALCULATE_AUTOMETICALY	               = 151;
var LBL_STRENGTHTEST			               = 152;
//var BTN_REGISTER                               = 153;
//var LBL_REGISTER_MANUALLY_OR_FACEBOOK          = 154;
//var BTN_FACEBOOK                               = 155;
//var TEX_SECURITY_QUESTIONS                     = 156;
//var TEX_ANSWER_SEQURITY_QUESTION               = 157;
//var PH_BIRTHDAY                                = 158;
//var PH_LENGTH                                  = 159;
//var MSG_INFO_PHASE_2_BEFORE_TEST               = 161;
var PH_STREET                                  = 162;
var PH_NUMBER                                  = 163;
var PH_BUS                                     = 164;
var PH_ZIPCODE                                 = 165;
var TEX_PLACE                                  = 166;
//var BTN_NEXT_ARROW                             = 167;
var TXT_COUNTRY                                = 168;
//var PH_TYPE_HERE                               = 169;
var TEX_YES                                    = 170;
var TEX_NO                                     = 171;
//var PH_SELECT_TEST_DAY                         = 172;
//var PH_AMOUNT                                  = 173;
//var PH_DATE_ENTRY                              = 174;
//var LBL_SIGNATURE_FULL_NAME                    = 175;
//var BTN_OK                                     = 176;
//var LBL_SELECT_ACTIVITY                        = 177;
//var LBL_MONTHLY                                = 178;
//var LBL_WEEKLY                                 = 179;
//var LBL_AVERAGE_TIME                           = 180;
var PH_MIN                                     = 181;
//var BTN_ADD_NEXT_ACTIVITY                      = 182;
//var PH_NAME_GP                                 = 183;
var PAGE_TTL_USER_PROFILE    				   = 184;
/*selva*/
//var LBL_HEAVY_PHY_LABOUR                       = 186;
//var LBL_SEDENTARY_JOBS                         = 188;
//var LBL_EXCELLENT                         	   = 189;
//var LBL_AVERAGE                                = 191;
//var LBL_POOR                                   = 192;
//var LBL_BAD                                    = 193;
//var LBL_LIGHT_PHYSICAL_LABOUR                  = 194;
//var LBL_DECLARE_FOR_REAL_READ                  = 195;
var PLEASE_ENTER_USERNMAE   			  	   = 196;
var PLEASE_ENTER_PASSWORD   			  	   = 197;
var LOGIN_USERNAMEPASSWORD_INCORRECT   		   = 198;
var USERNAMEPASSWORD_INCORRECT				   = 199;
var CONFIRM_SUBMIT_DUPLICATETEST			   = 200;
//var TXT_MANDATORY 							   = 201;
var MENU_MANAGE_MEMBERS						   = 202;
var MENU_HEALTH_CHECK						   = 204;
var MENU_MESSAGE						   	   = 206;
var MENU_AGENDA_CALLENTER					   = 207;
var MENU_SETTINGS						   	   = 208;
var MENU_SWITCH_USER					   	   = 209;
//var MSG_SUCCESS_CLIENT_ADD					   = 215;
var TXT_GALLERY								   = 232;
var TXT_CAMERA								   = 233;
var TXT_TESTS								   = 238;
var TXT_LOAD_WAITING_MSG                       = 254;
var TXT_AMOUNT_TRAINING_DONE				   = 260;
var TXT_SELECTED_TRAINING_PROGRAM			   = 261;
var TXT_NEW_MACHINES_EXERCISSES_PROGRAM	       = 262;
var TXT_POINTS								   = 263;
var EXCEPTION_DATACONNECTION_LOST              = 264;
var EXCEPTION_UNEXPECTED_PROCESSING_ERROR      = 265;
var MSG_SUCCESS_PROFILE_UPDATED                = 266;
var TXT_CONTACT								   = 269;
var TXT_STRENGTH_OVERVIEW					   = 270;
var TXT_CLUB_NAME							   = 271;
//var BTN_STRENGTH_PROGRAM					   = 272;
var TXT_CURRENT_FIT_LEVEL					   = 273;
var TXT_IMPROVED					 		   = 274;
var TXT_WEEKS_OF_TRAINING					   = 275;
var TXT_START_FIT_LEVEL					       = 276;
var TXT_FIT_LEVELS					           = 277;
var TXT_START_WEGHT                            = 278;
var EXCEPTION_CLIENT_PROFILE_INCOMPLETE	       = 280;
var TXT_FAT					                   = 281;
var TXT_START_FAT					           = 282;
var TXT_ADD_MEMBER					           = 283;
var TXT_FAT_FREE_WEIGHT					       = 284;
var TXT_TIME					               = 285;
var TXT_TEST					      		   = 286;
var TEX_NO_DEVICE_MAPPED			  		   = 287;
var TEX_DEVICE_MAPPED					       = 288;
var TXT_GETIMAGE					     	   = 289;
var TXT_GETMOVIE					     	   = 290;
var TXT_DESCRIPTION					     	   = 291;
var PAGE_TTL_FUNCTIONALITY_TEST_LIST           = 292;
var TXT_LEFT                                   = 293;
var TXT_RIGHT                                  = 294;
var BTN_NONE					       		   = 295;
var BTN_LIGHT					      		   = 296;
var BTN_MIDDLE					      		   = 297;
var BTN_HEAVY					      		   = 298;
var BTN_REGULAR					      		   = 299;
var BTN_UNREGULAR					      	   = 300;
var BTN_CARDIOWEEK					      	   = 301;
var BTN_STH_FREE_TRAINING					   = 302;
var TXT_TRAINING_ORG_VALUES					   = 303;
var TXT_SAVE_NEXT_TRAINING					   = 304;
var TXT_SHOW_WEIGHTS					       = 305;

var EXCP_MANADATORY_ERROR                      = 306;
var SUCCESS_SAVE_CARDIO_TEST                   = 307;
//var BTN_VIEW_PROFILE                           = 308;
//var TXT_GET_MOVIE                              = 309;
//var TXT_GET_IMAGE                              = 310;
var TXT_TOESTEL					               = 311;
var TXT_STARTWEEK					           = 312;
var TXT_CHANGE_SCHDULE					       = 313;
var TXT_CHANGE_SHOW					           = 314;
var TXT_ADD_MACHINE					           = 315;
var TXT_REMOVE_MACHINE					       = 316;
var TXT_REMOVE_NEXT					           = 317;
var BTN_IMAGES					           	   = 318;
var BTN_LIST					               = 319;
var BTN_EDIT					               = 320;
var BTN_ADD					                   = 321;
var BTN_DETETE					               = 322;
var LBL_NAME					               = 323;
var LBL_BRAND					               = 324;
var LBL_TYPE					               = 325;
var LBL_GROUP					               = 326;
var LBL_SUBTYPE					               = 327;
var LBL_STATUS					               = 328;
var LBL_CLUBN					               = 329;
var TXT_TRAINING_ONE					       = 330;
var TXT_TRAINING_TWO					       = 331;
var TXT_TRAINING_THREE					       = 332;
var TXT_TRAINING_FOUR					       = 333;
var TXT_TRAINING_FIVE					       = 334;
var TXT_TRAINING_SIX					       = 335;
var BTN_COOLDOWN     					       = 336;
var BTN_STOP     			     		       = 337;
var BTN_START     			     		       = 338;
var TXT_BODY_COMPOSITION_GRAPH       	       = 339;
var TXT_EVOLUTION_LIST              	       = 340;
var TXT_CARDIO_GRAPH                	       = 341;
var TXT_TEST_LEVEL_WATT                	       = 342;
var PAGE_TTL_MANAGE_BODYCOMPLIST			   = 343;
var PAGE_TTL_ADD_MEMBER			               = 344;
var PAGE_TTL_STRENGTH_PRGM_MAPPING			   = 345;
//var PAGE_TTL_COACH			                   = 346;
var TXT_ENTER_EMAIL			                   = 347;
/*PK - Added 2016 March 12*/
var LBL_START_LEVEL			                   = 348;
var LBL_RMP      			                   = 349;
var LBL_WATT      			                   = 350;
var LBL_TEST_EQUIPMENT                         = 351;
var LBL_HRM      			                   = 352;
/*PK - Date 15 March 2016*/
var LBL_READY    			                   = 353;
var LBL_TO_BE_ANALYSED    			           = 354;
var BTN_SIMULATE_HR_DATE    		           = 355;
var BTN_SHOW_RESULT    			               = 356;
var BTN_SHOW_LOAD_VALUE     	               = 357;
var LBL_FULL_DATE            	               = 358;
var LBL_TRIM            	                   = 359;
var LBL_JUMP            	                   = 360;
var LBL_HR               	                   = 361;
//var MSG_VALID_UK_POSTCODE                      = 362;
var BTN_SAVE_CHANGE_MACHIEN                    = 363;
var TXT_SLT_MACHINE                            = 364;
var LBL_HEART_RATE_GRAPH                       = 365;
var LBL_CAT_HEART_RATE_GRAPH                   = 366;
//var EXCEPTION_SELECT_DEVICE_ERROR              = 367;
//var EXCEPTION_INVALID_TEST_SELECTED            = 368;
var EXCEPTION_CONNECT_MACHINE                  = 369;

/*2016-03-17*/
//var MSG_NO_MEMBER_TEST_EXIST                   = 377;
var PH_ENTER_CHART_ANALYSIS                    = 378;
//var TXT_CAT_HEARRATE_Graph                     = 379;
var EXCEPTION_SELECT_VALID_USER                = 380;
//var BTN_HEARTRATE_DEVICE                       = 381;

var LBL_SUBJECT  							   = 403;
var LBL_ROUQUART							   = 404;
var LBL_NEW_MESSAGE							   = 405;
var LBL_SEND_SMS							   = 406;
var LBL_ATTACH_FILES					       = 407;
var LBL_SEND_MAIL    					       = 408;
var LBL_SEND_TO    					           = 409;

/*PK add(20160502)*/
var TXT_ENTER_SUBJECT						   = 422;
var TXT_ENQUIRY     						   = 423;
var TXT_CONTACT_US     						   = 424;
var BTN_ANALYSE_TEST    					   = 426;
var BTN_READY_TO_PRINT    					   = 427;
var BTN_REANALYSE    					       = 428;
var BTN_PRINT    					           = 429;
var LBL_MESSAGE    					           = 430;
var LBL_MAIL_PHONE    					       = 431;
var LBL_DATE_TIME    					       = 432;
var BTN_ALLMESSAGE    					       = 433;
var BTN_TODAY    					      	   = 434;
var BTN_2DAYS    					      	   = 435;
var BTN_3DAYS    					      	   = 436;
var BTN_INTERNALMSG    					       = 437;
var BTN_BACKOFFICE    					       = 438;
var BTN_SMS    					       		   = 439;
var BTN_WEB    					      		   = 440;
var BTN_INTERNAL   					     	   = 441;
var BTN_MARTINE   					     	   = 442;
var BTN_ALLCOACHES   					       = 443;
var BTN_IVO   					     	 	   = 444;
var BTN_PETERTEST   					       = 445;
var BTN_MAXTESTER   					       = 446;
var BTN_ROBPROPPER   					       = 447;
var LBL_TESTMAIL   					     	   = 448;
var TXT_FITNESS                                =491;
var TXT_POWER                                =492;
var TXT_AGILITY                                =493;



//var USER_SELECT_NONE		=	0;
var USER_SELECT_CARDIOTEST	=	1;
var USER_SELECT_STRENGTHTEST=	2;

var PATH_VIEWS	=	'src/views/';
/*Confirmation Exceptions*/
//var CONFIRM_SUBMIT_DUPLICATETEST	=	"No changes made. Do you wish to continue to save?";

/*Allows Exception*/
//var EXCEPTION_CLIENT_PROFILE_INCOMPLETE	=	"This person profile is incomplete. Profile need to be completed before doing the test.";
//var EXCEPTION_INCOMPLETE_PROFILE	=	"Profile is incomplete. Please continue to complete.";
//var EXCEPTION_PAGE_TOBE_ADDED		=	"Yet to receive the design.";
//var EXCEPTION_CAPTCHA_INVALID		=	"Invalid captcha please re-try";
//var EXCEPTION_EMPTY_CLICK_EVENT		= 	"Please select at least one element from list";
//var EXCEPTION_NOMEMBER_CARDIOTEST 	=	"No member added to the test";
//var EXCEPTION_FOR_DELETE_ITEMS		= 	"Are you sure you want to delete this item ?";
/*Allows Exception - Ends*/
var TEST_TYPE_CARDIO 	= 6;
var TEST_TYPE_BODYCOMP	= 7;
var TEST_TYPE_FLEXIBLITY= 8;

var AJAX_STATUS_TXT_SUCCESS = "success"; 
//var AJAX_STATUS_TXT_ERROR	= "error";

var UPLOAD_TYPE_USER_PROFILE = 1;  //MK Upload IMAGES for User Profile image
var UPLOAD_TYPE_MACHINE_IMAGE = 2;  //MK Upload IMAGES for User Profile image

/* Total Test Levels */
//var TOTAL_LEVEL = 5;

/*BODY COMPOSITION STRINGS */
var GET_OPTIONSTRING		=	{};
GET_OPTIONSTRING[31]		=	{1:'None',2:'Light',3:'Middle',4:'Heavy'};
GET_OPTIONSTRING[32]		=	{1:'None',2:'Light',3:'Middle',4:'Heavy'};
GET_OPTIONSTRING[33]		=	{1:'None',2:'Light',3:'Middle',4:'Heavy'};
GET_OPTIONSTRING[34]		=	{1:'Regular',0:'Un-Regular'};
GET_OPTIONSTRING[35]		=	{'Yes':'Yes','No':'No'};
/*SN Added Need To Change once*/
var TEST_TYPE_BODYCOMP_WEIGHT = 24;

 /* showing list of Dynamic menu page */
/*Menu Items*/
var MENU_ITEM_MANAGE_MEMBERS						=	12;
var MENU_TESTS								=	13;
var MENU_REPORTS							=	14;
var MENU_NEW_MACHINES_EXERCISSES_PROGRAMS	=	15;
var MENU_POINTS								=	16;
var MENU_CALENDAR							=	17;
var MENU_MESSAGES							=	18; 
var MENU_CONTACT							=	19;
//var MENU_STRENGTHOVERVIEW					=	20;

var MENU	=	{};
MENU[MENU_ITEM_MANAGE_MEMBERS]	=						{icon:'images/icon3.png',template:'manage-member',title:'MANAGE MEMBERS'};
MENU[MENU_TESTS]	=								{icon:'images/icon4.png',template:'cardio-test',title:'TESTS'};
MENU[MENU_REPORTS]	=								{icon:'images/icon5.png',template:'manage-reports',title:'REPORTS'};
MENU[MENU_NEW_MACHINES_EXERCISSES_PROGRAMS]	=		{icon:'images/icon6.png',template:'manage-newmachinesexerciseprogram',title:'NEW MACHINES EXERCISSES PROGRAMS'};
MENU[MENU_POINTS]	=								{icon:'images/icon7.png',template:'manage-points',title:'POINTS'};
MENU[MENU_CALENDAR]	=								{icon:'images/icon8.png',template:'manage-calender',title:'CALENDAR'};
MENU[MENU_MESSAGES]	=								{icon:'images/icon10.png',template:'manage-messages',title:'MESSAGES'};
MENU[MENU_CONTACT]	=								{icon:'images/icon9.png',template:'manage-contact',title:'Contact'};
//MENU[MENU_STRENGTHOVERVIEW]	=						{icon:'images/icon9.png',template:'strength-program',title:'Strength Program'};

/*MK - Device Types*/
var DEVICE_TYPE_HR_monitor	=	1;
/*MK - Cardio Test Monitor*/
//var CARDIO_TEST_RUN			=	0;
//var CARDIO_TEST_COMPLETED	=	1;
//var CARDIO_TEST_CHANGED		=	2;

var TEST_STATUS_YETTO_START	=	0;
var TEST_STATUS_STARTED		=	1;

var CARDIO_TEST_STATUS_stopped	=	0;
var CARDIO_TEST_STATUS_started	=	1;
var CARDIO_TEST_STATUS_cooldown	=	2;
var CARDIO_TEST_STATUS_completed=	3;
var CARDIO_TEST_STATUS_Editmode	=	40;
var CARDIO_TEST_STATUS_Reset	=	50;

var TITEMS_FAT_PERCENTAGE	=	28;
var TITEMS_FAT_FREE_WEIGHT	=	29;

//var STREGNTH_TESTTYPE_AUTO	=	0;//Automatic Calculation
//var STREGNTH_TESTTYPE_MANUAL=	1;//Manual Stregnth Test

/*Calculate strength Level*/

var FITLEVEL_PERCENTAGE = 80;
var STRENGTH_PERCENTAGE = 10;
var AGILITY_PERCENTAGE = 10;

/*Program Type*/
var FREE_PGM = 1;
var CIRCUIT_PGM = 2;
var MIX_PGM = 3;