$(function(){
        _setPageTitle("Add Machine");
        var urldata	=	getPathHashData();
        //var clubId	=	getselectedclubid();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var flag=	(urldata.args.flag) ? base64_decode(urldata.args.flag) :0;
        var programid	=	(urldata.args.programid && urldata.args.programid!='undefined') ? base64_decode(urldata.args.programid) :0;
        var $removemachinedata = $("button.removemachinedata");
    $removemachinedata.hide();
        if(flag == 1){
            _setPageTitle("Remove Machine");
            $removemachinedata.show();
            $("button.addMachinedata").hide();
        }
        /*get Machinelistdata by program id and member follwed by localstorage*/
        if(programid){
            _getMappedMachienbyProgramId(programid,memberId,function(response){
                $(".programtabel").find('tbody').html(response);
            },0);
        }
});

function saveStrengthProgramData(){
    logStatus("Calling saveStrengthProgramData click event",LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var programid	=	(urldata.args.programid && urldata.args.programid!='undefined') ? base64_decode(urldata.args.programid) :0;
        var checkval = [];
        $("input[type='checkbox']:checked").each(function(){
            var checkedval = $(this).attr("hrefvalue");
            checkval.push({checkedval:checkedval,value:0});
        });
        console.log(JSON.stringify(checkval));
        addOrRemoveNewMachineDetails(checkval,programid,function(response){
            //alert(JSON.stringify(response));
        });
    } catch(e){
        ShowExceptionMessage("saveStrengthProgramData",e);
    }
}

function removeStrengthProgramData(){
    logStatus("Calling removeStrengthProgramData click event",LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var programid	=	(urldata.args.programid && urldata.args.programid!='undefined') ? base64_decode(urldata.args.programid) :0;
        var checkval = [];
        $("input[type='checkbox']:checked").each(function(){
            var checkedval = $(this).attr("hrefvalue");
            checkval.push({checkedval:checkedval,value:0});
        });
        console.log(JSON.stringify(checkval));
        removeMachineformprogramid(checkval,programid,function(response){
            //alert(JSON.stringify(response));
        });
    } catch(e){

    }
}
