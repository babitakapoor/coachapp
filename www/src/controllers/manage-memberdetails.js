	$(function(){

    _setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_MEMBER_DETAILS));
    TranslationManageMemberDetails();
    //$(".newtest").text(GetLanguageText(BTN_START_NEW_TEST));
    var urldata	=	getPathHashData();
    var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    var testid	=	(urldata.args.testid && urldata.args.testid!='undefined') ? base64_decode(urldata.args.testid) :0;
    var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
	 $('.consult_dropdownformenus').html('<button class="selectbtn everydropdown" onclick="checkLastProgressTest('+memberId+',this,1)" hrefvalue="'+memberId+'">Menu</button>');
    $('#bodycomptostionuserdata').val(memberId);
    $('#idbodycomptiontestdata').val(testid);
    $('#mid').val(urldata.args.mid);
    $('#testid').val(urldata.args.testid);
    $('#clubid').val(urldata.args.clubid);
  
    _getClientInformationByID(memberId,function(response){
        if(response.last_name) {
            $(".memberName").html(response.first_name + ' '  + response.last_name);
			$(".memberEmail").html(response.email);
			$(".memberPhone").html(response.phone);
            if(response.userimage!=''){
                getValidImageUrl(PROFILEPATH + response.userimage,function(url){
                    $('#memberImage').attr('src',url);
                });
            }
        }
        var STRENGTH_LEVEL = [];
        var memberAge	=	getAgeByDOB(response.dob);
        var gender = response.gender;
        getStrengthLevel(memberAge,gender,memberId,function(result){
                if(result.status==1){
                   var data = result.response;
                   var agilitylevel = result.flexlevel;
                    $('.curstrengthlevl').html(data);
                    $('.curagilitylevel').html(agilitylevel);
                  
                }
        });
   
        //getNewTestdata(mid,testid,clubid);
        /**
         * @param summary.count This is  summary count
         * @param summary.analysed This is  summary analysed
         * @param summary.improvements This is  summary improvements
         * @param summary.bc This is  bc
         * @param summary.cardio This is  cardio
         * @param summary.strength This is  strength
         * @param improvements.begining This is  begining
         * @param analysed.cardio.na This is analysed cardio na
         * @param improvements.begining.fl This is improvements begining fl
         * @param improvements.current.wt This is improvements current wt
         */
		 
		/* checking the responsemovesmart***/
          var callresponse =function(response,leveltype)
		  {
			    console.log("Please get the response here");
			    console.log(response);
			   if(leveltype=='movesmart')
			   {
				     $('.curntfitnesslevel').html(response.movesmart.cardio_fitness);
			         $('.curstrengthlevl').html(response.movesmart.strength);
			         $('.curagilitylevel').html(response.movesmart.agility_flexbility);
					 $('.movelevel img').attr('src','images/piramide'+response.movesmart.movesmart_total_level+'.png');
				   
				   
			   }
			   else if(leveltype=='eatfreash')
			   {
				     $('.bmi').html(response.Eat.body_composition);
			         $('.vat').html(response.Eat.viscreal_fat);
					 if($.isNumeric(response.Eat.Cholesterol))
					 {
						  $('.colestrol').html(response.Eat.Cholesterol);
					 }
					 else
					 {
						  $('.colestrol').html(response.Eat.Cholesterol.replace(/[^0-9\.]+/g, ""));
					 }
					 $('.glucose').html(response.Eat.Glucose);
					 $('.eats img').attr('src','images/piramide'+response.Eat.eat_total_level+'.png');
			   }
			   else if(leveltype=='mindswitch')
			   {
				   
				    $('.vita16').html(response.Mind_Switch.vita_16);
			         $('.slaap').html(response.Mind_Switch.sleep);
			         $('.stress').html(response.Mind_Switch.stress);
					  $('.mindful').html(response.Mind_Switch.mindful);
					 $('.switch img').attr('src','images/piramide'+response.Mind_Switch.Mind_switch_total_level+'.png');
				   
			   }
			  
		  
		  }
        getTestSummaryByMember({userId:memberId},function(response){
			
			console.log("gettin the level values for movesmart");
			 var postData ={};
			 var getData  ={};
			 
			 getData.is_reporting =base_url+"/reporting/index.php/report/movesmart";
			 getData.user_id = memberId;
			do_service(getData,getData,function(response){
            if(callresponse){
                callresponse(response,'movesmart');
            }
        },'json');
		 getData.is_reporting =base_url+"/reporting/index.php/report/eat";
		do_service(getData,getData,function(response){
           if(callresponse){
                callresponse(response,'eatfreash');
            }
        },'json');
		 getData.is_reporting =base_url+"/reporting/index.php/report/mind_switch";
			do_service(getData,getData,function(response){
            if(callresponse){
                callresponse(response,'mindswitch');
            }
        },'json');
            if(response.status==1){
                var summary	=	response.summary;
                var counts	=	summary.count;
                var analysed=	summary.analysed;
                var improvements	=	summary.improvements;
                $("#bdcmpcurrenttests").val(counts.bc);
                $("#flexcurrenttests").val(counts.flex);
                $("#cardiocurrenttests").val(counts.cardio);
                $("#strngthcurrentest").val(counts.strength);

                
                $("#cardiotest_tobeanalysed").val(GetLanguageText(LBL_TO_BE_ANALYSED));
                $("#cardiotest_analysed").val(GetLanguageText(BTN_READY_TO_PRINT));
                $("#ctest_na").html(analysed.cardio.na);
                $("#ctest_a").html(analysed.cardio.a);
                var sFitLevel	=	(improvements.begining && improvements.begining.fl) ? improvements.begining.fl: 'N/A';
                var cFitLevel	=	(improvements.current && improvements.current.fl) ? improvements.current.fl: 'N/A';
                var flRemines	=	(cFitLevel!='N/A' && cFitLevel!='N/A') ? (sFitLevel-cFitLevel) :'N/A';
                var cWeight		=	(improvements.current && improvements.current.wt) ? improvements.current.wt: 'N/A';
                var sWeight		=	(improvements.begining && improvements.begining.wt) ? improvements.begining.wt: 'N/A';
                var impWeight	=	(cWeight!='N/A' && sWeight!='N/A') ? (sWeight-cWeight):'N/A';
                var cFatPercent	=	(improvements.current && improvements.current['fat_'+TITEMS_FAT_PERCENTAGE]) ? improvements.current['fat_'+TITEMS_FAT_PERCENTAGE]:'N/A';
                var sFatPercent	=	(improvements.current && improvements.begining['fat_'+TITEMS_FAT_PERCENTAGE]) ? improvements.begining['fat_'+TITEMS_FAT_PERCENTAGE]:'N/A';
                var impFatPercent=	(sFatPercent!='N/A' && cFatPercent!='N/A') ? (sFatPercent-cFatPercent):'N/A';

                var cFatFreeWt	=	(improvements.current &&  improvements.current['fat_'+TITEMS_FAT_FREE_WEIGHT]) ? improvements.current['fat_'+TITEMS_FAT_FREE_WEIGHT]:'N/A';
                var sFatFreeWt	=	(improvements.current &&  improvements.begining['fat_'+TITEMS_FAT_FREE_WEIGHT]) ? improvements.begining['fat_'+TITEMS_FAT_FREE_WEIGHT]:'N/A';
                var impFatFreeWt=	(sFatPercent!='N/A' && cFatPercent!='N/A') ? (sFatPercent-cFatPercent):'N/A';

                $('.curntfitnesslevel').html(cFitLevel);
                $('.imporivedfitlevel').html(flRemines);
                $('.lastfitlevel').html(sFitLevel);
                $('.user_weight').html(cWeight);
                $('.laststartweight').html(sWeight);
                $('.improvedweight').html(impWeight);
                $(".currentfatweight").html(cFatPercent);
                $(".lastfatweight").html(sFatPercent);
                $(".improvedfatpercent").html(impFatPercent);
                $(".currfatfreeweight").html(cFatFreeWt);
                $(".improvedfatfreeweight").html(sFatFreeWt);
                $(".lastfatfreeweight").html(impFatFreeWt);

            }
            var levelimg = getLevelImage();
            $('.movelevel img').attr('src',levelimg);
            $(".consu_center li").on("click", function(){
			var getData ={};
			getData._tk= "hjghd";
			getData.user_id = memberId;
			getData.type_id = $(this).attr('data-id'); 
			getData.type_ids = $(this).attr('data-cat');
			getData.level   =  $(this).find('span').text().replace(/[^0-9\.]+/g, "");
			getData.item_id = $(this).attr('data-type');
			getData.is_reporting =base_url+"/reporting/index.php/report/level_description";
			if(getData.type_ids=="eat"){
				
				getData.type_ch	= $(this).attr('data-ch');
					if(getData.type_ch=="bc")
					{
						$('.popupcst_right').empty();	
						getData.is_reporting =base_url+"/reporting/index.php/report/eat_level";
						do_service(getData,getData,function(response){
						$('.popupcst_right').html(response.body_string);
						},'json');
						$(".popupcst_left span ").html("");
						$(".popupcst_left h4 ").html("");
						var slecttext_span = $(this).find("span").text();
						var slecttext_h4 =  $(this).find("h4").text();
						$(".popupcst_left").find("span").html(slecttext_span);
						$(".popupcst_left").find("h4").html(slecttext_h4);
						$(".consult_bg").slideDown();
						$(".consult_popup").slideDown();
					}
					 else if(getData.type_ch=="vt")
					 {
						$('.popupcst_right').empty();
						getData.is_reporting =base_url+"/reporting/index.php/report/eat_level";
						do_service(getData,getData,function(response){
						
						$('.popupcst_right').html(response.visceral_string);
						},'json');
						$(".popupcst_left span ").html("");
						$(".popupcst_left h4 ").html("");
						var slecttext_span = $(this).find("span").text();
						var slecttext_h4 =  $(this).find("h4").text();
						$(".popupcst_left").find("span").html(slecttext_span);
						$(".popupcst_left").find("h4").html(slecttext_h4);
						$(".consult_bg").slideDown();
						$(".consult_popup").slideDown();
					 }
					 else if(getData.type_ch=="gl")
					 {
						$('.popupcst_right').empty();
						getData.is_reporting =base_url+"/reporting/index.php/report/eat_level";
						do_service(getData,getData,function(response){
							
						$('.popupcst_right').html(response.glucose_string);
						},'json');
						
						$(".popupcst_left span ").html("");
						$(".popupcst_left h4 ").html("");
						var slecttext_span = $(this).find("span").text();
						var slecttext_h4 =  $(this).find("h4").text();
						$(".popupcst_left").find("span").html(slecttext_span);
						$(".popupcst_left").find("h4").html(slecttext_h4);
						$(".consult_bg").slideDown();
						$(".consult_popup").slideDown();
					 }
					 else if(getData.type_ch=="cd")
					 {
						$('.popupcst_right').empty();
						getData.is_reporting =base_url+"/reporting/index.php/report/eat_level";
						do_service(getData,getData,function(response){
						
						$('.popupcst_right').html(response.cholestrol_desc);
						},'json');
						
						$(".popupcst_left span ").html("");
						$(".popupcst_left h4 ").html("");
						var slecttext_span = $(this).find("span").text();
						var slecttext_h4 =  $(this).find("h4").text();
						$(".popupcst_left").find("span").html(slecttext_span);
						$(".popupcst_left").find("h4").html(slecttext_h4);
						$(".consult_bg").slideDown();
						$(".consult_popup").slideDown();
					 }
					
				}
					else
				{
					$('.popupcst_right').empty();
					do_service(getData,getData,function(response){
						
					$('.popupcst_right').html(response.description);
					},'json');
					$(".popupcst_left span ").html("");
					$(".popupcst_left h4 ").html("");
					var slecttext_span = $(this).find("span").text();
					var slecttext_h4 =  $(this).find("h4").text();
					$(".popupcst_left").find("span").html(slecttext_span);
					$(".popupcst_left").find("h4").html(slecttext_h4);
					$(".consult_bg").slideDown();
					$(".consult_popup").slideDown();
				}						
            });

            $(".consult_closed, .consult_bg").on("click",function(){
                $(".consult_bg").slideUp();
                $(".consult_popup").slideUp();
            });


        });
    });

   // var agilitylevel = _getCacheArray('flexlevel');
  
  doFlexibilityCalculation();
    maincontent_leff_none();
    if(urldata.path=='consult'){
        maincontent_leftimage();

    }
  
 // movebodycomption();


$(document).ready(function(){
    traingSchema();
    });
function traingSchema()
{
logStatus("Calling traingSchema", LOG_FUNCTIONS);
        try {
        var postdata = {};
         var $idofuser = $('#idofuser');
         postdata.user_id   =   memberId;
         postdata.is_reporting=  base_url+"/reporting/index.php/reportpdf/training_schema";
         showstrenghtDatanew(postdata,function(response){
             logStatus("Calling traingSchema", LOG_FUNCTIONS);
                $('#personalgoal').html(response.goal); 
                $('#maxhf').html(response.target_credits.max_heart_rate);
				$('#iandhf').html(response.target_credits.iant_hr);
				$('#maxwattt').html(response.target_credits.max_load);
                $('#iandp').html(response.target_credits.iant_p);
                $('#prglvl img').attr('src','images/piramide'+response.movesmart.movesmart_total_level+'.png');
				
				$('#fietsen_fr').html(response.cycle_heart_zone_70); 
                //$('#fietsen_sec').html(response.cycle_heart_zone_70+'-'+ response.cycle_heart_zone_80); 
                $('#fietsen_thr').html(response.cycle_heart_zone_70 +'-'+ response.cycle_heart_zone_90); 
              //  $('#fietsen_frt').html(response.cycle_heart_zone_90 +'-'+ response.cycle_heart_zone_97);               

			   $('#watt_fr').html(response.iand_70); 
               // $('#watt_sec').html(response.iand_70 +'-'+ response.iand_80); 
                $('#watt_thr').html(response.iand_70 +'-'+ response.iand_90); 
               // $('#watt_frt').html(response.iand_90 +'-'+response.iand_97); 
				
                $('#credits_1').html(response.week_per_credits.week_1);
                $('#credits_2').html(response.week_per_credits.week_2); 
                $('#credits_3').html(response.week_per_credits.week_3); 
                $('#credits_4').html(response.week_per_credits.week_4); 
                $('#credits_5').html(response.week_per_credits.week_5); 
                $('#credits_6').html(response.week_per_credits.week_6); 
                $('#credits_7').html(response.week_per_credits.week_7); 
                $('#credits_8').html(response.week_per_credits.week_8); 
                $('#credits_9').html(response.week_per_credits.week_9); 
                $('#credits_10').html(response.week_per_credits.week_10); 
                $('#credits_11').html(response.week_per_credits.week_11); 
                $('#credits_12').html(response.week_per_credits.week_12);
				
                $('#run').html(response.hr_run.points_rate + ' credits<br>per 10 minuten<br>HSZ: '+ response.hr_run.hr_zone_a);
				
 				$('#cycle').html(response.hr_cycle.points_rate + ' credits<br>per 10 minuten<br>HSZ: '+ response.hr_cycle.hr_zone_a);
				
 				$('#swim').html(response.hr_swim.points_rate + ' credits<br>per 10 minuten<br>HSZ: '+ response.hr_swim.hr_zone_a);
				
 				$('#walk').html(response.hr_walk.points_rate + ' credits<br>per 10 minuten<br>HSZ: '+ response.hr_walk.hr_zone_a);
				
				$('#boat').html(response.hr_row.points_rate + ' credits<br>per 10 minuten<br>HSZ: '+ response.hr_row.hr_zone_a);
        });
    }
     catch(e) {
        ShowExceptionMessage("traingSchema", e);
    }
}
});

/***********************************
        Show Data Program
***********************************/

function TranslationManageMemberDetails(){
    TranslateText(".transstarnewtest", TEXTTRANS,BTN_START_NEW_TEST );
    TranslateText(".transcurrenttest", TEXTTRANS, LBL_CURRENT_TEST);
    TranslateText(".transanalysed", TEXTTRANS, LBL_ANALYSED);
    TranslateText(".transreports", TEXTTRANS, LBL_REPORTS);
    TranslateText(".transcrtfitlevel", TEXTTRANS, TXT_CURRENT_FIT_LEVEL);
    TranslateText(".transimproved", TEXTTRANS, TXT_IMPROVED);
    TranslateText(".transweekoftraining", TEXTTRANS, TXT_WEEKS_OF_TRAINING);
    TranslateText(".transstartfitlevel", TEXTTRANS, TXT_START_FIT_LEVEL);
    TranslateText(".transfitlevel", TEXTTRANS, TXT_FIT_LEVELS);
    TranslateText(".transwght", TEXTTRANS, LBL_WEIGHT);
    TranslateText(".transstartweight", TEXTTRANS, TXT_START_WEGHT);
    TranslateText(".transfat", TEXTTRANS, TXT_FAT);
    TranslateText(".transstartfatwas", TEXTTRANS, TXT_START_FAT);
    TranslateText(".transfatfreeweight", TEXTTRANS, TXT_FAT_FREE_WEIGHT);
    TranslateText(".transbodycomp", TEXTTRANS, BTN_BODYCOMPOSITION);
    TranslateText(".transflexibility", TEXTTRANS, BTN_FLEXIBILITY);
    TranslateText(".transcardioweek", TEXTTRANS, BTN_CARDIOWEEK);
    TranslateText(".transsthfreetraining", TEXTTRANS, BTN_STH_FREE_TRAINING);
    TranslateText(".transtoanalysed", VALTRANS, LBL_TO_BE_ANALYSED);
    TranslateText(".transtready", VALTRANS, LBL_READY);
    TranslateText(".transreadytoprint", TEXTTRANS, BTN_READY_TO_PRINT);
    TranslateText(".transfitness", TEXTTRANS, TXT_FITNESS);
    TranslateText(".transpower", TEXTTRANS, TXT_POWER);
    TranslateText(".transagility", TEXTTRANS, TXT_AGILITY);
}


/*MK Cardio Analysis Page*/
function moveCardioAnalysis(){
    logStatus("Calling moveCardioAnalysis",LOG_FUNCTIONS);
    try {
        var nacount	=	$.trim($("#ctest_na").html());
        var acount	=	$.trim($("#ctest_a").html());
        if((nacount!=0 && nacount!='') || (acount!=0 && acount!='')) {
            var memberid	=	GetUrlArg('mid');
            _movePageTo('cardio-analysis',{memberid:memberid});
        }
    } catch(e) {
        ShowExceptionMessage("moveCardioAnalysis", e);
    }
}
function getManageMemberTestData(action,_element,tab,pageindex){
    logStatus("Calling getManageMemberTestData", LOG_COMMON_FUNCTIONS);
    try{
        $('.tabsbox').on('click','span',function(){
             $('.tabsbox > span').removeClass("active");
             $(this).addClass("active");
         });
        _getManageMemberTestData(action,_element,tab,pageindex,function(response){
            $(_element).html(response);
            if(action=='bodyCompositionListGrid'){
                getManageBodycompositionGraph('bodyCompositionGraph','',tab,pageindex,function(respsonse){
                    var arrayBottomCaption = respsonse.dateGraph;
                    var arraySeries = respsonse.arraySeries;
                    var yAxisValue = respsonse.weightKgGraph;
                    var graphcnt = '<div class="accordion-holder">'
                                +'<div class="acc-row">'
                                    +'<h2 class="mt20">Graph<span class="icon icon-down"></span></h2>'
                                    +'<div class="acc-content">'
                                        +'<div class="compos-his-map">'
                                            +'<div id="weightFat" style="height: 400px;max-width:800px;margin: 0 auto;">';
                    graphOnload('',arrayBottomCaption,arraySeries,yAxisValue,'bodycomposition');
                    graphcnt+='</div>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                        +'</div>';
                });
            }if(action=='CardioListGrid'){
                getManageCardioGraph();
            }
        });
    }catch(e){
        ShowExceptionMessage("getManageMemberTestData", e);
    }
}
function  getManageCardioGraph(){
    logStatus("Calling getManageCardioGraph", LOG_COMMON_FUNCTIONS);
    try{
        var graphcnt = '<div class="accordion-holder">'
                                +'<div class="acc-row">'
                                    +'<h2 class="mt20">Graph<span class="icon icon-down"></span></h2>'
                                    +'<div class="acc-content">'
                                        +'<div class="compos-his-map">'
                                            +'<div id="weightFat" style="height: 400px;max-width:800px;margin: 0 auto;">';
                                            graphOnload('','','','','cardio');
                                        graphcnt+='</div>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                        +'</div>';
    }catch(e){
        ShowExceptionMessage("getManageCardioGraph", e);
    }
}
function getLevelImage(){
    logStatus("Calling getLevelImage", LOG_COMMON_FUNCTIONS);
    try {
        var curfitlevel = $('.curntfitnesslevel').html();
        var strengthlevel = $('.curstrengthlevl').html();
        var agilitylevel = $('.curagilitylevel').html();
        var corectfitval =((FITLEVEL_PERCENTAGE/100)*curfitlevel);

        var corectstrengthval = ((STRENGTH_PERCENTAGE/100)*strengthlevel);
        var corectagilityvel =((AGILITY_PERCENTAGE/100)*agilitylevel);
        var sum_of_level = Math.round(corectfitval+corectstrengthval+corectagilityvel);
        if (isNaN(sum_of_level)) {
                sum_of_level = 1;
            }
        var level = (sum_of_level==0) ?1:sum_of_level;
        var imgsrc = 'images/piramide'+level+'.png';
        return imgsrc;
    } catch(e){
        ShowExceptionMessage("getLevelImage", e);
    }
}

function registerHRmonitor(){
	logStatus("Calling registerHRmonitor", LOG_COMMON_FUNCTIONS);
	try {/*
		var urldata	=	getPathHashData();
		var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		console.log('sdfsdf');
		console.log(memberid);
		_getClientInformationByID(memberId,function(response){
			console.log('sdsds');
			console.log(response);

    });*/

	}	catch(e){
        ShowExceptionMessage("registerHRmonitor", e);
    }
}