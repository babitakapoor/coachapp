var _TotalSMSCharLimit	=250;
$(document).ready(function(){
    _setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_MESSAGES));
    TranslateGenericText();
    TransManageMessage();
    getManageMembersData('memberListGrid','#managetabledetails');
    setOption_AssociatedClubs("#users_club");
    $("#users_club").val(getselectedclubid());
    $(".templatecontent").hide();
    $(".templatecontent_list").show();
    maincontent_leff_none();
    $(window).resize(function(){
        //maincontent_leff_none();
    });
    loadMessageList(1);
    $(".templatecontent_sms .emailinfo").on("keydown",function(){
        var $this=	$(this);
        var runchars	=	$this.val().split('').length;
        var leftchar	=	(_TotalSMSCharLimit-runchars);
        leftchar	=	(leftchar<0) ? 0:leftchar;
        $(".charleft").html(leftchar + " char left");
        if(leftchar==0){
            return false;
        }
    });
    /*Quick Fix on click pagination*/
    $("#managetabledetails").find(".pagenation .pageindex").die("click").live("click",function(){
        var pageindex	=	$(this).attr("pageindex");
        getManageMembersData('memberListGrid','#managetabledetails','',pageindex);
    });

});
function loadMessageList(Page,onLoadSuccess){
    _getMessageInfo({page:Page},function(response){
        var HTML  ='';
                     HTML+='<div class="templatecontent">'+
                     '<table cellpadding="0" cellspacing="0">'+
                        '<tr>'+
                            '<th>'+GetLanguageText(LBL_MEMBER_ID)+'</th>'+
                            '<th>'+GetLanguageText(LBL_PERSONAL_ID)+'</th>'+
                            '<th>'+GetLanguageText(PH_FIRST_NAME)+'</th>'+
                            '<th>'+GetLanguageText(PH_LAST_NAME)+'</th>'+
                            '<th>'+GetLanguageText(LBL_TYPE)+'</th>'+
                            '<th>'+GetLanguageText(LBL_MESSAGE)+'</th>'+
                            '<th>'+GetLanguageText(LBL_MAIL_PHONE)+'</th>'+
                            '<th>'+GetLanguageText(LBL_DATE_TIME)+'</th>'+
                            '<th>'+GetLanguageText(LBL_ACTION)+'</th>'+
                        '</tr>';
        var bodycnt ='';
        /**
         * @param response.recordsfound This is recordsfound
         * @param response.messagelist This is message list
         * @param value.receiver_id This is receiver id
         * @param value.added_date This is added date
         */
        if(response.recordsfound >0){
                $.each(response.messagelist,function(key,value){
                    var d = new Date();
                    var month = d.getMonth()+1;
                    var day = d.getDate();
                    var currentdate = d.getFullYear() + '-' +
                        ((''+month).length<2 ? '0' : '') + month + '-' +
                        ((''+day).length<2 ? '0' : '') + day;
                     var date=(value.added_date);


                    var startDay = new Date(currentdate);
                    var endDay = new Date(date);
                    var diff = new Date(startDay - endDay);
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;

                    //var millisBetween = startDay.getTime() - endDay.getTime();
                    var days = diff / millisecondsPerDay;
                    var noofdays = Math.round(days);
                    var colorstyle = '';
                    if(noofdays==0){
                        colorstyle='todays';
                    }
                    if(noofdays==3){
                        colorstyle='threedays';
                    }
                    if(noofdays==2){
                        colorstyle='twodays';
                    }
                    if(value.type==5){
                        colorstyle='twodays';
                    }
                    if(value.type==4){
                        colorstyle='';
                    }
                    bodycnt+='<tr>'+
                            '<td><span class="internal '+colorstyle+'"></span>'+value.receiver_id+'</td>'+
                            '<td>'+value.first_name+'</td>'+
                            '<td>'+value.first_name+'</td>'+
                            '<td>'+value.last_name+'</td>'+
                            '<td>'+value.type+'</td>'+
                            '<td>'+value.message+'</td>'+
                            '<td>'+value.email+'</td>'+
                            '<td>'+value.added_date+'</td>'+
                            '<td><button class="selectbtn transselect">Select</button></td>'+
                        '</tr>';
                });
        }else{
            bodycnt+='<tr><td colspan="9">No Results Found</td></tr>';
        }
        HTML+=bodycnt+'<tr>'+
                   '<tr>'+
                        '<td colspan="9" class="footerlabel borderradius" >'+
                            '<span class="todaycol" >'+GetLanguageText(BTN_TODAY)+'</span>'+
                            '<span class="twodaycol" >'+GetLanguageText(BTN_2DAYS)+'</span>'+
                            '<span class="threedaycol" >'+GetLanguageText(BTN_3DAYS)+'</span>'+
                            '<span class="internalcol" >'+GetLanguageText(BTN_INTERNALMSG)+'</span>'+
                            '<span class="backofficecol" >'+GetLanguageText(BTN_BACKOFFICE)+'</span>'+
                        '</td>'+
                    '</tr>'+
                    '<td colspan="9"  class="borderradius" >'+
                        '<div class="pagenation">'+GetPaginationLink(response,Page)+
                            '<button class="fr transallmessages" onclick="ShowAllMesagesList(this)">'+GetLanguageText(BTN_ALLMESSAGE)+'</button>'+
                        '</div>'+
                    '</td>'+
                '</tr>'+
            '</table>'+
        '</div>';
        $('.templatecontent_list').html(HTML);
        if(onLoadSuccess) {
            onLoadSuccess();
        }
    });
}
/**
 * @return {string}
 */
function GetPaginationLink(data,CurrentPage){
    /**
     * @param data.total_records This is total rrecords count
     * @type {Number}
     */
    var pages	=	parseInt(data.total_records/data.recordsfound);
    var linkHtml	=	'';
    var linkLimitStart	=	(CurrentPage-2);
    var linkLimitEnd	=	(CurrentPage+2);
    var prevpage	=	CurrentPage-1;
    var nextpage	=	CurrentPage+1;
    var disableprev='';
    if(CurrentPage==1){
        disableprev='disabled';
    }
    var disablenext='';
    if(CurrentPage==pages){
        disablenext='disabled';
    }
    linkHtml+=	'<button onclick="loadMessagesWithPaging(this,1)"><<</button><button onclick="loadMessagesWithPaging(this,'+prevpage+')" ' + disableprev + '><</button>'+
                '<ul>';
    var linkscnt	=	1;
    for(var i=1;i<=pages;i++){
        if( (i>=linkLimitStart) && (i<=linkLimitEnd) ) {
            var cls="";
            if(CurrentPage==i){
                cls	=	'active';
            }
            linkHtml+=	'<li onclick="loadMessagesWithPaging(this,' + i + ')" class="' + cls + '">' + i + '</li>';
            linkscnt++;
        }
    }
    linkHtml+='</ul>';
    linkHtml+='<button onclick="loadMessagesWithPaging(this,'+nextpage+')" ' + disablenext + '>></button><button onclick="loadMessagesWithPaging(this,'+pages+')">>></button>';
    return linkHtml;
}
function ShowAllMesagesList(eobj){
    AddProcessingLoader(eobj);
    loadMessageList(-1,function(){
        RemoveProcessingLoader(eobj);
    });
}
function loadMessagesWithPaging(self,page){
    //var $self=	$(self);
    //var $parent=	$self.parent();
    AddProcessingLoader(self);
    loadMessageList(page,function(){
        RemoveProcessingLoader(self);
    });
}

function checkLastTestProgress(mid,_self,value){
    logStatus("Calling checkLastTestProgress", LOG_FUNCTIONS);
    logStatus(value, LOG_DEBUG);
    logStatus(mid, LOG_DEBUG);
    try {
        if($(_self).hasClass('addcolor')){
            $(_self).removeClass('addcolor');
        } else {
            $(_self).addClass('addcolor');
        }
    } catch(e) {
        ShowExceptionMessage("checkLastTestProgress", e);
    }
}
function showContent(content){
    logStatus("Calling showContent", LOG_FUNCTIONS);
    try {
        $(".templatecontent").hide();
        var $radiobtn	=	null;
        var $templatecontentbf= $(".templatecontent_bf");
        switch(content){
            case 'sms':
                $(".templatecontent_sms").show();
                $radiobtn	=	$(".radiobox.sms");
                break;
            case 'email':
                $(".templatecontent_email").show();
                $radiobtn	=	$(".radiobox.email");
                break;
            case 'internal':
                $(".templatecontent_internal").show();
                $radiobtn	=	$(".radiobox.intnl");
                break;
            case 'web':
                //Temp - $(".templatecontent_web").show();
                $templatecontentbf.show();//Temp -To be Replaced
                $radiobtn	=	$(".radiobox.web");
                break;
            case 'backoffice':
                $templatecontentbf.show();
                $radiobtn	=	$(".radiobox.bf");
                break;
            case 'list':
                $(".templatecontent_list").show();
                break;
        }
        $(".radiobox").removeClass("radiobtn");
        if ($radiobtn){
            $radiobtn.addClass("radiobtn")
        }
        //$radiobtn.find("input").trigger("change")
    } catch(e) {
        ShowExceptionMessage("showContent", e);
    }
}
function TransManageMessage(){
    logStatus("Calling TransManageMessage", LOG_FUNCTIONS);
    try {
        TranslateText(".translastname",PHTRANS, PH_LAST_NAME);
        TranslateText(".transcluborginasation",PHTRANS, LBL_CLUB_ORG);
        TranslateText(".transrouquart",PHTRANS, LBL_ROUQUART);
        TranslateText(".transnewmsg",TEXTTRANS, LBL_NEW_MESSAGE);
        TranslateText(".transsendsms",TEXTTRANS, LBL_SEND_SMS);
        TranslateText(".transfiles",TEXTTRANS, LBL_ATTACH_FILES);
        TranslateText(".transsent",TEXTTRANS, LBL_SEND);
        TranslateText(".transsubject",TEXTTRANS, LBL_SUBJECT);
        TranslateText(".transsendmail",TEXTTRANS, LBL_SEND_MAIL);
        TranslateText(".transsendto",TEXTTRANS, LBL_SEND_TO);
        TranslateText(".transtype",TEXTTRANS, LBL_TYPE);
        TranslateText(".transmessage",TEXTTRANS, LBL_MESSAGE);
        TranslateText(".transmailph",TEXTTRANS, LBL_MAIL_PHONE);
        TranslateText(".transdatetime",TEXTTRANS, LBL_DATE_TIME);
        TranslateText(".transtoday",TEXTTRANS, BTN_TODAY);
        TranslateText(".trans2days",TEXTTRANS, BTN_2DAYS);
        TranslateText(".trans3days",TEXTTRANS, BTN_3DAYS);
        TranslateText(".transintmsg",TEXTTRANS, BTN_INTERNALMSG);
        TranslateText(".transbackoffice",TEXTTRANS, BTN_BACKOFFICE);
        TranslateText(".transsms",TEXTTRANS, BTN_SMS);
        TranslateText(".transinternal",TEXTTRANS, BTN_INTERNAL);
        TranslateText(".transweb",TEXTTRANS, BTN_WEB);
        TranslateText(".transmartinerou",TEXTTRANS, BTN_MARTINE);
        TranslateText(".transallcoach",TEXTTRANS, BTN_ALLCOACHES);
        TranslateText(".transivo",TEXTTRANS, BTN_IVO);
        TranslateText(".transpetertest",TEXTTRANS, BTN_PETERTEST);
        TranslateText(".transmaxtester",TEXTTRANS, BTN_MAXTESTER);
        TranslateText(".transrobprobber",TEXTTRANS, BTN_ROBPROPPER);
        TranslateText(".transtestmail",TEXTTRANS, LBL_TESTMAIL);
    } catch(e) {
        ShowExceptionMessage("TransManageMessage", e);
    }
}
function saveEmailInfo(eobj){
    logStatus("Calling saveEmailInfo", LOG_FUNCTIONS);
    try {
        var mail_clients	=	[];
        $('.addcolor').each(function(i,row){
            var value=$(row).attr('hrefvalue');
            mail_clients.push({mid:value});
        });
        if(mail_clients.length>0){
            var data={};
            var userdata	=	getCurrentUser();
            data.currentid = userdata.user_id;
            data.text=$(".emailinfo").val();
            data.msgtemplate = $('input[name=msgtemplate]:checked').val();
            data.mid = JSON.stringify(mail_clients);
            data.clubid=$("#users_club").val();
            AddProcessingLoader(eobj);
            updateEmailInformation(data,function(response){
                var ack	=	"Message schedule process failed. Please try again later";
                if((response.status_code) && response.status_code==1){
                    ack	=	"Message Scheduled successfully";
                }
                ShowToastMessage(ack);
                RemoveProcessingLoader(eobj);
            });
        } else {
            ShowToastMessage("Please select atleast one-member to receive mail/message");
        }
    } catch(e) {
        ShowExceptionMessage("saveEmailInfo", e);
    }
}