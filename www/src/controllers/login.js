$(function() {
    _setPageTitle(GetLanguageText(PAGE_TTL_LOGIN_ON));
    TranslateGenericText();
    TranslateLogin();
    $(".main_left").show().removeClass("background3 background2 background4").addClass("background1");
    loginpage_boxmargin_top();
});
function TranslateLogin(){
     TranslateText(".transname", PHTRANS, PH_USERNAME);
     TranslateText(".transsubmit", VALTRANS, BTN_SUBMIT);
     TranslateText(".transpassword", PHTRANS, PH_PASSWORD);
     TranslateText(".transrememberme", TEXTTRANS, BTN_REMEMBER_ME);
     TranslateText(".transforgot", TEXTTRANS, LBL_FORGOT_PASSWORD);
     TranslateText(".transcardiotest", TEXTTRANS, PAGE_TTL_CARDIOTEST);
     TranslateText(".transstrengthtest", TEXTTRANS, LBL_STRENGTHTEST);
}
function forgetpswrdred(){
    logStatus("Calling forgetpswrdred", LOG_COMMON_FUNCTIONS);
    try {
        var email = $('#forgetemail').val();
        if(_validateEmail(email)!=''){
            _close_popup();
            _movePageTo('forgotpasword',{mid:base64_encode(email)});
        } else {
            var message = "Please Enter Email for further process";
            ShowToastMessage(message, _TOAST_LONG);
        }
    } catch(e) {
        ShowExceptionMessage("forgetpswrdred", e);
    }
}

function loginpage_boxmargin_top(){
    var contactheight = $(".loginpage").outerHeight();
    var header = $(".header").outerHeight();
    var footer = $("footer").outerHeight();
    var windowHEIGHT = $( window ).height();
    var contentheight = windowHEIGHT-(header+footer);
    var balanceheight = contentheight-contactheight;
    var margintop = (balanceheight)/2;
    if (margintop<0){
        margintop = 0;
    }
    $(".loginpage ").css({
        "margin-top": margintop
    });
}
$(window).resize(function(){
    loginpage_boxmargin_top();
});	