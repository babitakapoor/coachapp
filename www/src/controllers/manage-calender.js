var delemit_option_type	=	'_:_';
$(function(){
    maincontent_leff_none();
    TranslateGenericText();
    _setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_CALENTER));
    var height = $(window).height()-($('.header').outerHeight()+$('footer').outerHeight());
    var $pagecontent= $(".page_content");
    $pagecontent.css("width", "100%");
    $pagecontent.css("height", height-20);
    //XX $('.construction').html(GetLanguageText(EXCP_UNDER_CONSTUCTION_PAGE));
    loadinit();
});
function loadinit(){
    logStatus("Calling loadinit", LOG_FUNCTIONS);
    try{
        scheduler.config.multi_day = true;
        scheduler.config.xml_date="%Y-%m-%d %H:%i";
        scheduler.init('scheduler_here',new Date(2016,3,1),"month");
        /**
         * @param response.calendernotecount This is calender note count
         * @param response.calendernotes This is calender notes
         * @param response.programscount This is program  count
         * @param response.programs This is programs
         * @param response.memberscount This is member  count
         * @param memberscount.mixedcnt This is mixed count
         * @param memberscount.circutecnt This is circuit count
         */
        GetCalenderInfo(function(response){
            var dataset	=	(response.calendernotecount==1) ? [response.calendernotes]:response.calendernotes;
            var programs	=	(response.programscount==1 )? [response.programs]:response.programs;
            var memberscount=	response.memberscount;
            var programsdata=	[];
            programsdata.push({key:"", label:"Select Programs"});
            $.each(programs,function(idx,pgm){
                //if(PROGRAMS_CIRCUTE_Type=pgm.type);
                var memcount	=	memberscount.mixedcnt;
                if(pgm.type==PROGRAMS_CIRCUTE_Type){
                    memcount	=	memberscount.circutecnt;
                } else if(pgm.type==PROGRAMS_MIXED_Type){
                    memcount	=	memberscount.mixedcnt;
                }
                programsdata.push({key:pgm.strength_program_id+delemit_option_type+memcount, label:pgm.description});
            });
            //'selected_pgms',
            AddCalenderOptionBOX('program',programsdata,'Select Programs',onChangeProgram);
            AddCalenderTextBOX('memberscount','none',"Allowed Members");
            scheduler.parse(dataset,"json");
        });
        scheduler.attachEvent("onEventChanged", function(id,ev){
            var data	=	getCalenderEventData(id,ev);
            SaveCalenderNote(data,function(response){
                //loadinit();
            });
        });
        scheduler.attachEvent("onEventAdded", function(id,ev){
            var data	=	getCalenderEventData(0,ev);
            SaveCalenderNote(data,function(response){
                //loadinit();
            });
        });
        scheduler.attachEvent("onEventDeleted", function (id,ev){
            var data	=	getCalenderEventData(id,ev);
            data.isDelete	=	1;
            SaveCalenderNote(data,function(response){
                //loadinit();
            });
        });
        scheduler.attachEvent("onBeforeEventCreated", function (e){
            logStatus(e, LOG_DEBUG);
            return true;
        });
        scheduler.attachEvent("onDblClick", function (id, e){
            // //any custom logic here
        });
        scheduler.attachEvent("onClick", function(id, e){
            logStatus(e, LOG_DEBUG);
            scheduler.showLightbox(id);
        });

        scheduler.attachEvent("onLightBox",function(id){
            /**
             * @param event.max_members This max mebmers
             */
            var event = scheduler.getEvent(id);
            scheduler.formSection('program').setValue(event.programid);
            scheduler.formSection('memberscount').setValue(event.max_members);
            console.log(event);
        });
        scheduler.attachEvent("onTemplatesReady",function(){
            //normal 2 x Week configuration
            scheduler.date.workweek_start = scheduler.date.week_start;
            scheduler.date.get_workweek_end=function(date){
                return scheduler.date.add(date,14,"day");
            };
            scheduler.date.add_workweek=function(date,inc){
                return scheduler.date.add(date,inc*14,"day");
            };
            scheduler.templates.workweek_date = scheduler.templates.week_date;
            scheduler.templates.workweek_scale_date = scheduler.date.date_to_str("%D, %d");
        });
        //scheduler.load("./data/events.xml");
    } catch(e){
        ShowExceptionMessage("loadinit",e);
    }
}
function show_minical(){
    if (scheduler.isCalendarVisible())
        scheduler.destroyCalendar();
    else
        scheduler.renderCalendar({
            position:"dhx_minical_icon",
            date:scheduler._date,
            navigation:true,
            handler:function(date,calendar){
                logStatus(calendar, LOG_DEBUG);
                scheduler.setCurrentView(date);
                scheduler.destroyCalendar()
            }
        });
}
/*MK Added - get UI data from calendar*/
function getCalenderEventData(id,event){
    logStatus("Calling getCalenderEventData", LOG_FUNCTIONS);
    try{
        var sd	= event.start_date;
        var ed	= event.end_date;
        /*Date As formated */
        var start_date	=	[sd.getFullYear(),sd.getMonth()+1,sd.getDate()].join('/')+' '+[sd.getHours(),sd.getMinutes(),sd.getSeconds()].join(':');
        var end_date 	=	[ed.getFullYear(),ed.getMonth()+1,ed.getDate()].join('/')+' '+[ed.getHours(),ed.getMinutes(),ed.getSeconds()].join(':');
        var data		=	{};
        data.id			=	(id) ? id:0;
        data.text		=	scheduler.formSection('description').getValue();
        var pgmselected	=	scheduler.formSection('program').getValue();
        var pgm	=	getSelectedProgramIDAndType(pgmselected);
        data.program	=	pgm.value;
        data.memberscnt	=	scheduler.formSection('memberscount').getValue();
        data.start_date = (start_date) ? start_date:0;
        data.end_date = (end_date) ? end_date:0;
        return data;
    } catch(e){
        ShowExceptionMessage("getCalenderEventData",e);
    }
}
/* PK added */
function savacalender(id,text,start_date,end_date,program,memberscnt){
    var sd=start_date;
    var ed=end_date;
    start_date = [sd.getFullYear(),sd.getMonth()+1,sd.getDate()].join('/')+' '+[sd.getHours(),sd.getMinutes(),sd.getSeconds()].join(':');			   
    end_date = [ed.getFullYear(),ed.getMonth()+1,ed.getDate()].join('/')+' '+[ed.getHours(),ed.getMinutes(),ed.getSeconds()].join(':');
    var data ={};
    data.id = (id) ? id:0;
    data.text = (text) ? text:0;
    data.start_date = (start_date) ? start_date:0;
    data.end_date = (end_date) ? end_date:0;
    data.program	=	program;
    data.memberscnt	=	memberscnt;
    SaveCalenderNote(data,function(response){
        logStatus(response, LOG_DEBUG);
        loadinit();
    });
} 
function AddCalenderTextBOX(title,DefaultValue,label){
    scheduler.config.lightbox.sections.push({
        name:title,
        type:"textarea",
        map_to:title,
        height:25,
        width:"auto",
        default_value:DefaultValue
    });
    if(label && label!='')  {
        scheduler.locale.labels['section_'+title]	=	label;
    }
}
function AddCalenderOptionBOX(title,options,label,onChangeHandler){
    if(!onChangeHandler){
        onChangeHandler	=	function(){};
    }
    scheduler.config.lightbox.sections.push({
        name:title,
        type:"select",
        map_to:"select",
        options:options,
        onchange:onChangeHandler
    });
    if(label && label!='')  {
        scheduler.locale.labels['section_'+title]	=	label;
    }
}
function onChangeProgram(event){
    logStatus(event, LOG_DEBUG);
    var selected =	$(this).val();
    var sopt	=	getSelectedProgramIDAndType(selected);
    scheduler.formSection('memberscount').setValue(sopt.memcount);
}
function getSelectedProgramIDAndType(selectedval){
    var value	=	selectedval.split(delemit_option_type)[0];
    var memcount=	selectedval.split(delemit_option_type)[1];
    return {value:value,memcount:memcount};
}