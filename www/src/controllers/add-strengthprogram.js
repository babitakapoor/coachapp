$(function(){
    _setPageTitle("Strength Program");

    _getStrengthProgramType(function(response){
        $('.clscircuittype').html(response);
    });
});
function saveStrengthProgramData(){
    logStatus("Calling saveStrengthProgramData",LOG_FUNCTIONS);
    try {
        if(validateSteps('#strengthprogdescription')) {
            var description = $("#strengthprogdescription").val();
            var circutType 	= $("#clscircuittype").val();
            /**
             * @param response.strength_program_id This is strength program id
             */
                _getWeekAssignProgramm(description,circutType,function(response){
                    if(response.status_code == 200){
                        $('#strength_program_id').val(response.strength_program_id);
                        ShowToastMessage(response.status_message, _TOAST_LONG);
                        $('.programweekdetails').removeClass("strengthdetails");
                    }
                });

        }
    } catch(e){
        ShowExceptionMessage("saveStrengthProgramData", e);
    }

}
/* save Week program activity */
function saveWeekprgmanually(){
    logStatus("Calling saveWeekprgmanually", LOG_FUNCTIONS);
    try{
        var data = {};
        data.strengthProgramId 	= $("#strength_program_id").val();
        data.training_week 		= $(".clstrainingweek").val();
        data.series 			= $(".clsseries").val();
        data.reputation 		= $(".clsRep").val();
        data.time 				= $(".clsTime").val();
        data.strength_percentage = $(".clsstrength").val();
        data.rest 				 = $(".clsRest").val();
        data.loggedUserId 		 = getCurrentLoggedinUserId();
        /**
         *  @param response.lasinsertedweek This is last inserted week
         */
        _saveWeekprgmanually(data,function(response){
            if(response.status_code == 200){
                ShowToastMessage(response.status_message, _TOAST_LONG);
                $('.clsclearvalues').val('');
                var lastweek  = Number(response.lasinsertedweek) + Number(1);
                $("#idtrainingweek").val(lastweek);
                var programid = $("#strength_program_id").val();
                if(programid){
                    strengthProgramTraining(programid,function(responsedata){
                        $(".programweektabel").find('tbody').html(responsedata);
                    });
                }

            }
        });
    } catch(e){
        ShowExceptionMessage("saveWeekprgmanually")
    }
}