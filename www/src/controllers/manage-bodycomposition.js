var _isstrength = '';
$(document).ready(function(){
    logStatus("Calling Event ManageBodyComposition Load",LOG_FUNCTIONS);
    try {
        TranslateGenericText();
        TranslationManageBodyComposition();
     //_setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_BODYCOMPOSITION));
                  _setPageTitle("Consult");
        var urldata	=	getPathHashData();
        var mid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		 $('.show_dropdownformenu').html('<button class="selectbtn everydropdown" onclick="checkLastProgressTest('+mid+',this,1)" hrefvalue="'+mid+'">Menu</button>');
				if(urldata.args.testid==0)
				{
					var testid=0;
				}
				else{
					 var testid	=	(urldata.args.testid) ? base64_decode(urldata.args.testid) :0;
				}
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        var newTest	=	(urldata.args.newTest && urldata.args.newTest!='undefined') ? urldata.args.newTest :1;
        var tab		=	(urldata.args.tab) ? urldata.args.tab :0;
        $('#idofuser').val(mid);
        $('#idstartnewtest').val(testid);
        $('#idofclub').val(clubid);
        $('#idoftab').val(tab);
        $('#newtest').val(newTest);
        var ipdate	=	_getDateForInput();
        var $bdycom_testdate= $('#bdycom_testdate');
        $bdycom_testdate.val(ipdate);
        $bdycom_testdate.attr("min",ipdate);
        $('#flextestdate').val(ipdate);
        $('.date_testdate').attr("min",ipdate);
        $('#testDate').val(ipdate);
        getBodycomptiondata(mid);
        getTestMethodologies(clubid);
        getHoursListOption("#lsthours");
        getMinutesListOption("#lstminutes");
         _getClientProfileInformationByID(mid,function(data){
           var  profiledata = data[mid];
            _isstrength= 1; // TODO profiledata.isstrength;
           if(_isstrength==1){
               $('#strength').css('display','inline-block');
           }if(_isstrength==0){
               $('#strength').css('display','none');
           }
            var freepgmhtml = '<option value="0">loading</option>';
             var circuitpgmhtml = '<option value="0">loading</option>';
             var mixpgmhtml = '<option value="0">loading</option>';
             _getMachineByDefaultProgram(function(result){
             freepgmhtml = '<option value="0">Select Free Program</option>';
             circuitpgmhtml = '<option value="0">Select Circuit Program</option>';
             mixpgmhtml = '<option value="0">Select Mix Program</option>';
             var pgmlist = (result[USER_LANG])?result[USER_LANG]:[];
             var free_pgm = (pgmlist[FREE_PGM])?pgmlist[FREE_PGM]:[];
             var circuit_pgm = pgmlist[CIRCUIT_PGM]?pgmlist[CIRCUIT_PGM]:[];
             var mix_pgm = (pgmlist[MIX_PGM]) ?pgmlist[MIX_PGM]:[];
                $.each(free_pgm,function(i,row){
                   freepgmhtml+='<option value="'+row.strength_program_id+'">'+
                            row.titledesc+'</option>';
                        if(row.f_isdefault==1  && _isstrength==0){
                            $('#strdefaultfreepgmid').val(row.strength_program_id);
                        }
                });
                $.each(circuit_pgm,function(i,row){
                   circuitpgmhtml+='<option value="'+row.strength_program_id+'">'+
                            row.titledesc+'</option>';
                   if(row.f_isdefault==1  && _isstrength==0){
                         $('#strdefaultcircuitpgmid').val(row.strength_program_id);
                    }
                });
                $.each(mix_pgm,function(i,row){
                   mixpgmhtml+='<option value="'+row.strength_program_id+'">'+
                            row.titledesc+'</option>';
                    if(row.f_isdefault==1  && _isstrength==0){
                         $('#strdefaultmixpgmid').val(row.strength_program_id);
                    }
                });
                $('.freegrmlist').html(freepgmhtml);
                $('.circuitpgmlist').html(circuitpgmhtml);
                $('.mixpgmlist').html(mixpgmhtml);
                 /*$.each(pgmlist,function(i,row){
                     if(row.type==1){
                         freepgmhtml+ ='<option value="'+row.strength_program_id+'">'+
                                       '<option>';
                         $('#strdefaultfreepgmid').val(row.strength_program_id);
                         
                     }
                     if(row.type==2){
                         $('#strdefaultcircuitpgmid').val(row.strength_program_id);
                     }
                     if(row.type==3){
                         $('#strdefaultmixpgmid').val(row.strength_program_id);
                     }
                 });*/
             });
         });    
         _getMachineByClubId(clubid,function(response){
             var machinecnt ='';
             $.each(response,function(i,row){
                 machinecnt+='<li>'
                                +'<span class="checkouter">'
                                    +'<input type="checkbox"  class="strmachine" value="'+row.r_strength_machine_id+'"> '
                                +'</span>'
                                +'<p class="radio_label">'+row.machine_name+'</p>'
                        +'</li>';
             });
             $('.pgmmachinelist').html(machinecnt);
         });
		 $('.loader_back').css('display','block');
        $('.loader_flex').css('display','block');
        $('.loader_cardio').css('display','block');
		$('.loader_strength').css('display','block');
		$('.loader_mind').css('display','block');
        if(tab!=0){
			
            //$( '#'+tab ).trigger('click');
            tabtriggerclick(tab,true);
            var testtypeid	=	getValidTestTypeID(tab);
            /**
             * @param row.itemdata This is item data
             * @param response.test_level This is test level
             * @param response.test_date This is test date
             */
            getLastTestResultdata(mid,testtypeid,function(response){
                /*SN ADDED 20160215 - Save Exception on No changes*/
                if(tab=='bodycomposition'){
                    $.each(response,function(i,row){
                        var $clsbdcomptestparamer= $(".clsbdcomptestparamer_"+row.itemdata);
                        $clsbdcomptestparamer.val(row.value);
                        $clsbdcomptestparamer.attr('datavalold',row.value);
                    });      
                    /*Not generic method*/
                    updateHoursMinutes();
                }
                if(tab=='flexibility'){
                	$('.loader_tab').css('display','block');
                	$.each(response,function(i,row){
                        var $style ='';
                        if(row.value==1){
                            $style = '75%';
                        }
                        if(row.value==2){
                            $style = '100%';
                        }
                        if(row.value==0){
                            $style = '50%';
                        }
                        if(row.value==-1){
                            $style = '25%';
                        }
                        if(row.value==-2){
                            $style = '0%';
                        }
						/*-----------------------28-02-17 ------------------------------*/
						if(response[0].itemdata==53)
						{
							if(response[0].value==1)
							{
								$('#Calfs_l_new').html('+');
								$('#Calfs_l_text').html('36° – 45°');
							}
							if(response[0].value==2)
							{
								$('#Calfs_l_new').html('++');
								$('#Calfs_l_text').html('>46°');
							}
							if(response[0].value==-1)
							{
								$('#Calfs_l_new').html('-');
								$('#Calfs_l_text').html('16° – 25°');
							}
							if(response[0].value==-2)
							{
								$('#Calfs_l_new').html('--');
								$('#Calfs_l_text').html('<15°');
							}
							if(response[0].value==0)
							{
								$('#Calfs_l_new').html('0');
								$('#Calfs_l_text').html('26° – 35°');
							}
						}
						if(response[1].itemdata==54)
						{
							if(response[1].value==1)
							{
								$('#Calfs_r_new').html('+');
								$('#Calfs_r_text').html('36° – 45°');
							}
							if(response[1].value==2)
							{
								$('#Calfs_r_new').html('++');
								$('#Calfs_r_text').html('>46°');
							}
							if(response[1].value==-1)
							{
								$('#Calfs_r_new').html('-');
								$('#Calfs_r_text').html('16° – 25°');
							}
							if(response[1].value==-2)
							{
								$('#Calfs_r_new').html('--');
								$('#Calfs_r_text').html('<15°');
							}
							if(response[1].value==0)
							{
								$('#Calfs_r_new').html('0');
								$('#Calfs_r_text').html('26° – 35°');
							}
						}
						if(response[2].itemdata==55)
						{
							if(response[2].value==1)
							{
								$('#Illipsoas_l_new').html('+');
								$('#Illipsoas_l_text').html('Beneden hor');
							}
							if(response[2].value==2)
							{
								$('#Illipsoas_l_new').html('++');
								$('#Illipsoas_l_text').html('Ver beneden hor');
							}
							if(response[2].value==-1)
							{
								$('#Illipsoas_l_new').html('-');
								$('#Illipsoas_l_text').html('Boven hor');
							}
							if(response[2].value==-2)
							{
								$('#Illipsoas_l_new').html('--');
								$('#Illipsoas_l_text').html('Ver boven hor');
							}
							if(response[2].value==0)
							{
								$('#Illipsoas_l_new').html('0');
								$('#Illipsoas_l_text').html('Horizontaal');
							}
						}
						if(response[3].itemdata==56)
						{
							if(response[3].value==1)
							{
								$('#Illipsoas_r_new').html('+');
								$('#Illipsoas_r_text').html('Beneden hor');
							}
							if(response[3].value==2)
							{
								$('#Illipsoas_r_new').html('++');
								$('#Illipsoas_r_text').html('Ver beneden hor');
							}
							if(response[3].value==-1)
							{
								$('#Illipsoas_r_new').html('-');
								$('#Illipsoas_r_text').html('Boven hor');
							}
							if(response[3].value==-2)
							{
								$('#Illipsoas_r_new').html('--');
								$('#Illipsoas_r_text').html('Ver boven hor');
							}
							if(response[3].value==0)
							{
								$('#Illipsoas_r_new').html('0');
								$('#Illipsoas_r_text').html('Horizontaal');
							}
						}	
						if(response[4].itemdata==57)
						{
							if(response[4].value==1)
							{
								$('#Hamstrings_l_new').html('+');
								$('#Hamstrings_l_text').html('95° - 100°');
							}
							if(response[4].value==2)
							{
								$('#Hamstrings_l_new').html('++');
								$('#Hamstrings_l_text').html('>100°');
							}
							if(response[4].value==-1)
							{
								$('#Hamstrings_l_new').html('-');
								$('#Hamstrings_l_text').html('75° - 85°');
							}
							if(response[4].value==-2)
							{
								$('#Hamstrings_l_new').html('--');
								$('#Hamstrings_l_text').html('<75°');
							}
							if(response[4].value==0)
							{
								$('#Hamstrings_l_new').html('0');
								$('#Hamstrings_l_text').html('85° - 95°');
							}
						}
						if(response[5].itemdata==58)
						{
							if(response[5].value==1)
							{
								$('#Hamstrings_r_new').html('+');
								$('#Hamstrings_r_text').html('95° - 100°');
							}
							if(response[5].value==2)
							{
								$('#Hamstrings_r_new').html('++');
								$('#Hamstrings_r_text').html('>100°');
							}
							if(response[5].value==-1)
							{
								$('#Hamstrings_r_new').html('-');
								$('#Hamstrings_r_text').html('75° - 85°');
							}
							if(response[5].value==-2)
							{
								$('#Hamstrings_r_new').html('--');
								$('#Hamstrings_r_text').html('<75°');
							}
							if(response[5].value==0)
							{
								$('#Hamstrings_r_new').html('0');
								$('#Hamstrings_r_text').html('85° - 95°');
							}
						}						
						if(response[6].itemdata==59)
						{
							if(response[6].value==1)
							{
								$('#Breast_l_new').html('+');
								$('#Breast_l_text').html('Onder de tafel');
							}
							if(response[6].value==2)
							{
								$('#Breast_l_new').html('++');
								$('#Breast_l_text').html('Ver onder de tafel');
							}
							if(response[6].value==-1)
							{
								$('#Breast_l_new').html('-');
								$('#Breast_l_text').html('Minder dan een vuist');
							}
							if(response[6].value==-2)
							{
								$('#Breast_l_new').html('--');
								$('#Breast_l_text').html('Meer dan een vuist');
							}
							if(response[6].value==0)
							{
								$('#Breast_l_new').html('0');
								$('#Breast_l_text').html('Tegen de tafel');
							}
						}
                        if(response[7].itemdata==60)
						{
							if(response[7].value==1)
							{
								$('#Breast_r_new').html('+');
								$('#Breast_r_text').html('Onder de tafel');
							}
							if(response[7].value==2)
							{
								$('#Breast_r_new').html('++');
								$('#Breast_r_text').html('Ver onder de tafel');
							}
							if(response[7].value==-1)
							{
								$('#Breast_r_new').html('-');
								$('#Breast_r_text').html('Minder dan een vuist');
							}
							if(response[7].value==-2)
							{
								$('#Breast_r_new').html('--');
								$('#Breast_r_text').html('Meer dan een vuist');
							}
							if(response[7].value==0)
							{
								$('#Breast_r_new').html('0');
								$('#Breast_r_text').html('Tegen de tafel');
							}
						}
						if(response[8].itemdata==61)
						{
							if(response[8].value==1)
							{
								$('#Quadriceps_l_new').html('+');
								$('#Quadriceps_l_text').html('tegen zitvlak');
							}
							if(response[8].value==2)
							{
								$('#Quadriceps_l_new').html('++');
								$('#Quadriceps_l_text').html('naast zitvlak');
							}
							if(response[8].value==-1)
							{
								$('#Quadriceps_l_new').html('-');
								$('#Quadriceps_l_text').html('boven één vuist');
							}
							if(response[8].value==-2)
							{
								$('#Quadriceps_l_new').html('--');
								$('#Quadriceps_l_text').html('ver boven één vuist');
							}
							if(response[8].value==0)
							{
								$('#Quadriceps_l_new').html('0');
								$('#Quadriceps_l_text').html('één vuist');
							}
						}
						if(response[9].itemdata==62)
						{
							if(response[9].value==1)
							{
								$('#Quadriceps_r_new').html('+');
								$('#Quadriceps_r_text').html('tegen zitvlak');
							}
							if(response[9].value==2)
							{
								$('#Quadriceps_r_new').html('++');
								$('#Quadriceps_r_text').html('naast zitvlak');
							}
							if(response[9].value==-1)
							{
								$('#Quadriceps_r_new').html('-');
								$('#Quadriceps_r_text').html('boven één vuist');
							}
							if(response[9].value==-2)
							{
								$('#Quadriceps_r_new').html('--');
								$('#Quadriceps_r_text').html('ver boven één vuist');
							}
							if(response[9].value==0)
							{
								$('#Quadriceps_r_new').html('0');
								$('#Quadriceps_r_text').html('één vuist');
							}
						}
						/*----------------------------28-02-17--------------------------------*/
                        var $clsbdcmpflextestparamer= $(".clsbdcmpflextestparamer_"+row.itemdata);
                        var $classsllidervalue= $(".clsslidervalue"+row.itemdata);
                        $clsbdcmpflextestparamer.find("span").attr('tabindex',row.value);
                        $clsbdcmpflextestparamer.find("span").css('left',$style);
                        $('.bdcmpflexiblityremarks').val(row.remarks);
                        var rngvalue=	 (row.value && row.value!='') ? row.value:0;
                        $classsllidervalue.text(rngvalue);
                        $classsllidervalue.html(rngvalue);
                        $classsllidervalue.attr('datavalold',rngvalue);
                        if(doFlexibilityCalculation) {
                            doFlexibilityCalculation();
                        }
                        //$(".clsslidervalue"+row.itemdata).val(row.value);
                    });$('.loader_flex').css('display','none');
					$('.loader_back').css('display','none');
                }
                if(tab=='cardio'){
                	$('.loader_tab').css('display','block');
                	 $("#testoption option[value='"+response.r_test_id+"']").prop('selected', true);
                    var weight	=	(response.weight_on_test) ? parseInt(response.weight_on_test):0;
                    if(weight!=0 || weight== "undefined")
                    {
					 $('#idcardiotestweight').val(weight);
                    }

                    /*SN Issue Req- Cardio should Track weight From On Bodycomposition */
                    else{
					var $idcardiotestweight = $("#idcardiotestweight");
					//alert($idcardiotestweight);
                    $idcardiotestweight.val(weight);
                    $idcardiotestweight.attr('datavalold',weight);
                    getLastTestResultdata(mid,TEST_TYPE_BODYCOMP,function(_bcdata){
                        $.each(_bcdata,function(i,row){
                            if(row.itemdata==TEST_TYPE_BODYCOMP_WEIGHT){
                                 $idcardiotestweight.val(row.value);
                                 $idcardiotestweight.trigger("blur");
                            }
                        });
                   });

					$('.loader_cardio').css('display','none');
					$('.loader_back').css('display','none');
                    }

                    /* SN commented - Age By DOB */
                    /*$("#ageOnTest").val(response.age_on_test);
                    $("#ageOnTest").attr('datavalold',response.age_on_test);*/
                  //  var  $test_level= $("#test_level");
                    //var  $test_level= $("#testLevel");
                    var  $monitor_testdate= $("#monitor_testdate");
                    $('#testLevel').val(response.test_level);
                    $('#testDate').val(response.test_date.split(" ")[0]);
					$('.loader_cardio').css('display','none');
					$('.loader_back').css('display','none');
                    // response.test_date.split
                   //  $test_level.val(response.test_level);
                     //$test_level.attr('datavalold',response.test_level);
                      // $('#testLevel').val(response.test_level);
                    // if(response.test_date && response.test_date.split(" ")[0]){
                    //     $monitor_testdate.val(_getDateForInput(response.test_date.split(" ")[0]));
                    //     $monitor_testdate.attr('datavalold',_getDateForInput(response.test_date.split(" ")[0]));
                    // }
                }
				if(tab=='mind'){
					$('.loader_tab').css('display','block');
				
				Minddatashow();
				}
                if(tab=='strength'){
                	$('.loader_tab').css('display','block');
					//strenghtdatashownew();
					strengthmachinelist();
                	setTimeout(function(){ strenghtdatashownew(); }, 3000);
                }
            });
        }
        /*SN added change date onlt future,pass date is never possible 20160208*/
        $('[type="date"].date_testdate').prop('min', function(){
            return new Date().toJSON().split('T')[0];
        });
                  
        $("#heightbdycompid").bind('keyup mouseup', function () {
                                   calculateNewBMI();
                                   });
        $("#weightbodycompo").bind('keyup mouseup', function () {
               calculateNewBMI();
                  });
                  
                  $('.range_check').change(function() {
                   var max = parseInt($(this).attr('max'));
                   var min = parseInt($(this).attr('min'));
                   if ($(this).val() > max)
                   {
                   $(this).val(max);
                   }
                   else if ($(this).val() < min)
                   {
                   
                   $(this).val(min);
                   }       
                   });
                  
        } catch(e) {
        ShowExceptionMessage("Event.ManageBodyComposition.Load", e);
    }
});

function energylevel()
{
	var energylevel = $('#energylevel').val();

	if(energylevel>35 || energylevel<0)
	{
		alert("PLEASE ENTER VALUE BETWEEN 0 – 35");
		$("#energylevel").val(null);
	}

	var motivation = $('#motivation').val();

	if(motivation>42 || motivation<0)
	{
		alert("PLEASE ENTER VALUE BETWEEN 0 – 42");
		$("#motivation").val(null);
	}

	var resilience = $('#resilience').val();

	if(resilience>35 || resilience<0)
	{
		alert("PLEASE ENTER VALUE BETWEEN 0 – 35");
		$("#resilience").val(null);
	}

	var sleep = $('#sleep').val();

	if(sleep>21 || sleep<0)
	{
		alert("PLEASE ENTER VALUE BETWEEN 0 – 21");
		$("#sleep").val(null);
	}

}


function stressfun()
{
var stress = $('#stress').val();
	if(stress>70 || stress<14 || stress== '')
	{
		alert("PLEASE ENTER VALUE BETWEEN 14 – 70");
		$("#stress").focusin();
		$("#stress").val(null);
	}

}

function mindfullnessfun()
{

var mindfullness = $('#mindfullness').val();
	if(mindfullness>90 || mindfullness<15 || mindfullness== '')
	{
		alert("PLEASE ENTER VALUE BETWEEN 15 – 90");
		$("#mindfullness").focusin();
		$("#mindfullness").val(null);
		
	}

}

function calculateNewBMI(){
    var weightVal = $("#weightbodycompo").val();
    var heightVal = $("#heightbdycompid").val();
    if(weightVal.length > 0 || heightVal.length > 0)
    {
        var BMI = weightVal / ((heightVal*0.01) * (heightVal*0.01));
        var BMIVALUES = (BMI && typeof(BMI)!='string') ? BMI.toFixed(2):0; // display two digits after point
        $('.clsbdcomppersonbmi').val(BMIVALUES);
    }
    else{
    $('.clsbdcomppersonbmi').val("");
    }
}
/* MK Commented - Unwanted code *
$(document).delegate("#bodycomposition","click",function(){
    logStatus("Calling bodycomposition click event bodycomposition",LOG_FUNCTIONS);
    try {
        $(".testtab_list span").removeClass("active");
        $(".bodycom_box").hide();
        $(this).addClass("active");
        $(".bodycomposition").show();
    } catch(e) {
        ShowExceptionMessage("bodycomposition click event bodycomposition", e);
    }
});		
$(document).delegate("#flexibility","click",function(){
    logStatus("Calling flexibility click event bodycomposition",LOG_FUNCTIONS);
    try {
        $(".testtab_list span").removeClass("active");
        $(".bodycom_box").hide();
        $(this).addClass("active");
        $(".flexibility").show();
    } catch(e) {
        ShowExceptionMessage("flexibility click event bodycomposition", e);
    }
});
$(document).delegate("#cardio","click",function(){
    logStatus("Calling cardio click event bodycomposition",LOG_FUNCTIONS);
    try {
        $(".testtab_list span").removeClass("active");
        $(".bodycom_box").hide();
        $(this).addClass("active");
        $(".cardio").show();
    } catch(e) {
        ShowExceptionMessage("cardio click event bodycomposition", e);
    }
});
$(document).delegate("#strength","click",function(){
    logStatus("Calling strength click event bodycomposition",LOG_FUNCTIONS);
    try {
        $(".testtab_list span").removeClass("active");
        $(".bodycom_box").hide();
        $(this).addClass("active");
        $(".strength").show();
    } catch(e) {
        ShowExceptionMessage("strength click event bodycomposition", e);
    }
});
MK Commented Unwanted code*/
/** Lean Weight**/
$(document).delegate('.clsbdcomppersonbodyfat','change',function(){
    logStatus("Calling clsbdcomppersonbodyfat click event in bodycomposition",LOG_FUNCTIONS);
    try {
        doBodycompoCalculation()
    } catch(e) {
        ShowExceptionMessage("clsbdcomppersonbodyfat click event in bodycomposition", e);
    }
});
/** Basic Metabolism**/
$(document).delegate('.rdgrp_lifestyle',"change",function(){
    logStatus("Calling rdgrp_lifestyle radio click event in bodycomposition",LOG_FUNCTIONS);
    try {
        doBodycompoCalculation();
    } catch(e) {
        ShowExceptionMessage("rdgrp_lifestyle radio click event in bodycomposition", e);
    }
});

/** sport charge, sport specific **/
$(document).delegate('.rdgrp_sports','change',function(){
    logStatus("Calling rdgrp_sports radio click event in bodycomposition",LOG_FUNCTIONS);
    try {
        doBodycompoCalculation();
    } catch(e) {
        ShowExceptionMessage("rdgrp_sports radio click event in bodycomposition", e);
    }
});
/** Labour charge, Energy need **/

$(document).delegate('input[name="inp_proffession"]','change',function(){
    logStatus("Calling inp_proffession radio click event in bodycomposition",LOG_FUNCTIONS);
    try {
        doBodycompoCalculation();
    } catch(e) {
        ShowExceptionMessage("inp_proffession radio click event in bodycomposition", e);
    }
});
function tabtriggerclick(tabid,doNotMove){
    logStatus("Calling tabtriggerclick",LOG_FUNCTIONS);
    try {
        var _DontMove	=	doNotMove ? doNotMove:false;
        var userid = $('#idofuser').val();
        var testid = $('#idstartnewtest').val();
        var clubid = $('#idofclub').val();
        var newTest = $('#newtest').val();
        $(".testtab_list span").removeClass("active");
        $(".bodycom_box").hide();
        $("."+tabid).show();
        var $tabid = $("#"+tabid);
        $tabid.addClass("active");
        $tabid.removeClass("showsideimage");
        if ((tabid == 'cardio') || (tabid == 'strength') || (tabid == 'mind')){
            $tabid.addClass("showsideimage");
        }
        if(!_DontMove)  {
            var pagearg	=	{mid:base64_encode(userid),testid:base64_encode(testid),clubid:base64_encode(clubid),tab:tabid,newTest:newTest};
            _movePageTo('manage-bodycomposition',pagearg);
        }
    } catch(e) {
        ShowExceptionMessage("tabtriggerclick", e);
    }
}
/* 
 * Comments: To handle all Manage-Bodycomposition Data related ajax request. 
 * author SK Shanethatech * 
 * Created Date  : JAN-20-2016 
 */
/********************
Glucose Data
********************/
function onevalue(){
    if($('#glocosesober').val() != '' || $('#glocosesober').val()=='')
        {
            $("#glucose").val('');
            $("#glucose").attr('disabled',true);
             $("#glucose").removeClass("req-nonempty");
        }
}

function onevalues(){
    if($('#glucose').val() != '' || $('#glucose').val()=='')
        {
            $("#glocosesober").val('');
            $("#glocosesober").attr('disabled',true);
             $("#glocosesober").removeClass("req-nonempty");
        }

}
function savebodyCompositionTest(moveNextStep){
    logStatus("Calling savebodyCompositionTest", LOG_FUNCTIONS);
    try {
        if(validateSteps('#savebodycomption')){
            /*MK 04 Feb 2016
            Need Re-Enable future */
            /*Save Body Composition compatible to the Track From Changes*/
            DisableProcessBtn(".nextcls", false,'',true);
            if(!_isFormDataChanged('#savebodycomption')){
                ShowAlertConfirmMessage(GetLanguageText(CONFIRM_SUBMIT_DUPLICATETEST),function(isconfirm){
                    if(isconfirm == 1){
                        saveBodyComposition(moveNextStep);
                    } else {
                        /*Do nothing*/
                        DisableProcessBtn(".nextcls", true, GetLanguageText(LBL_NEXT));
                    }
                }, 'yes,no');
            } else {
                saveBodyComposition(moveNextStep);
            }
            /**/
            //saveBodyComposition(moveNextStep);
        }
    } catch (e) {
        ShowExceptionMessage("savebodyCompositionTest", e);
    }
}
/*MK Save Body Composition compatible to the Track From Changes*/
function saveBodyComposition(moveNextStep){
    logStatus("Calling saveBodyComposition", LOG_FUNCTIONS);
    try {
        var postdata = {};
        var items = [];
        $(".clsbdcomptestparamer").each(function(){
            var testItemId = $(this).attr("testitemid");
            var optionvalue	=	'';
            if($(this).hasClass('bodycom_radiobox')){
                var inputvalue  = $('.clsradiobtnli_'+testItemId).find('.radiobtn > input:checked').val();
                optionvalue	=	GET_OPTIONSTRING[testItemId][inputvalue];
            }else{
                optionvalue = $(this).val();
            }
            items.push({testItemId:testItemId, value:optionvalue, note: '', measValue: '', testScore: '', refer: '', score: ''});
        });
        var $idofuser = $('#idofuser');
        postdata.items 		=	JSON.stringify(items);
        postdata.userId 	=	$idofuser.val();
        postdata.testId 	=	TEST_TYPE_BODYCOMP;
        postdata.testdate	=	$('#testDate').val();
        postdata.createdby	=	getCurrentLoggedinUserId();
        var redirectuserid	=	$idofuser.val();
        var redirectclub 	=	$('#idofclub').val();
        var redirectidstartnewtest	=	$('#idstartnewtest').val();
        postbodyCompositionTest(postdata,function(response){
            DisableProcessBtn(".nextcls", true, GetLanguageText(LBL_NEXT));
            //showstandardajaxmessage(response);
            if(moveNextStep==1){
                if(response.status_code==1){
                    
                     _movePageTo('manage-bodycomposition',{mid:base64_encode(redirectuserid),testid:base64_encode(redirectidstartnewtest),clubid:base64_encode(redirectclub),tab:'flexibility'});
                }
            }
        });
    } catch(e) {
        ShowExceptionMessage("savebodyCompositionTest", e);
    }
}
/*------------------------------------------*/
function saveBodyComposition(moveNextStep){
    logStatus("Calling saveBodyComposition", LOG_FUNCTIONS);
    try {
        var postdata = {};
        var items = [];
        $(".clsbdcomptestparamer").each(function(){
            var testItemId = $(this).attr("testitemid");
            var optionvalue	=	'';
            if($(this).hasClass('bodycom_radiobox')){
                var inputvalue  = $('.clsradiobtnli_'+testItemId).find('.radiobtn > input:checked').val();
                optionvalue	=	GET_OPTIONSTRING[testItemId][inputvalue];
            }else{
                optionvalue = $(this).val();
            }
            items.push({testItemId:testItemId, value:optionvalue, note: '', measValue: '', testScore: '', refer: '', score: ''});
        });
        var $idofuser = $('#idofuser');
        postdata.items 		=	JSON.stringify(items);
        postdata.userId 	=	$idofuser.val();
        postdata.testId 	=	TEST_TYPE_BODYCOMP;
        postdata.testdate	=	$('#testDate').val();
        postdata.createdby	=	getCurrentLoggedinUserId();
        var redirectuserid	=	$idofuser.val();
        var redirectclub 	=	$('#idofclub').val();
        var redirectidstartnewtest	=	$('#idstartnewtest').val();
        postbodyCompositionTest(postdata,function(response){
            DisableProcessBtn(".nextcls", true, GetLanguageText(LBL_NEXT));
            //showstandardajaxmessage(response);
            if(moveNextStep==1){
                if(response.status_code==1){
                    _movePageTo('manage-bodycomposition',{mid:base64_encode(redirectuserid),testid:base64_encode(redirectidstartnewtest),clubid:base64_encode(redirectclub),tab:'flexibility'});
                }
            }
        });
    } catch(e) {
        ShowExceptionMessage("savebodyCompositionTest", e);
    }
}

/*-------------------------------------------*/
/* 
 * Comments: To handle all members related ajax request.
 * author SK Shanethatech * 
 * Created Date  : JAN-22-2016
 */
function saveFlexbilityDetails(){
    logStatus("Calling saveFlexbilityDetails",LOG_FUNCTIONS);
    try {
        DisableProcessBtn(".nextcls", false,'',true);
        if(!_isFormDataChanged('#saveflexibility')){
                ShowAlertConfirmMessage(GetLanguageText(CONFIRM_SUBMIT_DUPLICATETEST),function(isconfirm){
                    if(isconfirm == 1){
                        saveFlexibility();
                    } else {
                        /*Do nothing*/
                        DisableProcessBtn(".nextcls", true, GetLanguageText(LBL_NEXT));
                    }
                }, 'yes,no');
            } else {
                saveFlexibility();
            }

    } catch(e){
          ShowExceptionMessage("saveFlexbilityDetails", e);
    }
}
function saveFlexibility(){
    logStatus("Calling saveFlexibility",LOG_FUNCTIONS);
    try {
        var postdata = {};
        var flextestitems = [];
        $(".clsbdcmpflextestparamer").each(function(i,elem){
            var $eachElem	=	$(elem);
            var testItemId	=	$eachElem.attr("sliderid");
            //var inputvalue	=	$eachElem.val();
            var ipdataval	=	$.trim($eachElem.html());
            flextestitems.push({testItemId:testItemId, value: ipdataval, note: '', measValue: '', testScore: '', refer: '', score: ''});
        });
        var $idofuser = $('#idofuser');
        postdata.items = JSON.stringify(flextestitems);
        postdata.userId 				=$idofuser.val();
        postdata.testId 				= TEST_TYPE_FLEXIBLITY;
        postdata.testdate 				= $('#flextestdate').val();
        postdata.createdby 			= getCurrentLoggedinUserId();
        postdata.remarks = $('#txtflexremarks').val();
        postdata.flexLevel = $('#flexLevel').val();
        postdata.age = $('#idbdcomppersonage').val();
        var redirectuserid = $idofuser.val();
        var redirectclub = $('#idofclub').val();
        var redirectidstartnewtest = $('#idstartnewtest').val();
        /**
         * @param response.error_code This is error code status
         */
        postFlexbilityTestdata('submitFlexiblityTest',postdata,function(response){
            /*
             * Comments: To Handel Page confirmation to go cardio|not.
             * author SK Shanethatech *
             * Created Date  : Sept-24-2014 */
             DisableProcessBtn(".nextcls", true, GetLanguageText(LBL_NEXT));
            showstandardajaxmessage(response);
            if(response.error_code == 200){
                /*customAlertMessage("If you just want to continue to the third part �cardio� of the testprocedure",function(){*
                $('.mask-screen').remove();
                $('.mask-message').remove();*/

                // });
                _movePageTo('manage-bodycomposition',{mid:base64_encode(redirectuserid),testid:base64_encode(redirectidstartnewtest),clubid:base64_encode(redirectclub),tab:'cardio'})
            }
          });
    } catch(e){
          ShowExceptionMessage("saveFlexibility", e);
    }
}
function saveMindTestResultData(){
	logStatus("Calling saveMindTestResultData", LOG_FUNCTIONS);
	try{
		MindUpdateActivities();
         if(validateSteps('#savemindtest')){
             DisableProcessBtn(".finishbtncls", false,'',true);
            if(!_isFormDataChanged('#savecardiotest','cardio')){
                ShowAlertConfirmMessage(GetLanguageText(CONFIRM_SUBMIT_DUPLICATETEST),function(isconfirm){
                    if(isconfirm == 1){
						
                        //saveCardio();
						saveMindData();
                    } else {
                        /*Do nothing*/
                        DisableProcessBtn(".finishbtncls", true, GetLanguageText(BTN_NEXT_ARROW));
                    }
                },'yes,no');
            } else {
                //saveCardio();
				saveMindData();
            }
        }
    }catch(e){
        ShowExceptionMessage("saveMindTestResultData", e);
    }
}

function saveMindData(){
logStatus("Calling saveMindData", LOG_FUNCTIONS);
try {
        var postdata = {};
        var items = [];
        $(".clsmindtestparamer").each(function(){
            var testItemId = $(this).attr("testitemid");
            var optionvalue	=	'';
            optionvalue = $(this).val();
            items.push({testItemId:testItemId, value:optionvalue, note: '', measValue: '', testScore: '', refer: '', score: ''});
        });
        var $idofuser = $('#idofuser');
        postdata.items 		=	JSON.stringify(items);
        postdata.userId 	=	$idofuser.val();
		postdata.is_reporting=  base_url+"/reporting/index.php/mind_switch";
        postdata.testId 	=	"9";
        postdata.testdate	=	$('#testDate').val();
        postdata.createdby	=	getCurrentLoggedinUserId();
        postdata.personalgoal	=	$('#personalgoal').val();
	    saveMindTestStarted(postdata,function(response){
        DisableProcessBtn(".finishbtncls", true, "Finish");
            
			if(response.status==1){
				//_movePageTo('home')
                          ShowAlertConfirmMessage("Saved Successfully",function(isconfirm){
                                                  if(isconfirm == 1){
                                                  _movePageTo('home')
                                                  } else {
                                                  /*Do nothing*/
                                                  
                                                  }
                                                  },'ok');
                          }
			
        });
    } catch(e) {
        ShowExceptionMessage("savebodyCompositionTest", e);
    }
}
/***********Save Mind Data**** 14-11-16 *****/
function Minddatashow()
	{
		logStatus("Calling Minddatashow", LOG_FUNCTIONS);
		try {
		var postdata = {};
		 var $idofuser = $('#idofuser');
		 postdata.user_id 	=	$idofuser.val();
		 postdata.is_reporting=  base_url+"/reporting/index.php/report/mind_switch";
		 showMindDatanew(postdata,function(response){
			 logStatus("Calling gettingresponsenew", LOG_FUNCTIONS);
				  $('#sleep').val(response.Mind_Switch.sleep_val); 
                  $('#stress').val(response.Mind_Switch.stress_val);
                  $('#mindfullness').val(response.Mind_Switch.mindful_val);
				  $('#motivation').val(response.Mind_Switch.motivation);
                  $('#resilience').val(response.Mind_Switch.resilience);
				  $('#energylevel').val(response.Mind_Switch.energy);
				  $("#personalgoal option[value='"+response.Mind_Switch.personalgoal+"']").prop('selected', true);
        });
		$('.loader_mind').css('display','none');
		$('.loader_back').css('display','none');
	}
	 catch(e) {
        ShowExceptionMessage("Minddatashow", e);
    }
}
 /* 
 * Comments: To handle all members related ajax request.
 * author SK Shanethatech * 
 * Created Date  : JAN-22-2016
 */
/*MK Edited and Changed - 04 Feb 2016 */
function saveCardioTestResultData(){
    logStatus("Calling saveCardioTestResultData", LOG_FUNCTIONS);
    try{
         if(validateSteps('#savecardiotest')){
             DisableProcessBtn(".nextarrowcls", false,'',true);
            if(!_isFormDataChanged('#savecardiotest','cardio')){
                ShowAlertConfirmMessage(GetLanguageText(CONFIRM_SUBMIT_DUPLICATETEST),function(isconfirm){
                    if(isconfirm == 1){
                        saveCardio();
                    } else {
                        /*Do nothing*/
                        DisableProcessBtn(".nextarrowcls", true, GetLanguageText(BTN_NEXT_ARROW));
                    }
                },'yes,no');
            } else {
                saveCardio();
            }
        }
    }catch(e){
        ShowExceptionMessage("saveCardioTestResultData", e);
    }
}
function saveCardio(){	
    logStatus("Calling saveCardio", LOG_FUNCTIONS);
    try{
         //if(validateSteps('#savecardiotest')){
            //var _dataelements ={};
            var $idofuser= $('#idofuser');
            var $testDate= $('#testDate');
            var clientid	 = $idofuser.val();
           //var testid  	 = 1;
            //var testdate 	= $testDate.val();
            var hiddenuser = $idofuser.val();
            var hiddenclub = $('#idofclub').val();
            var hiddennewtest = $('#idstartnewtest').val();
            var newtest = $('#newtest').val();
            var newTest = (newtest)? newtest:0;
            var testData	=	{};
            testData.newTest= newTest;
            testData.testDate= $testDate.val();
            testData.testName 	= 'Ergometer Cycle Test';
            testData.testOptions 		= $('#testoption').val();
            testData.ageOnTest			= $('#ageOnTest').val();
            testData.cdbdcmpgendertype	= $('.cdbdcmpgendertype').val();
            testData.weightOnTestDate 	= $('#idcardiotestweight').val();
            testData.testLevel 			= $('#testLevel').val();
            testData.isstrength 			= _isstrength;
            testData.strdefaultfreepgmid 	= $('#strdefaultfreepgmid').val();
            testData.strdefaultcircuitpgmid  = $('#strdefaultcircuitpgmid').val();
            testData.strdefaultmixpgmid 	= $('#strdefaultmixpgmid').val();
            testData.height 			= $('.clsbdcomppersonheight').val();
            /*MK Save Total Load Value on Cardio Test Save*/
            var startspeed	=	(SPEED_LEVEL[testData.testLevel]) ? SPEED_LEVEL[testData.testLevel]:0;
            var tlvalue	=	getLoadValueForSpeed(startspeed,testData.weightOnTestDate);
            testData.totalLoadValue	= JSON.stringify(tlvalue);
            var clubId	=	getselectedclubid();
            if(!clubId){
                var loginInfo	=	getCurrentLoginInfo();
                var userclubs	=	loginInfo.user_session.associatedClubs;
                if(userclubs.length==1) {
                    if(userclubs[0].r_club_id) {
                        clubId	=	userclubs[0].r_club_id;
                    }
                }
            }
            saveCardioTestStarted(clientid,testData,function(response){
			DisableProcessBtn(".nextarrowcls", true, GetLanguageText(BTN_NEXT_ARROW));
                if(response.status_code == 200){
                    if(_isstrength==1){
                        _movePageTo('manage-bodycomposition',{mid:base64_encode(hiddenuser),testid:base64_encode(hiddennewtest),clubid:base64_encode(hiddenclub),tab:'strength'});
                    }
					else{
						_movePageTo('manage-bodycomposition',{mid:base64_encode(hiddenuser),testid:base64_encode(hiddennewtest),clubid:base64_encode(hiddenclub),tab:'strength'});
					}
                }
            },clubId);
        //}
    }catch(e) {
        ShowExceptionMessage("saveCardio", e);
    }
}
 /* 
 * Comments: To handle page redirection on click of next
 * author SK Shanethatech * 
 * Created Date  : JAN-29-2016
 */
/*
function redirectpagenavigation(tab){
    logStatus('Calling redirectpagenavigation event', LOG_FUNCTIONS);
    try{
         var redirectuserid = $('#idofuser').val();
         var redirectclub = $('#idofclub').val();
         var redirectidstartnewtest = $('#idstartnewtest').val();

         _movePageTo('manage-bodycomposition',{mid:base64_encode(redirectuserid),testid:base64_encode(redirectidstartnewtest),clubid:base64_encode(redirectclub),tab:tab});
    }catch(e) {
        ShowExceptionMessage("redirectpagenavigation ", e);
    }
}
*/
 // *** [SK] comments by shanethatech ***//
// *** Bind user data into bodycomption test page ***//
function getBodycomptiondata(memberId){
    logStatus("Calling getBodycomptiondata", LOG_FUNCTIONS);
    try {
			var urldata	=	getPathHashData();
		var tab		=	(urldata.args.tab) ? urldata.args.tab :0;
		if(tab=='bodycomposition'){
		$('.loader_body').css('display','block');
		$('.loader_back').css('display','block');
		}
        var user	=	getCurrentUser();

        $("#idcoachname").val(user.first_name);
        _getClientInformationByID(memberId,function(response){
            if(response.last_name) {
                $("#clientname_inputfield").html(response.first_name + ' '  + response.last_name);
				$("#clientemail_inputfield").html(response.email);
				$("#clientphone_inputfield").html(response.phone);
                if(response.userimage!=''){
                    getValidImageUrl(PROFILEPATH + response.userimage,function(url){
                        $('#memberImage').attr('src',url);
                    });
                }
            }
            /* SN Added - Age By DOB */
            var memberAge	=	getAgeByDOB(response.dob);
            $('#idbdcomppersonage').val(memberAge);
            $('.clsbdcomppersonage').val(memberAge);
			/* if(response.gender==0)
			{
				$('#gendertypeid').val('male');
			}
			if(response.gender==1)
			{
				$('#gendertypeid').val('female');
			} */
			 
			
            /*MK Commented Temp * /

            LoadTestOptionList(function(response){
                PopulateTestOptions('#testoption', response);
            })

            /**/
            /**
             * @param response.memberTestResultArr  This is member test result arr
             * @param response.memberTestResultArr.test_end_date This is memberTestResultArr test end date
             * @param response.memberTestResultArr.user_test_level This is user test level
             * @param response.memberDetails This is member details
             */
            newTestDetails('testResult',memberId,'1',function(response){
                if(response.status==1)  {
                    var newDate = dateFormat(response.memberTestResultArr.test_end_date);
                    $('.gendertype').val(response.memberTestResultArr.gender);
				$('.loader_body').css('display','none');
				$('.loader_back').css('display','none');
                    /* SN commanded 20160215 - Old test values display issues/
                    $('.clsbdcomppersonweight').val(response.memberTestResultArr.weight);*/
                    //$('.clsbdcomppersonheight').val(response.memberTestResultArr.height);
                /*	var freq	=	response.memberTestResultArr.training_freq;
                    if(freq==0) {
                        freq='';
                    }*/
                    /*SN commanded 20160215 - Old test values display issues
                    $('.clsbdcomppresonfreq').val(freq);
                    $('.clsbdcomppersonIntensity').val(response.memberTestResultArr.intensity);
                    $('.clsbdcomppersonageonweight').val(response.memberTestResultArr.weight_on_test);*/
                    if(response.memberDetails.age_on_test!=0){

                    }
                    /*SN commanded - Age By DOB */
                    /*$('#idbdcomppersonage').val(response.memberDetails.age_on_test);
                    $('.clsbdcomppersonage').val(response.memberDetails.age_on_test);*/
                    $('.clsbdcomppersontestlevel').val(response.memberTestResultArr.test_level);
                    $('.clsbdcomppersontestdate').val(newDate);
                    app_tempvars('cardiotest_type',response.memberTestResultArr.test_name);
                    $('#testoption').val(response.memberTestResultArr.test_name);
                    $(".clsbdcomppersonweight").trigger("blur");
                    app_tempvars('cardiotest_level',response.memberTestResultArr.user_test_level);
                    $("#testlevel").val(response.memberTestResultArr.user_test_level);
                    $("#tst_lvl_val").val(response.memberTestResultArr.user_test_level);
                    //if(response.user_test_level !=''){
                        //$('#testlevel').val(PopulateTestLevels(response.user_test_level));
                        // $("#testlevel").each(function(i,row){
                            // alert(row);
                             // console.log(row);
                        // });
                    //}
                    /** Display BMI **/
                    //var meter = response.height/100; // convert cms to meter;
                    //response.memberTestResultArr.height	=	undefined;
					
					//comment on 16-11-16
					// console.log('hello cs');
					// console.log(response.memberTestResultArr.weight);
					// console.log(response.memberTestResultArr.height);
					
                   // var BMI = response.memberTestResultArr.weight / ((response.memberTestResultArr.height*0.01) * (response.memberTestResultArr.height*0.01));

                    //comment on 16-11-16
					//var BMIVALUES = (BMI && typeof(BMI)!='string') ? BMI.toFixed(2):0; // display two digits after point
                    
					var BMIVALUES = $('.clsbdcomppersonbmi').val();


                    //var lean = Weight - Weight * fat% / 100 ;
                    var Lean = (1-(BMIVALUES/100))*response.memberTestResultArr.weight;
                    $('#idbmcpleanweight').val(Lean);
                    $('.rdgrp_lifestyle').trigger("change");
                    /*
                    * Comments: To handle, Feteching Dynamic Test methodliges based on seelcted club ID details.
                    * author SK Shanethatech *
                    * Created Date  : JAN-22-2016
                    */
                    /*getTestMethdologies('fetchTestMethodologies',clubid){

                    }*/
                }
            })
        });
    } catch (e){
        ShowExceptionMessage("getBodycomptiondata", e);
    }
}
/* added by saikrishna **/
function getTestMethodologies(clubid){
    logStatus("Calling getTestMethodologies",LOG_FUNCTIONS);
    try {
        var action = "fetchTestTypes";
        //var clubId = clubid;
        /**
         * @param key.r_bcomp_id This is bodycomposition id
         * @param key.method_name This is  method name
         */
        getTesttypes(clubid,action,function(response){
            var HTML = '';
            /*SN added -Test method option added standard manually*/
            HTML+='<option value="7">Manual Entry</option>';
            $.each(response,function(i,key){
                HTML+= '<option value="'+key.r_bcomp_id+'">'+key.method_name+'</option>';
            });
            $('select#selectedtestmethodoliges').html(HTML);
            $('#cardiotestoption').html(HTML);
        });
    } catch(e){
          ShowExceptionMessage("getTestMethodologies", e);
    }
}

 /* 
 * Comments: To handle Strength  Test.
 * author SK Shanethatech * 
 * Created Date  : JAN-28-2016
 */
function saveStrengthTest(moveCardioTest){
    logStatus("Calling saveStrengthTest", LOG_FUNCTIONS);
    try{
       // var value =$('input[name="inp_proff"]:checked').val();
        var userid = $('#idofuser').val();
		var testid = $('#idstartnewtest').val();
        var userdata		=	getCurrentLoginInfo();
        var LoguserID = userdata.user.user_id;
        var weight = $('.clsbdcomppersonweight').val();
        var height = $('.clsbdcomppersonheight').val();
        var freepgmid = $('#strdfreepgmid').val();
        var circuitpgmid = $('#strcircuitpgmid').val();
        var mixpgmid = $('#strmixpgmid').val();
        var situpval = $('#situpid').val();
        var pushupval = $('#pushupid').val();
        var quadricepsval = $('#quadriceps').val();
		var clubId	=	getselectedclubid();
        var uncheckedmachinearr = [];
        $('input[class="strmachine"]:not(:checked)').each(function(i,row){
            uncheckedmachinearr.push({machines:$(row).val()});
        });
        var uncheckedmachine = JSON.stringify(uncheckedmachinearr);
        var disabled_btn = (moveCardioTest) ?'.savestartcls':'.transsave';
        //DisableProcessBtn(disabled_btn, false,'',true);
		 DisableProcessBtn(".nextarrowcls", false,'',true);
        var testdata = {};
        testdata.userId=userid;
      //  testdata.testoptiontype=value;
        testdata.coach_id=LoguserID;
        testdata.weight=weight;
        testdata.height=height;
        testdata.clubID	=	getselectedclubid();
        testdata.freepgmid	=	freepgmid;
        testdata.circuitpgmid	=	circuitpgmid;
        testdata.mixpgmid	=	mixpgmid;
        testdata.situpval	=	situpval;
        testdata.pushupval	=	pushupval;
        testdata.quadricepsval	=	quadricepsval;
        testdata.uncheckedmachine	=	uncheckedmachine;
	
        strengthtest(testdata,function(response){
		
            //DisableProcessBtn(disabled_btn, true,GetLanguageText(BTN_SAVE),true);
			 DisableProcessBtn(".nextarrowcls", true, GetLanguageText(BTN_NEXT_ARROW));
            //showstandardajaxmessage(response);
            if(moveCardioTest){
                cardiotest();
            } else {
                //_movePageTo('home')
				_movePageTo('manage-bodycomposition',{mid:base64_encode(userid),testid:base64_encode(testid),clubid:base64_encode(clubId),tab:'mind'});
            }
        });
    } catch(e){
         ShowExceptionMessage("saveStrengthTest", e);
    }
}
 /* 
 * Comments: To handle Basic metabolism.
 * author SK Shanethatech * 
 * Created Date  : JAN-29-2016
 */
/** Basic Metabolism**/
function basicmetabolism(){
    logStatus("Calling basicmetabolism", LOG_FUNCTIONS);
    try{
        var basicmetabolism = $('input[class="rdgrp_lifestyle"]:checked').val();
        var lean = $('.clsbdcomppersonfatfreeweight').val();
        if(!lean){
            lean	=	0;
        }
        var basic_metabolism = '';
        if(basicmetabolism == 1){
            basic_metabolism = lean * 24;
        } else {
            basic_metabolism = lean * 24 * 1.23;
        }
        var BASIC_METABOLISM_RESULT = basic_metabolism.toFixed(2);
        $('.clsbdcomppersonmethabolism').val(BASIC_METABOLISM_RESULT);

    } catch(e){
         ShowExceptionMessage("basicmetabolism", e);
    }
}
 /* 
 * Comments: To handle sport specific charge and sport specific
 * author SK Shanethatech * 
 * Created Date  : JAN-29-2016
 */
function sportcharge_sportspecific(){
    logStatus("Calling Sport charge  and Sport specific", LOG_FUNCTIONS);
    try{
        var sportrise  = $('input[class="rdgrp_sports"]:checked').val();
        var basic_metabolism = $('.clsbdcomppersonmethabolism').val();
        var sport_charge = "";
        var coef = '';
        if(sportrise == 4){
            sport_charge = basic_metabolism * 0.1;
            coef = basic_metabolism / 100 *  15;
        } else {
            sport_charge = 0;
            coef = basic_metabolism / 100 *  10;
        }
        var sportcharge = sport_charge.toFixed(2);
        var coefcharge = coef.toFixed(2);
        $('.clsbdcompperonssportcharge').val(sportcharge);
        $('.clsbdcomppersonsportspecific').val(coefcharge);

    } catch(e){
         ShowExceptionMessage("sportcharge_sportspecific", e);
    }
}
 /* 
 * Comments: To handle labour charge and energy need.
 * author SK Shanethatech * 
 * Created Date  : JAN-29-2016
 */
function labourcharge_energyneed(){
    logStatus("Calling Labour charge & energy need", LOG_FUNCTIONS);
    try{
        var selectedprofess	=	 parseInt($('input[class="rdgrp_proffession"]:checked').val());
        var selectedactivity  = parseInt($('input[class="rdgrp_activity"]:checked').val());
        var selectedsport  = parseInt($('input[class="rdgrp_sports"]:checked').val());
        var basic_metabolism = $('.clsbdcomppersonmethabolism').val();
        var sport_charge = $('.clsbdcompperonssportcharge').val();
        var sport_specific = $('.clsbdcomppersonsportspecific').val();
        var a =  (selectedprofess * 100 + selectedactivity * 10 + selectedsport ) / 10;
        var laborcharge = a * basic_metabolism / 100 ;
        //var labor_charge = Math.round(laborcharge).toFixed(2);
        var labor_charge = laborcharge.toFixed(2);
        $('.clsbdcomppersonlabourcharge').val(labor_charge);
        /** energy need **/
        var energyneed = Number(basic_metabolism) + Number(sport_charge) + Number(sport_specific) + Number(labor_charge) ;
        var ENERGY_NEED_RESULT = energyneed.toFixed(2);
        $('.clsbdcomppersonenergyneed').val(ENERGY_NEED_RESULT);
    } catch(e){
         ShowExceptionMessage("labourcharge_energyneed", e);
    }
}
 /* 
 * Comments: To handle Lean weight.
 * author SK Shanethatech * 
 * Created Date  : JAN-29-2016
 */
function leanweight(){
    logStatus("Calling leanweight", LOG_FUNCTIONS);
    try{
        var fatweight = $('.clsbdcomppersonbodyfat').val();
        if((!fatweight) || fatweight == '' || typeof(fatweight)== 'undefined'){
            fatweight = 0;
        }
        var weight = $('.clsbdcomppersonweight').val();
        var Lean_Weight = weight - weight * fatweight / 100;
        var LeanWeight = Lean_Weight.toFixed(2);
        $('.clsbdcomppersonfatfreeweight').val(LeanWeight);
    } catch(e){
         ShowExceptionMessage("leanweight", e);
    }
}
function doBodycompoCalculation(){
    logStatus("Calling doBodycompoCalculation", LOG_FUNCTIONS);
    try{
        leanweight();
        basicmetabolism();
        sportcharge_sportspecific();
        labourcharge_energyneed();
    } catch(e){
         ShowExceptionMessage("doBodycompoCalculation", e);
    }
}

/*MK Added Flexibility Calculation - Ends*/

function TranslationManageBodyComposition(){
     TranslateText("#bodycomposition", TEXTTRANS, BTN_BODYCOMPOSITION);
     TranslateText("#flexibility", TEXTTRANS, BTN_FLEXIBILITY);
     TranslateText("#cardio", TEXTTRANS, BTN_CARDIO);
     TranslateText("#strength", TEXTTRANS, LBL_STRENGTH);
     TranslateText(".testdatecls", TEXTTRANS, LBL_TEST_DATE);
     TranslateText(".testmethodcls", TEXTTRANS, LBL_TEST_METHOD);
     TranslateText(".gendercls", TEXTTRANS, LBL_GENDER);
     TranslateText(".agecls", TEXTTRANS, LBL_AGE);
     TranslateText(".heightcls", TEXTTRANS, LBL_HEGHT);
     TranslateText(".weightcls", TEXTTRANS, LBL_WEIGHT);
     TranslateText(".bodyfatcls", TEXTTRANS, LBL_BODY_FAT);
     TranslateText(".bmicls", TEXTTRANS, LBL_BMI);
     TranslateText(".bellygrowthcls", TEXTTRANS, LBL_BELLYGROWTH);
     TranslateText(".fatfreeweightcls", TEXTTRANS, LBL_FATFREEWEIGHT);
     TranslateText(".preofessioncls", TEXTTRANS, LBL_PREOFESSION);
     TranslateText(".activitycls", TEXTTRANS, LBL_ACTIVITY);
     TranslateText(".sportscls", TEXTTRANS, LBL_SPORTS);
     TranslateText(".lifestylecls", TEXTTRANS, LBL_LIFESTYLE);
     TranslateText(".timecls", TEXTTRANS, LBL_STRESS);
     TranslateText(".laborchargepercls", TEXTTRANS, LBL_LABORCHARGE_PER);
     TranslateText(".sportchargecls", TEXTTRANS, LBL_SPORT_CHARGEE);
     TranslateText(".energyneed", TEXTTRANS, LBL_ENERGY);
     TranslateText(".freqweekcls", TEXTTRANS, LBL_FREQWEEK);
     TranslateText(".intensitycls", TEXTTRANS, LBL_INTENSITY);
     TranslateText(".stresscls", TEXTTRANS, LBL_STRESS);
     TranslateText(".agetestcls", TEXTTRANS, LBL_AGE_TEST);
     TranslateText(".testlevelcls", TEXTTRANS, LBL_TEST_LEVEL);
     TranslateText(".testoption", TEXTTRANS, LBL_TEST_OPTION);
     TranslateText(".coachcls", TEXTTRANS, LBL_COACH);
     TranslateText(".testdate", TEXTTRANS, LBL_TEST_DATE);
     TranslateText(".nextarrowcls", TEXTTRANS, BTN_NEXT_ARROW);
     TranslateText(".savecls", TEXTTRANS, BTN_SAVE);
     TranslateText(".savestartcls", TEXTTRANS, BTN_SAVE_START_TEST);
     TranslateText(".verygood_btn",VALTRANS, BTN_GOOD);
     TranslateText(".transflexlevel", TEXTTRANS, BTN_FLEX_LEVE);
     TranslateText(".transflexcat", TEXTTRANS, BTN_FLEX_CAT);
     TranslateText(".transcalfs",TEXTTRANS, BTN_CALFS);
     TranslateText(".transillipsoas",TEXTTRANS, BTN_ILLIPSOAS);
     TranslateText(".transhamstrings",TEXTTRANS, BTN_HAMSTRINGS);
     TranslateText(".transbreast",TEXTTRANS, BTN_BREAST);
     TranslateText(".transquadriceps",TEXTTRANS, BTN_QUADRICEPS);
     TranslateText(".transremarks",TEXTTRANS, BTN_REMARKS);
     TranslateText(".transcalculate",TEXTTRANS, LBL_CALCULATE_AUTOMETICALY);
     TranslateText(".transstrengthtest",TEXTTRANS, LBL_STRENGTHTEST);
     TranslateText(".transnone",TEXTTRANS, BTN_NONE);
     TranslateText(".translaborcharge", TEXTTRANS, LBL_LABORCHARGE);
     TranslateText(".transmetabolism", TEXTTRANS, LBL_BASICMETABOLISM);
     TranslateText(".transsportspecific", TEXTTRANS, LBL_SPORTS_SPECIFIC);
     TranslateText(".translight",TEXTTRANS, BTN_LIGHT);
     TranslateText(".transmiddle",TEXTTRANS, BTN_MIDDLE);
     TranslateText(".transheavy",TEXTTRANS, BTN_HEAVY);
     TranslateText(".transregular",TEXTTRANS, BTN_REGULAR);
     TranslateText(".transunregular",TEXTTRANS, BTN_UNREGULAR);
     TranslateText(".transyes",TEXTTRANS, TEX_YES);
     TranslateText(".transno",TEXTTRANS, TEX_NO);
 
}
/*MK Added Flexibility Calculation - Ends*/
function updateHoursMinutes(){
    logStatus("Calling updateHoursMinutes", LOG_FUNCTIONS);
    try{
        var seltime	=	$("#timehrmn").val();
        if(seltime!=''){
            var hr_min	=	seltime.split(':');
            $("#lsthours").val(hr_min[0]);
            $("#lstminutes").val(hr_min[1]);
        }
    } catch(e){
        ShowExceptionMessage("updateHoursMinutes", e);
    }
}

function sendReport(){
    try {
			var userid = $('#idofuser').val();
            var POPUP_HTML	= '<div class="field_in_box"><ul>'+
        '<li class="selectbtn" onclick="sendEmailReport('+userid+')">Send Email</li>'+
         /*'<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')">'+GetLanguageText(BTN_ANALYSE_TEST)+'</li>'+
        '<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')"> '+GetLanguageText(BTN_READY_TO_PRINT)+'</li>'+
        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'consult'+'\')">Movesmart Report</li>'+
       '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-reports'+'\')">'+GetLanguageText(LBL_REPORTS)+'</li>'+
         '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-messages'+'\')">'+GetLanguageText(BTN_MESSAGES_PAGE)+'</li>'+
         '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'strength-program'+'\')">'+GetLanguageText(BTN_STRENGTH_PROGRAM)+'</li>'+
         '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'member-viewprofile'+'\')">'+GetLanguageText(BTN_VIEW_PROFILE)+'</li>'+/**/
        '</ul></div>';
        
        _popup(POPUP_HTML,{close:false, closeOnOuterClick:false});
    } catch(e) {
        ShowExceptionMessage("checkLastTestProgress", e);
    }
}

function sendEmailReport(usid){
     _close_popup();
    sendReportService(usid);
    ShowAlertConfirmMessage("Consult rapportage met succes verzonden",function(isconfirm){
                            if(isconfirm == 1){
                            _movePageTo('home')
                            } else {
                            /*Do nothing*/
                            
                            }
                            },'ok');
}

/*
$(document).delegate('#cardio','click',function(){
    //maincontent_leftimage();
    $(".page_content .comm_innerbox").addClass("showsideimage");
    $(".main_left").show().removeClass("background1 background2 background4").addClass("background3");
});

$(document).delegate('#strength','click',function(){
    //maincontent_leftimage();
    $(".page_content .comm_innerbox").addClass("showsideimage");
    $(".main_left").show().removeClass("background2 background3 background4").addClass("background1");
});
*/
$(document).delegate('#bodycomposition, #flexibility, #cardio, #strength, #mind','click',function(){
    //maincontent_leff_none();
    $(".page_content .comm_innerbox").removeClass("showsideimage");
    $(".main_left").hide();
});

/*----------------
    Fetch Strenght Data
----------------*/
/*previous 19-june-2017*/
function savestrengthdata()
{
    
logStatus("Calling saveMindData", LOG_FUNCTIONS);
try {
        var postdata = {};
        var test_option_type = [];
        var $idofuser = $('#idofuser');
          var userid = $('#idofuser').val();
          var testid = $('#idstartnewtest').val();
          var clubId  =   getselectedclubid();
           postdata.situp    =   $('#situpid').val();
            postdata.pushup   =   $('#pushupid').val();
             postdata.quadriceval  =   $('#quadriceps').val();
        postdata.userId     =   $idofuser.val();
        postdata.is_reporting=  base_url+"/reporting/index.php/strength";
        logStatus("Calling savestrengthdata ===>>> "+JSON.stringify(postdata), LOG_FUNCTIONS); 
        saveStrenghtTestStarted(postdata, function(response){

            //showstandardajaxmessage(response);
            DisableProcessBtn(".nextarrowcls", true, GetLanguageText(BTN_NEXT_ARROW));
            //showstandardajaxmessage(response);
            if(response.status==11){
                //_movePageTo('home')
                 _movePageTo('manage-bodycomposition',{mid:base64_encode(userid),testid:base64_encode(testid),clubid:base64_encode(clubId),tab:'mind'});
            }
           
        });
    } catch(e) {
        ShowExceptionMessage("savestrengthdata", e);
    }
}
/*********************/

/*31-3-2017*/
/************************************
19-june-2017
function savestrengthdata()
{
    
logStatus("Calling saveMindData", LOG_FUNCTIONS);
try {

			var mchin_id = jQuery.map($(':checkbox[name=check\\[\\]]:checked'), function (n, i) {
		    return n.value;

		}).join(',');


			var program_id = $('#listfree').val();
			var program_id2 = $('#listgoal').val();
			if(program_id!='' && program_id2==null){
				var program = program_id;
			}
			else if(program_id2!='' && program_id==null){
				var program = program_id2;

			}
        var postdata = {};
        var test_option_type = [];
        var $idofuser = $('#idofuser');
          var userid = $('#idofuser').val();
          var testid = $('#idstartnewtest').val();
          var clubId  =   getselectedclubid();
           postdata.situp    =   $('#situpid').val();
            postdata.pushup   =   $('#pushupid').val();
             postdata.quadriceval  =   $('#quadriceps').val();
               postdata.machine_id  =   mchin_id;
                postdata.clubid  =   clubId;
               postdata.r_strength_program_id  = program;
        postdata.userId     =   $idofuser.val();
        postdata.is_reporting=  base_url+"/reporting/index.php/strength";
        logStatus("Calling savestrengthdata ===>>> "+JSON.stringify(postdata), LOG_FUNCTIONS); 
        saveStrenghtTestStarted(postdata, function(response){
            DisableProcessBtn(".nextarrowcls", true, GetLanguageText(BTN_NEXT_ARROW));
            if(response.status==11){
                _movePageTo('manage-bodycomposition',{mid:base64_encode(userid),testid:base64_encode(testid),clubid:base64_encode(clubId),tab:'mind'});
            }
           
        });
    } catch(e) {
        ShowExceptionMessage("savestrengthdata", e);
    }
}
*************************/
/*----------------
   previous Show Strenght Data 19-june-2017
----------------*/

 function strenghtdatashownew()
{

        logStatus("Calling strenghtdatashownew", LOG_FUNCTIONS);
        try {
        var postdata = {};
         var $idofuser = $('#idofuser');
         postdata.userId   =   $idofuser.val();
         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getstre";
         showstrenghtDatanew(postdata,function(response){
             logStatus("Calling gettingresponsenew", LOG_FUNCTIONS);
                  $('#situpid').val(response.strength.f_situpval); 
                  $('#pushupid').val(response.strength.f_pushupval);
                  $('#quadriceps').val(response.strength.f_quadriceval);
        });
		$('.loader_strength').css('display','none');
		$('.loader_back').css('display','none');
    }
     catch(e) {
        ShowExceptionMessage("strenghtdatashownew", e);
    }


} 

/********************19-june-2017
function strenghtdatashownew()
{

        logStatus("Calling strenghtdatashownew", LOG_FUNCTIONS);
        try {
        var postdata = {};
         var $idofuser = $('#idofuser');
         postdata.userId   =   $idofuser.val();
         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getstre";
         showstrenghtDatanew(postdata,function(response){
             logStatus("Calling gettingresponsenew", LOG_FUNCTIONS);
                  $('#situpid').val(response.strength.f_situpval); 
                  $('#pushupid').val(response.strength.f_pushupval);
                  $('#quadriceps').val(response.strength.f_quadriceval);
                  $.each(response.strength.machine_map, function(index, value ){
                  
                  		$("#machine"+value.r_machine_id).attr('checked', true);
                  });
                  var pro = response.strength.programs;

                 var split_arr =  pro.split(",");
					array  = [];
					array = split_arr;
					var pro = array[0];
					var type_pro = array[1]; 
				$('#type').val(type_pro);
				$('#type_pro').val(pro);
				
        });
    }
     catch(e) {
        ShowExceptionMessage("strenghtdatashownew", e);
    }


}
**********************/
function strengthmachinelist()
{
        logStatus("Calling strengthmachinelist", LOG_FUNCTIONS);
        try {
        var postdata = {};
        var ClubId	=	getselectedclubid();
         postdata.Clubid   =  ClubId;
         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getmachineList";
         strength_machinelist(postdata,function(data){
         	if(data.status== 1){
			$.each( data.response, function(index, value ){
			    var machine_id = value.strength_machine_id;
			     var machine_name = value.machine_name;
			     $('#listmachine').append('<span class="check_box"><input type="checkbox" id="machine'+machine_id+'" value="'+machine_id+'" name="check[]">'+machine_name+'</span>');
			});
 
 			
	}			
             
                
        });
    }
     catch(e) {
        ShowExceptionMessage("strengthmachinelist", e);
    }


}


 /*   Saving the data with Free Program  */

function get_FreestrengthProg()
		{
			 
        			$("#listgoal").hide();
		        logStatus("Calling get_FreestrengthProg", LOG_FUNCTIONS);
		        try {
		        var postdata = {};
		        var clubid	=	getselectedclubid();
		        var type	=	1;
		              var selected_program_type = $('#type').val();
					  var selected_program = $('#type_pro').val();
		         postdata.type  =  type;
		         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getstrengthProg";
		         strength_Prog(postdata,function(data){
		         		 if(data.status== 1){
		         		 	$("#listfree").toggle();
			         $("#listgoal").empty();
					 $('#listfree').html('<option value="">Select free program</option>');	
						$.each( data.response, function(index, value ){
						    var strength_program_id = value.strength_program_id;
						    var title_english = value.title_english;
							var program_type = value.type;
							var option_list_val = strength_program_id+','+program_type;
								var selected_option_val = selected_program+','+selected_program_type;
								if(option_list_val == selected_option_val){
						     $('#listfree').append('<option value="'+strength_program_id+','+program_type+'" selected="selected">'+title_english+'</option>');
								}
								else{
									$('#listfree').append('<option value="'+strength_program_id+','+program_type+'">'+title_english+'</option>');
								}
						
						});
			 
			 			
				}

			
		         });
				 if(selected_program_type==1){
					  $('#listfree option[value="'+selected_program+'"]').attr("selected", "selected");
				 }
		    }
		     catch(e) {
		        ShowExceptionMessage("get_FreestrengthProg", e);
		    }

		}


		function get_GoalstrengthProg()
		{
			$("#listfree").hide();
		        logStatus("Calling get_GoalstrengthProg", LOG_FUNCTIONS);
		        try {
		        var postdata = {};
		        var clubid	=	getselectedclubid();
		        var type	=	2;	
				var selected_program_type = $('#type').val();
				var selected_program = $('#type_pro').val();
		         postdata.type  =  type;
		         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getstrengthProg";
		         strength_Prog(postdata,function(data){
		         		
				if(data.status== 1){
						
		         		 	$("#listgoal").toggle();
			         $("#listfree").empty();
					 $('#listgoal').html('<option value="">Select goal program</option>');	
						$.each( data.response, function(index, value ){
						    var strength_program_id = value.strength_program_id;
						    var title_english = value.title_english;
							var program_type = value.type;	
							var option_list_val = strength_program_id+','+program_type;
								var selected_option_val = selected_program+','+selected_program_type;
								if(option_list_val==selected_option_val){
										 $('#listgoal').append('<option value="'+option_list_val+'" selected="selected">'+title_english+'</option>');
							}
							else{
								
								$('#listgoal').append('<option value="'+option_list_val+'">'+title_english+'</option>');
							}
						
						});
			 
			 		
				}
	
		         });
				 
				 
		    }
		     catch(e) {
		        ShowExceptionMessage("get_GoalstrengthProg", e);
		    }
		}

		/*update stautus for mind tab*/
function MindUpdateActivities()
    {
		
        logStatus("Calling MindUpdateActivities", LOG_FUNCTIONS);
        try {
        var postdata = {};
         var idofuser = $('#idofuser');
         var idcoachname = $('#idcoachname');
         var coachvalu = getCurrentLoggedinUserId();
         
         postdata.user_id   =   idofuser.val();
         postdata.coach_id  =   getCurrentLoggedinUserId();
         postdata.is_reporting=  base_url+"/reporting/index.php/mind_switch/mind_activities";
         updatemindstatusdata(postdata,function(response){
                if(response.status==1){
              console.log(response);
                }
				else{
					 console.log(response);
				}
            
        });
    }
     catch(e) {
        ShowExceptionMessage("MindUpdateActivities", e);
    }
}
