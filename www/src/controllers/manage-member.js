/*MK jQuery Envents - Starts*/
$(function(){
    TranslationManageMember();
    TranslateGenericText();
    _setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_MEMBER));
    getManageMembersData('memberListGrid','.managetable');
    setOption_AssociatedClubs("#users_club");
    $("#users_club").val(getselectedclubid());

    maincontent_leff_none();

    $(window).resize(function(){
        //maincontent_leff_none();
    });

});

function TranslationManageMember(){
     TranslateText(".transmemberlist", TEXTTRANS, BTN_MEMBER_LIST);
     TranslateText(".transcardio", TEXTTRANS, BTN_CARDIO_TEST);
     TranslateText(".transbodycomp", TEXTTRANS, BTN_BODY_COMP);
     TranslateText(".transflexibility", TEXTTRANS, BTN_FLEXIBILITY_TEST);
     TranslateText(".transreports", TEXTTRANS, BTN_REPORTS);
     TranslateText(".transovertraining", TEXTTRANS, BTN_OVERTRAINING);
     TranslateText(".transcluborg", TEXTTRANS, LBL_CLUB_ORG);
} 
$(document).delegate("#users_club","change",function(){
    logStatus("Event #users_club Change", LOG_FUNCTIONS);
    try {
        storeselectedclubid($("#users_club").val());
    }  catch(e) {
        ShowExceptionMessage("Event selecrfiltervalue click", e);
    }
});
$(document).delegate("#selecrfiltervalue","change",function(){
    logStatus("Event selecrfiltervalue click", LOG_FUNCTIONS);
    try {
        var filterval = $(this).find("option:selected").text();
        $('#searchinputvalue').attr('placeholder',filterval);
    }  catch(e) {
        ShowExceptionMessage("Event selecrfiltervalue click", e);
    }
});
$(document).on("click",'.pageindex',function(){
    logStatus("Event pageindex click", LOG_FUNCTIONS);
    try {
        var $self	=	$(this);
        var manageAction	=	"";
        var manageElement	=	"";
        var page		=	$self.attr('page');
        var pageindex		=	$self.attr('pageindex');
        var action_tab		=	$self.attr('action_tab');
        if(page=='members'){
            manageAction	=	'memberListGrid';
            manageElement	=	'.managetable';
        }
				/*Changes Date : 04-04-17*/
		var searchValue	=	$("#searchinputvalue").val();
		var filterby = $('#selecrfiltervalue').val();
		if(searchValue!=''&&filterby!=''){
			getManageMembersData(manageAction,manageElement,action_tab,pageindex,searchValue,filterby);
			return false;
		}
		else{
			getManageMembersData(manageAction,manageElement,action_tab,pageindex,'','');
			return false;
		}
        /*End*/
        //getManageMembersData(manageAction,manageElement,action_tab,pageindex);
    }catch(e) {
        ShowExceptionMessage("Event pageindex click", e);
    }
});
/*MK jQuery Envents - Ends*/
/*MK Functions - Starts*/
// *** redirect appropriate page on select Grid list ***//
function checkLastTestProgress(mid,_self,value){
    logStatus("Calling checkLastTestProgress", LOG_FUNCTIONS);
    logStatus(value, LOG_DEBUG);
    logStatus(_self, LOG_DEBUG);
    try {
        var testid  = $("#testID_"+mid).val();
        testid	=	(testid) ? testid:0;
        var clubid	 	=  $("#users_club").val();
        var POPUP_HTML	= '<div class="field_in_box"><ul>'+
                        //'<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'checkresult'+'\')">'+GetLanguageText(BTN_TEST_PAGE)+'</li>'+
						
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'checkresult'+'\')">Check Result</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-cardiotest3'+'\')">Start Consult</li>'+
                        '<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')">'+GetLanguageText(BTN_ANALYSE_TEST)+'</li>'+
                        //'<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')"> '+GetLanguageText(BTN_READY_TO_PRINT)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'consult'+'\')">Movesmart Report</li>'+
						/*19-june-2017 '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'overview'+'\')">Training</li>'+ */
						'<li class="selectbtn" onclick="addextracredit('+mid+','+testid+','+clubid+')">Extra Credit</li>'+
                        // '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'program'+'\')">Result Program</li>'+
                        /*'<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-reports'+'\')">'+GetLanguageText(LBL_REPORTS)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-messages'+'\')">'+GetLanguageText(BTN_MESSAGES_PAGE)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'strength-program'+'\')">'+GetLanguageText(BTN_STRENGTH_PROGRAM)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'member-viewprofile'+'\')">'+GetLanguageText(BTN_VIEW_PROFILE)+'</li>'+/**/
                        '</ul></div>';

        _popup(POPUP_HTML,{close:false, closeOnOuterClick:true});
    } catch(e) {
        ShowExceptionMessage("checkLastTestProgress", e);
    }
}
/*Go To analysis Page*/
function moveCardioAnalysisPage(memid){
    logStatus("Calling moveCardioAnalysis",LOG_FUNCTIONS);
    try {
        _close_popup();
        var memberid	=	base64_encode(memid);
        _movePageTo('cardio-analysis',{memberid:memberid});
    } catch(e) {
        ShowExceptionMessage("moveCardioAnalysis", e);
    }
}
/*MK Functions - Ends*/
/*MK Cardio Analysis Page*/

function getSearchResult(action,_element){
    logStatus("Calling getSearchResult", LOG_COMMON_FUNCTIONS);
    try {
        var searchValue	=	$("#searchinputvalue").val();
        var userid = getCurrentLoggedinUserId();
        var tabidx = 'members_list';
        var $activetabobj = $(".clsmanagememtbs .clsmngmemtab.active");
        if ($activetabobj){
            tabidx = $activetabobj.attr("serchlist");
        }
        var filterby = $('#selecrfiltervalue').val();
        _getSearchResult(action,_element,searchValue,userid,tabidx,filterby,function(response){
            $(_element).find('tbody').html(response);
        });
    }catch(e){
        ShowExceptionMessage("getSearchResult", e);
    }
}
/*for the menus on every page*/
function checkLastProgressTest(mid,_self,value){
    logStatus("Calling checkLastProgressTest", LOG_FUNCTIONS);
    logStatus(value, LOG_DEBUG);
    logStatus(_self, LOG_DEBUG);
    try {
        var testid  = $("#testID_"+mid).val();
        testid  =   (testid) ? testid:0;
        var clubid      = getselectedclubid();
        var POPUP_HTML  = '<div class="field_in_box"><ul>'+
                        //'<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'checkresult'+'\')">'+GetLanguageText(BTN_TEST_PAGE)+'</li>'+
                        
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'checkresult'+'\')">Check Result</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-cardiotest3'+'\')">Start Consult</li>'+
                        '<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')">'+GetLanguageText(BTN_ANALYSE_TEST)+'</li>'+
                        //'<li class="selectbtn" onclick="moveCardioAnalysisPage('+mid+')"> '+GetLanguageText(BTN_READY_TO_PRINT)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'consult'+'\')">Movesmart Report</li>'+
                        /* 19-june-217'<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'overview'+'\')">Training</li>'+ */
						'<li class="selectbtn" onclick="addextracredit('+mid+','+testid+','+clubid+')">Extra Credit</li>'+
                        // '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'program'+'\')">Result Program</li>'+
                        /*'<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-reports'+'\')">'+GetLanguageText(LBL_REPORTS)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'manage-messages'+'\')">'+GetLanguageText(BTN_MESSAGES_PAGE)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'strength-program'+'\')">'+GetLanguageText(BTN_STRENGTH_PROGRAM)+'</li>'+
                        '<li class="selectbtn" onclick="moveSelectMemberPage('+mid+','+testid+','+clubid+',\''+'member-viewprofile'+'\')">'+GetLanguageText(BTN_VIEW_PROFILE)+'</li>'+/**/
                        '</ul></div>';

        _popup(POPUP_HTML,{close:false, closeOnOuterClick:true});
    } catch(e) {
        ShowExceptionMessage("checkLastProgressTest", e);
    }
}
function addextracredit(userid,b,c){
logStatus("Calling addextracredit", LOG_FUNCTIONS);
    try {
		$('.popup').css('display','none');
		$('.popupmask').css('background','none');
		_getClientInformationByID(userid,function(response){
			var firstname = response.first_name;
			var lastname = response.last_name;
			var user_id = response.user_id;
			
			var POPUP_HTML	= '<div class="popuptitle addextracredit">'+'Extra Credits'+'</div><div class="field_in_box" style="text-align:center;font-size:16px;"><p>Je geeft 1000 credits aan klant ' +firstname+' '+lastname+', ben je zeker?</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="confirmcredit('+user_id+')">'+'Ja'+'</button><button class="selectbtn" onclick="_close_popup()">'+'Nee'+'</button></div></div>';
			_popup(POPUP_HTML,{close:false});
				$('.popup').css('height','230');
			});
		}
		catch(e) {
        ShowExceptionMessage("addextracredit", e);
    }
}
function confirmcredit(user_id)
{
	var postdata = {};
	 var idofuser = user_id;
	 postdata.user_id   =   idofuser;
	 postdata.group_id   =   '4';
	 postdata.phase_id   =   '1';
	 postdata.f_points   =   '1000';
	 postdata.f_status   =   '0';
	 postdata.is_reporting=  base_url+"/reporting/index.php/report/getextracredit";
	 addextracreditdata(postdata,function(response){
		if(response.status==1){
			$('.popup').css('display','none');
			$('.popupmask').css('background','none');
			$('.popup').css('height','230');
			var POPUP_HTML	= '<div class="popuptitle addextracredit">'+'Extra Credits'+'</div><div class="field_in_box" style="text-align:center;font-size:18px;"><p>1000 credits toegevoegd</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
			_popup(POPUP_HTML,{close:false});
			$('.popup').css('height','230');
		}
		else{}
		});
}