$(function(){
    logStatus("Calling change-strength program document",LOG_FUNCTIONS);
    try{
        _setPageTitle(GetLanguageText(PAGE_TTL_STRENGTH_PRGM_MAPPING));
        var urldata	=	getPathHashData();
        //var clubId	=	getselectedclubid();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        //var testid	=	(urldata.args.testid && urldata.args.testid!='undefined') ? base64_decode(urldata.args.testid) :0;
        _getClientInformationByID(memberId,function(response){
            $("#client_id_name").val(response.first_name+" "+response.last_name );
            $("#client_id_email").val(response.email);
        });
        /**
        * @param response.result.programname This is program name
         */
        _getMachienMappedetails(memberId,function(response){
            var prog_name = '';
            if(response.result.programname){
                prog_name = response.result.programname;
            } else {
                prog_name = 'Basic '; // deaclerd statically for testing purpose, need to set one basic program while create a program ;
            }
            $(".currenProgramName").html(prog_name);
            var pid = response.result.r_strength_program_id;
            var programid = (pid) ? pid: 50; // Deafult Id is 50 for base standarad;
            if(programid){
                strengthProgramOverview(programid,function(responsedata){
                    $(".programtabel").find('tbody').html(responsedata);
                });
            }
        });
    } catch (e){
        ShowExceptionMessage("change-Strength Program document", e);
    }
});
$(document).delegate("#idprogramlist","click",function(){
    logStatus("Calling idprogramlist click event",LOG_FUNCTIONS);
    try{
        var tapvalue = $(this).find('input').attr("hrefValue");
        var descript = $(this).find('input').attr("descript");
        $(".selectedProgramName").html(descript);
        $(".selectedNew_ProgamID").val(tapvalue);
        if(tapvalue !='' || tapvalue !='undefined'){
            strengthProgramTraining(tapvalue,function(response){
                $(".programweektabel").find('tbody').html(response);
            });
        }
    } catch(e){
        ShowExceptionMessage("idprogramlist",e);
    }

});

$(document).on("click",'.pageindex',function(){
    logStatus("Event pageindex click", LOG_FUNCTIONS);
    try {
        var $self		=	$(this);
        var manageAction	=	"";
        var manageElement	=	"";
        var page		=	$self.attr('page');
        var pageindex	=	$self.attr('pageindex');
        var action_tab	=	$self.attr('action_tab');
        if(page=='members'){
            var mAction = (action_tab == 'strengthOverviewprogram') ? 'strengthOverviewprogram' : 'strengthProgramTraining';
            var melement = (action_tab == 'strengthOverviewprogram') ? 'programtabel' : 'programweektabel';
            manageAction	=	mAction;
            manageElement	=	melement;
        }
        getManageData(manageAction,manageElement,action_tab,pageindex)
    }catch(e) {
        ShowExceptionMessage("Event pageindex click", e);
    }
});

function getManageData(action,table,activity,pageindex){
    logStatus("Calling getManageData ", LOG_FUNCTIONS);
    logStatus(action,LOG_DEBUG);
    logStatus(table,LOG_DEBUG);
    try{
        $('#Serachtypeval').val(activity);
        var clubId	=	getselectedclubid();
        var pathdata	=	getPathHashData();
        var listtype	=	(pathdata.args._tp) ? pathdata.args._tp:'l';
        $('.tabsbox').on('click','span',function(){
            $('.tabsbox > span').removeClass("active");
            $(this).addClass("active");
        });
        var postdata	=	{};
        if(pageindex){
            postdata.page	=	pageindex;
        }
        postdata.tab	=	activity;
        getListMachinesData({listType:listtype,clubId:clubId},postdata,function(response){
            if(listtype=='l'){
                $('.managetable tbody').html(response);
            }else{
                $('.managetable').html('');
                $('.exercise_gridbox').html(response);
                }
        });
    } catch(e) {
        ShowExceptionMessage("getManageData", e);
    }
}
function saveNewProgrmDetails(){
    logStatus("Calling getManageData ", LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var programvalue = $(".selectedNew_ProgamID").val();

        $(".popup_new > .grid-block").removeClass('displaynonetable');
        //alert(programvalue);
        $(".popup_new_bg").show();
        $(".popup_new").show();
        return false;
        if(programvalue!=''){
            /**
             * @param response.status_code This is status code
             */
            var data = {};
            data.strengthProgramId 	= programvalue;
            data.clubID	   			=  getselectedclubid();
            data.hiddenUserId  		=  memberId;
            data.coach_id			= getCurrentLoggedinUserId();
            data.testOption			= 1; // max load statically;
            data.r_strength_user_test_type	= 1; // max load statically;
            data.testValuesCalculation		= 1; // max load statically;
            //data.test_status				= 1; // max load statically;
            return false;

            /*_saveNewProgramStatus(data,function(response){
                //alert(JSON.stringify(response));
                if(response.status_code == 200){
                    ShowToastMessage(response.status_message, _TOAST_LONG);
                }
            });*/
        }
    } catch(e) {
        ShowExceptionMessage("getManageData", e);
    }
}