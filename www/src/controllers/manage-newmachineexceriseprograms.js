$(function(){
    logStatus("Calling Event.MachineList.Load", LOG_FUNCTIONS);
    try{
        TranslateGenericText();
        Translationnewmachinexcerise();
        _setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_NEWMACHINESEXERCISE));
        // $("#selecrfilterdatavalue").attr('placeholder',GetLanguageText(PH_SEARCH));
        // $(".machinescls").text(GetLanguageText(BTN_MACHINES));
        // $(".exercisescls").text(GetLanguageText(BTN_EXERCISES));
        // $(".activitiescls").text(GetLanguageText(BTN_ACTIVITIES));
        // $(".dumbellscls").text(GetLanguageText(BTN_DUMBELLS));
        /**
         * @param  pathdata.args._tp This is argument tp
         */
        var pathdata=	getPathHashData();
        var listtype	=	(pathdata.args._tp) ? pathdata.args._tp:'l';
        var clubId	=	getselectedclubid();
        getListMachinesData({listType:listtype,clubId:clubId},{},function(response){
            if(listtype=='l'){
                $('.managetable tbody').html(response);
            }else{
                var $managetable= $('.managetable');
                $managetable.html('');
                $('.exercise_gridbox').html(response);
                $managetable.hide();
                }
        });
    } catch(e) {
        ShowExceptionMessage("getManageData", e);
    }
});

function Translationnewmachinexcerise(){
     TranslateText(".transearch", PHTRANS,TEX_SEARCH );
     TranslateText(".transmachines", TEXTTRANS, BTN_MACHINES);
     TranslateText(".transexercises", TEXTTRANS, BTN_EXERCISES);
     TranslateText(".transcardio", TEXTTRANS, BTN_CARDIO_TEST);
     TranslateText(".transactivities", TEXTTRANS, BTN_ACTIVITIES);
     TranslateText(".transdumbells", TEXTTRANS, BTN_DUMBELLS);
    TranslateText(".transimages", TEXTTRANS, BTN_IMAGES);
    TranslateText(".transname", TEXTTRANS, LBL_NAME);
    TranslateText(".transbrand", TEXTTRANS, LBL_BRAND);
    TranslateText(".transtype", TEXTTRANS, LBL_TYPE);
    TranslateText(".transgroup", TEXTTRANS, LBL_GROUP);
    TranslateText(".transsubtype", TEXTTRANS, LBL_SUBTYPE);
    TranslateText(".transstatus", TEXTTRANS, LBL_STATUS);
    TranslateText(".transclubn", TEXTTRANS, LBL_CLUBN);

}

/* MK - Moved Code to Structure*/
/* SK-New machine Pagination click event*/
$(document).on("click",'.pageindex',function(){
    logStatus("Event pageindex click", LOG_FUNCTIONS);
    try {
        var $self		=	$(this);
        var manageAction	=	"";
        var manageElement	=	"";
        var page		=	$self.attr('page');
        var pageindex	=	$self.attr('pageindex');
        var action_tab	=	$self.attr('action_tab');
        if(page=='members'){
            manageAction	=	'ListMachinesData';
            manageElement	=	'.managetable';
        }
        getManageData(manageAction,manageElement,action_tab,pageindex)
    }catch(e) {
        ShowExceptionMessage("Event pageindex click", e);
    }
});
$(document).delegate('#idgridlistimages',"click",function(){
    logStatus("Calling idgridlistimages click event",LOG_FUNCTIONS);
    try {
        var assoc = $(this).attr("attr");
        if(assoc){
            if(assoc!='' && assoc!= 'undefined' && assoc!= "NULL"){
                _movePageTo("manage-listmachines",{editID:base64_encode(assoc)});
            }
        }
    } catch(e){
        ShowExceptionMessage("idgridlistimages",e);
    }
});
function getManageData(action,table,activity,pageindex){
    logStatus("Calling getManageData ", LOG_FUNCTIONS);
    logStatus(action, LOG_DEBUG);
    logStatus(table, LOG_DEBUG);
    try{
        $('#Serachtypeval').val(activity);
        var clubId	=	getselectedclubid();
        var pathdata	=	getPathHashData();
        var listtype	=	(pathdata.args._tp) ? pathdata.args._tp:'l';
        $('.tabsbox').on('click','span',function(){
            $('.tabsbox > span').removeClass("active");
            $(this).addClass("active");
        });
        var postdata	=	{};
        if(pageindex){
            postdata.page	=	pageindex;
        }
        postdata.tab	=	activity;
        getListMachinesData({listType:listtype,clubId:clubId},postdata,function(response){
            if(listtype=='l'){
                $('.managetable tbody').html(response);
            }else{
                $('.managetable').html('');
                $('.exercise_gridbox').html(response);
                }
        });
    } catch(e) {
        ShowExceptionMessage("getManageData", e);
    }
}
function SearchResult(action,_element,search_type){
    logStatus("Calling SearchResult ", LOG_FUNCTIONS);
    try{
        var clubId	=	getselectedclubid();
        var tab = $("#Serachtypeval").val();
      //  var filterby = $('#selecrfiltervalue').val();
      var searchValue	=	$("#selecrfilterdatavalue").val();
        getSearchResultexercisedata(action,_element,clubId,'',search_type,tab,searchValue,function(response){
            $(_element).find('tbody').html(response);
        });
    } catch(e) {
        ShowExceptionMessage("SearchResult", e);
    }

}
/*delete new machine details*/ 
function deleteMachine(){
    logStatus("Calling deleteMachine", LOG_FUNCTIONS);
    try{
        var checkedval =  $(".machinedetails:checked").attr("hrefvalue");
        /*var checkval = new Array();
        $(".machinedetails:checked").each(function(){
            var checkedval = $(this).attr("hrefvalue");
            checkval.push(checkedval);
        });
        var checkedval = checkval.join(); */
        if(checkedval){
            deleteNewMachine(checkedval,function(response){
                ShowToastMessage(response.status_message, _TOAST_LONG);
                _movePageTo('manage-newmachinesexerciseprogram');
            });
        } else {
            ShowToastMessage(GetLanguageText(EXCP_INVALID_CLICK_EVENT_MSG), _TOAST_LONG);
        }

    } catch(e){
        ShowExceptionMessage("deleteMachine",e);
    }
}
/*edit new machine details*/ 
function editMachine(){
    logStatus("Calling editMachine", LOG_FUNCTIONS);
    try{
        var chekedval 	= $(".machinedetails:checked").attr("hrefvalue");
        /*var checkval = new Array();
        $(".machinedetails:checked").each(function(){
            var checkedval = $(this).attr("hrefvalue");
            checkval.push(checkedval);
        });
        var data = checkval.shift();*/
        if(chekedval){
            if(chekedval!='' && chekedval!= 'undefined' && chekedval!= "NULL"){
                _movePageTo("manage-listmachines",{editID:base64_encode(chekedval)});
            }
        } else {
            ShowToastMessage(GetLanguageText(EXCP_INVALID_CLICK_EVENT_MSG), _TOAST_LONG);
        }

    } catch(e){
        ShowExceptionMessage("editMachine",e);
    }
}
/*edit new machine data*/
/*
function machinetestdetails(val){
    logStatus("Calling machinetestdetails",LOG_FUNCTIONS);
    try{
        var editID = val;
        if(editID!='' && editID!= 'undefined' && editID!= "NULL"){
            _movePageTo("manage-listmachines",{editID:base64_encode(editID)});
        }

    } catch(e){
        ShowExceptionMessage("machinetestdetails",e);
    }
}
*/
$(document).ready(function(){

    maincontent_leff_none();
    // $(".main_left").removeClass("background3").addClass("background3");

    $(window).resize(function(){
        //maincontent_leff_none();
    });
});