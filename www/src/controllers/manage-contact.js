$(document).ready(function(){
    _setPageTitle(GetLanguageText(PAGE_TTL_CONTACT));
    TranslateGenericText();
    TransManageContact();
    //maincontent_leftimage();
    $(".main_left").show().removeClass("background3 background2 background4").addClass("background1");
    contact_boxmargin_top();
});
function contact_boxmargin_top(){
    var $contactbox = $('.contact_box');
    var contactheight = $contactbox.outerHeight();
    var header = $(".header").outerHeight();
    var footer = $("footer").outerHeight();
    var windowHEIGHT = $( window ).height();
    var contentheight = windowHEIGHT-(header+footer);
    var balanceheight = contentheight-contactheight;
    var margintop = (balanceheight)/2;
    if (margintop<0){
        margintop = 0;
    }
    $contactbox.css({
        "margin-top": margintop
    });
}
$(window).resize(function(){
    contact_boxmargin_top()
});			
function saveContactFormDetail(eobj){
        logStatus("Calling saveContactdetail",LOG_FUNCTIONS);
        try{
            if(validateSteps('#contactform')){
                var data = {};
                data.name=$("#name").val();
                data.emailid=$("#emailid").val();
                data.phoneno=$("#phoneno").val();
                data.subject=$("#subject").val();
                data.message=$("#message").val();
                AddProcessingLoader(eobj);
                saveContactForm(data,function(response){
                    var ack	=	"Mail sending failed. Please try again later";
                    if((response.status_code) && response.status_code==1){
                        ack	=	"Mail sent successfully";
                    }
                    ShowToastMessage(ack);
                    RemoveProcessingLoader(eobj);
                });
            }
        } catch(e){
            ShowExceptionMessage("saveContactdetail", e);
        }
    }

function TransManageContact(){
    TranslateText(".transsubject",TEXTTRANS, LBL_SUBJECT);
    TranslateText(".transname", TEXTTRANS, LBL_NAME);
    TranslateText(".transphone", TEXTTRANS, PH_PHONE);
    TranslateText(".transphone", TEXTTRANS, MENU_MESSAGE);
    TranslateText(".transpname", PHTRANS, LBL_NAME);
        TranslateText(".transpemail", PHTRANS, PH_EMAIL);
    TranslateText(".transtelephone", PHTRANS, PH_PHONE);
    TranslateText(".transpsubject", PHTRANS, TXT_ENTER_SUBJECT);
    TranslateText(".transenquiry", PHTRANS, TXT_ENQUIRY);
    TranslateText(".transcontactus", TEXTTRANS, TXT_CONTACT_US);
    TranslateText(".transsend", TEXTTRANS, LBL_SEND);

}