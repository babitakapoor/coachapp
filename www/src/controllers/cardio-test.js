var tMember	=	{};
var MAPPED_DEVICE_DET = {};
var HeartRateLoadValueTimer = null;
var tMemberList = [];
var _TEST_MONITOR_REFRESH_TIMER;

var _MAIN_CHAR_OBJECT = null;
var _MAIN_CHAR_OBJECT_SR_WATT = [];
var _MAIN_CHAR_OBJECT_SR_RPM = [];
var _MAIN_CHAR_OBJECT_SR_HRM = [];
var _MAIN_CHAR_OBJECT_RPM = null;
var _MAIN_CHAR_OBJECT_SR_RPMM = [];

$(function(){
    logStatus("Calling Event.CardioTest.Load", LOG_FUNCTIONS);
    try {
        TranslateGenericText();
        _setPageTitle(GetLanguageText(PAGE_TTL_CARDIOTEST));
        TranslationCardioTest();
        var urldata	=	getPathHashData();
        var memberID	=	GetUrlArg('member',true,"0");
        _setCacheValue('lasttestmemberid',memberID);

        if(urldata.path=='cardio-devices'){
            CardioDeviceLoad();
        } else if(urldata.path=='manage-test'){
            $(".manage_test_popup").css({"top": $(".header").outerHeight(), "bottom": $("footer").outerHeight()});
            loadMembersTestPanels();
        } else if(urldata.path=='cardio-monitor'){
            /*MK Test Step:1 - Check Member Selected*/
            if(memberID!=0){
                loadMemberTest();
            } else {
                /*Select Member for test*/
                ShowToastMessage(GetLanguageText(EXCEPTION_SELECT_VALID_USER),_TOAST_LONG);
                _movePageTo('cardio-test');
            }
        } else {
            //Show list of client to show whether it is mapped or not "cardio-test" page
            LoadCardioTestPage();
        }
    } catch (e) {
        ShowExceptionMessage("Event.CardioTest.Load", e);
    }
});

/* General Functions  - Start */
function TranslationCardioTest(){
    TranslateText(".welcome_para", TEXTTRANS, TEX_WELCOME_STRONG);
    TranslateText(".transenterclientcode",PHTRANS, PH_ENTER_CLIENT_CODE);
    TranslateText(".transsearchclient",PHTRANS, PH_SEARCH_CLIENT);
    TranslateText(".code_label",TEXTTRANS, LBL_CODE_LABEL);
}
function clientPathReidrect(val, page){
    logStatus("Calling clientPathReidrect", LOG_FUNCTIONS);
    try {
        /*Map To Device Need Implement Now Temporarily set to Device Map*/
        _movePageTo(page,{member:base64_encode(val)});
    } catch (e) {
        ShowExceptionMessage("clientPathReidrect", e);
    }
}
/* General Functions  - End */


/* cardio-test  - Start */
function LoadCardioTestPage(){
    var clubId	=	getselectedclubid();
    var testDate=	_getDateForInput();
    var  $selectlistul= $('.selectlist_ul');
    getClientsdata({status:TEST_STATUS_YETTO_START+','+TEST_STATUS_STARTED+','+CARDIO_TEST_STATUS_Reset+','+CARDIO_TEST_STATUS_Editmode+','+CARDIO_TEST_STATUS_cooldown},function(response){
        var countlen = response.length;
        logStatus(countlen,LOG_DEBUG);
        $selectlistul.html("");
        if(response) {
            /**
             * @param rowdata.attacheddevice This is attach device
             * @param hrmapdata.device_code This is  device code
             */
            $.each(response,function(key,rowdata){
                if(rowdata)  {
                    getValidImageUrl(PROFILEPATH + rowdata.userimage,function(imageurl){
                        var mappeddevicelist = '';
                        var mappeddevicelisthidden = '';
                        if (rowdata.attacheddevice){
                            if ((rowdata.attacheddevice.mapDevice) && ((rowdata.attacheddevice.mapDevice) != '')){
                                if (rowdata.attacheddevice.total_records == 1){
                                    rowdata.attacheddevice.mapDevice = new Array(rowdata.attacheddevice.mapDevice);
                                }
                                $.each(rowdata.attacheddevice.mapDevice,function(mapkey,hrmapdata){
                                    if (hrmapdata.device_code != ''){
                                        if (mappeddevicelist!=''){
                                            mappeddevicelist += '<br>';
                                            mappeddevicelisthidden += ',';
                                        }
                                        mappeddevicelist += hrmapdata.device_code;
                                    }
                                });
                            }
                        }
                        if (mappeddevicelist == ''){
                            mappeddevicelist = 'No device Mapped';
                        }
                        //On Click we need to add this device to the client
                        var HTML =	'<li onclick="clientPathReidrect('+rowdata.user_id+', \'cardio-devices\')">'+
                                '<div class="listdiv_left">'+
                                    '<img src="' + imageurl + '" alt="user_image" id="cleintimage" />'+
                                '</div>'+
                                '<div class="listdiv_right">'+
                                    '<h3>'+rowdata.first_name+' '+rowdata.last_name+'<span></span></h3>'+
                                    '<p>'+mappeddevicelist+'</p>'+
                                '</div>'+
                        '</li>';
                        $selectlistul.append(HTML);
                    });
                }
            });
        } else {
            if($selectlistul.html()==''){
                $selectlistul.html(GetLanguageText(EXCP_NO_MEMBER_MAPPED));
            }
        }
    },clubId,testDate);
}

/* cardio-test  - End */


/* cardio-devices  - Start */
function CardioDeviceLoad(){
    var memberID = _getCacheValue('lasttestmemberid');
    _getClientInformationByID(memberID,function(response){
        if(response.first_name && response.last_name){
            $('#memberImage').attr("alt",response.first_name);
            $(".memberName").html(response.first_name);
            getValidImageUrl(PROFILEPATH + response.userimage,function(url){
                $('#memberImage').attr('src',url);
            });
        }
        getMappedDeviceInfo(function(response){
            $.each(response,function(mapkey,hrmapdata){
                MAPPED_DEVICE_DET[hrmapdata.device_code] = hrmapdata;
            });
            ScanHR_BLEDevice();
        })
    });
}

function SetHREventDetails(){
    logStatus("SetHREventDetails", LOG_FUNCTIONS);
   // $(".clsscanneddevicelist").die("click").live("click",function(evt){
    $(".clsscanneddevicelist").click(function(evt){
        //$(document).delegate(".heartratebox li","click",function(){
            logStatus(".heartratebox li.click", LOG_FUNCTIONS);
            try {
                var memberid = _getCacheValue('lasttestmemberid');
                var deviceid =  $(this).attr("deviceid");
                var $curdeviceelm	=	$(this).find(".listdiv_right");
                //listdiv_right
                var actualHtml	=	$curdeviceelm.html();
                var ph	=	getLogingPlaceholder();
                DisableProcessBtn($curdeviceelm[0],false,ph,true);
                updateMappedMemberDevice({deviceCode:deviceid,deviceType:DEVICE_TYPE_HR_monitor,memberId:memberid},function(response){
                    DisableProcessBtn($curdeviceelm[0],true,actualHtml,true);
                    if(response.status==1){
                        _setCacheValue('lasttestdeviceid', deviceid);
                        clientPathReidrect(memberid, 'cardio-monitor');
                    } else {
                        ShowToastMessage("Device is not updated. Please try again later")
                    }
                });
                evt.preventDefault()
            } catch (e) {
                ShowExceptionMessage(".heartratebox li.click", e);
            }
        //});
    });
}
/* cardio-devices  - End */

/* Devices call back start */
/* BLE Call backs by Sankar - Start */
function OnHearRateDeviceScanSuccess(devicelist){
    logStatus("OnHearRateDeviceScanSuccess", LOG_FUNCTIONS);
    if ((devicelist) && (devicelist.length>0)){
        var HTML	=	'';
        $.each(devicelist,function(_idx,rowdata){
            if (rowdata.id){
                var avilablestatus	=	"No Member Mapped";
                if (MAPPED_DEVICE_DET[rowdata.id]){
                    avilablestatus=	'Device is Mapped to '+MAPPED_DEVICE_DET[rowdata.id].last_name;
                }
                //HTML+=	'<li deviceid="'+rowdata.id+'" onclick="selectbtdevice("'+rowdata.id+'")">'+
                HTML+=	'<li class="clsscanneddevicelist" deviceid="'+rowdata.id+'">'+
                        '<div class="listdiv_left">'+
                            '<img src="images/userempty_image.png" alt="user_image" />'+
                        '</div>'+
                        '<div class="listdiv_right">'+
                            '<h4>' + avilablestatus + '</h4>'+
                            '<p>' + rowdata.id + '</p>'+
                        '</div>'+
                    '</li>';

            }
        });
        $(".heartratebox").html(HTML);
        $(".scanloading").html("");
        SetHREventDetails();
    }
}

function OnHeartRateConnectedCallBack(connecteddevice){
    logStatus("OnHeartRateConnectedCallBack", LOG_FUNCTIONS);

    StartBLENotificationProcess(connecteddevice.id, OnHeartRateDataSuccessCallBack, OnHeartRateDataErrorCallBack);

    /* Moved the below process to the start button. Since HR device will be connected when they are in this page * /
    /*MK Test Step:8 Start Heartrate Notification* /
    if((!$.isEmptyObject(tMember)) && (tMember.serverdata.user_test_id)){
        updateCardioTestActive(tMember.serverdata.user_test_id, function(){
            UpdateLocalMemberStatus(CARDIO_TEST_STATUS_started);
            tMember.chartobj = {};
            tMember.chartobj.chart = createNewChartObj('monitor_hrate');
            tMember.chartobj.data = {};
            tMember.chartobj.data.watt	=	tMember.chartobj.chart.series[0];
            tMember.chartobj.data.rpm	=	tMember.chartobj.chart.series[1];
            tMember.chartobj.data.hrm	=	tMember.chartobj.chart.series[2];
            StartBLENotificationProcess(connecteddevice.id, OnHeartRateDataSuccessCallBack, OnHeartRateDataErrorCallBack);
            tMember.loadActiveIndex	=	0;
            UpdateLoadValueOnTest();
            startWorkout(function(){
                UpdateTargetWATT(tMember.loadvalueObject[tMember.loadActiveIndex].load);//Since this can only be set after start
                checkAndUpdateTargetWATT(0);
            });
        });
        SendDetailsToCoach();
    } else {
        ShowAlertMessage(EXCEPTION_INVALID_TEST_SELECTED);
    }
    */
}
function OnHeartRateDisconnectCallBack(reason){
    logStatus("OnHeartRateDisconnectCallBack", LOG_FUNCTIONS);
    ShowAlertMessage('HR Device disconnected: ' + reason);
}

function OnHeartRateDataErrorCallBack(reason){
    logStatus("OnHeartRateDataErrorCallBack", LOG_FUNCTIONS);
    ShowAlertMessage('HR Device Error Reading HR Data:' + reason);
}

/*ASYNCH:1*/
function OnHeartRateDataSuccessCallBack(buffer){
    logStatus("OnHeartRateDataSuccessCallBack", LOG_FUNCTIONS);
    //if( ( (!_STOPPED) || (_CoolDownStarted) ) && isValidTestMonitorPage()){
    var data ='';
    if (isValidTestMonitorPage()){
        if ((tMember.isTestActive && tMember.machinedata.elapsedtime)) { //Just to makesure the machine started and Active
             data = new Uint8Array(buffer);
            var curdatetime = new Date();
            if ((!tMember.CurrHRTime) || (tMember.CurrHRTime == 0)){
                tMember.CurrHRTime = curdatetime;
            }
            var hrtime = Math.round((curdatetime - tMember.CurrHRTime)/1000);
            var hrtimeinmin = hrtime/60;


            if(!tMember.rpmGroupValue[tMember.loadActiveIndex]){
                tMember.rpmGroupValue[tMember.loadActiveIndex]	=	[];
            }
            if(!tMember.heartRateValue[tMember.loadActiveIndex]){
                tMember.heartRateValue[tMember.loadActiveIndex]	=	[];
            }
            if(tMember.ManualStatus == CARDIO_TEST_STATUS_cooldown){
                if(!tMember.coolDownHeartrateValue[tMember.loadActiveIndex]){
                    tMember.coolDownHeartrateValue[tMember.loadActiveIndex]	=	[];
                }
                tMember.coolDownHeartrateValue[tMember.loadActiveIndex].push(data[1]);
            }

            tMember.heartRateValue[tMember.loadActiveIndex].push({time:hrtime, data: data[1]});
            tMember.rpmGroupValue[tMember.loadActiveIndex].push({time:hrtime, data: tMember.machinedata.currentrpm});

            var hrcd	=	'';
            //var hrvalue	=	'';
            if(tMember.ManualStatus == CARDIO_TEST_STATUS_cooldown) {
                hrcd	=	JSON.stringify(tMember.coolDownHeartrateValue);
            }
            var hrvalue	=	JSON.stringify(tMember.heartRateValue);
            var updthrs	=	parseInt(hrtimeinmin/60);
            var seconds	=	parseInt(hrtime%60);

            updthrs	=	(updthrs.toString().split('').length>1) ? updthrs:'0'+updthrs;
            logStatus(updthrs,LOG_DEBUG);
            var updtmin	=	(parseInt(hrtimeinmin).toString().split('').length>1) ? parseInt(hrtimeinmin):'0'+parseInt(hrtimeinmin);
            var updsec	=	(seconds.toString().split('').length>1) ? seconds:'0'+seconds;
            //Update - TestElapsedTime:current_test_time
            logStatus(updtmin,LOG_DEBUG);
            logStatus(updsec,LOG_DEBUG);
            if (!tMember.machinedata.currentwatts){
                tMember.machinedata.currentwatts = 'N/A';
            }
            if (!tMember.machinedata.currentrpm){
                tMember.machinedata.currentrpm = 'N/A';
            }
            if (!tMember.machinedata.elapsedtime){
                tMember.machinedata.elapsedtime = 'N/A';
            }

            $("#cHRM").html(data[1]);
            $("#cWATT").html(tMember.machinedata.currentwatts+'');
            $("#cRPM").html(tMember.machinedata.currentrpm+'');
            $("#monitor_timer").val(tMember.machinedata.elapsedtime+'');

            /*MK Test Step:9 Push data into Update Object*/
            tMember.Update	=	{rpm:tMember.machinedata.currentrpm,rpmgroup:JSON.stringify(tMember.rpmGroupValue)};
            tMember.Update.actualtimeinsec 	= hrtime;
            tMember.Update.cRPM		=	tMember.machinedata.currentrpm;//This gets set from the machine matrix data
            tMember.Update.cHRM		=	data[1];
            tMember.Update.heartrate=	hrvalue;
            tMember.Update.coolDownHeartrate	=	hrcd;
            tMember.Update.rpmGroupValue	=	JSON.stringify(tMember.rpmGroupValue);
            tMember.Update.elapsedTime = tMember.machinedata.elapsedtime; //This gets set from the machine matrix data
            tMember.Update.TestElapsedTime = tMember.machinedata.elapsedtime;
            if (isNaN(tMember.Update.cRPM)){
                tMember.Update.cRPM = 0;
            }
            //PushChartData(tMember.chartobj.data.watt, {x: hrtimeinmin,y: tMember.CurrTargetWATT,color:'#659355',marker:{ fillColor: 'red'}});
            PushChartData(tMember.chartobj.data.watt, [hrtimeinmin, tMember.CurrTargetWATT]);
            PushChartDataRPM(tMember.chartobj.data.rpm, [hrtimeinmin, tMember.Update.cRPM]);
            PushChartData(tMember.chartobj.data.hrm, [hrtimeinmin, tMember.Update.cHRM]);
        } else {
             data = new Uint8Array(buffer);

            $("#cHRM").html(data[1]);
        }
    }else{
        //MK peripheralid could not find how to get
        StopBLENotificationProcess(_getCacheValue('lasttestdeviceid'));
    }
}
/* BLE Call backs by Sankar - End*/


/*ASYNCH:2*/
/* Cybex Call backs by Sankar - Start*/
function CallBackFromCardioMachineMatrixdata(machinedata){
    logMessage("CallBackFromCardioMachineMatrixdata");
    /*{elapsedtime: '%@', calories: '%d', caloriesPerHour: '%d', mets: '%.1f', watts: '%d', averageWatts: '%d', distanceInmiles: '%.2f', bpm: '%d', peakBpm: '%d', secondsInHeartRateZone: '%d', speedInMil: '%.1f', avgspeedInmiles: '%.1f', paceInSec: '%d:%02d', avgPaceInSec: '%d:%02d', currentStridesPerMinute: '%d', averageStridesPerMinute: '%d', currentTargetSpm: '%d', currentCrankRPM: '%d', averageCrankRPM: '%d'}*/
    tMember.machinedata.currentrpm = machinedata.currentCrankRPM;
    tMember.machinedata.elapsedtime = machinedata.elapsedtime;
    tMember.machinedata.currentwatts = machinedata.watts;
    //logMessage("CallBack:"+JSON.stringify(machinedata));
    //logMessage("CallBack:"+machinedata.watts);
}

function OnEquipmentAttachSuccess(){
    getConnectedMachineName(function(machine){
        $("#idconnectedmachinename").val(machine);
    });
}
/* Cybex Call backs by Sankar - End*/

/* Devices call back end */

/* Devices Instructions start */
function UpdateTargetWATT(newwatt){
    logStatus("UpdateTargetWATT", LOG_FUNCTIONS);
    tMember.CurrTargetWATT = parseInt(newwatt);
    setCurrentTargetWatt(tMember.CurrTargetWATT);

}

function checkAndUpdateTargetWATT(_index){
    logStatus("Calling checkAndUpdateTargetWATT", LOG_FUNCTIONS);
    try {
        var index	=	(_index) ? _index:0;
        //var custom	=	(_custom) ? _custom:{};
        if(!$.isEmptyObject(tMember.loadvalueObject[index])){
            if (HeartRateLoadValueTimer){
                clearTimeout(HeartRateLoadValueTimer);
            }
            var currtimediff = tMember.loadvalueObject[index].timediff * 1000;
            HeartRateLoadValueTimer	=	setTimeout(function(){
                if(index < tMember.loadvalueObject.length){
                    if((tMember.ManualStatus != CARDIO_TEST_STATUS_cooldown) && (tMember.ManualStatus != CARDIO_TEST_STATUS_stopped)){
                        index++;
                        tMember.loadActiveIndex	=	index;
                        UpdateTargetWATT(tMember.loadvalueObject[index].load);
                        UpdateLoadValueOnTest();
                        checkAndUpdateTargetWATT(index);
                    }
                }
            },currtimediff);
        }
    } catch (e) {
        ShowExceptionMessage("checkAndUpdateTargetWATT", e);
    }
}
/* Devices Instructions end */


/* cardio-monitor - client View  - Start */
function loadMemberTest(mid){
    logStatus("Calling loadMemberTest", LOG_FUNCTIONS);
    logStatus(mid, LOG_DEBUG);
    try {
        var MemberID = _getCacheValue('lasttestmemberid');
		
        /*MK Test Step:2 - Get Connect Device For Test - Confirm once*/
        OnEquipmentAttachSuccess(); //Just to set the machine name
        /*MK Test Step:3 - Service Call for Get Member Data*/
        ResetMemberObject();
        GetMemberTestDetail(function(response){
            if(response && response[0]) {
                /*MK Test Step:4 Prepare Test to start*/
                UpdateMemberResponseObject(response[0]);
                PrepareTestToRun(function(){//Service 2: Reset Test Record To Start
                    UpdateLocalMemberStatus(CARDIO_TEST_STATUS_stopped);
                    SetClientDetailsFromObj();
                    CheckChangesFromCoach();

                    //Added Since we want to show the HearRate already;
                    var deviceid = _getCacheValue('lasttestdeviceid');
                    if (deviceid != ''){
                        /*MK Test Step:7 Connect Heartrate monitor*/
                        BLEDeviceConnect(deviceid);
                    } else {
                        /*MK Test Step : Connected HeartRate Device*/
                        ShowAlertMessage('Please go back and select the device.');
                    }

                })
            } else {
                /*MK No Test Invalid*/
                ShowAlertMessage("Member have no test / Or Invalid Test Request")
            }
        },MemberID);//MemberID optional
    } catch (e) {
        ShowExceptionMessage("loadMemberTest", e);
    }
}

function GetMemberTestDetail(onTestResponseSuccess,MemberID){
    logStatus("Calling GetMemberTestDetail", LOG_FUNCTIONS);
    try {
        var clubId	=	getselectedclubid();
        var testDate=	_getDateForInput();
        var allowedStatus	=	TEST_STATUS_YETTO_START+','+TEST_STATUS_STARTED+','+CARDIO_TEST_STATUS_Reset+','+CARDIO_TEST_STATUS_Editmode+','+CARDIO_TEST_STATUS_cooldown;
        //Service 1: Get Member Test Details
        getMembersTestResultdataForClubandDate(clubId,testDate,allowedStatus,onTestResponseSuccess,MemberID);
    } catch (e) {
        ShowExceptionMessage("GetMemberTestDetail", e);
    }
}
function PrepareTestToRun(onResetSuccess){
    logStatus("Calling PrepareTestToRun", LOG_FUNCTIONS);
    try {
        /**
         * @param serverdata.r_user_test_id This is user test id
         */
        _updateHeartRateOnServer({test_active:1,
            status:CARDIO_TEST_STATUS_stopped,
            loadData:'',
            rpm:'',
            rpmgroup:'',
            heartrate:'',
            coolDownStart:0,
            coolDownStop:0,
            coolDownHeartrate:'',
            TestElapsedTime:''},
            tMember.serverdata.r_user_test_id,
        function(response){
            if(onResetSuccess){
                onResetSuccess(response)
            }
        });
    } catch (e) {
        ShowExceptionMessage("PrepareTestToRun", e);
    }
}

function startCardioTestDevice(ismanual){
    logStatus("Calling startCardioTestDevice", LOG_FUNCTIONS);
    logStatus(ismanual, LOG_DEBUG);
    try {
        /**
         * @param serverdata.user_test_id This is user test id
         */
        DisableProcessBtn("#monitor_cooldown", false);
        //var _ManualStarted	=	ismanual;
        getConnectedMachineFamily(function(m){
            /*MK Test Step:5 Check Connected Device*/
            if(m>0){
                setMachinetoConstantPowerMode(function(){
                    /*MK Test Step:6 Set Constant Power mode*/

                    /* Commented As they want to start the HR when they are in this page. Instead checking whether devices is connected *
                    var deviceid = _getCacheValue('lasttestdeviceid');
                    if (deviceid != ''){
                        /*MK Test Step:7 Connect Heartrate monitor* /
                        BLEDeviceConnect(deviceid);
                    } else {
                        /*MK Test Step : Connected HeartRate Device* /
                        ShowAlertMessage(EXCEPTION_SELECT_DEVICE_ERROR);
                    }
                    /**/
                    var deviceid = _getCacheValue('lasttestdeviceid');
                    if (deviceid != ''){
                        /*MK Test Step:7 Check Heartrate monitor is connected*/
                        isHR_Connected(deviceid, function(){
                            /*MK Test Step:8 Start Heartrate Notification*/
                            if((!$.isEmptyObject(tMember)) && (tMember.serverdata.user_test_id)){
                                updateCardioTestActive(tMember.serverdata.user_test_id, function(){
                                    UpdateLocalMemberStatus(CARDIO_TEST_STATUS_started);
                                    tMember.chartobj = {};
                                    tMember.chartobj.chart = createNewChartObj('monitor_hrate');
                                    tMember.chartobj.data = {};
                                    tMember.chartobj.data.watt	=	tMember.chartobj.chart.series[0];
                                    tMember.chartobj.data.rpm	=	tMember.chartobj.chart.series[1];
                                    tMember.chartobj.data.hrm	=	tMember.chartobj.chart.series[2];
                                    //StartBLENotificationProcess(connecteddevice.id, OnHeartRateDataSuccessCallBack, OnHeartRateDataErrorCallBack);
                                    tMember.loadActiveIndex	=	0;
                                    UpdateLoadValueOnTest();
                                    startWorkout(function(){
                                        UpdateTargetWATT(tMember.loadvalueObject[tMember.loadActiveIndex].load);//Since this can only be set after start
                                        checkAndUpdateTargetWATT(0);
                                        $("#monitor_cooldown").removeAttr("disabled");
                                    });
                                });
                                SendDetailsToCoach();
                            } else {
                                ShowAlertMessage("Invalid Test is selected");
                            }

                        }, function(){
                            /*MK Test Step : Connected HeartRate Device*/
                            ShowAlertMessage('Please go back and select the device.');
                        });
                    } else {
                        /*MK Test Step : Connected HeartRate Device*/
                        ShowAlertMessage('Please go back and select the device.');
                    }

                });
            } else {
                ShowAlertMessage(EXCEPTION_CONNECT_MACHINE);
            }
        });
    } catch (e) {
        ShowExceptionMessage("startCardioTestDevice", e);
    }
}

function startCoolDown(){
    logStatus("Calling startCoolDown", LOG_FUNCTIONS);
    try {
        DisableProcessBtn("#monitor_cooldown", false);
        var coolDownLimitInterval	= 0.25;//minutes
        logStatus(coolDownLimitInterval,LOG_DEBUG);
        var stopTimeoutLimit= 3;//minutes
        var CoolDownWATT	= 40; //Need to move Constants / get From Data
        if (!tMember.Update){
            return
        }
        _updateHeartRateOnServer({status:CARDIO_TEST_STATUS_cooldown,coolDownStart:tMember.Update.actualtimeinsec},tMember.serverdata.r_user_test_id,function(){
            $("#monitor_cooldown").removeAttr("disabled");
            UpdateLocalMemberStatus(CARDIO_TEST_STATUS_cooldown);
            coolWorkout();
            UpdateTargetWATT(CoolDownWATT);
            tMember.loadActiveIndex++;
            tMember.loadValueData.push({"speed":"-1","time":"-1","timediff":"-1","load":CoolDownWATT,"loadtype":"3"});
            UpdateLoadValueOnTest();
            if(tMember.autostoptimer){
                clearTimeout(tMember.autostoptimer);
            }
            tMember.autostoptimer = setTimeout(function(){
                stopworkoutprocess();
            },stopTimeoutLimit*60*1000);

        });
    } catch (e) {
        ShowExceptionMessage("startCoolDown", e);
    }
}

function stopworkoutprocess(){
    logStatus("Calling stopworkoutprocess", LOG_FUNCTIONS);
    try {
        DisableProcessBtn("#monitor_cooldown", false);
        if(tMember.autostoptimer){
            clearTimeout(tMember.autostoptimer);
        }
        stopWorkout();
        if (!tMember.Update){
            return
        }
        _updateHeartRateOnServer({coolDownStop:tMember.Update.actualtimeinsec,test_active:0,testCompleted:1,status:CARDIO_TEST_STATUS_completed},tMember.serverdata.r_user_test_id,function(){
            UpdateLocalMemberStatus(CARDIO_TEST_STATUS_completed);
            ShowToastMessage(GetLanguageText(SUCCESS_SAVE_CARDIO_TEST),_TOAST_LONG);
            _movePageTo('manage-cardiotest4');
            $("#monitor_cooldown").removeAttr("disabled");
        })
    } catch (e) {
        ShowExceptionMessage("stopworkoutprocess", e);
    }
}

function createNewChartObj(selector, seriesdata){
    logStatus("createNewChartObj", LOG_FUNCTIONS);
    var wattdata = [];
    //var rpmdata = [];
    var hrdata = [];
    if (seriesdata){
        wattdata = (seriesdata['wattdata'])?seriesdata['wattdata']:[];
        //rpmdata = (seriesdata['rpmdata'])?seriesdata['rpmdata']:[];
        hrdata = (seriesdata['hrdata'])?seriesdata['hrdata']:[];
    }
    var newchartobj	=	new Highcharts.Chart({
        chart: {
            renderTo:selector
        },
        xAxis: {
            tickInterval: 2,
            minPadding: 0,
            maxPadding: 0,
			endOnTick:false,
			min:0,
			max:45
        },
		yAxis: {
            tickInterval: 25,
            minPadding: 0,
            maxPadding: 0,
			endOnTick:false,
			title: {
            text: ''
        },
			min:0,
			max:250

        },
		 credits: {
			enabled: false
		},
		title :{
            text: "",
            /* wrap:true,
            fontSize: 30, */
        },
        series: [{

                    step: 'left',
                    name:'Watt',
                    type:'area',
                    data: wattdata,
					showInLegend: false, 
                    marker : {
                        enabled : false
                    }
                },
				  /* {
                    name:'RPM',
                    type:'spline',
                    data: rpmdata,
                    color: 'blue',
                    marker : {
                        enabled : false
                    }
                }, */
                {
                    name:'HRM',
                    type:'spline',
                    data: hrdata,
                    color: 'red',
					showInLegend: false, 
                    marker : {
                        enabled : false
                    }
                }]
    });
    return newchartobj;
}

function createNewChartObjRPM(selector, seriesdata){
    logStatus("createNewChartObjRPM", LOG_FUNCTIONS);
    var rpmdata = [];
    if (seriesdata){
        rpmdata = (seriesdata['rpmdata'])?seriesdata['rpmdata']:[];
    }
    var newchartobjRPM	=	new Highcharts.Chart({
        chart: {
            renderTo:selector
        },
        yAxis: {
			endOnTick:false,
			labels:{enabled: false},
			visible: false,
			tickInterval:15,
			max:100
        },
		xAxis: {
			labels:{enabled: false},
			visible: false,
			tickInterval:1,
			min:0,
			max:30
        },
		 credits: {
			enabled: false
		},
		title :{
            text: "",
        }, 
        series: [{
                    name:'RPM',
                    type:'spline',
                    data: rpmdata,
                    color: 'blue',
					showInLegend: false, 
                    marker : {
                        enabled : false
                    }
                }]
    });
    return newchartobjRPM;
}

/*ASYNCH:3*/
function SendDetailsToCoach(){
    _updateHeartRateOnServer(tMember.Update,tMember.serverdata.r_user_test_id,function(){
        if(tMember.tmrsendchangestocoach){
            clearTimeout(tMember.tmrsendchangestocoach);
        }
        if (isValidTestMonitorPage()){
            tMember.tmrsendchangestocoach=setTimeout(function(){
                SendDetailsToCoach();
            },3000);
        }
    });
}

/*ASYNCH:4*/
function CheckChangesFromCoach(){
    GetMemberTestDetail(function(response){
        UpdateMemberResponseObject(response[0]);
        UpdateTestDetails();

        if(tMember.timerchangesfromcoach){
            clearTimeout(tMember.timerchangesfromcoach);
        }
        if (isValidTestMonitorPage()){
            tMember.timerchangesfromcoach=setTimeout(function(){
                CheckChangesFromCoach();
            },3000);//Object update should be in pulse
        }

    },tMember.serverdata.user_id);
}

function SetClientDetailsFromObj(){
    var clientobj = tMember.serverdata;
    if(clientobj.first_name && clientobj.last_name){
        $('#memberImage').attr("alt",clientobj.first_name);
        $(".memberName").html(clientobj.first_name + " " + clientobj.last_name);
        getValidImageUrl(PROFILEPATH + clientobj.userimage,function(url){
            $('#memberImage').attr('src',url);
        });
    }
    var memeberage = getAgeByDOB(clientobj.dob);
    $("#monitor_ageontest").val(memeberage);
    if(clientobj.test_date){
        $("#monitor_testdate").val(getDateFromDBDateAndTime(clientobj.test_date));
    }
}

function UpdateMemberResponseObject(TestData){
    /**
     * @param TestData.total_load_value This is total load value
     */
    tMember.serverdata		=	TestData;
    try{
        tMember.loadvalueObject= (TestData.total_load_value!='') ? JSON.parse(TestData.total_load_value):[];
    }catch(e){
        tMember.loadvalueObject= []
    }

    //tMember.isCoolingDown	=	CheckIsCoolingDown(TestData);
    tMember.isTestActive	=	(TestData.test_active && TestData.test_active==1) ? true:false;
}

function UpdateTestDetails(){
    var  $monitor_weightontest= $("#monitor_weightontest");
    var  $monitor_cooldown= $("#monitor_cooldown");
    /**
     * @param serverdata.testweight This is test weight
     */
    if((!$.isEmptyObject(tMember)) && (tMember) && (!$.isEmptyObject(tMember))){

        if ( ($monitor_weightontest.val() != tMember.serverdata.testweight) ||
              ($("#idtestlevels").val() != tMember.serverdata.test_level)){

            LoadTestLevelList(tMember.serverdata.testweight, function(levelresponse){
                PopulateTestLevels('.cdbdcmppersontestlevel', levelresponse);
                var selectedLevel =	tMember.serverdata.test_level;
                if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
                    $("#idtestlevels").val(selectedLevel);
                }
            });
            $monitor_weightontest.val(tMember.serverdata.testweight);
        }


        if((tMember.serverdata.status == CARDIO_TEST_STATUS_Reset) || (tMember.serverdata.status == CARDIO_TEST_STATUS_stopped)){
            if (tMember.ManualStatus != CARDIO_TEST_STATUS_stopped){
                stopworkoutprocess();
            }
            $monitor_cooldown.html(GetLanguageText(BTN_START));
            $monitor_cooldown.attr("onclick","startCardioTestDevice(true)");
            $monitor_cooldown.removeAttr("disabled");
        } else if(tMember.serverdata.status == CARDIO_TEST_STATUS_started){
            //If it is manual start this object would have been set. Hence we assume that this could have initiated by the coach
            if (tMember.ManualStatus != CARDIO_TEST_STATUS_started){
                startCardioTestDevice(true);
            }
            $monitor_cooldown.html(GetLanguageText(BTN_COOLDOWN));
            $monitor_cooldown.attr("onclick","startCoolDown()");
            $monitor_cooldown.removeAttr("disabled");
        } else if(tMember.serverdata.status == CARDIO_TEST_STATUS_cooldown){
            if (tMember.ManualStatus != CARDIO_TEST_STATUS_cooldown){
                startCoolDown();
            }
            $monitor_cooldown.html(GetLanguageText(BTN_STOP));
            $monitor_cooldown.attr("onclick","stopworkoutprocess()");
            $monitor_cooldown.removeAttr("disabled");
        }

        /*
        if(isValidTestMonitorPage()){
            if(CardioTestTimer){
                clearTimeout(CardioTestTimer);
            }
            CardioTestTimer	=	setTimeout(function(){
                cardioTestDetailsByID(TestId);
            },1000);
        }
        */
    }
}

function ResetMemberObject(){
    logStatus("Calling ResetMemberObject", LOG_FUNCTIONS);
    try {
        tMember	=	{};
        tMember.serverdata =	{};
        tMember.rpmGroupValue = {};
        tMember.heartRateValue = {};
        tMember.coolDownHeartrateValue = {};
        tMember.rpmGroupValue = {};
        tMember.Update	=	{};
        tMember.machinedata	=	{};
        tMember.loadActiveIndex = 0;
        tMember.loadValueData = [];
        tMember.ManualStatus = CARDIO_TEST_STATUS_stopped;
        tMember.CurrHRTime = 0;
        tMember.autostoptimer = null;
    } catch (e) {
        ShowExceptionMessage("ResetMemberObject", e);
    }
}

function updateCardioTestActive(userTestID,onUpdateSuccess){
    logStatus("Calling updateCardioTestActive", LOG_FUNCTIONS);
    try {
    _updateHeartRateOnServer({test_active:1,coolDownStart:0,coolDownStop:0,loadData:'',heartrate:'',rpm:'',coolDownHeartrate:'',status:CARDIO_TEST_STATUS_started},userTestID,function(){
            if(onUpdateSuccess){
                onUpdateSuccess();
            }
        });
    } catch (e) {
        ShowExceptionMessage("updateCardioTestActive", e);
    }
}
function UpdateLoadValueOnTest(){
    logStatus("Calling UpdateLoadValueOnTest", LOG_FUNCTIONS);
    try {
        if (tMember.ManualStatus != CARDIO_TEST_STATUS_cooldown){
            tMember.loadValueData.push(tMember.loadvalueObject[tMember.loadActiveIndex]);
        }
        _updateHeartRateOnServer({loadData:tMember.loadValueData},tMember.serverdata.user_test_id,function(){
            /*Still No Need to handle success may need to add in future Here*/
        });
    } catch (e) {
        ShowExceptionMessage("UpdateLoadValueOnTest", e);
    }
}

function isValidTestMonitorPage(){
    logStatus("Calling isValidTestMonitorPage", LOG_FUNCTIONS);
    try {
        return (is_page('cardio-monitor') || is_page('manage-test') || is_page('cardio-chart'));//May need to add list of pages later with Or
    } catch (e) {
        ShowExceptionMessage("isValidTestMonitorPage", e);
    }
}

function PushChartData(obj, data){
    if(obj) {
        obj.addPoint(data, true, false);
		//obj.redraw();
    }
}

function UpdateLocalMemberStatus(newstatus){
    tMember.ManualStatus = newstatus;
}
/* cardio-monitor - client View  - End */


/* manage-test - Coach View  - Start */
function PushChartDataIfOpend(obj, data){
	if ($("#monitorchart").is(":visible") || $("#monitorchartrpm").is(":visible")){
		
		PushChartData(obj, data);
		
	}
}

function loadMembersTestPanels(){
    logStatus("Calling loadMembersTestPanels", LOG_FUNCTIONS);
    try {
        loadMemberPaneldata(function(){
            updateMemberTestMonitor();
        });
    } catch (e) {
        ShowExceptionMessage("loadMembersTestPanels", e);
    }
}
function loadMemberPaneldata(onRefreshSuccess){
    logStatus("Calling loadMemberPaneldata", LOG_FUNCTIONS);
    try {

        var clubId	=	getselectedclubid();
        var testDate=	_getDateForInput();
        var allowedStatus	=	TEST_STATUS_YETTO_START+','+TEST_STATUS_STARTED+','+CARDIO_TEST_STATUS_Reset+','+CARDIO_TEST_STATUS_Editmode+','+CARDIO_TEST_STATUS_cooldown;
        //This will fetch the client details to build the Panel
        getMembersTestResultdataForClubandDate(clubId,testDate,allowedStatus,function(response){
            var countlen = response.length;
            logStatus(countlen,LOG_DEBUG);
            $(".testsectionpanel").html("");
            //console.log(response[0].user_id);
            UpdateMemberObjectFromResponse(response, onRefreshSuccess);
        }, null, '1');
    } catch (e) {
        ShowExceptionMessage("loadMemberPaneldata", e);
    }
}

function UpdateMemberObjectFromResponse(response, successhandler){
    logStatus("Calling UpdateMemberObjectFromResponse", LOG_FUNCTIONS);
    try {
        tMemberList = [];
        var imageloadindex = 0;
        var incactualcnt = 0;
        if((response) && (response.length>0) && (response[0].user_id)) {
            $(".testsectionpanel").html("");
            $.each(response,function(key,rowdata){
                if ((rowdata.user_id) && (rowdata.user_id > 0)){

                    incactualcnt++;
                    getValidImageUrl(PROFILEPATH + rowdata.userimage,function(imageurl){
                        imageloadindex++;
                        var newmemobj = GetNewMemberObjforArray();
                        newmemobj.cardiotest_info = rowdata;
                        newmemobj.imageurl = imageurl;
                        newmemobj.imageloadindex = imageloadindex;
                        newmemobj.first_name = rowdata.first_name;
                        newmemobj.last_name = rowdata.last_name;
                        newmemobj.user_id = rowdata.user_id;

                        tMemberList[rowdata.user_id] = newmemobj;

                        createNewChartPanel(newmemobj);
                        if(imageloadindex==incactualcnt){
                            $(".testsectionpanel").append('<div class="clear"></div>');
                            if(successhandler){
                                successhandler();
                            }
                        }
                    });
                }
            });
        } else {
            $(".testsectionpanel").html('<h4 class="emt_cnt">' + GetLanguageText(EXCP_NO_MEMBER_MAPPED) +'</h4>');
            if(successhandler){
                successhandler();
            }
        }
    } catch (e) {
        ShowExceptionMessage("UpdateMemberObjectFromResponse", e);
    }
}

function createNewChartPanel(memberdata){
    var rowdata = memberdata;
    var cweight =	rowdata.cardiotest_info.testweight;
    var ctest_level =	rowdata.cardiotest_info.test_level;
    var memeberage = getAgeByDOB(rowdata.cardiotest_info.dob);
    var HTML = '<div class="testsection_list" >'+
        '<div class="testsection_header" >'+
            '<span id="memberimage' + rowdata.user_id + '" class="userimagepg">'+
                '<img src="' + rowdata.imageurl + '" alt="' + rowdata.first_name + '" />'+
            '</span>'+
            '<h4 id="membername' + rowdata.user_id + '">' + rowdata.first_name +' '+ rowdata.last_name + '</h4>'+
            '<label class="test_cout">' + (rowdata.imageloadindex) + '</label>'+
        '</div>'+
        '<div class="testborder">'+
            '<div class="pageheader1_fr_field">'+
                '<label>Age at test date</label>'+
                /*'<input id="ageontest'+rowdata.user_id+'" type="number" min="0" value="' + rowdata.cardiotest_info.age_on_test + '" class="mtclsageontest" onkeypress="return _isNumber(event)" disabled="disabled">'+*/
                '<input id="ageontest'+rowdata.user_id+'" type="text" min="0" value="' + memeberage + '" class="mtclsageontest" disabled="disabled">'+
            '</div>'+
            '<div class="pageheader1_fr_field">'+
                '<label>Weight on testdate</label>'+
                '<input id="weightontest'+rowdata.user_id+'" type="number" min="0" value="' + cweight + '" class="mtclsweightontest" onblur="loadvaluesbasedonweight('+rowdata.user_id+', this)" onkeypress="return _isNumber(event)">'+
            '</div>'+
        '</div>'+
        '<div class="borderbottom" >'+
            '<div class="pageheader1_fr_field">'+
                '<label>'+GetLanguageText(LBL_START_LEVEL)+'</label>'+
                '<select id="startlevel'+rowdata.user_id+'" class="blu_input blu_input_drop" onchange="updatewattvaluechange('+rowdata.user_id+', this)"></select>'+
                //'<input id="startlevel'+rowdata.user_id+'" type="text" value="">'+
            '</div>'+
            '<div class="time_panel">'+
                '<h2 id="rpm'+rowdata.user_id+'" class="colorblue">N/A</h2>'+
                '<label>'+GetLanguageText(LBL_RMP)+'</label>'+
            '</div>'+
            '<div class="time_panel">'+
                '<h2 id="watt'+rowdata.user_id+'" class="">N/A</h2>'+
                '<label>'+GetLanguageText(LBL_WATT)+'</label>'+
            '</div>'+
        '</div>'+
        '<div class="borderbottom" >'+
            '<input id="timecout'+rowdata.user_id+'" class="timecout" type="text" value="00:00:00" readonly="readonly"/>'+
            '<button id="btncooldown'+rowdata.user_id+'" class="coolstartbtn" onclick="StartWorkOutByCoach(\''+rowdata.cardiotest_info.r_user_test_id+'\')">'+GetLanguageText(BTN_START)+'</button>'+
            '<div class="time_panel time_panelw1">'+
                '<h2 id="hrm'+rowdata.user_id+'" class="colorred">N/A</h2>'+
                '<label>'+GetLanguageText(LBL_HRM)+'</label>'+
            '</div>'+
        '</div>'+
        '<div class="borderbottom" >'+
            '<button id="btneditsave' + rowdata.user_id + '" disabled class="savebtn"  onclick="SaveSelectedCardioTest(' + rowdata.user_id + ')">'+GetLanguageText(BTN_SAVE)+'</button>'+
            '<button id="canceltest' + rowdata.user_id + '" disabled class="canclebtn" onclick="cancelChanges(\''+rowdata.user_id+'\')" >'+GetLanguageText(BTN_CANCEL)+'</button>'+
        '</div>'+
        '<button class="arrowtopbtn" onclick="openCardioTest(\''+rowdata.user_id + '\')"></button>'+
    '</div><div id="coach_hrmonitor' + rowdata.user_id + '" style="display:none"></div>';
    $(".testsectionpanel").append(HTML);

    $('#startlevel'+rowdata.user_id).html('<option>'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');//Need to be changed as loader
    LoadTestLevelList(cweight, function(levelresponse){
        PopulateTestLevels('#startlevel'+rowdata.user_id, levelresponse);
        var selectedLevel =	ctest_level;
        if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
            $('#startlevel'+rowdata.user_id).val(selectedLevel);
        }
        /**/
    });
    /*
    $("#canceltest"+rowdata.user_id).html(GetLanguageText(TXT_LOAD_WAITING_MSG));
    $("#canceltest"+rowdata.user_id).attr("disabled",true);
    */
}

function loadvaluesbasedonweight(userid, obj){
    $("#btneditsave" + userid).removeAttr("disabled");
    $("#canceltest" + userid).removeAttr("disabled");
    var weight = $(obj).val();
    var ctest_level = tMemberList[userid].cardiotest_info.test_level;
    LoadTestLevelList(weight, function(levelresponse){
        PopulateTestLevels('#startlevel'+userid, levelresponse);
        var selectedLevel =	ctest_level;
        if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
            $('#startlevel'+userid).val(selectedLevel);
        }
    });
}

function updatewattvaluechange(userid, obj){
    logStatus("Calling updatewattvaluechange", LOG_FUNCTIONS);
    logStatus(obj,LOG_DEBUG);
    try {
        $("#btneditsave" + userid).removeAttr("disabled");
        $("#canceltest" + userid).removeAttr("disabled");
    } catch (e) {
        ShowExceptionMessage("updatewattvaluechange", e);
    }
}

function cancelChanges(userid){
    logStatus("Calling cancelTest", LOG_FUNCTIONS);
    try {
        $("#btneditsave" + userid).attr("disabled","disabled");
        $("#canceltest" + userid).attr("disabled","disabled");
    } catch (e) {
        ShowExceptionMessage("cancelTest", e);
    }
}


function updateMemberPanelView(memberID){
    logStatus("Calling updateMemberPanelView", LOG_FUNCTIONS);
    try {
        var cMember	=	tMemberList[memberID];
        app_tempvars('current_testid' + memberID,cMember.cardiotest_info.r_user_test_id);
        /*
        $("#canceltest" + memberID).attr("onclick","cancelTest('" + cMember.cardiotest_info.r_user_test_id + "')");
        $("#canceltest" + memberID).html("Cancel");
        $("#canceltest" + memberID).removeAttr("disabled");
        */
        var $weightontest= $("#weightontest"+memberID);
        var $startlevel= $("#startlevel"+memberID);
        var $btncooldown= $("#btncooldown"+memberID);
		
        if ( (!$weightontest.is(":focus")) && ($("#btneditsave" + memberID).is(":disabled")) ) {

            //console.log("Reddy to update" + $("#weightontest"+memberID).val() + ' - ' + cMember.cardiotest_info.testweight);
            //console.log("Reddy to update" + $("#startlevel"+memberID).val() + ' - ' + cMember.cardiotest_info.test_level);
            if ( ($weightontest.val() != cMember.cardiotest_info.testweight) ||
                  ($startlevel.val() != cMember.cardiotest_info.test_level)){
                //console.log("Changed" + $("#weightontest"+memberID).val() + ' - ' + cMember.cardiotest_info.testweight);
			
				LoadTestLevelList(cMember.cardiotest_info.testweight, function(levelresponse){
                    PopulateTestLevels("#startlevel"+memberID, levelresponse);
                    var selectedLevel =	cMember.cardiotest_info.test_level;
                    if( (selectedLevel) && (selectedLevel!='') &&  typeof(selectedLevel)!='undefined') {
                        $startlevel.val(selectedLevel);
                    }
                });
                $weightontest.val(cMember.cardiotest_info.testweight);
            }
        }

        if(cMember.cardiotest_info.status == CARDIO_TEST_STATUS_stopped){
            $btncooldown.html(GetLanguageText(BTN_START));
            $btncooldown.attr("onclick","StartWorkOutByCoach('" + cMember.cardiotest_info.r_user_test_id + "')");
            $btncooldown.removeAttr("disabled");
        } else if(cMember.cardiotest_info.status == CARDIO_TEST_STATUS_started){
            $btncooldown.html(GetLanguageText(BTN_COOLDOWN));
            $btncooldown.attr("onclick","CoolDownWorkOutByCoach('" + cMember.cardiotest_info.r_user_test_id + "')");
            $btncooldown.removeAttr("disabled");
        } else if(cMember.cardiotest_info.status == CARDIO_TEST_STATUS_cooldown){
            $btncooldown.html(GetLanguageText(BTN_STOP));
            $btncooldown.attr("onclick","StopWorkOutByCoach('" + cMember.cardiotest_info.r_user_test_id + "',true)");
            $btncooldown.removeAttr("disabled");
        }
        /**
         * @param cardiotest_info.load_value This is load value
         * @param cardiotest_info.heart_rate_value This is heart rate value
         * @type {boolean}
         */
        var isCoolingDown	=	(cMember.cardiotest_info.status  == CARDIO_TEST_STATUS_cooldown);
        logStatus(isCoolingDown,LOG_DEBUG);
        var loadValue	=	cMember.cardiotest_info.load_value.replace("\\", ""); 
        var hrValue		=	cMember.cardiotest_info.heart_rate_value.replace('\\', "");
        //var _loadValue	=	[];
        //var _hrValue	=	{};
        if(loadValue!=''){
            try{
            //var _loadValue	=	JSON.parse(loadValue);
            }catch(e){

            }
        }
        var lvhrData	=	GetLatestLoadValue(loadValue,hrValue);
        var curwatt	=	'N/A';
        var curhrm	=	'N/A';
        var curhrmtime = 0;
        var rpm = '';
        var currenttesttime = '00:00:00';
        if(lvhrData.lvdata && lvhrData.lvdata.load){
            curwatt	=	lvhrData.lvdata.load;
        }
        if(lvhrData.hrdata && lvhrData.hrdata.length>0) {
            var hrlen	=	lvhrData.hrdata.length;
            curhrm	=	(lvhrData.hrdata[hrlen-1]) ? lvhrData.hrdata[hrlen-1].data:'N/A';
            curhrmtime	=	(lvhrData.hrdata[hrlen-1]) ? lvhrData.hrdata[hrlen-1].time:'N/A';
            rpm = cMember.cardiotest_info.rpm.replace('\\', "");
            currenttesttime = cMember.cardiotest_info.current_test_time;
        }
        rpm	=	(rpm!='') ? rpm:'N/A';
        $("#watt"+memberID).html(curwatt);
        $("#hrm"+memberID).html(curhrm);
        $("#rpm"+memberID).html(rpm);

        var curhrmtimemin = curhrmtime/60;
		var curhrmtimeminFormat = secondsTimeSpanToHMS(curhrmtime);
		console.log('format ime');
		console.log(curhrmtimeminFormat);
		var newtime = cMember.cardiotest_info.current_test_time;
        //if(curhrmtime>0)
        //{
       	//$("#timecout" + memberID).val(newtime);
        $("#timecout" + memberID).val(curhrmtimeminFormat);
	
        PushChartDataIfOpend(_MAIN_CHAR_OBJECT_SR_HRM, [curhrmtimemin, parseInt(curhrm)]);
        PushChartDataIfOpend(_MAIN_CHAR_OBJECT_SR_WATT, [curhrmtimemin, parseInt(curwatt)]);
		PushChartDataIfOpend(_MAIN_CHAR_OBJECT_SR_RPMM, [curhrmtimemin, parseInt(cMember.cardiotest_info.rpm)]);
		
		//}
		//else
		//{
			
		//}
        if( ($("#monitormember").val()==memberID) && $("#monitorchart").is(":visible") ) {
            initPopupControls(memberID);
        }
    } catch (e) {
        ShowExceptionMessage("updateMemberPanelView", e);
    }
}

function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
	return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s);
    //return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
}

function SaveSelectedCardioTest(MemberID){
    logStatus("Calling SaveSelectedCardioTest", LOG_FUNCTIONS);
    try {
        /*Need To Implement*/
        var weightOnTest=	$("#weightontest" + MemberID).val();
        var startlevel=	$("#startlevel" + MemberID).val();
        var ageOnTest	=	$("#ageontest" + MemberID).val();
        var memobj = tMemberList[MemberID];
        var userTestID = memobj.cardiotest_info.r_user_test_id;
        if(userTestID && userTestID!=''){
            var startspeed	=	SPEED_LEVEL[startlevel];
            var totalLoadValue=	getLoadValueForSpeed(startspeed,weightOnTest);
            _updateHeartRateOnServer({
                weightOnTest:weightOnTest,
                ageOnTest:ageOnTest,
                startlevel:startlevel,
                totalLoadValue:JSON.stringify(totalLoadValue),
                test_active:1,
                coolDownStart:0,
                coolDownStop:0,
                loadData:'',
                heartrate:'',
                rpm:'',
                coolDownHeartrate:'',
                status:CARDIO_TEST_STATUS_Reset
            },userTestID,function(){
                $("#btneditsave" + memobj.user_id).attr("disabled","disabled");
                $("#canceltest" + memobj.user_id).attr("disabled","disabled");
				
				alert("Saved Successfully");
            });
        }
    } catch (e) {
        ShowExceptionMessage("SaveSelectedCardioTest", e);
    }
}

function StartWorkOutByCoach(testid){
    updateCardioTestActive(testid);
}

function CoolDownWorkOutByCoach(testid){
    _updateHeartRateOnServer({status:CARDIO_TEST_STATUS_cooldown}, testid, null);
	setTimeout(function(){ _movePageTo('home'); }, 190000);
}

function StopWorkOutByCoach(testid){
    _updateHeartRateOnServer({status:CARDIO_TEST_STATUS_stopped}, testid, null);
}
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function openCardioTest(memberID){
    logStatus("Calling openCardioTest", LOG_FUNCTIONS);
    try {
		/**
         * @param memberdata.rpm_group This is rpm group
         * @type {{}}
         */
		 
		var seriesdata = {};
		var checkArray = [];
		var rpmArray = [];
        if (tMemberList[memberID]){
            var memberdata = tMemberList[memberID].cardiotest_info;
			var loadValue = memberdata.load_value;
            var hrdata = memberdata.heart_rate_value;
            var rpmgroup = memberdata.rpm_group;
			//Add total load
	 
	        if(loadValue!='') {
			   var loadValuearray = [];
                var hrdataarray = [];
                var rpmgrouparray = [];
				try{
					   loadValuearray =  JSON.parse(loadValue);
					   hrdataarray = JSON.parse(hrdata);
					   rpmgrouparray = JSON.parse(rpmgroup);
					  
				
					}catch(e){
                }
                seriesdata['hrdata'] = [];
                seriesdata['wattdata'] = [];
                seriesdata['rpmdata'] = [];
                var incwatt = 0;
				

	
               $.each(hrdataarray, function(key, HRObj){

					//outer ey
					var currentload = parseInt(loadValuearray[incwatt].load);
					$.each(HRObj, function(eachkey, HRData){
						if(key == 0 && HRData.time > 60)
						{
							console.log("Outer Key "+key+" eachkey "+eachkey+" time "+HRData.time);
						}
						else
						{
							var hrtimeinmin = HRData.time/60;
							//var hrtimeinmin = parseInt(parseInt(HRData.time)/60);
							var hrval = HRData.data;
							if(checkArray.length == 0 || $.inArray( hrval, checkArray ) == -1)
							{
								seriesdata['hrdata'].push([hrtimeinmin, hrval]);
								checkArray.push(hrval);
							}
							seriesdata['wattdata'].push([hrtimeinmin, currentload]);
						}
                    });
                    incwatt++;
                });

                $.each(rpmgrouparray, function(key, RPMObj){
                    $.each(RPMObj, function(eachkey, RPMData){
						if(key == 0 && RPMData.time > 60)
						{
							console.log("Outer Key "+key+" eachkey "+eachkey+" time "+RPMData.time);
						}
						else
						{
							var rpmtimeinmin = RPMData.time/60;
						   // var rpmtimeinmin = parseInt(parseInt(RPMData.time)/60);
							var rpmval = RPMData.data;
							if(rpmArray.length == 0 || $.inArray( rpmval, rpmArray ) == -1)
							{
							seriesdata['rpmdata'].push([rpmtimeinmin, rpmval]);
							//seriesdata['rpmdata'].push([rpmval, rpmtimeinmin]);
							rpmArray.push(rpmval);
							}
						}
                    })
                }) 
            }
        }
        _MAIN_CHAR_OBJECT = createNewChartObj('monitorchart', seriesdata);
		_MAIN_CHAR_OBJECT_RPM = createNewChartObjRPM('monitorchartrpm', seriesdata);
        _MAIN_CHAR_OBJECT_SR_WATT	=	_MAIN_CHAR_OBJECT.series[0];
       // _MAIN_CHAR_OBJECT_SR_RPM	=	_MAIN_CHAR_OBJECT.series[1];
        _MAIN_CHAR_OBJECT_SR_HRM	=	_MAIN_CHAR_OBJECT.series[1];
		
		_MAIN_CHAR_OBJECT_SR_RPMM	=	_MAIN_CHAR_OBJECT_RPM.series[0];

        initPopupControls(memberID);
        $(".manage_test_popup").show();
        $("#monitorchart").show();
        $("#monitormember").val(memberID);
        //XX $("#monitorchart"+memberID).show();
        var graphheight =  $(".highcharts-container").height();
		$(".highcharts-container").css('width','660px');
		$(".highcharts-background").css('width','660px');
		$(".highcharts-plot-background").css('width','660px');
        $("#monitor_testchart").css({
            "height": graphheight
        });
    } catch (e) {
        ShowExceptionMessage("openCardioTest", e);
		
		//console.log("checking log here please wait");
		//console.log(e)
    }
}


/*ASYN : 1 MK Update On Every Pulse From Server*/
function updateMemberTestMonitor(){
    logStatus("Calling updateMemberTestMonitor", LOG_FUNCTIONS);
    try {
		var clubId	=	getselectedclubid();
		//console.log("We are updating here");
		
        var testDate= _getDateForInput();
		//get all test related information from Server
        var allowedStatus	=	TEST_STATUS_YETTO_START+','+TEST_STATUS_STARTED+','+CARDIO_TEST_STATUS_Reset+','+CARDIO_TEST_STATUS_Editmode+','+CARDIO_TEST_STATUS_cooldown;
        getMembersTestResultdataForClubandDate(clubId,testDate,allowedStatus,function(responseobjs){
            if (responseobjs){
                if (CheckMemberListChangedWithResp(responseobjs)){
                    UpdateMemberObjectFromResponse(responseobjs);
                }else{
                    $.each(responseobjs, function(key, response){
                        if (response.user_id){
                            //console.log("loading all json here");
                            //console.log(response.total_load_value);
                             //console.log("End loading all json here");
							 //alert(response.total_load_value);
                            tMemberList[response.user_id].cardiotest_info = response;
                            updateMemberPanelView(response.user_id);
                        }
                    })
                }
            }
            /*else { commented fo the seeking still stoped ? have any changes from client */
            if(isValidTestMonitorPage()){
                if (_TEST_MONITOR_REFRESH_TIMER){
                    clearTimeout(_TEST_MONITOR_REFRESH_TIMER);
                }
                _TEST_MONITOR_REFRESH_TIMER = setTimeout(function(){
                    updateMemberTestMonitor();
                },1000);
            }
            /*}*/
        }, null, '1');
    } catch (e) {
        ShowExceptionMessage("updateMemberTestMonitor", e);
    }
}

/**
 * @return {boolean}
 */
function CheckMemberListChangedWithResp(newresp){
    var mids	=	[];
    var memlistcnt = getActualLenghtOfthememobj();
    $.each(newresp, function(key, response){
        if (response.userid){
            var memberID = response.userid;
            if (mids.indexOf(memberID)==-1){
                mids.push(memberID);//Just to make sure member id is unique to get the count.
            }
        }
    });
    var memchanged	=	false;
    if (memlistcnt != mids.length){
        memchanged = true
    }else{
        $.each(mids,function(idx,memID){
            if( (memID) && (!tMemberList[memID])){
                memchanged=true;
                return false;
            }
        });
    }
    return memchanged;
}

function getActualLenghtOfthememobj(){
    var cnt = 0;
    $.each(tMemberList,function(idx,memobj){
        if ((memobj) && (memobj.user_id)){
            cnt++;
        }
    });
    return cnt;
}

function GetNewMemberObjforArray(){
    var membobj = {};
    membobj.cardiotest_info = {};
    membobj.imageurl = '';
    membobj.imageloadindex = 0;
    membobj.first_name = '';
    membobj.last_name = '';
    membobj.user_id = 0;
    return membobj;
}


function GetLatestLoadValue(loadValue,hrValue){
    logStatus("Calling GetLatestLoadValue", LOG_FUNCTIONS);
    try {
        var loadValueRow=	{};
        var data	=	{};
        var hrdata	=	{};
        /*Need To Implement - Optimized code*/
        if(loadValue && loadValue!=''){
            data	=	JSON.parse(loadValue);
            
        }
        if(hrValue && hrValue!=''){
            hrdata	=	JSON.parse(hrValue);
        }
        if(!$.isEmptyObject(data[0])){
       		var curwatt	=	'N/A';
            var curhrm	=	'N/A';
            logStatus(curhrm,LOG_DEBUG);
            logStatus(curwatt,LOG_DEBUG);
            if(data.length>0){
            	var lvIdx	=	data.length-1;
            	var hrIndex = '';
				hrIndex	= lvIdx;
            	/*if(Object.keys(hrdata).length < data.length )
            	{
            		var thereDifference = data.length - Object.keys(hrdata).length;
            		hrIndex	= (data.length-thereDifference)-1;
            	}
            	else
            	{
            		hrIndex	= lvIdx;
            	}*/
            	//var lvIdx	=	data.length-1;
                loadValueRow.lvdata	=	data[lvIdx];
                loadValueRow.hrdata	=	(hrdata[hrIndex]) ? hrdata[hrIndex]:[];
            }
        }
        return loadValueRow;
    } catch (e) {
        ShowExceptionMessage("GetLatestLoadValue", e);
    }
}

function closeChartPopup(){
    logStatus("Calling closeChartPopup", LOG_FUNCTIONS);
    try {
        //InitializeHRFullGraph("monitor_testchart");
        $(".manage_test_popup").hide();
    } catch (e) {
        ShowExceptionMessage("closeChartPopup", e);
    }
}
/* manage-test - Coach View  - End */
function initPopupControls(memberID){
    logStatus("Calling initPopupControls", LOG_FUNCTIONS);
    try {
        var $cdbtn		=	$("#btncooldown"+memberID);
        var $cdbtnpopup	=	$(".btncooldown_popup");
        $cdbtnpopup.html($cdbtn.html());
        $cdbtnpopup.click(function(){
            $cdbtn.click();
        });
        var $rpm		=	$("#rpm"+memberID);
        var $watt		=	$("#watt"+memberID);
        var $hrm		=	$("#hrm"+memberID);
        var $timecount	=	$("#timecout" + memberID);
        var $memberimage=	$("#memberimage" + memberID).find('img');
        var $membername	=	$("#membername" + memberID);
        var $ageontest	=	$("#ageontest" + memberID);
        var $weightontest	=	$("#weightontest" + memberID);
        var $startlevel	=	$("#startlevel" + memberID);
        var $startlevel_value =   $("#startlevel" + memberID).val();
        $("#cHRM").html($hrm.html());
        $("#cRPM").html($rpm.html());
        $("#cWATT").html($watt.html());
        var $memberImage= $("#memberImage");
        $memberImage.attr("alt",$memberimage.attr("alt"));
        $memberImage.attr("src",$memberimage.attr("src"));
        $(".memberName").html($membername.html());
        $("#monitor_ageontest").val($ageontest.val());
        $("#monitor_weightontest").val($weightontest.val());
        $("#idtestlevels").html($startlevel.html());
        $("#idtestlevels").val($startlevel_value);
        $("#popup_timer").val($timecount.val());
        $("#monitor_testdate").val(getDateTimeByFormat('d/m/Y'));
    } catch (e) {
        ShowExceptionMessage("initPopupControls", e);
    }
}
function showCardioPopup(onpoupsuccess){
    //Apply mask
    var mask	=	'<div class="monitor-chartmask"></div>';
    $('body').append(mask);
    $(".manage_test_popup").show();
    $("#monitorchart").show();
    if(onpoupsuccess){
        onpoupsuccess()
    }
}
function backtoTestMonitor(){
    $(".manage_test_popup").hide();
    $("#monitorchart").hide();
    var $monitorchartmask = $(".monitor-chartmask");
    $monitorchartmask.remove();
    $monitorchartmask.remove();
}
