$(function(){
    _setPageTitle("New Machines/Exercises/Activities");
    Translistmachines();
    TranslateGenericText();
    /* getting edit id from new machine list table */
    var urldata	=	getPathHashData();
    var editID	=	(urldata.args.editID) ? base64_decode(urldata.args.editID) :0;
    $("#machine_edit_id").val(editID);
    var clubId	=	getselectedclubid();
    $('#imageloader').hide();
    $('#videoloader').hide();
    if(editID){
        $('#hideItemsData').css("display","none");
        var $idisactivemachine= $('#idisactivemachine');
        $idisactivemachine.attr("disabled",false);
        $idisactivemachine.removeClass("checkbtn");
        /** Edit Machine details**/
        /**
         * @param response.uniqueid This is unique id
         * @param response.machine_name This is machine name
         * @param response.machineimage This is machine image
         * @param response.videourl This is machine video url
         * @param response.active This is machine active
         * @param response.outoforder This is machine outoforder
         * @param response.is_certified This is is_certified
         */
        getNewMachinedetailsbyeditid(editID,function(response){
            gettypelistdetail('',clubId,function(){
                $('#clubiddetails').html(clubId);
                $('#clubauotgnerateid').html(response.uniqueid);
                $('#idmachdescription').html(response.description);
                $('#machinename').val(response.machine_name);
                var $idmachbrandlist= $('#idmachbrandlist');
                var $idmachTypelist= $('#idmachTypelist');
                var $idmachGrouplist= $('#idmachGrouplist');
                var $idmachSubtypelist= $('#idmachSubtypelist');
                $idmachbrandlist.val(response.brand);
                $idmachTypelist.val(response.type);
                $idmachGrouplist.val(response.type);
                $idmachSubtypelist.val(response.subtype);
                getValidImageUrl(MACHINE_PATH + response.machineimage, function(url){
                    $('#exerciseImage').attr('src',url);
                });
                    $('#idmachineImage').val(response.machineimage);
                /*getValidImageUrl(MACHINE_PATH + response.videourl, function(url){ // commeted for display video data for checking purpose
                    $('#exerciseVideo').attr('src',url);
                });*/
                $('#exerciseVideo').attr('src',MACHINE_PATH +response.videourl);
                $('#idmachineVideo').val(response.videourl);
                if(response.active == 1){
                    var  $inp_active= $('#inp_active');
                    $inp_active.parent().addClass('checkbtn');
                    $inp_active.attr('checked', true);
                }if(response.outoforder == 1){
                    var $rdgrp_machineworkignorder = $('#rdgrp_machineworkignorder');
                    $rdgrp_machineworkignorder.parent().addClass('checkbtn');
                    $rdgrp_machineworkignorder.attr('checked', true);
                }/*if(response.allow_mix == 1){ // commented by saikrishna ;
                    $('#inp_mixtraining').parent().addClass('checkbtn');
                    $('#inp_mixtraining').attr('checked', true);
                }if(response.allow_circuit == 1){
                    $('#inp_circuittraining').parent().addClass('checkbtn');
                    $('#inp_circuittraining').attr('checked', true);
                }if(response.allow_free_training == 1){
                    $('#inp_freetraining').parent().addClass('checkbtn');
                    $('#inp_freetraining').attr('checked', true);
                }*/
                if(response.is_certified == 1){
                    var $inp_certified = $('#inp_certified');
                    $inp_certified.parent().addClass('checkbtn');
                    $inp_certified.attr('checked', true);
                }
                $idmachbrandlist.trigger("change");
                $idmachTypelist.trigger("change");
                $idmachGrouplist.trigger("change");
                $('#idmachGroupNamelist').trigger("change");
                $idmachSubtypelist.trigger("change");
            });
        });
    } else {
        $('#hideItemsData').css("display","none");
        $('#hideDeleteMenu').css("display","none");
        $('#hideAddIcon').css("display","none");
        $('#inputclub_id').val(clubId);
        getclubnamedetails('getclubnamedetails',function(responsedata){
            $('#clubiddetails').html(clubId);
            gettypelistdetail(responsedata.club_name,clubId);
        });
    }
});
$(document).delegate("#idmachbrandlist","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#idmachbrandlist_text").text("brand  : " +selecttext);
});

$(document).delegate("#idmachTypelist","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#idmachTypelist_text").text("Type  : " +selecttext);
});

$(document).delegate("#idmachGrouplist","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#idmachGrouplist_text").text("Group  : " +selecttext);
});
$(document).delegate("#idmachSubtypelist","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#idmachSubtypelist_text").text("Subtype  : " +selecttext);
});
$(document).delegate("#idProgramlist","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#idProgram_text").text("Subtype  : " +selecttext);
});
$(document).delegate('#idisactivemachine',"click",function(){
    logStatus("Calling active machine click event",LOG_COMMON_FUNCTIONS);
    try{
        var $rdgrp_machineworkignorder= $('#rdgrp_machineworkignorder');
        if($(this).find("#inp_active").is(":checked")){
            $rdgrp_machineworkignorder.removeAttr("checked");
            $rdgrp_machineworkignorder.parent().removeClass("checkbtn");
        } else {
            $rdgrp_machineworkignorder.attr("checked",true);
            $rdgrp_machineworkignorder.parent().addClass("checkbtn");
        }
    } catch(e){
        ShowExceptionMessage("profileimagesb click event", e);
    }
});
/*MK Added - Machine Image */
$(document).delegate(".uploadMachinedetails","click",function(){ 
    logStatus("Calling Event.uploadMachinedetails.click",LOG_FUNCTIONS);
    try {
        if(!IsWebLoad()){
            $(".choosephoto_popup").show();
        } else {
            $(this).find('input[type="file"]').trigger("click");
        }
    } catch (e){
        ShowExceptionMessage("Event.uploadMachinedetails.click", e);
    }
    //
});
$(document).delegate('#idismachienoutoforder',"click",function(){
    logStatus("Calling active machine order click event",LOG_COMMON_FUNCTIONS);
    try {
        var $inp_active = $('#inp_active');
        if($(this).find("#rdgrp_machineworkignorder").is(":checked")){
            $inp_active.removeAttr("checked");
            $inp_active.parent().removeClass("checkbtn");
        } else {
            $inp_active.attr("checked",true);
            $inp_active.parent().addClass("checkbtn");
        }
    } catch(e){
        ShowExceptionMessage("profileimagesb click event", e);
    }
});
$(document).delegate(".uploadMachineImg","change",function(){
    logStatus("Calling Machine image event",LOG_FUNCTIONS);
    try {
        $('#imageloader').show();
        //var iddata 	= $(this).attr('id');
        var userdata 	= getCurrentLoginInfo();
        var userid 		= userdata.user.user_id;
        //var machine 	= $('#machine_edit_id').val();
        //var machineid	= (machine !='') ? machine:0;
        var formData = new FormData();
        formData.append('imagefile', 'image');
        formData.append('uploadtype',UPLOAD_TYPE_MACHINE_IMAGE);
        formData.append('modeUploadonly',1);
        formData.append('image', $('input[name=uploadMachineImgs]')[0].files[0]);
        updateProfileImage('insertMachineImage',formData,userid,function(response){
            $('input[name="uploadMachineImgs"]').val('');
            if (response.status == AJAX_STATUS_TXT_SUCCESS){
                $('#imageloader').hide();
                ShowToastMessage(response.status_message, _TOAST_LONG);
                if(response.imagefile){
                    var $externalImageUrl = $('#externalImageUrl');
                    $externalImageUrl.attr("disabled",true);
                    $externalImageUrl.addClass("disabled");
                    getValidImageUrl(MACHINE_PATH + response.imagefile,function(url){
                        $('#exerciseImage').attr('src',url);
                    });
                    $('#idmachineImage').val(response.imagefile);
                }
            }
        });
    } catch (e){
        ShowExceptionMessage("profilepic click event", e);
    }
});
/** Comments: To handle save new machine data.TASKID = '4307'
  * author SK Shanethatech * 
  * Created Date  : FEB-05-2016
 */
function saveNewMachineDetails(){
    logStatus("Calling saveNewMachineDetails", LOG_FUNCTIONS);
    try {
        var editID	=	$('#machine_edit_id').val();
        if(validateSteps('#listNewMachineDetails')){
            var data ={};
            data.nameofmachine 	= $("#machinename").val();
            data.brand 			= $('#idmachbrandlist').val();
            data.type 			= $('#idmachTypelist').val();
            data.group 			= $('#idmachGrouplist').val();
            data.subtype			= $('#idmachSubtypelist').val();
            data.description 		= $('#idmachdescription').val();
            data.active 			= ($('.rdgrp_active').is(":checked"))?1:2;
            data.machine_order 		= ($('.rdgrp_machineworkignorder').is(":checked"))?1:0;
            data.mixtraining 		= ($('.rdgrp_mixtraining').is(":checked"))?1:0;
            data.circuittraining		= ($('.rdgrp_circuittraining').is(":checked"))?1:0;
            data.freetraining 		= ($('.rdgrp_freetraining').is(":checked"))?1:0;
            data.certified 			= ($('.rdgrp_certified').is(":checked"))?1:0;
            data.coach_club 		=  getselectedclubid();
            data.createdby			=  getCurrentLoggedinUserId();
            data.clubautoid			= $('#clubauotgnerateid').text();
            data.editID			= (editID) ? editID:0;
            data.imagename		= $("#idmachineImage").val();
            data.exercisevideo		= $("#idmachineVideo").val();
            savenewMachinedata(data,function(response){
                    ShowToastMessage(response.status_message, _TOAST_LONG);
                    if(response.status_code ==1){
                        _movePageTo('manage-newmachinesexerciseprogram');
                    }
            });
        } else {
            ShowToastMessage(GetLanguageText(EXCP_MANADATORY_ERROR),_TOAST_LONG);
            return false;
        }

    } catch (e){
        ShowExceptionMessage("saveNewMachineDetails", e);
    }

}

function deleteNewEditMachine(){
    logStatus("Calling deleteNewEditMachine", LOG_FUNCTIONS);
    try {
        var editID	=	$('#machine_edit_id').val();
        if(editID){
            ShowAlertConfirmMessage(GetLanguageText(EXCP_DEL_COFRM_MSG),function(isconfirm){
            if(isconfirm == 1){
                    deleteStrengthMachienItem(editID,function(response){
                        ShowToastMessage(response.status_message, _TOAST_LONG);
                        if(response.status_code == 1){
                            _movePageTo('manage-newmachinesexerciseprogram');
                        }
                    });
                } else {

                    /*Do nothing*/
                }
            }, 'yes,no');
        }

    } catch (e){
        ShowExceptionMessage("deleteNewEditMachine", e);
    }
}

function gettypelistdetail(clubname,clubId,onLoadSuccess){
    logStatus("Calling deleteNEwmachinelistdetails", LOG_FUNCTIONS);
    try {
        /**
         * @param response.brands This is  brands
         * @param row.brand_id This is  brand id
         * @param row.brand_name This is  brand name
         * @param row.machine_type_id This is  machine type id
         * @param row.machine_type This is  machine type
         * @param row.group_id This is  group id
         * @param row.group_name This is  group name
         * @param row.subtype_id This is  subtype id
         * @param row.subtype_name This is  subtype name
         * @param row.maxclubid This is  max club id
         */
        gettypelistdetails(clubId,function(response){
            var HTML= '';
            var GROUPHTML= '';
            var TYPEHTML= '';
                var SUBTYPEHTML= '';
                var selected='selected';
                $.each(response.brands,function(i,row){
                    HTML += '<option value='+row.brand_id+' '+selected+'>'+row.brand_name+'</option>' ;
                    selected='';
                });
                selected='selected';
                $.each(response.type,function(i,row){
                     TYPEHTML += '<option value='+row.machine_type_id+' '+selected+'>'+row.machine_type+'</option>';
                     selected='';
                });
                selected='selected';
                $.each(response.group,function(i,row){
                     GROUPHTML += '<option value='+row.group_id+' '+selected+'>'+row.group_name+'</option>';
                     selected='';
                });
                selected='selected';
                $.each(response.subtype,function(i,row){
                     SUBTYPEHTML += '<option value='+row.subtype_id+' '+selected+'>'+row.subtype_name+'</option>' ;
                     selected='';
                });
                var maxid = response.maxclubid;
                var club_name = clubname.substr(0,2);
                var padclubname = club_name.toUpperCase();
                var maxclubid = padclubname+maxid;
                var $clubauotgnerateid= $('#clubauotgnerateid');
                var $idmachbrandlist= $('#idmachbrandlist');
                var $idmachTypelist= $('#idmachTypelist');
                var $idmachGrouplist= $('#idmachGrouplist');
                var $idmachGroupNamelist= $('#idmachGroupNamelist');
                var $idmachSubtypelist= $('#idmachSubtypelist');
                $clubauotgnerateid.html(maxclubid);
                $clubauotgnerateid.trigger("change");
                $idmachbrandlist.html(HTML);
                $idmachbrandlist.trigger("change");
                $idmachTypelist.html(TYPEHTML);
                $idmachTypelist.trigger("change");
                $idmachGrouplist.html(GROUPHTML);
                $idmachGrouplist.trigger("change");
                $idmachGroupNamelist.html(GROUPHTML);
                $idmachGroupNamelist.trigger("change");
                $idmachSubtypelist.html(SUBTYPEHTML);
                $idmachSubtypelist.trigger("change");
                if(onLoadSuccess){
                    onLoadSuccess();
                }
            });

    } catch (e){
        ShowExceptionMessage("deleteNEwmachinelistdetails", e);
    }


}
// add new machine data
function addNewMachine(){
    logStatus("Calling addNewMachine",LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var editID	=	(urldata.args.editID) ? base64_decode(urldata.args.editID) :0;
        if(editID){
            _movePageTo('manage-listmachines');
        } else {
            _movePageTo('manage-listmachines');
        }
    } catch (e){
        ShowExceptionMessage("addNewMachine", e);
    }

}
/* SK- Chnages Upload Image & video Through URL path */
$(document).delegate('#externalImageUrl',"click",function(){
    logStatus("Calling externalImageUrl",LOG_FUNCTIONS);
    try{
        $(".popup_new_bg").show();
        $(".popup_new").show();
        var flag = $(this).attr("flag");
        if(flag == 0){
            $("#externalUrlPath").attr("flag",1); // image
        } else {
            $("#externalUrlPath").attr("flag",2); //video
        }
    } catch(e){
        ShowExceptionMessage("externalImageUrl",e);
    }
});

$(document).delegate('.popup_new_bg',"click",function(){
    $(".popup_new").hide();
    $(".popup_new_bg").hide();
});

function external_Url_Path(){
    logStatus("Calling external_Url_Path",LOG_FUNCTIONS);
    try{
        var $externalUrlPath= $("#externalUrlPath");
        var url = encodeURIComponent($externalUrlPath.val()); // fetch url from input field;
        var attr = $externalUrlPath.attr("flag");
        $(".popup_new").hide();
        $(".popup_new_bg").hide();
        $("#imageloader").show();
        console.log(JSON.stringify(url));
        uploadExternalUrldata(url,function(response){
            $('#externalUrlPath').val('');
            if (response.status == AJAX_STATUS_TXT_SUCCESS){
                $("#imageloader").hide();
                ShowToastMessage(response.status_message, _TOAST_LONG);
                if(response.imagefile){
                    if(attr == 2){
                        var  $uploadMachineImg= $('#uploadMachineImg');
                        $uploadMachineImg.attr("disabled",true);
                        $uploadMachineImg.addClass("disabled");
                        getValidImageUrl(MACHINE_PATH + response.imagefile,function(url){
                            $('#exerciseImage').attr('src',url);
                        });
                        $('#idmachineImage').val(response.imagefile); //inser image to hidden element.
                    } else if(attr == 1){
                        var $uploadMachineVideo  = $('#uploadMachineVideo');
                        $uploadMachineVideo.attr("disabled",true);
                        $uploadMachineVideo.addClass("disabled");
                        $('#exerciseVideo').attr('src',MACHINE_PATH +response.imagefile);
                    /*	getValidImageUrl(MACHINE_PATH + response.imagefile,function(url){ commented for displaying video purpose
                            $('#exerciseVideo').attr('src',url);
                        });*/
                        $('#idmachineVideo').val(response.imagefile);
                    }

                }
            }
        });

    } catch(e){
        ShowExceptionMessage("external_Url_Path",e)
    }
}

function uploadMachineVideo(){
    logStatus("Calling uploadMachineVideo",LOG_FUNCTIONS);
    try{

        var formData = new FormData();
        formData.append('imagefile', 'image');
        formData.append('uploadtype',0);
        formData.append('file', $('input[name=uploadMachinevideo]')[0].files[0]);
        updateProfileImage('insertMachineImage',formData,function(response){
            //alert(JSON.stringify(response));
        });
    } catch(e){
        ShowExceptionMessage("uploadMachineVideo",e);
    }
}
/*document.getElementById('exerciseVideo').onclick = function (){
    document.getElementById('helpvideobox').play();
};*/

$(document).delegate('#exerciseVideo',"click",function(){
    logStatus("Calling exerciseVideo play",LOG_FUNCTIONS);
    try{
        var $exerciseVideo=$('#exerciseVideo');
        var url =$exerciseVideo.attr("src");
        playexercisevideo(url);
        //$(".helpvideobox").show();
        $exerciseVideo[0].src += "&autoplay=1";
    //	$(".helpvideobox").show();
        $exerciseVideo.unbind("click");
    } catch(e){
        ShowExceptionMessage("uploadMachineVideo",e);
    }
});
function playexercisevideo(url){
    logStatus("Calling playexercisevideo play",LOG_FUNCTIONS);
    try{
        //window.open(url);
        var $helpvideobox = $(".helpvideobox");
        if(url){
            try{
                url.pause();
                setTimeout(function(){
                    if (url.paused) {
                        url.currentTime = 0;
                        url.pause();
                        url.play();
                        $(".helpvideobox").show();
                    }
                }, 200);
            }catch(err){
                $helpvideobox.show();
                url.play();
                $helpvideobox.hide();
            }
        }
    } catch(e){
        ShowExceptionMessage("playexercisevideo",e);
    }
}

function Translistmachines(){
     TranslateText(".transname", TEXTTRANS, LBL_NAME);
     TranslateText(".transimageurl", TEXTTRANS, LBL_IMAGE_URL);
     TranslateText(".transmovieurl", TEXTTRANS, LBL_MOVEI_URL);
     TranslateText(".transclear", TEXTTRANS, LBL_CLEAR);
     TranslateText(".transactivelbl", TEXTTRANS, TEX_ACTIVE);
     TranslateText(".transoutord", TEXTTRANS, LBL_OUT_OF_ORDER);
     TranslateText(".transmixallow", TEXTTRANS, LBL_USE_MIX_TRAINING_ALLOW);
     TranslateText(".transcircuitallow", TEXTTRANS, LBL_USE_CIRCUIT_TRAINING_ALLOW);
     TranslateText(".freeallow", TEXTTRANS, LBL_USE_FREE_TRAINING_ALLOW);
     TranslateText(".transcertified", TEXTTRANS, LBL_MOVESMART_CERTIFIET);
     TranslateText(".classbrknex", TEXTTRANS, LBL_ONLY_ALLOWED_WITH_MACHINES);
     TranslateText(".transenterurl", TEXTTRANS, LBL_ENTER_URL);
     TranslateText(".transphenterurl",PHTRANS, PH_ENTER_URL);
     TranslateText(".transgetimgcls", TEXTTRANS, TXT_GETIMAGE);
     TranslateText(".transgetmoviecls", TEXTTRANS, TXT_GETMOVIE);
     TranslateText(".transdescls", TEXTTRANS, TXT_DESCRIPTION);
     TranslateText(".transgetmovie", TEXTTRANS, TXT_GETMOVIE);
     TranslateText(".transgetimage", TEXTTRANS, TXT_GETIMAGE);
     TranslateText(".transdiscription", TEXTTRANS, TXT_DESCRIPTION);
}

$(document).ready(function(){
    maincontent_leff_none();

    $(window).resize(function(){
        //maincontent_leff_none();
    });
});