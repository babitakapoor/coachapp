$(function(){
    //_setPageTitle("Forgot Password");
     _setPageTitle(GetLanguageText(PAGE_TTL_FORGOTPASSWORD));
    TranslationForgotpassword();
    var urldata	=	getPathHashData();
    var email	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    $('#useremaildata').attr('value',email);
    /**
     * @param reponse.securityQuestions This is securityQuestions
     * @param _ques.secure_question_id This is security Questions id
     */
    getsecurityQuestions('getSequirityQuestions',email,function(reponse){
        var _sqQuestions=	reponse.securityQuestions;
        var quesHTML	=	"";
        $.each(_sqQuestions,function(_index,_ques){
            quesHTML+=	'<input class="blu_input sq_ques" type="text" id="sqQues_'+_ques.secure_question_id+'" placeholder="'+_ques.question+'" /> ';
        });
        $(".questions_list").html(quesHTML);
        /* setCaptchaContent(".recapchabox",function(){
            /*On load Captcha Success* /
        });/* */
    });

});

function TranslationForgotpassword(){
     TranslateText(".transsave",VALTRANS, LBL_SEND);
}
// *** [SK] comments by shanethatech ***//
// ***Get forget Password Details ***//
function sendRequest(){
    logStatus("Calling sendRequest", LOG_COMMON_FUNCTIONS);
    try {
        //getCaptchaValid(function(){
        DisableProcessBtn(".savebtn", false);
            var email = $('#useremaildata').val();
            var question	=	{};
            $(".sq_ques").each(function(i,eachitem){
                var idstr	=	$(eachitem).attr("id");
                var answer	=	$(eachitem).val();
                var _qid	=	idstr.split('sqQues_')[1];	
                question[_qid]=answer;
            });
            sendPasswordrequest('checkSecurityAnswers', email,question, function(response){
                ShowToastMessage(response.message, _TOAST_LONG);
                DisableProcessBtn(".savebtn", true, "Send");
                if(response.status_code=='1'){
                    _movePageTo('login');
                }
            },'json');
        //});
    } catch(e) {
        ShowExceptionMessage("sendRequest", e);
    }
}
