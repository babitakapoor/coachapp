 $(document).ready(function(){
		var memberID	=	GetUrlArg('memberid',true);
		getuser(memberID);
		getweight(memberID);	
});  
function getuser(memberId){

	logStatus("Calling userdetail", LOG_FUNCTIONS);
        try {
        var postdata = {};
         postdata.user_id   =  memberId;
         postdata.is_reporting=  base_url+"/reporting/index.php/strength/getuser";
         userdetail(postdata,function(data){
         	if(data.status== 1){
				var name = data.response.first_name+' '+data.response.last_name;
				$('.fulluser_name').html(name);
				var userimage = data.response.userimage;
				if(userimage!=''){
					 var imageUrl = BASEURL+'images/uploads/movesmart/profile/'+userimage;

					  imageExists(imageUrl, function(exists) {
						//Show the result
						if(exists==true){
						$('.img_box').html('<img src="'+BASEURL+'images/uploads/movesmart/profile/'+userimage+'">');
						}
						else{
							$('.img_box').html('<img src="/images/userempty_image.png">');
						}
					  });
				}
				else{
					$('.img_box').html('<img src="/images/userempty_image.png">');
				}
			}			
             
                
        });
    }
     catch(e) {
        ShowExceptionMessage("userdetail", e);
    }
	
	
	
}
 
    function imageExists(url, callback) {
    var img = new Image();
    img.onload = function() { callback(true); };
    img.onerror = function() { callback(false); };
    img.src = url;
  }
  
  function saveManualData(){
	  logStatus("Calling savemandata", LOG_FUNCTIONS);
        try {
			var postdata = {};
			  var urldata	=	getPathHashData();
			  var memberID = GetUrlArg('memberid',true);
			  var clubId  =   getselectedclubid();
			  var coach = getCurrentLoggedinUserId();
			  var workload_start = $('#testLevel').val();
			  var workload_iant = $('#workload_iant').val();
			  var runtime = $('#runtime').val();
			  var hr_start = $('#hr_start').val();
			  var hr_iant = $('#hr_iant').val();
			   var weight = $('#weight').val();
			   postdata.user_id  =   memberID;
			   postdata.coach_id  =   getCurrentLoggedinUserId(); 
			   postdata.club_id  =   clubId;
			   postdata.test_level  =   workload_start; 
			   postdata.beats  =   workload_iant;
			    postdata.runtime  =   runtime;
				 postdata.start_beats  =   hr_start;
				  postdata.on_iant  =   hr_iant;
				  postdata.weight  =   weight;
			 postdata.is_reporting=  base_url+"/reporting/index.php/report/save_manual_data";
			if(weight==''){
				 $('#weight').css('border','1px solid red');
			 }
			 /*else if(workload_start==''){
				 $('#testLevel').css('border','1px solid red');
			 }
			 else  if(workload_iant==''){
				 $('#workload_iant').css('border','1px solid red');
			 }*/
			 else  if(runtime==''){
				 $('#runtime').css('border','1px solid red');
			 }
			 else  if(hr_start==''){
				 $('#hr_start').css('border','1px solid red');
			 }
			 else  if(hr_iant==''){
				 $('#hr_iant').css('border','1px solid red');
			 }
			 else{
				 savemandata(postdata,function(response){
							if(response.status==1){
						  ShowAlertMessage("Data saved successfully.");

						  var memberid	=	base64_encode(memberID);
						   _movePageTo('cardio-analysis',{memberid:memberid});
							}
							else
							   {
								if(response.data!="")
								{
								 alert(response.data);
								}
								else{
								alert("Data not saved.");
								 }
							   }
				
					});
			 }
    }
     catch(e) {
        ShowExceptionMessage("savemandata", e);
    }
  }
  function getweight(userid){
	  logStatus("Calling getweight", LOG_FUNCTIONS);
        try {
        var postdata = {};
         postdata.user_id   =  userid;
         postdata.is_reporting=  base_url+"/reporting/index.php/report/get_weight";
         userweight(postdata,function(response){
         	if(response.status== 1){
				var weights = response.data.weight;
				var level = response.data.test_level;
				$('#weight').val(weights);
				$( "#weight" ).blur();
				app_tempvars('cardiotest_newlevel',level);
			}			  
        });
    }
     catch(e) {
        ShowExceptionMessage("getweight", e);
    }
	  
  }
 