$(function () {
	
	_setPageTitle("Check Result");
	var urldata	=	getPathHashData();
    var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    var testid	=	(urldata.args.testid && urldata.args.testid!='undefined') ? base64_decode(urldata.args.testid) :0;
    var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        _getClientInformationByID(memberId,function(response){
        if(response.last_name) {
            $(".memberName").html(response.first_name + ' '  + response.last_name);
            if(response.userimage!=''){
                getValidImageUrl(PROFILEPATH + response.userimage,function(url){
                    $('#memberImage').attr('src',url);
                });
            }

        }
    });
	$('#usid').val(memberId);


    // Uncomment to style it like Apple Watch
    /*
    if (!Highcharts.theme) {
        Highcharts.setOptions({
            chart: {
                backgroundColor: 'black'
            },
            colors: ['#F62366', '#9DFF02', '#0CCDD6'],
            title: {
                style: {
                    color: 'silver'
                }
            },
            tooltip: {
                style: {
                    color: 'silver'
                }
            }
        });
    }
    // */
   $(document).ready(function(){
	   var res = 'hello';
	  
			logStatus("Calling ShowCheckResultDataGraph", LOG_FUNCTIONS);
				try {
				var postdata = {};

				 var $idofuser = $('#idofuser');
				 postdata.user_id 	=	memberId;
				 postdata.is_reporting=  base_url+"/reporting/index.php/Reportpdf/check_result";
				 showcheckresultdatagraps(postdata,function(response){
					 logStatus("Calling gettingresponsenew", LOG_FUNCTIONS);
					
					myresult(response);

				});
			}
			 catch(e) {
				ShowExceptionMessage("ShowCheckResultDataGraph", e);
			}
			
			
		});
		
		
		
		
		
		
	
});

function getOptionColor(value) {
    logStatus("getOptionColor", LOG_FUNCTIONS);
    try {
        if (value <= 20) {
            return '#fc0404';
        } else if ((value > 20) && (value <= 60)) {
            return '#fc5c01';
        } else {
            return '#03fb2b';
        }
    } catch (e) {
        ShowExceptionMessage("getOptionColor", e);
    }
}
function getOptionColorPane(value) {
    logStatus("getOptionColorPane", LOG_FUNCTIONS);
    try {
        if (value <= 20) {
            return '#fe8385';
        } else if ((value > 20) && (value <= 60)) {
            return '#fc9f62';
        } else {
            return '#aeffbc';
        }
    } catch (e) {
        ShowExceptionMessage("getOptionColorPane", e);
    }
}

   function myresult(res)
   {
    Highcharts.chart('resultchart', {
       credits: {
      enabled: false
  },
	  chart: {
            type: 'solidgauge',
            marginTop: 20
        },

        title: {
            text: null,
            style: {
                fontSize: '12px'
            }
        },

        tooltip: {
            borderWidth: 0,
            backgroundColor: 'none',
            shadow: false,
            style: {
                fontSize: '12px'
            },
            
            positioner: function (labelWidth) {
                return {
                    x: 200 - labelWidth / 2,
                    y: 180
                };
            }
        },

        pane: {
                        startAngle: 0,
                        endAngle: 360,
                        background: [{ // Track for Move
                            outerRadius: '112%',
                            innerRadius: '88%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
                            backgroundColor: getOptionColorPane(res.mov_creditindex),
                            borderWidth: 0
                        }, { // Track for Exercise
                            outerRadius: '87%',
                            innerRadius: '63%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.3).get(),
                            backgroundColor: getOptionColorPane(res.eat_creditindex),
                            borderWidth: 0
                        }, { // Track for Stand
                            outerRadius: '62%',
                            innerRadius: '38%',
                            //backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.3).get(),
                            backgroundColor: getOptionColorPane(res.mind_creditindex),
                            borderWidth: 0
                        }]
                    },

        yAxis: {
                        min: 0,
                        max: 100,
                        lineWidth: 0,
                        verticalAlign: 'middle',
                        tickPositions: []
                    },

        plotOptions: {
            solidgauge: {
                borderWidth: '12px',
                  dataLabels: {
                                enabled: true,
                                useHTML: true,
                                y: -20,
                                borderWidth: 0,
                             
                               
                                format: '<span class="totalprecentage" style="font-size:2em; color: {point.color}; font-weight: bold" onclick="openResultSlidePage()">' + res.total_creditindex + '</span>'
                            },
                linecap: 'round',
                stickyTracking: false
            }
        },

        series: [{
            name: 'Move',
            borderColor: getOptionColor(res.mov_creditindex),
            data: [{
                color: getOptionColor(res.mov_creditindex),
                radius: '100%',
                innerRadius: '100%',
                y: res.mov_creditindex
            }]
        }, {
            name: 'Eat',
            borderColor: getOptionColor(res.eat_creditindex),
            data: [{
                color: getOptionColor(res.eat_creditindex),
                radius: '75%',
                innerRadius: '75%',
                y: res.eat_creditindex
            }]
        }, {
            name: 'Mind',
            borderColor: getOptionColor(res.mind_creditindex),
            data: [{
                color: getOptionColor(res.mind_creditindex),
                radius: '50%',
                innerRadius: '50%',
                y: res.mind_creditindex
            }]
        }]
    },

    /**
     * In the chart load callback, add icons on top of the circular shapes
     */
    function callback() {

        // Move icon
        //this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
			this. renderer.image('./images/move_logo.png', -106, -22, 16, 16)
            .attr({
                'stroke': '#303030',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                'zIndex': 10
            })
            .translate(190, 26)
            .add(this.series[0].group);

        // Exercise icon
        //this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8, 'M', 8, -8, 'L', 16, 0, 8, 8])
		this. renderer.image('./images/eat_logo.png', -106, -39, 16, 16)
            .attr({
                'stroke': '#303030',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                'zIndex': 10
            })
            .translate(190, 61)
            .add(this.series[1].group);

        // Stand icon
        //this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
		this. renderer.image('./images/mind_logo.png', -107, -56, 14, 14)
            .attr({
                'stroke': '#303030',
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                'stroke-width': 2,
                'zIndex': 10
            })
            .translate(190, 96)
            .add(this.series[2].group);
    }
	
	);
	
	
   }
function mindStatus()
{
logStatus("Calling mindStatus", LOG_FUNCTIONS);
	try {
		
        var urldata	=	getPathHashData();
        var usid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		var currentuser = getCurrentUser();
		var postdata ={};
		postdata.coach_id = currentuser.user_id;
		postdata.user_id 	=	usid;
		postdata.is_reporting=  base_url+"/reporting/index.php/mind_switch/activateMindTab";
		statusMindActivation(postdata,function(response){
				if(response.status==1){
					  //alert('Mind questionnaire tab is activate.');
					  var POPUP_HTML = '<div class="popuptitle addextracredit">'+'MOVESMART'+'</div><div class="field_in_box"><p>De MIND vragenlijst is geactiveerd.</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
				}
				else{
					//alert('Mind questionnaire tab is already activated.');
					var POPUP_HTML = '<div class="popuptitle addextracredit">'+'MOVESMART'+'</div><div class="field_in_box"><p>Mind-vragenlijst tabblad is al geactiveerd.</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
				}
				_popup(POPUP_HTML,{close:false});
			});
		}
	catch(e) 
	{
	ShowExceptionMessage("mindStatus", e);
	}
}   

//---------------------POP FOR DISPLAY THE GRAPH (12-04-17)--------------
$("#move").click(function() {
		var urldata	=	getPathHashData();
		var postdata ={};
        var mid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		postdata.is_reporting=  base_url+"/reporting/index.php/report/getstareddate";
		postdata.user_id 	=	mid;
		postdata.group_id 	=	1;
		postdata.phase_id 	=	2;
		postdata.reftypeid 	=	2;
		postdata.f_status 	=	0;
		getMovedata(postdata,function(response){
			if(response.status==1){
				$('.popup_move').css('display','block');
				showmovegraph(response);
			}
			else{
				var POPUP_HTML = '<div class="popuptitle addextracredit">'+'MOVESMART'+'</div><div class="field_in_box"><p>Phase 1 is not started.</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
				_popup(POPUP_HTML,{close:false});
			}
			});
	
});

$("#eat").click(function() {
  		var urldata	=	getPathHashData();
		var postdata ={};
        var mid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		postdata.is_reporting=  base_url+"/reporting/index.php/report/getstareddate";
		postdata.user_id 	=	mid;
		postdata.group_id 	=	2;
		postdata.phase_id 	=	2;
		postdata.reftypeid 	=	1;
		postdata.f_status 	=	0;
		getMovedata(postdata,function(response){
			if(response.status==1){
				$('.popup_eat').css('display','block');
				  showeatgraph(response);
			}
			else{
				var POPUP_HTML = '<div class="popuptitle addextracredit">'+'MOVESMART'+'</div><div class="field_in_box"><p>Phase 1 is not started.</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
				_popup(POPUP_HTML,{close:false});
			}
			});

});

$("#mind").click(function() {
  		var urldata	=	getPathHashData();
		var postdata ={};
        var mid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
		postdata.is_reporting=  base_url+"/reporting/index.php/report/getstareddate";
		postdata.user_id 	=	mid;
		postdata.group_id 	=	3;
		postdata.phase_id 	=	2;
		postdata.reftypeid 	=	1;
		postdata.f_status 	=	0;
		getMovedata(postdata,function(response){
			if(response.status==1){
				$('.popup_mind').css('display','block');
				  showmindgraph(response);
			}
			else{
				var POPUP_HTML = '<div class="popuptitle addextracredit">'+'MOVESMART'+'</div><div class="field_in_box"><p>Phase 1 is not started.</p><div class="pupopfooter forgotpup_btn"><button class="selectbtn" onclick="_close_popup()">'+'Ok'+'</button></div></div>';
				_popup(POPUP_HTML,{close:false});
			}
			});

});

$("#movecross").click(function() {
  $('.popup_move').css('display','none');
})

$("#eatcross").click(function() {
  $('.popup_eat').css('display','none');
})
$("#mindcross").click(function() {
  $('.popup_mind').css('display','none');
})

//------------------------- Display the move graph----------------------
function showmovegraph(response)
{
	var daywisedata = [];
	for(var i=1;i<=14;i++)
	{
		var arr1 = [];
			
			arr1[0] = i.toString();
			arr1[1] = parseInt(0);
			daywisedata.push(arr1);
	}
	var tempdaywisedt = [];
	$.each(response.data,function(key,val){
			var arr = [];
			arr[0] = val.DiffDate;
			arr[1] = parseFloat(val.credit);
			tempdaywisedt.push(arr);
	});
	$.each(daywisedata,function(idx,dval){
		$.each(tempdaywisedt,function(key,val){
			if(val[0]==dval[0])
			{
				dval[0] = val[0];
				dval[1] = val[1];
				return;
			}
		
		});
	});
	Highcharts.chart('container_move', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Credits MOVE - fase 1'
    },
    xAxis: {
		type: 'category',
		title: {
            text: 'Dagen'
        },
        labels: {
            rotation: 0,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        max :50,
        lineWidth: 2,
        tickInterval: 5,
        title: {
            text: 'FIT-punten'
        },
		 plotLines: [{
                color: '#FF0000',
                width: 2,
                value: 50
            }]
    },
    legend: {
        enabled: true
    },
    credits: { enabled: false },
    series: [{
        name: 'FIT-punten per dag',
        data: daywisedata,
        dataLabels: {
            enabled: false,
            rotation: 0,
            color: '#ffffff',
            align: 'center',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
}
//--------------------------End the move graph---------------------------

//------------------------- Display the eat graph----------------------
function showeatgraph(response)
{
	var daywisedata = [];
	for(var i=1;i<=14;i++)
	{
		var arr1 = [];
			
			arr1[0] = i.toString();
			arr1[1] = parseInt(0);
			daywisedata.push(arr1);
	}
	var tempdaywisedt = [];
	$.each(response.data,function(key,val){
			var arr = [];
			arr[0] = val.DiffDate;
			arr[1] = parseFloat(val.credit);
			tempdaywisedt.push(arr);
	});
	$.each(daywisedata,function(idx,dval){
		$.each(tempdaywisedt,function(key,val){
			if(val[0]==dval[0])
			{
				dval[0] = val[0];
				dval[1] = val[1];
				return;
			}
		
		});
	});
	Highcharts.chart('container_eat', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Credits EAT - fase 1'
    },

    xAxis: {
		type: 'category',
		title: {
            text: 'Dagen'
        },
        labels: {
            rotation: 0,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        max :50,
        lineWidth: 1,
        tickInterval: 5,
        title: {
            text: 'Credits'
        },
		plotLines: [{
                color: '#FF0000',
                width: 2,
                value: 50
            }]
    },
    legend: {
        enabled: true
    },
    credits: { enabled: false },
    series: [{
        name: 'Credits per dag',
        data: daywisedata,
        dataLabels: {
            enabled: false,
            rotation: -90,
            color: '#ffffff',
            align: 'center',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
}
//--------------------------End the eat graph---------------------------

//------------------------- Display the mind graph----------------------
function showmindgraph(response)
{
	
	var daywisedata = [];
	for(var i=1;i<=14;i++)
	{
		var arr1 = [];
			
			arr1[0] = i.toString();
			arr1[1] = parseInt(0);
			daywisedata.push(arr1);
	}
	var tempdaywisedt = [];
	$.each(response.data,function(key,val){
			var arr = [];
			arr[0] = val.DiffDate;
			arr[1] = parseFloat(val.credit);
			tempdaywisedt.push(arr);
	});
	$.each(daywisedata,function(idx,dval){
		$.each(tempdaywisedt,function(key,val){
			if(val[0]==dval[0])
			{
				dval[0] = val[0];
				dval[1] = val[1];
				return;
			}
		
		});
	});Highcharts.chart('container_mind', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Credits MIND - fase 1'
    },

    xAxis: {
		type: 'category',
		title: {
            text: 'Dagen'
        },
        labels: {
            rotation: 0,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        max :50,
        lineWidth: 1,
        tickInterval:5,
        title: {
            text: 'Credits'
        },
		plotLines: [{
                color: '#FF0000',
                width: 2,
                value: 50
            }]
    },
    legend: {
        enabled: true
    },
    credits: { enabled: false },
    series: [{
        name: 'Credits per dag',
		data: daywisedata,
        dataLabels: {
            enabled: false,
            rotation: 0,
            color: '#ffffff',
            align: 'center',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
}
//--------------------------End the mind graph---------------------------

