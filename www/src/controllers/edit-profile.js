$(function(){
    maincontent_leff_none();
    TranslateEditProfil();
    TranslateGenericText();
    _setPageTitle(GetLanguageText(PAGE_TTL_USER_PROFILE));
     editProfilePage();

    $(window).resize(function(){
        //maincontent_leff_none();
    });

});

function TranslateEditProfil(){
     TranslateText(".transfirstname", PHTRANS, PH_FIRST_NAME);
     TranslateText(".translastname", PHTRANS, PH_LAST_NAME);
     TranslateText(".transemail", PHTRANS, PH_EMAIL);
     TranslateText(".transphone", PHTRANS, PH_PHONE);
     TranslateText(".translinked", TEXTTRANS, TEXT_LINKED);
     TranslateText(".transadministrator", TEXTTRANS, LBL_ADMINISTRATOR);
     TranslateText(".transcardio", TEXTTRANS, BTN_CARDIO);
     TranslateText(".transstrengh", TEXTTRANS, LBL_STRENGTH);
     TranslateText(".transnutrition", TEXTTRANS, LBL_NUTRITION);
     TranslateText(".transstrenth", TEXTTRANS, LBL_STRENGTH);
     TranslateText(".transgroupmessage", TEXTTRANS, LBL_GROUP_MESSAGES);
     TranslateText(".transallclients", TEXTTRANS, LBL_All_CLIENTS);
     TranslateText(".transright", TEXTTRANS, TEX_RIGHT);
     TranslateText(".transgetpicture", TEXTTRANS, TEX_GET_IMAGE);
     TranslateText(".transactive", TEXTTRANS, TEX_ACTIVE);
     TranslateText(".transnonactive", TEXTTRANS, TEX_NON_ACTIVE);
     TranslateText(".transparameters", PHTRANS, PH_EXTRA_PARAMETERS);
     TranslateText(".transpasword", PHTRANS, PH_PASSWORD);
}

/*SN added profilr pic 20160123*/
$(document).delegate(".profileimages ","click",function(){ 
    logStatus("Calling profileimages click event",LOG_COMMON_FUNCTIONS);
    try {
        if(!IsWebLoad()){
            $(".choosephoto_popup").show();
        } else {
            uploadProfileImage();
        }
    } catch (e){
        ShowExceptionMessage("profileimagesb click event", e);
    }
    //
});

$(document).delegate('#saveProfieldata','click',function(){
    logStatus("Calling saveProfieldata click event",LOG_COMMON_FUNCTIONS);
    try {
    //getCaptchaValid(function(){ // For current Build Captach is not neccessory , as per latest discussion (20160121);
        var userdata 	= getCurrentLoginInfo();
        var userid 		= userdata.user.user_id;
        var questionid = [];
        for(var i = 1 ; i <=3;i++ ){
            questionid .push(i+':'+$('#sqQues_'+i).val());
        }
        var question	=	{};
        $(".sq_ques").each(function(i,eachitem){
            var idstr	=	$(eachitem).attr("id");
            var answer	=	$(eachitem).val();
            if($.trim(answer)!=""){
                var _qid	=	idstr.split('sqQues_')[1];
                question[_qid]=answer;
            }
        });
        var _datapost = {};
        _datapost.first_name = $('#userfirstname').val();
        _datapost.last_name = $('#userlastname').val();
        _datapost.email = $('#useremail').val();
        _datapost.passwrd = $('#userpswrd').val();
        _datapost.phone = $('#userphone').val();
        _datapost.gender = $('#usergenderdata').val();
        _datapost.language = $('#languagelist').val();
        DisableProcessBtn("#saveProfieldata", false);
        updateSecurityQuestion('updateProfiledata',userid,question,_datapost,function(){
            if (LOGIN_INFO.user){
                LOGIN_INFO.user.first_name = _datapost.first_name;
                LOGIN_INFO.user.last_name = _datapost.last_name;
                LOGIN_INFO.user.email = _datapost.email;
                LOGIN_INFO.user.passwrd = _datapost.passwrd;
                LOGIN_INFO.user.phone = _datapost.phone;
                LOGIN_INFO.user.gender = _datapost.gender;
                LOGIN_INFO.user.r_language_id = _datapost.language;
                USER_LANG	=	_datapost.language;
                updateCurrentLoginInfo();
            }
            DisableProcessBtn("#saveProfieldata", true,GetLanguageText(BTN_SAVE));
            ShowToastMessage(GetLanguageText(MSG_SUCCESS_PROFILE_UPDATED), _TOAST_LONG);
        });
    //});
    } catch (e){
        ShowExceptionMessage("saveProfieldata click event", e);
    }
});

$(document).delegate(".capturephotoopt","touchstart",function(){
    logStatus("Calling capturephotoopt touchstart event",LOG_COMMON_FUNCTIONS);
    try {
        var selectedphotoopt = $(this).attr("selectopt");
        if (!selectedphotoopt){
            selectedphotoopt = 0;
        }
        /**
         * @param  Camera
         * @param  Camera.EncodingType This is  EncodingType
         * @param  Camera.EncodingType.JPEG This is  EncodingType jpeg
         */
        getCurrentCapturedImage(function(fileurl){
                $('.choosephoto_popup').hide();
                UploadProfilePic(fileurl, URL_SERVICE);
            }, function(){
                $('.choosephoto_popup').hide();
                ShowAlertMessage("Capture Failed");
                //Need to handle the failure
            }, { quality : 25, allowEdit : true,
                  encodingType: Camera.EncodingType.JPEG,
                  targetWidth: 200,
                  targetHeight: 200, sourceType : selectedphotoopt}
        );
    } catch (e){
        ShowExceptionMessage("capturephotoopt touchstart", e);
    }
});


$(document).delegate("#profilepic","change",function(){
    logStatus("Calling Event#profilepic.Change",LOG_FUNCTIONS);
    try {
        UploadProfilePic('', null);
    } catch (e){
        ShowExceptionMessage("Event#profilepic.Change", e);
    }
});

$(document).delegate("#usergenderdata","change",function(){
    var selecttext = $( "#usergenderdata").find("option:selected" ).text();
    $("#usergenderdata_text").html(selecttext);
});

$(document).delegate("#languagelist","change",function(){
    var selecttext = $( "#languagelist").find("option:selected" ).text();
    $("#languagelist_text").html(selecttext);
});
/* Uploading the Profile Picture by SNK*/
function UploadProfilePic(filepath, uploadservicepath){
    logStatus("Calling UploadProfilePic",LOG_FUNCTIONS);
    try {
        var userdata 	= getCurrentLoginInfo();
        var userid 		= userdata.user.user_id;
        var postdata = {};
        postdata.imagefile = 'image';
        postdata.id = userid;
        postdata.uploadtype = UPLOAD_TYPE_USER_PROFILE;
        if (filepath == ''){
            postdata.image = $('input[type=file]')[0].files[0];
        }
        var getdata = {};
        getdata.action = 'updateProfileimage';

        $(".profileimages span").html("Uploading...");
        UploadJpgImageToServer(filepath, uploadservicepath, function(response){
                if (response){
                    $(".profileimages span").html("Get Picture");
                    if(typeof response !=='object'){
                        try{
                            response=$.parseJSON(response);
                        }catch(e){

                        }
                    }
                    if (response.status){
                        if (response.status == AJAX_STATUS_TXT_SUCCESS){
                            ShowToastMessage(response.status_message, _TOAST_LONG);
                            if (response.imagefile){
                                $('#userprofile').attr('src',PROFILEPATH+response.imagefile);
                                $('#userprofiledata').attr('src',PROFILEPATH+response.imagefile);
                            }
                        }else{
                            ShowAlertMessage(response.status_message);
                        }
                    }
                }
            }, function(){
          }, getdata, postdata);
    } catch(e) {
        ShowExceptionMessage("UploadProfilePic", e);
    }
}


// *** [SK] comments by shanethatech ***//
// *** Page user profile data information ***//
function editProfilePage(){
     logStatus("Calling editProfilePage", LOG_FUNCTIONS);
    try {
        /**
         * @param  login_info.user.is_deleted This is is_deleted
         */
        var login_info = getCurrentLoginInfo();
        var user = getCurrentUser();
        if(!$.isEmptyObject(user) && !$.isEmptyObject(login_info)){
            /*
            _getUserImage('getUserImage',login_info.user.user_id,function(response){
                alert(response);
                var filename = response.split('.').pop();
                if(jQuery.inArray(filename, EXT) !== -1){
                    alert(PROFILEPATH + response);
                    $('#userprofile').attr('src',PROFILEPATH + response);
                }
            });
            */
            $('select[name^="gender_details"] option[value='+user.gender+']').attr("selected","selected");
            var $userfirstname = $('#userfirstname');
            var $isactive = $('#isactive');
            var $isnotactive = $('#isnotactive');
            $userfirstname.val(login_info.user.first_name);
            $userfirstname.val(login_info.user.first_name);
            $('#userlastname').val(login_info.user.last_name);
            $('#useremail').val(login_info.user.email);
            $('#loginemail').val(login_info.user.email);
            $('#userphone').val(login_info.user.phone);
            $('#usergenderdata').val(login_info.user.gender);
			var gender = login_info.user.gender;
			   if(gender==0){
				$('#usergenderdata_text').html('Male');
			   $('select[name^="gender_details"] option[value="0"]').attr("selected","selected");
			   }
			   else{
				$('#usergenderdata_text').html('Female');
				$('select[name^="gender_details"] option[value="1"]').attr("selected","selected");
			   }
            var isactive = login_info.user.is_deleted;
            if(isactive){
                $isactive .addClass('radiobtn');
                $isnotactive.removeClass('radiobtn');
            } else {
                $isnotactive.addClass('radiobtn');
                $isactive.removeClass('radiobtn');
            }
            var associativecluns = login_info.user_session.associatedClubs;
            var assoc_clubli	=	"";
            $.each(associativecluns,function(key,value){
                assoc_clubli	+= '<li class="clubdetails">'+
                    //'<span class="radiobox radiobtn">'+'<input type="radio"  class="radioclub" checked="checked"></span>'+
                    '<span class="radiobox radiobtn">'+'<input type="radio"  id="club_Org" value="'+value.r_club_id+'"></span>'+
                    '<label>'+value.club_name+'</label>'+
                '</li>';
            });
            if(assoc_clubli==''){
                assoc_clubli	=	'<li>No Clubs associated</li>';
            }
            $('.associativeclubs .clublist').html(assoc_clubli);
            var _sqQuestions=	login_info.securityQuestions;
            var quesHTML	=	"";
            $.each(_sqQuestions,function(_index,_ques){
                quesHTML+=	'<input class="blu_input sq_ques" type="text" id="sqQues_'+_ques.secure_question_id+'" placeholder="'+_ques.question+'" /> ';
            });
            var langHTML = "";
            $("#languagelist").html('<option selected="selected">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
            /**
             * @param value.language_id This is langauage id
             * @param value.title This is title
             */
            getlanguages(function(response){
                $.each(response,function(key,value){
                    langHTML+= '<option value="'+value.language_id+'">'+value.title+'</option>';
                });
                var $languagelist = $("#languagelist");
                $languagelist.html(langHTML);
                $languagelist.val(login_info.user.r_language_id);
                var language = login_info.user.r_language_id;
                $languagelist.find('option[value='+language+']').attr('selected', 'selected');
                var selecttext = $languagelist.find("option:selected" ).text();
                $("#languagelist_text").html(selecttext);

            });
            $(".questions_list").html(quesHTML);
            /*setCaptchaContent(".recapchabox",function(){
                /*On load Captcha Success* /
            });*/
        }
    } catch(e) {
        ShowExceptionMessage("editProfilePage", e);
    }
}
function uploadProfileImage(){ 
    logStatus("Calling uploadProfileImage",LOG_FUNCTIONS);
    try {
        $("input#profilepic").trigger("click.#profilepic");
    } catch(e) {
        ShowExceptionMessage("uploadProfileImage", e);
    }
}
