$(document).ready(function(){
    //maincontent_leftimage();
    $(".main_left").show().removeClass("background1 background3 background4").addClass("background2");

    // $(window).resize(function(){
        // maincontent_leftimage();
    // });

    TranslateGenericText();
    TranslationManageCardioTest();
    //_setPageTitle(GetLanguageText(PAGE_TTL_MANAGE_CARDIO_TEST));

    var urldata	=	getPathHashData();
    var mid	=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    var testid	=	(urldata.args.testid) ? base64_decode(urldata.args.testid) :0;
    var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
    /**
     * @param response.current_test_time This is current test time
     * @param response.age_on_test This is age on test
     * @param response.weight_on_test This is weight on test
     */
    getEditMember('testResultsListDetail',mid,testid,clubid,function(response){
        var date = dateFormat(response.test_date);
        $('.rpmtime').html(response.rpm);
        $('.timefeild').html(response.current_test_time);
        $('.cardiomemName').html(response.last_name);
        $('.ageontest').attr('value',response.age_on_test);
        $('.testdate').attr('value',date);
        $('.weightontestdate').attr('value',response.weight_on_test);
        $('.testmethodname').attr('value',response.test_name);
    });
});

function TranslationManageCardioTest(){
    TranslateText(".welcome_para", TEXTTRANS, TEX_WELCOME_STRONG);
    TranslateText(".transenterclientcode",PHTRANS, PH_ENTER_CLIENT_CODE);
    TranslateText(".transsearchclient",PHTRANS, PH_SEARCH_CLIENT);
    TranslateText(".nodevicemapcls",TEXTTRANS, TEX_NO_DEVICE_MAPPED);
    TranslateText(".devicemapcls",TEXTTRANS, TEX_DEVICE_MAPPED);
}