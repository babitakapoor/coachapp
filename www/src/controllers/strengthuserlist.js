var MAPPED_DEVICE_DET = {};
var _usetlistimages = {};
//var clicked = false;
var _isTestStarted = false;

var _TimeSeconds	=	0;
//XX var _StopWatchTimer;
//XX var _StopSeriesTimer;
//XX var _TimerStopped	=	false;
var _Maxseriescount = 1;
var _Maxrepscount = 12;
var _pinposition = 5;

var _repscount = 0;
var _seriescount = 0;
var _workoutstep = 0;

var _timerobj;

var _lastuserid = 0;
//var _lastrepdistance = 0;
//var _lastrepupdistance = 0;
var _prevdata = -1;
$(document).ready(function(){
    logStatus("Calling document.ready.Strengthuser List", LOG_FUNCTIONS);
    try {
        _setPageTitle('Chest Press');
        maincontent_leff_none();
        var userdata	=	getCurrentUser();
        var curusrid = userdata.user_id;
        var HTML  ='';
        var devicesettings = _getCacheArray('devicesettings');
        $('.devicemacname').text(devicesettings.machine_name);
        $.each(devicesettings,function(i,row){
            logStatus(row, LOG_DEBUG);
            logStatus(i, LOG_DEBUG);
            var machineimg = 'images/noimage.png';
            if(devicesettings.machineimage){
                machineimg = MACHINE_PATH+devicesettings.machineimage;
            }
            
            var img = '<img src="" alt="loading.." />';
            $('.chestpress_right').html(img);
            getValidImageUrl(machineimg,function(url){
                $('.chest_detail_fl img').attr('src',url);
                $('.chestpress_right img').attr('src',url);
            });
            
        });
        getStrengthUserList(curusrid,function(response){
            $.each(response,function(key,row){
                //var imgname = PROFILEPATH+row.userimage;
                _usetlistimages[row.user_id] = row;
                HTML+='<li onclick="openStrengthUserDetail('+row.user_id+')">'+
                        '<span class="user_iconbox" >'+
                            '<img src="" alt="user_image1" class="strusrimg_'+row.user_id+'"/>'+
                        '</span>'+
                        '<h4>'+row.first_name+'</h4>'+
                    '</li>'	;

            });
            $('.strengthusrlist').html(HTML);
            $.each(_usetlistimages,function(userid,eachusr){
                 getValidImageUrl(PROFILEPATH+eachusr.userimage,function(url){
                     $('.strusrimg_'+userid).attr('src',url);
                 });
            });
            strengthusrlist_height();
        });
        getMappedDeviceInfo(function(response){
            $.each(response,function(mapkey,hrmapdata){
                MAPPED_DEVICE_DET[hrmapdata.device_code] = hrmapdata;
            });
            ScanHR_BLEDevice();
        });
        $(".chectpressclose_btn").click(function(){
            $(".chectpress_popup_outer").hide();
            $(".chectpress_popup").hide();
        });
        $(".chectpressskip_btn").click(function(){
            ShowStrengthWorkoutScreen();
        });

        $(window).resize(function(){
            strengthusrlist_height();
        });
    }catch(e) {
        ShowExceptionMessage("Calling document.ready.Strengthuser List", e);
    }
});
function openStrengthUserDetail(usrid){
    logStatus("Calling openStrengthUserDetail", LOG_FUNCTIONS);
    try {
        _lastuserid = usrid;
        $(".chectpress_popup,.chectpress_popup_outer").show();
        $(".chectpress_popup").show();
        /**
         * @param userdetaildata.userimage This is user image
         */
        var userdetaildata =  _usetlistimages[usrid];
        //_movePageTo('chestpress_detail');
        $('.slctusrname').text('Welcome, '+userdetaildata.first_name);
        getValidImageUrl(PROFILEPATH+userdetaildata.userimage,function(url){
            $('.slctusrimage').attr('src',url);
        });
        var devicesettings = _getCacheArray('devicesettings');
        var machineid = devicesettings.strength_machine_id;
        var coef_man = devicesettings.coef_man;
        var coef_woman = devicesettings.coef_woman;
        $('.devicemacname').text(devicesettings.machine_name);
        var maxweight =  '';
        if(userdetaildata.gender==1){
           maxweight = coef_woman*userdetaildata.weight;
        }
        if(userdetaildata.gender==0){
           maxweight = coef_man*userdetaildata.weight;
        }
       
        _getMachinePinPosition(machineid,maxweight,function(response){

           /* $.each(response[0],function(i,row){
                console.log(row.pinposition);
                $(".clspinposition").html(row.pinposition);
            });*/
            var pinposition = (response[0]) ?response[0].pin_position:0;
            $(".clspinposition").html(pinposition);
        });
		
        $(".clsmaxseriescnt").html("/"+_Maxseriescount);
        $(".clsmaxrepscnt").html("/"+_Maxrepscount);
        $('.strength_count').text(_Maxrepscount);
        $('.clscurrentseries').text(_Maxseriescount);
    }catch(e) {
        ShowExceptionMessage("openStrengthUserDetail", e);
    }

}
function seriesStrengthCount(){
    /*if (_StopSeriesTimer){
        clearInterval(_StopSeriesTimer);
        count = 1;
        _TimeSeconds = 0;
    }
    _StopSeriesTimer=setInterval(function(){
        $('.strength_count').text(count);
        count++;
    }, 10000);*/
}
function startProgramClock() {	
    //XX $('.chest_graph_activebg').removeClass('animaterom_stop').addClass('animaterom');
    _isTestStarted = true;
    ContactStrengthMachine('start', function(){}, function(){});
    $('.chest_graph_activebg').removeClass('animaterom_stop');
    ResetStrengthworkout();
    seriesStrengthCount();
    StartStrengthTimer();
}

function ContactStrengthMachine(action, successhandler, errorhandler){
    /**
     * @param window.plugins
     * @param window.plugins.strengthmovetolive
     * @param window.plugins.strengthmovetolive.test
     */
    if (window.plugins){
        if (window.plugins.strengthmovetolive){
            if (window.plugins.strengthmovetolive.test){
                window.plugins.strengthmovetolive.test(action, successhandler, errorhandler);
            }
        }
    }
}

function ResetStrengthworkout(){
    _TimeSeconds = 0;
    _repscount = _Maxrepscount;
    _seriescount = _Maxseriescount;
    $('.strength_count').text(_repscount);
    $('.clscurrentseries').text(_seriescount);
    $('#timer').val(formatHHMMSSFromSec(_TimeSeconds));
}

function stopClock() {
    _isTestStarted = false;
    StopStrengthTimer();
    ResetStrengthworkout();
    ContactStrengthMachine('stop', function(){}, function(){});
    /*if (_StopSeriesTimer){
        clearInterval(_StopSeriesTimer);
        count = 0;
        _TimeSeconds = 0;
        _StopSeriesTimer = null;
        $(".chest_graph_activebg").removeClass('animaterom');
    }*/
    $('.chest_graph_activebg').addClass('animaterom_stop');
    //Temp - Fix : On Stop Clock It should logout
    var btid = _getCacheValue('lasttestdeviceid');
    if ((btid) && (btid != '')){
        StopBLENotificationProcess(btid);
    }
    $('#btmcustomfooter').show();
    $('.strength_usrlist_detail').hide();
    $('.stregth_usrlist').show();
    $('.chectpress_popup_outer').hide();
    $('.chectpress_popup').hide();
}
function OnHearRateDeviceScanSuccess(devicelist){
    logStatus("OnHearRateDeviceScanSuccess", LOG_FUNCTIONS);
    console.log(JSON.stringify(devicelist));
    if ((devicelist) && (devicelist.length>0)){
        var HTML	=	'';
        $.each(devicelist,function(_idx,rowdata){
            if (rowdata.id){
                var avilablestatus	=	"No Member Mapped";
                if (MAPPED_DEVICE_DET[rowdata.id]){
                    avilablestatus=	'Device is Mapped to '+MAPPED_DEVICE_DET[rowdata.id].last_name;
                }
                //HTML+=	'<li deviceid="'+rowdata.id+'" onclick="selectbtdevice("'+rowdata.id+'")">'+
                HTML+=	'<li class="clsscanneddevicelist clsstrength" deviceid="'+rowdata.id+'">'+
                        '<div class="listdiv_left">'+
                            '<img src="images/product_2.png" alt="user_image" />'+
                        '</div>'+
                        '<div class="listdiv_right">'+
                            '<h4>' + avilablestatus + '</h4>'+
                            '<p>' + rowdata.id + '</p>'+
                        '</div>'+
                    '</li>';

            }
        });
        $(".userstrengthdevice").html(HTML);
        $(".scanloading").html("");
        SetHREventDetails();
    }
}
function SetHREventDetails(){
    logStatus("SetHREventDetails", LOG_FUNCTIONS);
   // $(".clsscanneddevicelist").die("click").live("click",function(evt){
    $(".clsscanneddevicelist").click(function(evt){
        //$(document).delegate(".heartratebox li","click",function(){
            logStatus(".heartratebox li.click", LOG_FUNCTIONS);
            try {
                var memberid = _lastuserid;
                var deviceid =  $(this).attr("deviceid");
                _setCacheValue('lasttestdeviceid', deviceid);
                var $curdeviceelm	=	$(this).find(".listdiv_right");
                //listdiv_right
                var actualHtml	=	$curdeviceelm.html();
            /**/
                var ph	=	getLogingPlaceholder();
                DisableProcessBtn($curdeviceelm[0],false,ph,true);
                updateMappedMemberDevice({deviceCode:deviceid,deviceType:DEVICE_TYPE_HR_monitor,memberId:memberid},function(response){
                    DisableProcessBtn($curdeviceelm[0],true,actualHtml,true);
                    if(response.status==1){
                        var lastdeviceid = _getCacheValue('lasttestdeviceid');

                        if (lastdeviceid){
                            BLEDeviceConnect(lastdeviceid);
                        }
                        ShowStrengthWorkoutScreen();
                        //XX _movePageTo("chestpress");
                        //clientPathReidrect(memberid, 'cardio-monitor');
                    } else {
                        ShowToastMessage("Device is not updated. Please try again later")
                    }
                });
                evt.preventDefault()
            } catch (e) {
                ShowExceptionMessage(".heartratebox li.click", e);
            }
        //});
    });
}

function ShowStrengthWorkoutScreen(){
    $('#btmcustomfooter').hide();
    $('.strength_usrlist_detail').show();
    $('.stregth_usrlist').hide();
    $('.chectpress_popup_outer').hide();
    $('.chectpress_popup').hide();
    //var ProductSlider = new Swiper ('.swiperproductbox',{
    new Swiper ('.swiperproductbox',{
        preventClicks:true,
        slideToClickedSlide:false,
        mode:'horizontal',
        loop: true,
        autoplay:10000
    });
}

function OnHeartRateConnectedCallBack(connecteddevice){
    logStatus("OnHeartRateConnectedCallBack", LOG_FUNCTIONS);
    StartBLENotificationProcess(connecteddevice.id, OnHeartRateDataSuccessCallBack, OnHeartRateDataErrorCallBack);
}

function OnHeartRateDisconnectCallBack(reason){
    logStatus("OnHeartRateDisconnectCallBack", LOG_FUNCTIONS);
    ShowAlertMessage('HR Device disconnected: ' + reason);
}

function OnHeartRateDataErrorCallBack(reason){
    logStatus("OnHeartRateDataErrorCallBack", LOG_FUNCTIONS);
    ShowAlertMessage('HR Device Error Reading HR Data:' + reason);
}

/*ASYNCH:1*/
function OnHeartRateDataSuccessCallBack(buffer){
    logStatus("OnHeartRateDataSuccessCallBack", LOG_FUNCTIONS);
    //if( ( (!_STOPPED) || (_CoolDownStarted) ) && isValidTestMonitorPage()){
    var data = new Uint8Array(buffer);
    var hrdata =  data[1];
    $("#strength_hmrate").html(hrdata);

    //console.log(_StopSeriesTimer);
    /*
    if (_isTestStarted){
        _TimeSeconds++;
        $('#timer').val(formatHHMMSSFromSec(_TimeSeconds));
    }
    */
}

function StartStrengthTimer(){
    StopStrengthTimer();
    _timerobj = setTimeout(function(){
        _TimeSeconds++;
        $('#timer').val(formatHHMMSSFromSec(_TimeSeconds));
        StartStrengthTimer();
    }, 1000)
}

function StopStrengthTimer(){
    if (_timerobj){
        clearTimeout(_timerobj);
    }
}

function CallBackFromCardioMachineMatrixdata(machinedata){

}

function strengthusrlist_height(){
    var windowheight = $(window).height();
    var chestpress_header_box = $(".chestpress_header_box").height();
    var headerheight = $(".header").outerHeight();
    var footerheight = $("footer").outerHeight();
    var chestpress_list_footer = $(".chestpress_list_footer").outerHeight();
    var strengthusrlistheight =  chestpress_list_footer + chestpress_header_box +headerheight + footerheight;
    $(".strengthusrlist").css({
        "height": windowheight-strengthusrlistheight-45
    });
}

function UpdateStrengthSensorParam(data){
    //var distance = 10;
    if ((_prevdata > 0) && (data < _prevdata)){
        if (_workoutstep == 1){
            _repscount--;
            $('.strength_count').text(_repscount);
            if (_repscount == 0){
                _repscount = _Maxrepscount;
                _seriescount--;
                $('.clscurrentseries').text(_seriescount);
                if (_seriescount == 0){
                    _seriescount = _Maxseriescount;
                }
            }
            _workoutstep = 0;
        }
    }
    if (data > _prevdata){
        _workoutstep = 1;
    }
    /* Leg Press
    var distance = 10;
    if ((_prevdata > 0) && (data < _prevdata)){
        if (_lastrepdistance == 0){
            if (data > 30){
                _lastrepdistance = data;
            }
        }
        var deccountdif = (_lastrepdistance - data)
        if (deccountdif > distance){
            //Increase step
            if (_workoutstep == 1){
                _repscount--;
                $('.strength_count').text(_repscount);
                if (_repscount == 0){
                    _repscount = _Maxrepscount;
                    _seriescount--;
                    $('.clscurrentseries').text(_seriescount);
                    if (_seriescount == 0){
                        _seriescount = _Maxseriescount;
                    }
                }
                _lastrepdistance = 0;
                _workoutstep = 0;
                _lastrepupdistance = 0;
            }
        }
    }
    if (data > _prevdata){
        if (_lastrepupdistance == 0){
            if (data > 10){
                _lastrepupdistance = data;
            }
        }
        var inccountdif = (_lastrepupdistance - data)
        if (inccountdif > distance){
            _workoutstep = 1;
            _lastrepdistance = 0;
        }
    }
    */
    _prevdata = data;
    $('.chest_graph_activebg').css("width", data+"%");
}