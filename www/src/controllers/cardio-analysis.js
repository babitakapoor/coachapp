// See BLE heart rate service http://goo.gl/wKH3X7
var _HRChart = null;
var _CurrTime=0;
var _WARM_UP_SECOND = 30;//150;
/*MK Added Temporarily - Need to Add Option Box Later*/
//_AppeatType:0 - Full Data
//_AppeatType:1 - Test Data
var _warmupLoadType		=	1;
var _workoutTestType 	= 	2;
var _coolDownLoadType	=	3;
var _TestList			=	[];
var _Flag_ReAnalyze	=	false;
$(document).ready(function(){
    $(".comm_innerbox").css("max-width", "inherit");
    $(".comm_rightbox").css({"width":"99%", "float": "none", "margin": "0 auto"});
    TransCardioAnalysis();
    TranslateGenericText();
    var memberID	=	GetUrlArg('memberid',true);
	$('#memberid').val(memberID);
	 $('.show_manageMenus').html('<button class="selectbtn everydropdown" onclick="checkLastProgressTest('+memberID+',this,1)" hrefvalue="'+memberID+'">Menu</button>');
    _getClientInformationByID(memberID,function(memberdetail){
        if(memberdetail.first_name && memberdetail.last_name){
            $('#memberImage').attr("alt",memberdetail.first_name);
            $(".memberName").html(memberdetail.first_name);
			$(".memberEmail").html(memberdetail.email);
			$(".memberPhone").html(memberdetail.phone);
            getValidImageUrl(PROFILEPATH + memberdetail.userimage,function(url){
                $('#memberImage').attr('src',url);
            });
        }
        ShowAnalysedResult(memberID);
    });

    $("#idjumptrimup").click(function(){
        TCATChartAnalysis.TRIMBOLDOption = $(".clsreporttrimjumpoption:checked").val();
        TrimOrJumpUp();
        SetAbilityJumpUp();
        SetAbilityJumpDown()
    });

    $("#idjumptrimdown").click(function(){
        TCATChartAnalysis.TRIMBOLDOption = $(".clsreporttrimjumpoption:checked").val();
        TrimOrJumpDown();
        SetAbilityJumpUp();
        SetAbilityJumpDown();
    });

    $("#idshowloadvalue").click(function(){
        var StartSpeed = 5;
        var TestWeight = 80;

        var loadobj = getLoadValueForSpeed(StartSpeed, TestWeight);
        logStatus(loadobj,LOG_DEBUG);
    });

    $("#pretest").click(function(){
        var cindex	=	+app_tempvars('cindex');
        var index	=	(cindex-1);
        if(index<0){
            index	=	0;
        }
        LoadChartData(index);
    });
    $("#nexttest").click(function(){
        var cindex	=	+app_tempvars('cindex');
        var index	=	(cindex+1);
        if(index>_TestList.length){
            index	=	_TestList.length;
        }
        LoadChartData(index);
    });
});
function LoadAnalysisGraph(){
	try{
    DrawHRAnalysisChart();
    InitializeCATAnalysisChart();
    DrawCATAnalysisChart();
}
catch(e){
        ShowExceptionMessage("LoadAnalysisGraph", e);
    }
}

function PrepareHRAnalysisChart(){
    /**
     * @param cardioobj This is cardioobj
     * @param cardioobj._HR_Max This is cardioobj  heart rate Max
     * @param cardioobj.GetANT_HR() This is cardioobj GetANT_HR function
     * @param cardioobj._VertLines_ This is cardioobj  Vertical lines
     * @type {null}
     */
    THRChartAnalysis.ChartObj = null;
    THRChartAnalysis.ChartHRData = [];
    THRChartAnalysis.ChartHRStripline = [];
    THRChartAnalysis.ChartWARMUP = [];
    THRChartAnalysis.ChartCOOLDOWN = [];
    THRChartAnalysis.HRIAND = [];
    var Start = 0;
    var Fin = _ClientData_g.CardioTest_.getHRCount();
    var heattime = parseFloat(_ClientData_g.CardioTest_.getWarmUpTimeForAppearType(cardioobj._AppearType_));
    var cooldowntime = parseFloat(_ClientData_g.CardioTest_.getCooldownTime());
    var fulltime = parseFloat(_ClientData_g.CardioTest_.getFullTimeForAppearType(cardioobj._AppearType_));

    if (cardioobj._AppearType_ == _C_taTestData){
       if (this._ZoomFlag_) {
          Start = this._ZoomBg_ + _ClientData_g.CardioTest_.DataBg_;
          Fin = this._ZoomEnd_ + _ClientData_g.CardioTest_.DataBg_;
       }
       else{
         Start = _ClientData_g.CardioTest_.DataBg_;
         Fin = _ClientData_g.CardioTest_.CoolDnBg_;
       }

    }else{
        var charheight = parseInt(cardioobj._HR_Max)+5;
        THRChartAnalysis.ChartWARMUP.push({x: 0, y:charheight});
        THRChartAnalysis.ChartWARMUP.push({x: heattime, y:charheight});

        THRChartAnalysis.ChartCOOLDOWN.push({x: cooldowntime, y:charheight});
        THRChartAnalysis.ChartCOOLDOWN.push({x: fulltime, y:charheight});
    }
    var hrIAND = cardioobj.GetANT_HR();
    THRChartAnalysis.HRIAND.push({x: hrIAND.X, y:hrIAND.Y});

    var phr = _ClientData_g.CardioTest_.HRData_;
    var hrtime = 0;
    for (var i = Start; i < Fin; i++) {
        if (phr[i]){
            var graphdata = {
                x: hrtime/_C_SampleRate_,
                y: parseInt(phr[i].trim())
            };
            THRChartAnalysis.ChartHRData.push(graphdata);
			hrtime++;
			
        }
    }


    /* Just to show the extra data *
    var maxtime = Math.round(hrtime/12)+.1;
    var graphdata = {
        x: maxtime,
        y: null
    };
    THRChartAnalysis.ChartHRData.push(graphdata);
    /* Just to show the extra data - End*/
    for (var j = 1; j < cardioobj._VertLines_.length; j++) {
        var linedata = heattime + cardioobj._VertLines_[j];
        THRChartAnalysis.ChartHRStripline.push({value:linedata, color: "#6605CD"});
    }
}

function DrawHRAnalysisChart(){
logStatus("Calling DrawHRAnalysisChart", LOG_FUNCTIONS);	
	try{
    /**
     * @param cardioobj._HR_Min This is heart rate min
     */
     PrepareHRAnalysisChart();
    var width = $(".comm_rightbox").width() * .97;
    var height = $(document).height() *.80;
    //alert(width + " - " + height);
    $("#chartResult").css({"width":width, "height":height})
    THRChartAnalysis.ChartObj = new CanvasJS.Chart("chartResult",{
        title :{
            text: "HeartRate Graph",
            wrap:true,
            fontSize: 30,
            maxWidth:width//115
        },
        axisX:{
            title: "Time, min",
            interval : 1,
            minimum: 0,
            stripLines: THRChartAnalysis.ChartHRStripline,
            titleFontSize: 25,
            labelFontSize: 20
          },
          axisY:{
              title: "HR, 1/min",
              minimum: parseFloat(cardioobj._HR_Min)-1,
              maximum: parseFloat(cardioobj._HR_Max)+5,
              titleFontSize: 25,
              labelFontSize: 20
          },
        data:[

              {
                color: "#FFFFA2",
                type: "area",
                markerSize: 1,
                showInLegend: false,
                name: "HRType",
                dataPoints: THRChartAnalysis.ChartWARMUP
              },
              {
                color: "#79FFFF",
                type: "area",
                markerSize: 1,
                showInLegend: false,
                name: "HRType",
                dataPoints: THRChartAnalysis.ChartCOOLDOWN
              },
              {
                color: "#FF0000",
                type: "line",
                markerSize: 1,
                showInLegend: true,
                name: "HR",
                dataPoints: THRChartAnalysis.ChartHRData
              },
              {
                color: "#000080",
                type: "scatter",
                markerSize: 10,
                showInLegend: false,
                name: "HRIAND",
                dataPoints: THRChartAnalysis.HRIAND
              }
          ]
    });
    THRChartAnalysis.ChartObj.render();
}

catch(e){
        ShowExceptionMessage("DrawHRAnalysisChart", e);
    }
}


function InitializeCATAnalysisChart(){
	logStatus("Calling InitializeCATAnalysisChart", LOG_FUNCTIONS);
	try{
 TCATChartAnalysis.ChartObj = null;
	}
   catch(e){
        ShowExceptionMessage("InitializeCATAnalysisChart", e);
    }
}

function PrepareCATAnalysisChart(){
	try{
	TCATChartAnalysis.XAxisMin = 0;
    TCATChartAnalysis.SPMData = [];
    TCATChartAnalysis.IANTWATT = 0;
    TCATChartAnalysis.IANTHR = 0;
    TCATChartAnalysis.TRIMBOLDOption = $(".clsreporttrimjumpoption:checked").val();
    TCATChartAnalysis.TRAINZONEMIN = [];
    TCATChartAnalysis.TRAINZONEMAX = [];
    TCATChartAnalysis.REPDATA1 = [];
    TCATChartAnalysis.REPDATA2 = [];
    /**
     * @param ClientData_g.SPMed_.SPM_[i].SMG
     */
	 
	 

    var watt='';
    var hr='';
	
    for (var i = 0; i < _ClientData_g.SPMed_.SPM_.length; i++) {
		 watt = getWattFromSpeed(_ClientData_g.SPMed_.SPM_[i].SMG.X, _ClientData_g.weight_);
		 
        if ((TCATChartAnalysis.XAxisMin == 0) || (TCATChartAnalysis.XAxisMin > watt)){
            TCATChartAnalysis.XAxisMin = watt;
        }
        var  SPMMarker = "circle";
        var  SPMMarkerColor = "blue";

        if (!_ClientData_g.SPMed_.SPM_[i].Status){
            SPMMarker = "cross";
            SPMMarkerColor = "red";
        }		
        var graphdata = {
            x: watt,
            y: parseInt(_ClientData_g.SPMed_.SPM_[i].SMG.Y), markerType: SPMMarker, markerColor:SPMMarkerColor, click: SPMChartMarkToggle
        };
        TCATChartAnalysis.SPMData.push(graphdata);
		if(i < _ClientData_g.SPMed_.SPM_.length){
			var one = graphdata['x'];
			var two = graphdata['y'];
	}
		
    }
	/* Display the Value in header */
	 /* Get the MAX herat Rate value*/ 
	 var Start = 0;
	 var Fin = _ClientData_g.CardioTest_.getHRCount();
	    var phr = _ClientData_g.CardioTest_.HRData_;
        this._HR_Max = phr[Start];
        this._HR_Min = phr[Start];
        for (var i = Start; i < Fin - 1; i++) {
            if (parseInt(phr[i]) >= this._HR_Max) {
                this._HR_Max = parseInt(phr[i]);
            } else {
                if (parseInt(phr[i]) <= this._HR_Min) {
                    this._HR_Min = parseInt(phr[i]);
                }
            }
        }
	var hrmax = this._HR_Max;
		
	 /*===================*/
	var wattlow = getWattFromSpeed(_ClientData_g.SPMed_._DeflPoint_.X, _ClientData_g.weight_);
	var inthrlow = _ClientData_g.SPMed_._DeflPoint_.Y;
	
	$('#frth_aaa').val(wattlow);
	$('#thr').val(one);
	$('#frt_aaa').val(hrmax);
	$('#sec_aaa').val(inthrlow);
    cardioobj.getFitnesslevel(function(fitleveldata){
                var fitlevel = fitleveldata.fitlevel;
				if(fitlevel!=''){
			$('#fitlevel').val(fitlevel);
				}
				else{
					$('#fitlevel').val(0);
				}
	  });
	 var re_cof = _ClientData_g.SPMed_.Regr1_;
	 if(re_cof!=''){
	$('#regression1').val(re_cof.R);
	 }
    /*Fetch Value for max Values In report*/
	var t = $('#thr').val();
	var y = $('#frt_aaa').val();
	
	$('#max_wat').val(t);
	$('#max_hrt').val(y);
	
	
	if (_ClientData_g.SPMed_._DeflPoint_){
        TCATChartAnalysis.IANTWATT = getWattFromSpeed(_ClientData_g.SPMed_._DeflPoint_.X, _ClientData_g.weight_);
        TCATChartAnalysis.IANTHR = _ClientData_g.SPMed_._DeflPoint_.Y;
    }
    if ((_ClientData_g.SPMed_.RLP1) && (_ClientData_g.SPMed_.RLP2)){
         watt = getWattFromSpeed(_ClientData_g.SPMed_.RLP1.X, _ClientData_g.weight_);
         hr = _ClientData_g.SPMed_.RLP1.Y;
        TCATChartAnalysis.REPDATA1.push({ x: watt, y: hr });
         watt = getWattFromSpeed(_ClientData_g.SPMed_.RLP2.X, _ClientData_g.weight_);
         hr = _ClientData_g.SPMed_.RLP2.Y;
        TCATChartAnalysis.REPDATA1.push({ x: watt, y: hr });
    }
    if ((_ClientData_g.SPMed_.RLP3) && (_ClientData_g.SPMed_.RLP4)){
         watt = getWattFromSpeed(_ClientData_g.SPMed_.RLP3.X, _ClientData_g.weight_);
         hr = _ClientData_g.SPMed_.RLP3.Y;
        TCATChartAnalysis.REPDATA2.push({ x: watt, y: hr });
         watt = getWattFromSpeed(_ClientData_g.SPMed_.RLP4.X, _ClientData_g.weight_);
         hr = _ClientData_g.SPMed_.RLP4.Y;
        TCATChartAnalysis.REPDATA2.push({ x: watt, y: hr });
    }
    /**
     * @param cardioobj._TrainZone_.Min This is cardioobj TrainZone Min
     * @param cardioobj._TrainZone_.Max This is cardioobj TrainZone Max
     */
    if ((cardioobj._TrainZone_.Max) && (cardioobj._TrainZone_.Min)){
         watt = getWattFromSpeed(cardioobj._TrainZone_.Min.X, _ClientData_g.weight_);
         hr = cardioobj._TrainZone_.Min.Y;
        if (TCATChartAnalysis.XAxisMin > watt){
            TCATChartAnalysis.XAxisMin = watt-10;
        }
        TCATChartAnalysis.TRAINZONEMIN.push({ x: 0, y: hr });
        TCATChartAnalysis.TRAINZONEMIN.push({ x: watt, y: hr });
         watt = getWattFromSpeed(cardioobj._TrainZone_.Max.X, _ClientData_g.weight_);
         hr = cardioobj._TrainZone_.Max.Y;
        TCATChartAnalysis.TRAINZONEMAX.push({ x: 0, y: hr });
        TCATChartAnalysis.TRAINZONEMAX.push({ x: watt, y: hr });
		
			var min_y = $('#min_y').val(TCATChartAnalysis.TRAINZONEMIN[1].y);
			var max_y = $('#max_y').val(TCATChartAnalysis.TRAINZONEMAX[1].y);
    }
}
catch(e){
        ShowExceptionMessage("PrepareCATAnalysisChart", e);
    }
}
function DrawCATAnalysisChart(){
	logStatus("Calling DrawHRAnalysisChart", LOG_FUNCTIONS);
	try{
		    PrepareCATAnalysisChart();
    TCATChartAnalysis.ChartObj = new CanvasJS.Chart("CATchartResult",{
		
        title :{
            /*text: "CAT HeartRate Graph"*/
            text: "FIT-Graph"
        },
        axisX:{
            title: "WATT",
            minimum: TCATChartAnalysis.XAxisMin-1
          },
          axisY:{
            title: "HR, 1/min",
            minimum: parseFloat(cardioobj._HR_Min)-1,
            maximum: parseFloat(cardioobj._HR_Max)+5
          },
        data:[
              {
                color: "#EEC6BE",
                type: "area",
                markerSize: 1,
                showInLegend: false,
                name: "TRAINZONEMAX",
                dataPoints: TCATChartAnalysis.TRAINZONEMAX
              },
              {
                color: "#FCF0EE",
                type: "area",
                markerSize: 1,
                showInLegend: false,
                name: "TRAINZONEMIN",
                dataPoints: TCATChartAnalysis.TRAINZONEMIN
              },

              {
                color: "#990000",
                type: "scatter",
                markerSize: 10,
                showInLegend: false,
                name: "HR",
                dataPoints: TCATChartAnalysis.SPMData
              },
              {
                color: "#6605CD",
                type: "line",
                markerSize: 1,
                showInLegend: false,
                name: "Def WATT",
                dataPoints: [{ x: TCATChartAnalysis.IANTWATT, y: 0 }, { x: TCATChartAnalysis.IANTWATT, y: TCATChartAnalysis.IANTHR }]
              },
              {
                color: "#6605CD",
                type: "line",
                markerSize: 1,
                showInLegend: false,
                name: "Def HR",
                dataPoints: [{ x: 0, y: TCATChartAnalysis.IANTHR }, { x: TCATChartAnalysis.IANTWATT, y: TCATChartAnalysis.IANTHR }]
              },
              {
                color: "#000",
                type: "line",
                markerSize: 1,
                showInLegend: false,
                name: "REPDATA1",
                dataPoints: TCATChartAnalysis.REPDATA1
              },
              {
                color: "#000",
                type: "line",
                markerSize: 1,
                showInLegend: false,
                name: "REPDATA2",
                dataPoints: TCATChartAnalysis.REPDATA2
              }
          ]
    });
    TCATChartAnalysis.ChartObj.render();
	}
catch(e){
        ShowExceptionMessage("DrawCATAnalysisChart", e);
    }
}

function DrawCATAnalysisChart_Refresh(){
    InitializeCATAnalysisChart();
    DrawCATAnalysisChart();
    /**
    PrepareCATAnalysisChart();
    TCATChartAnalysis.ChartObj.render();
    /**/
}

function TrimOrJumpUp(){
    /**
     * @param cardioobj This is cardioobj
     * @param cardioobj._ANTI_ This is cardioobj ANTI
     * @param cardioobj._VArray_
     * @param cardioobj._TrimBold_
     * @param cardioobj._TrimJust_
     */
    if (TCATChartAnalysis.TRIMBOLDOption == _C_TrimBoldFlag){ //Jump
        if (cardioobj._ANTI_ <	(cardioobj._VArray_.length - 1)){
            cardioobj._TrimBold_++;
            JumpUpdate();
        }
    }else{
       if (cardioobj._TrimJust_ < 200){
            cardioobj._TrimJust_++;
            TrimUpdate();
       }
    }
}

function TrimOrJumpDown(){
    if (TCATChartAnalysis.TRIMBOLDOption == _C_TrimBoldFlag){ //Jump
        if (cardioobj._ANTI_ >	2){
            cardioobj._TrimBold_--;
            JumpUpdate();
        }
    }else{
       if (cardioobj._TrimJust_ > -200){
            cardioobj._TrimJust_--;
            TrimUpdate();
       }
    }
}

function JumpUpdate(){
    /**
     * @param  cardioobj.Treashold()  This is cardioobj Treashold function
     * @param  cardioobj.GetTrainZone()  This is cardioobj GetTrainZone function
     * @param  cardioobj.DefRegrLines()  This is cardioobj DefRegrLines function
     */
    cardioobj.Treashold();
    cardioobj.GetTrainZone(_ClientData_g.SPMed_.TrainType_);
    cardioobj.DefRegrLines();
    DrawCATAnalysisChart_Refresh();
}

function TrimUpdate(){
    /**
     * @param cardioobj._DefPoint0 This is cardioobj _DefPoint0
     * @param cardioobj.DefPulse This is cardioobj DefPulse
     * @param cardioobj._TempDefPoint This is cardioobj _TempDefPoint

     */
    _ClientData_g.SPMed_._DeflPoint_.X = cardioobj._DefPoint0.X  + (0.1 * cardioobj._TrimJust_);
    _ClientData_g.SPMed_._DeflPoint_.Y = cardioobj.DefPulse(_ClientData_g.SPMed_._DeflPoint_.X );
    cardioobj._TempDefPoint.X = _ClientData_g.SPMed_._DeflPoint_.X;
    cardioobj._TempDefPoint.Y = _ClientData_g.SPMed_._DeflPoint_.Y;

    cardioobj.GetTrainZone(_ClientData_g.SPMed_.TrainType_);
    cardioobj.DefRegrLines;

    DrawCATAnalysisChart_Refresh();
}

function SPMChartMarkToggle(e){
    var SPMIdx = e.dataPointIndex;
    //_ClientData_g.SPMed_.SPM_[SPMIdx].Status = !_ClientData_g.SPMed_.SPM_[SPMIdx].Status;
    _ClientData_g.SPMed_.SPM_[SPMIdx].Status = !(_ClientData_g.SPMed_.SPM_[SPMIdx] && _ClientData_g.SPMed_.SPM_[SPMIdx].Status);
    var count = GetActiveSPMCount();
    if (count < 4){
        _ClientData_g.SPMed_.SPM_[SPMIdx].Status = true;
    }
    MarkRecalc();
    //alert(JSON.stringify(e));
}

/**
 * @return {number}
 */
function GetActiveSPMCount(){
    var cnt = 0;
    for (var i = 0; i < _ClientData_g.SPMed_.SPM_.length; i++) {
        if (_ClientData_g.SPMed_.SPM_[i].Status){
            cnt++
        }
    }
    return cnt;
}

function MarkRecalc(){
    /**
    *@param cardioobj.NewCalcRegressionLines() This is NewCalcRegressionLines function
     */
    cardioobj.NewCalcRegressionLines();
    cardioobj.GetTrainZone(_ClientData_g.SPMed_.TrainType_);
    cardioobj.DefRegrLines();
    DrawCATAnalysisChart_Refresh();
}
function getShowHRateResult(MemberID,onGetHrDataSuccess){
    logStatus("Calling getShowHRateResult", LOG_FUNCTIONS);
    try {
        var hrarr	=	[];
        var hrbgdata=	0;
        var typeID	=	getValidTestTypeID('cardio');
        logStatus(typeID,LOG_DEBUG);
        var MemberData = {};
        //getLastTestResultdata(MemberID,typeID,function(response){
        //getLastTestResultdata(MemberID,typeID,function(response){
        $(".clsanalysenav").attr("disabled","disabled");
        getMemberTestTobeAnalysed({userId:MemberID},function(responseobjs){
            $.each(responseobjs,function(idx,rowobj){
                //_TestList[rowobj.user_test_id]	=	rowobj;
               MemberData	=	rowobj;
                /*if (responseobjs.length > 0){
                    MemberData	=	responseobjs[responseobjs.length-1];
                }*/
                var response =	MemberData;
                //console.log("loadval:"+responseobjs.length);
                if(response.load_value && response.load_value!=''){
                    var loadValue	=	JSON.parse(response.load_value);
                    var heartRateValue	=	{};
                    if(response.heart_rate_value!='')
                     {
                      heartRateValue	=	JSON.parse(response.heart_rate_value);
                    }
                    var loopindex = 0;
                    var bgdataindex=0;
                    var hrbgcddataindex=	0;
                    $.each(loadValue,function(idx,loadrow){
                        if(heartRateValue[idx] && heartRateValue[idx].length>0){
                            if((hrbgdata == 0) && (loadrow.loadtype ==_workoutTestType)){
                                hrbgdata	=	hrarr.length;
                            }
							else if((hrbgcddataindex == 0) && (loadrow.loadtype ==_coolDownLoadType)){
                                hrbgcddataindex	=	hrarr.length-hrbgdata;
                            }
                            $.each(heartRateValue[idx],function(hridx,hrdata){
                                /*Mod 5 Denotes the Every 5 seconds the heartrate is
                                    updated need to mine while inserting into analysis*/
								if(loopindex==0 || (loopindex%5)==0){
                                    hrarr.push(hrdata.data.toString());
                                    /*
                                    if(loadrow.loadtype==_warmupLoadType){
                                        hrbgdata	=	bgdataindex;
                                    } else if(loadrow.loadtype==_coolDownLoadType && hrbgcddataindex==0){
                                        hrbgcddataindex	=	bgdataindex;
                                    }
                                    bgdataindex++;
                                    */
                                }
                                loopindex++;
                            })
                        }
                    });

                    /*Need To remmove later*/
                    if(hrbgcddataindex==0){
                        hrbgcddataindex	=	bgdataindex;
                    }
                }
                _TestList.push({
                    hrarr:hrarr,
                    MemberData:MemberData,
                    graphindexdata:{hrbg:hrbgdata,hrcdbg:hrbgcddataindex} // find XXX
                    // graphindexdata:{hrbg:10,hrcdbg:10}
                });
            });
            if(onGetHrDataSuccess){
                $(".clsanalysenav").removeAttr("disabled");
                onGetHrDataSuccess();
            }
        });
    } catch(e){
        ShowExceptionMessage("getShowHRateResult", e);
    }
}

function ShowAnalysedResult(MemberID,testindex){
	
    logStatus("Calling ShowAnalysedResult", LOG_FUNCTIONS);
    try {
		$('.load_dat').css('display','block');
        //_ClientData_g.weight_ = 82;
        //_ClientData_g.CardioTest_.DataBg_ = 31;
        //_ClientData_g.CardioTest_.DataBg_ = 0;
        //_ClientData_g.CardioTest_.CoolDnBg_ = 199;
		
        getShowHRateResult(MemberID,function(HRdata,aMember,bgdata){
            var index	=	testindex ? testindex : _TestList.length-1;
			if(index==-1){
				//alert("hello");
				$('.load_dat').css('display','none');
			}
            app_tempvars('cindex',index.toString());
            LoadChartData(index);
        });
        /**/
    } catch(e){
		
        ShowExceptionMessage("ShowAnalysedResult", e);
    }
}
function LoadChartData(index){
    logStatus("Calling LoadChartData", LOG_FUNCTIONS);
    try {
        /**
         * @param aMember.is_analysed This is member isanalysed
         * @param aMember.analysed_data This is member analysed data
         * @param cardioobj.CardioCalc() This is cardioobj CardioCalc function
         */
        var $idsaveanalysisdata = $("#idsaveanalysisdata");
        var $idreanalyzedata = $("#idreanalyzedata");
        var $jumpdownup = $("#idjumptrimdown, #idjumptrimup");
        if(index>=0 && index<_TestList.length){
          app_tempvars('cindex',index.toString());
           var data=	_TestList[index];
            var aMember	=	data.MemberData;
            var HRdata	=	data.hrarr;
            var bgdata	=	data.graphindexdata;

            if((bgdata.hrbg) && (bgdata.hrbg!=0) && (bgdata.hrcdbg) && (bgdata.hrcdbg!=0)) {
                _ClientData_g = new TClientData();
                $idsaveanalysisdata.show();
                $idreanalyzedata.hide();
                if ( (aMember.is_analysed == 0) || (_Flag_ReAnalyze) ){
                    _ClientData_g.TestStatus_ = _C_tsToAnalyze;
                    $idsaveanalysisdata.attr("onclick","saveAnalysisChart(this)");
                    $idsaveanalysisdata.html(GetLanguageText(BTN_SAVE));
                    //$("#idsaveanalysisdata").hide();
                    $jumpdownup.removeAttr("disabled");
                } 

                else if (aMember.is_analysed == 2){
                    _ClientData_g.TestStatus_ = _C_tsAnalyzed;
                    $idsaveanalysisdata.attr("onclick","printAnalysedReport(this)");
                    $idsaveanalysisdata.html("Print Again");
                    $jumpdownup.attr("disabled","disabled");
                    //$("#idsaveanalysisdata").hide();
                } else {

                    $idreanalyzedata.show();
                    _ClientData_g.TestStatus_ = _C_tsAnalyzed;
                    $idsaveanalysisdata.attr("onclick","printAnalysedReport(this)");
                    $idsaveanalysisdata.html(GetLanguageText(BTN_PRINT));
                    $jumpdownup.attr("disabled","disabled");
                    //$("#idsaveanalysisdata").hide();
                }

                _ClientData_g.TestMachine_ = _C_tmCycle; //Need to updated from equipment id
                _ClientData_g.gender_ = aMember.gender;
                _ClientData_g.age_ = getAgeBetweenDates(aMember.dob, aMember.test_start_date);
                _ClientData_g.weight_	=	aMember.weight;
                _ClientData_g.SPMed_.Regr1_ = {};
                _ClientData_g.SPMed_.Regr2_ = {};
                _ClientData_g.SPMed_._DeflPoint_ = {};

                ShowHideReAnalyze();
                if(aMember.is_analysed==0){
                    $idreanalyzedata.hide();
                }

                SetAbilityJumpUp();
                SetAbilityJumpDown();

                if ((aMember.analysed_data) && (aMember.analysed_data!='{}') && (!_Flag_ReAnalyze)){
                    var analyseddataobj = {};

                    try{

                    	
                        analyseddataobj = JSON.parse(aMember.analysed_data);
                    } catch(e) {

                    }
                    _ClientData_g.weight_ = analyseddataobj.weight;
                    _ClientData_g.age_ = analyseddataobj.age;
                    _ClientData_g.TestStatus_ = _C_tsAnalyzed;
                    _ClientData_g.SPMed_.Regr1_ = analyseddataobj.regr1;
                    _ClientData_g.SPMed_.Regr2_ = analyseddataobj.regr2;
                    _ClientData_g.SPMed_._DeflPoint_ = analyseddataobj.defpoint;
                }
                

                $("#analysenotes").val(aMember.analysed_info);
                var HRdataArr = HRdata;
                //_load_value
                /*MK Confirm once*/
                _ClientData_g.CardioTest_.DataBg_	=	bgdata.hrbg+1;//Since Workout Starts from the next record
                _ClientData_g.CardioTest_.MarkBgL_	=	0;
                _ClientData_g.CardioTest_.CoolDnBg_	=	bgdata.hrcdbg;//bgdata.hrcdbg;
                _ClientData_g.CardioTest_.MarkBgR_	=	HRdataArr.length;
                _ClientData_g.TestStart_ = _load_value[aMember.test_level];
				
				/**/
                if (HRdataArr.length>_ClientData_g.CardioTest_.DataBg_){
                    _ClientData_g.CardioTest_.HRData_ = HRdataArr;

                    cardioobj._ZoomFlag_ = false;
                    cardioobj._ZoomBg_ = 0;
                    cardioobj._ZoomEnd_ = 0;

                    cardioobj._AppearType_ = _C_taFullData;//_C_taTestData;//_C_taFullData; 
					cardioobj.CardioCalc();
					
					//_C_taTestData;//$(".clsreporttestoption:checked").val();
                    LoadAnalysisGraph();
                }
            } else {
                $idsaveanalysisdata.hide();
                $("#chartResult, #CATchartResult").html("<p>Test result is incomplete/invalid</p>");
            }
        }
    } catch(e){
        ShowExceptionMessage("LoadChartData", e);
    }
}
function saveAnalysisChart(selfobj){
    logStatus("Calling saveAnalysisChart", LOG_FUNCTIONS);
    try {
        /**
         * @param cardioobj.get12WeeksTrainingPlan() This is cardioobj get12WeeksTrainingPlan function
         * @param aMember.r_user_id This is   Member user id
         */
        /*If We save data on the New Table need to create & use service call
        _saveAnalysisChart otherwise proceed with this */
        var cindex		=	+app_tempvars('cindex');
        var aMember		=	_TestList[cindex].MemberData;
        var userTestID	=	aMember.user_test_id;
        var $selfobj	=	$(selfobj);
		var user_id = aMember.r_user_id;
        var analysenotes=	$("#analysenotes").val();
        if(userTestID!=0){
            var analysisdata = {};
            var hrIAND = cardioobj.GetANT_HR();
            analysisdata['age'] = _ClientData_g.age_;
            analysisdata['weight'] = _ClientData_g.weight_;
            analysisdata['max_hr'] = _ClientData_g._HR_Max;
            analysisdata['iant_hr'] = hrIAND.Y;
			analysisdata['max_max_load'] = getWattFromSpeed(_ClientData_g.SPMed_._DeflPoint_.X, _ClientData_g.weight_);
			analysisdata['max_load'] = $('#max_wat').val();
			analysisdata['max_heart_rate'] = $('#max_hrt').val();
            analysisdata['max_watt'] = _ClientData_g._Watt_Max;
			analysisdata['min_hr_70'] = $('#min_y').val();
			analysisdata['max_hr_90'] = $('#max_y').val();
            analysisdata['defpoint'] = _ClientData_g.SPMed_._DeflPoint_;
            analysisdata['regr1'] = _ClientData_g.SPMed_.Regr1_;
            analysisdata['regr2'] = _ClientData_g.SPMed_.Regr2_;
            analysisdata['spm'] = _ClientData_g.SPM_;
            var imgEncodedHR	=	$("#chartResult").find('canvas').attr("id","hrcanvas");
            var imgEncodedCATHR	=	$("#CATchartResult").find('canvas').attr("id","cathrcanvas");
            var hrcanvas	= document.getElementById("hrcanvas");
            var cathrcanvas	= document.getElementById("cathrcanvas");
            var hrgraphimg = hrcanvas.toDataURL();
            var cathrgraphimg = cathrcanvas.toDataURL();
            var encodedgraph		=	{};
            encodedgraph.hr		=	hrgraphimg;
            encodedgraph.cathr	=	cathrgraphimg;
            AddProcessingLoader(selfobj);
            cardioobj.getFitnesslevel(function(fitleveldata){
                analysisdata['fitlevel'] = fitleveldata.fitlevel;
                cardioobj.get12WeeksTrainingPlan(fitleveldata.fitlevel, function(pointdata){
                    _updateHeartRateOnServer({analysed_info:analysenotes,dataAnalysis:JSON.stringify(analysisdata),isAnalysed:1,fitplan: JSON.stringify(pointdata),analysis_graphdata:JSON.stringify(encodedgraph)},userTestID,user_id,function(){
                        RemoveProcessingLoader(selfobj);
                        _Flag_ReAnalyze	=	false;
                        ShowAnalysedResult(aMember.r_user_id);
                        //$("#idsaveanalysisdata").hide();
                    })
                });
				
            });

        } else {
            //MK Need to change Language Text
            ShowAlertMessage('No member test exist');
        }
    } catch(e){
        ShowExceptionMessage("saveAnalysisChart", e);
    }
}
function TransCardioAnalysis(){
    logStatus("Calling TransCardioAnalysis", LOG_FUNCTIONS);
    try {
        TranslateText(".transsimlatedate", TEXTTRANS, BTN_SIMULATE_HR_DATE);
        TranslateText(".transshowresult", TEXTTRANS, BTN_SHOW_RESULT);
        TranslateText(".transloadvalue", TEXTTRANS, BTN_SHOW_LOAD_VALUE);
        TranslateText(".transtestdate", TEXTTRANS, LBL_TEST_DATE);
        TranslateText(".transfulldate", TEXTTRANS, LBL_FULL_DATE);
        TranslateText(".transtrim", TEXTTRANS, LBL_TRIM);
        TranslateText(".transjump", TEXTTRANS, LBL_JUMP);
        TranslateText(".transgraph", TEXTTRANS, LBL_HEART_RATE_GRAPH);
        TranslateText(".transcatgraph", TEXTTRANS, LBL_CAT_HEART_RATE_GRAPH);
        TranslateText(".chartanalysis", PHTRANS, PH_ENTER_CHART_ANALYSIS);
        TranslateText(".transreanalyse", TEXTTRANS, BTN_REANALYSE);
        TranslateText(".transcancel", TEXTTRANS, BTN_CANCEL);
    } catch(e){
        ShowExceptionMessage("TransCardioAnalysis", e);
    }
}
function printAnalysedReport(selfobj){
    logStatus("Calling printAnalysedReport", LOG_FUNCTIONS);
    try {
        //selfobj
        var cindex		=	+app_tempvars('cindex');
        var aMember		=	_TestList[cindex].MemberData;
        var userTestID	=	aMember.user_test_id;
        var gender	=	aMember.gender;
        var memberId	=	aMember.r_user_id;
        var memage	=	getAgeByDOB(aMember.dob);
        var data = {};
        data.userTestId = userTestID;
        data.gender = gender;
        data.memage = memage;
        data.memberId = memberId;
        AddProcessingLoader(selfobj);
        MailAnalysisReportPDF(data,function(response){
            RemoveProcessingLoader(selfobj);
            if(response.status=='success'){
                _updateHeartRateOnServer({isAnalysed:2},userTestID,function(){
                    $("#chartResult").find('canvas').removeAttr("id");
                    $("#CATchartResult").find('canvas').removeAttr("id");
                });
            }
        });
    } catch(e){
        ShowExceptionMessage("printAnalysedReport", e);
    }
}
function ReAnalyzeTest(self){
    logStatus("Calling ReAnalyzeTest", LOG_FUNCTIONS);
    logStatus(self, LOG_DEBUG);
    try {
        _Flag_ReAnalyze	=	true;
        var memberID	=	GetUrlArg('memberid',true);
        var cindex	=	+app_tempvars('cindex');
        ShowAnalysedResult(memberID,cindex);//LoadChartData(cindex);
    } catch(e){
        ShowExceptionMessage("ReAnalyzeTest", e);
    }
}
function CancelReAnalyzeTest(self){
    logStatus("Calling CancelReAnalyzeTest", LOG_FUNCTIONS);
    logStatus(self,LOG_DEBUG);
    try {
        _Flag_ReAnalyze	=	false;
        var cindex	=	+app_tempvars('cindex');
        var memberID	=	GetUrlArg('memberid',true);
        ShowAnalysedResult(memberID,cindex);//LoadChartData(cindex);
    } catch(e){
        ShowExceptionMessage("CancelReAnalyzeTest", e);
    }
}
function ShowHideReAnalyze(){
    logStatus("Calling ShowHideReAnalyze", LOG_FUNCTIONS);
    try {
        if(_Flag_ReAnalyze){
            $("#idcancelreanalyzedata").show();
            $("#idreanalyzedata").hide();
        } else {
            $("#idcancelreanalyzedata").hide();
            $("#idreanalyzedata").show();
        }
    } catch(e){
        ShowExceptionMessage("ShowHideReAnalyze", e);
    }
}

function SetAbilityJumpUp(){
    logStatus("Calling SetAbilityJumpUp", LOG_FUNCTIONS);
    try {
        if (TCATChartAnalysis.TRIMBOLDOption == _C_TrimBoldFlag){ //Jump
            if(cardioobj._ANTI_ >=(cardioobj._VArray_.length - 1)) {
                $("#idjumptrimup").attr("disabled","disabled");
            } else {
                $("#idjumptrimup").removeAttr("disabled");
            }
        }else{
           if (cardioobj._TrimJust_ >= 200){
           //Xx Just to make it work at the moment $("#idjumptrimup").attr("disabled","disabled");
           } else {
                $("#idjumptrimup").removeAttr("disabled");
            }
        }
    } catch(e){
        ShowExceptionMessage("SetAbilityJumpUp", e);
    }
}
function SetAbilityJumpDown(){
    logStatus("Calling SetAbilityJumpDown", LOG_FUNCTIONS);
    try {
        if (TCATChartAnalysis.TRIMBOLDOption == _C_TrimBoldFlag){ //Jump
            if (cardioobj._ANTI_ <=	2){
                $("#idjumptrimdown").attr("disabled","disabled");
            } else {
                $("#idjumptrimdown").removeAttr("disabled");
            }
        }else{
            if (cardioobj._TrimJust_ > -200){
            //Xx Just to make it work at the moment $("#idjumptrimdown").attr("disabled","disabled");
            } else {
                $("#idjumptrimdown").removeAttr("disabled");
            }
        }
    } catch(e){
        ShowExceptionMessage("SetAbilityJumpDown", e);
    }
}
function manualtraining(){
	var memid = $('#memberid').val();
	 var memberid	=	base64_encode(memid);
        _movePageTo('manual_data',{memberid:memberid});
	
	
}