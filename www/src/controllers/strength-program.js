$(function(){
    TranslateGenericText();
    _setPageTitle(GetLanguageText(PAGE_TTL_STRENGTH_PROGRAM_OVERVIEW));
    //"Strength Program Overview"
    TransStrengthProgram();
    var urldata	=	getPathHashData();
    //var clubId	=	getselectedclubid();
    var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    //var testid	=	(urldata.args.testid && urldata.args.testid!='undefined') ? base64_decode(urldata.args.testid) :0;
    /**
     * @param response.test_start_date This is test strat date
     */
    _getClientInformationByID(memberId,function(response){
        $('#client_name').val(response.first_name+" "+response.last_name );
        $('#client_email').val(response.email);
        var sdate = new Date();
        var newdate = sdate.toUTCString(response.test_start_date);
        $("#idstartdate").val(newdate);
        var currentweek = getCurrentWeekdate(newdate);
        $("#trainingweekdata").val(currentweek);
    });
    /* get client Maplist Details */
    /**
     * @param response.result.r_strength_program_id This is strength program id
     * @param response.result.strength_user_test_id This is strength user test id
     */
    _getMachienMappedetails(memberId,function(response){ // getting mapping values through member id.
    //alert(JSON.stringify(response));
        $(".clsLastProgramName").html(response.result.testid);
        $(".clsLastProgramNameHidden").val(response.result.r_strength_program_id);
        $(".clsLastProgramNameid").val(response.result.strength_user_test_id);
        $(".trainingsdone").val(response.count);
        /**
         * @param responsedata.week_start_date This is week start date
         * @param responsedata.week_end_date This is week end date
         */
            _getweekdatelist(response.result.strength_user_test_id,memberId,response.count,function(responsedata){
                //alert(responsedata.week_start_date);
                $("#trainingweekdata").val(responsedata.week_start_date+"-"+responsedata.week_end_date);
            });
    });
    /* show Grid Data */
    getMachienMappedetailsGrid(memberId,function(response){
        $("#strengthOverViewProgram").html(response);
    });
});

/* getting week Details Dynamically */
$(document).delegate(".clsweektraining","change",function(){
    logStatus("Event getCurrentWeekdate click", LOG_FUNCTIONS);
    try{
        var selectweek = $( ".clsweektraining option:selected" ).val();
        var seelcteddate = $("#idstartdate").val();
        var getweekdetails = getCurrentWeekdate(seelcteddate,selectweek);
        $("#trainingweekdata").val(getweekdetails);
    } catch (e){
        ShowExceptionMessage("getCurrentWeekdate",e);
    }
});
/* getting week Details Dynamically */
function getCurrentWeekdate(newdate,week){
    logStatus("Event getCurrentWeekdate click", LOG_FUNCTIONS);
    try{
        var weekdays = (week) ? week:0;
        var last = 0;
        var startday = 0;
        var lastday = 0;
    //	alert(weekdays * 6);
        var formateddate= '';
        if(weekdays > 0){
            var d = new Date(newdate);
            d.setDate(d.getDate() + Number(weekdays * 6));
            var date = d.getDate();
             last = date + 6;
             startday = new Date(d.setDate(date)).toUTCString();
             lastday = new Date(d.setDate(last)).toUTCString();
            formateddate = (dateFormats(startday)+ "-" +dateFormats(lastday));
        } else {
            var curr = new Date(newdate);
            var first = curr.getDate(); // First day is the day of the month - the day of the week
             last = first + 6; // last day is the first day + 6
             startday = new Date(curr.setDate(first)).toUTCString();
             lastday = new Date(curr.setDate(last)).toUTCString();
            formateddate = (dateFormats(startday)+ "-" +dateFormats(lastday));
        }
        return formateddate;
    } catch (e){
        ShowExceptionMessage("getCurrentWeekdate",e);
    }
}
/* Proper Date Format */
function dateFormats(newDate){
    logStatus("Event dateFormats click", LOG_FUNCTIONS);
    try{
        var da = new Date(newDate);
        var dy = da.getFullYear() ;
        var dm = da.getMonth() + 1 ;
        var dd = da.getDate() ;
        if ( dy < 1970 ) dy = dy + 100;
        var ys = String(dy);
        var ms = String(dm) ;
        var ds = String(dd) ;
        if ( ms.length == 1 ) ms = "0" + ms;
        if ( ds.length == 1 ) ds = "0" + ds;
        ys = ds + "/" + ms + "/" + ys	;
        return ys;
    } catch(e){
        ShowExceptionMessage("dateFormats",e);
    }
}
/* Strength Program Data by Dynamically */
function changeShowStrengthprogram(){
    logStatus("Calling changeShowStrengthprogram event function ",LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0; // passing member id;
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0; // passing club id;
        var programid	= $(".clsLastProgramNameid").val(); // passing based by program id; base class can be get form backend service data;
        var pid = (programid) ? programid: 50;
        if(memberId!='' && clubid !='' && pid!=''){
                _movePageTo('change-strengthprogram',{mid:base64_encode(memberId),programid:base64_encode(pid),clubid:base64_encode(clubid)});
        }
    } catch(e) {
        ShowExceptionMessage("changeShowStrengthprogram",e);
    }
}

function changeSheduleProgram(){
    logStatus("Calling changeSheduleProgram", LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        var programid	= $(".clsLastProgramNameid").val();
        var pid = (programid) ? programid: 50; // basic Exercise Plan
        if(memberId!='' && clubid !='' && pid!=''){
                _movePageTo('change-shedulemachine',{mid:base64_encode(memberId),programid:base64_encode(pid),clubid:base64_encode(clubid)});
        }
    } catch(e){
        ShowExceptionMessage("changeSheduleProgram",e);
    }
}

function addMachinetoProgram(){
    logStatus("Calling changeSheduleProgram", LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        var programid	= $(".clsLastProgramNameHidden").val();
        var pid = (programid) ? programid: 50; // basic Exercise Plan
        if(memberId!='' && clubid !='' && pid!=''){
            _movePageTo('add-machine',{mid:base64_encode(memberId),programid:base64_encode(pid),clubid:base64_encode(clubid)});
        }
    } catch (e){
        ShowExceptionMessage("changeSheduleProgram",e);
    }
}
/* remove Machine for feature taining/program*/
function removeMachienForfeatureTrainign(){
    logStatus("Calling removeMachienForfeatureTrainign ", LOG_FUNCTIONS);
    try{
        var urldata = getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        var programid	= $(".clsLastProgramNameid").val();
        var flag	= 1;
        var pid = (programid) ? programid: 50; // basic Exercise Plan
        if(memberId!='' && clubid !='' && pid!=''){
            _movePageTo('add-machine',{mid:base64_encode(memberId),programid:base64_encode(pid),clubid:base64_encode(clubid),flag:base64_encode(flag)});
        }

    } catch(e){
        ShowExceptionMessage("removeMachienForfeatureTrainign",e);
    }
}
/* remove Machine for next training only*/
/*
function removeMachineForNextTraining(){
    logStatus("Calling removeMachienForNextTraining",LOG_FUNCTIONS);
    try{
        var urldata = getPathHashData();
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
        var programid	= $(".clsLastProgramNameid").val();
        var pid = (programid) ? programid: 50; // basic Exercise Plan
        if(memberId!='' && clubid !='' && pid!=''){
            _movePageTo('change-shedulemachine',{mid:base64_encode(memberId),programid:base64_encode(pid),clubid:base64_encode(clubid)});
        }
    } catch(e){
        ShowExceptionMessage("removeMachineForNextTraining",e);
    }
}
*/
function TransStrengthProgram(){
     TranslateText(".transstrengthweek", TEXTTRANS, LBL_STRENGTH_WEEK);
     TranslateText(".transmemberlist", TEXTTRANS, BTN_MEMBER_LIST);
     TranslateText(".transcardio", TEXTTRANS, BTN_CARDIO_TEST);
     TranslateText(".transbodycomp", TEXTTRANS, BTN_BODY_COMP);
     TranslateText(".transflexibility", TEXTTRANS, BTN_FLEXIBILITY_TEST);
     TranslateText(".transreports", TEXTTRANS, BTN_REPORTS);
     TranslateText(".transovertraining", TEXTTRANS, BTN_OVERTRAINING);
     TranslateText(".transcluborg", TEXTTRANS, LBL_CLUB_ORG);
     TranslateText(".transamttrainingdone", TEXTTRANS, TXT_AMOUNT_TRAINING_DONE);
     TranslateText(".transslcttraining", TEXTTRANS, TXT_SELECTED_TRAINING_PROGRAM);
     TranslateText(".transtrainingngorgval", TEXTTRANS, TXT_TRAINING_ORG_VALUES);
     TranslateText(".transsavenexttrng", TEXTTRANS, TXT_SAVE_NEXT_TRAINING);
     TranslateText(".transshowweights", TEXTTRANS, TXT_SHOW_WEIGHTS);
     TranslateText(".transtoestel", TEXTTRANS, TXT_TOESTEL);
     TranslateText(".transstartweek", TEXTTRANS, TXT_STARTWEEK);
     TranslateText(".transchangeschdule", TEXTTRANS, TXT_CHANGE_SCHDULE);
     TranslateText(".transchangeshow", TEXTTRANS, TXT_CHANGE_SHOW);
     TranslateText(".transaddmachine", TEXTTRANS, TXT_ADD_MACHINE);
     TranslateText(".transremovemachine", TEXTTRANS, TXT_REMOVE_MACHINE);
     TranslateText(".transremovenext", TEXTTRANS, TXT_REMOVE_NEXT);
     TranslateText(".transtrainingone", TEXTTRANS, TXT_TRAINING_ONE);
     TranslateText(".transtrainingtwo", TEXTTRANS, TXT_TRAINING_TWO);
     TranslateText(".transtrainingthree", TEXTTRANS, TXT_TRAINING_THREE);
     TranslateText(".transtrainingfour", TEXTTRANS, TXT_TRAINING_FOUR);
     TranslateText(".transtrainingfive", TEXTTRANS, TXT_TRAINING_FIVE);
     TranslateText(".transtrainingsix", TEXTTRANS, TXT_TRAINING_SIX);
}

$(document).ready(function(){
    maincontent_leff_none();
    $(window).resize(function(){
        //maincontent_leff_none();
    });
});