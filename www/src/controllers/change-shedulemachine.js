$(function(){
    TranslateGenericText();
    TranslateChangeSheduleMachine();
    logStatus("Calling change-strength program document",LOG_FUNCTIONS);
    try{
        _setPageTitle(GetLanguageText(PAGE_TTL_STRENGTH_PRGM_MAPPING));
        var urldata	=	getPathHashData();
        var clubId	=	getselectedclubid();
        logStatus(clubId,LOG_DEBUG);
        var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
        var pid	=	(urldata.args.programid && urldata.args.programid!='undefined') ? base64_decode(urldata.args.programid) :0;
        _getClientInformationByID(memberId,function(response){
            $("#client_id_name").val(response.first_name+" "+response.last_name );
            $("#client_id_email").val(response.email);
        });
        if(pid){
            /* get program list mappeb by user followe by userid and program id */
            _getMappedMachienbyProgramId(pid,memberId,function(response){
                $(".programtabel").find('tbody').html(response);
            },1);
        }

    } catch (e){
        ShowExceptionMessage("change-Strength Program document", e);
    }
});


$(document).delegate("#idprogramlist","click",function(){
    logStatus("Calling idprogramlist click event",LOG_FUNCTIONS);
    try{
        var tapvalue = $(this).find('input').attr("circuttype");
        if(tapvalue !='' || tapvalue !='undefined'){
            getWeekProgramTraining(tapvalue,function(response){
                $(".programweektabel").find('tbody').html(response);
            },1,'');
        }
    } catch(e){
        ShowExceptionMessage("idprogramlist",e);
    }

});

$(document).on("click",'.pageindex',function(){
    logStatus("Event pageindex click", LOG_FUNCTIONS);
    try {
        var $self		=	$(this);
        var manageAction	=	"";
        var manageElement	=	"";
        var page		=	$self.attr('page');
        var pageindex	=	$self.attr('pageindex');
        var action_tab	=	$self.attr('action_tab');
        if(page=='members'){
            manageAction	=	'ListMachinesData';
            manageElement	=	'.managetable';
        }
        getManageData(manageAction,manageElement,action_tab,pageindex)
    }catch(e) {
        ShowExceptionMessage("Event pageindex click", e);
    }
});
function getManageData(action,table,activity,pageindex){
    logStatus("Calling getManageData ", LOG_FUNCTIONS);
    try{
        logStatus(action,LOG_DEBUG);
        logStatus(table,LOG_DEBUG);
        $('.tabsbox').on('click','span',function(){
            $('.tabsbox > span').removeClass("active");
            $(this).addClass("active");
        });
        var postdata	=	{};
        if(pageindex){
            postdata.page	=	pageindex;
        }
        postdata.tab	=	activity;
        /*getListMachinesData1(action,table,postdata,function(response){
            //alert(JSON.stringify(response));

        });*/
    } catch(e) {
        ShowExceptionMessage("getManageData", e);
    }
}
/*
function isAllowedColen(evt) {
    logStatus("Calling isAllowedColen ", LOG_FUNCTIONS);
    try{
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 58)
        return false;
    } catch (e){
        ShowExceptionMessage("isAllowedColen", e);
    }
}
*/
$(document).delegate(".strg_val", "click",function(){
    logStatus("Calling strg_val ", LOG_FUNCTIONS);
    try{
        var $this = $(this);
        $this.hide();
        $this.parent().find(".strg_int").show();
    } catch (e){
        ShowExceptionMessage("strg_val", e);
    }
});

$(document).delegate("#OnEditweekdetails","click",function(){
    logStatus("Calling strg_val ", LOG_FUNCTIONS);
    try{
        var href = $(this).attr('hrefValue');
        $('.programtabel table td .selectbtn').removeClass("acti_cls");
        $(this).addClass("acti_cls");
        var value = $(this).attr('name');
        $("#selectedMachinedet").html(value);
        var tapvalue = $(this).attr("circuittype");
        if(tapvalue !='' || tapvalue !='undefined'){
            getWeekProgramTraining(tapvalue,function(response){
                $(".programweektabel").find('tbody').html(response);
            },0,href);
        }
    } catch (e){
        ShowExceptionMessage("strg_val", e);
    }
});
/*
function AddMachineToProgram(){
    logStatus("Calling strg_val ", LOG_FUNCTIONS);
    try{
        var urldata	=	getPathHashData();
        var pid	=	(urldata.args.programid && urldata.args.programid!='undefined') ? base64_decode(urldata.args.programid) :0;
        logStatus(pid,LOG_DEBUG);
        _popup('<div class="popuptitle">Select your Location</div><div class="field_in_box"></div>',{close:false})
    } catch (e){
        ShowExceptionMessage("strg_val", e);
    }
}
*/
/*
function addMachinetoProgramSet(){
    logStatus("Calling strg_val ", LOG_FUNCTIONS);
    try{
        var checkval =[];
        $(".machineDetails:checked").each(function(){
            var checkedval = $(this).attr("hrefvalue");
            checkval.push(checkedval);
        });
        var machineval = checkval.join();
        logStatus(machineval,LOG_DEBUG);
    } catch (e){
        ShowExceptionMessage("strg_val", e);
    }
}
*/
function SavechangesselcMachiennextTraining(){
    logStatus("Calling strg_val ", LOG_FUNCTIONS);
    try{
        var ischnagval = [];
        $(".strg_int").each(function(){
            var checkedval = $(this).val();
            ischnagval.push(checkedval);
        });

    }catch (e){
        ShowExceptionMessage("strg_val", e);
    }
}
/* Add or Remove program for Client purpose */
/*
function SavechangesselcMachiennextTraining(){
    logStatus("Calling SavechangesselcMachiennextTraining",LOG_FUNCTIONS);
    try{

    } catch(e){

    }
}
*/
 function TranslateChangeSheduleMachine(){
     TranslateText(".transcardio", TEXTTRANS, BTN_CARDIO_TEST);
     TranslateText(".transbodycomp", TEXTTRANS, BTN_BODY_COMP);
     TranslateText(".transflexibility", TEXTTRANS, BTN_FLEXIBILITY_TEST);
     TranslateText(".transreports", TEXTTRANS, BTN_REPORTS);
     TranslateText(".transovertraining", TEXTTRANS, BTN_OVERTRAINING);
     TranslateText(".transsltmachine", TEXTTRANS, TXT_SLT_MACHINE);
     TranslateText(".transchangemachien", TEXTTRANS, BTN_SAVE_CHANGE_MACHIEN);
 }
