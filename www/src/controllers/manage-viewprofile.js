$(function(){
    TransManageViewProfile();
    var urldata	=	getPathHashData();
    var memberId=	(urldata.args.mid) ? base64_decode(urldata.args.mid) :0;
    //var testid	=	(urldata.args.testid && urldata.args.testid!='undefined') ? base64_decode(urldata.args.testid) :0;
    //var clubid	=	(urldata.args.clubid) ? base64_decode(urldata.args.clubid) :0;
    var profiledata = '';
    /**
     * @param profiledata.t_ismedicalinfo This is user data t_ismedicalinfo
     * @param profiledata.address This is  user address
     * @param profiledata.doorno This is  user door no
     * @param profiledata.bus This is  user bus
     * @param profiledata.zipcode This is  user zipcode
     * @param profiledata.dob This is  user dob
     * @param profiledata.doctor This is  user doctor
     * @param profiledata.doctor_address This is  user doctor address
     * @param profiledata.doctor_doorno This is  user doctor door no
     * @param profiledata.doctor_bus This is  user doctor bus
     * @param profiledata.doctor_zipcode This is  user doctor zip code
     * @param profiledata.doctor_city This is  user doctor city
     * @param profiledata.doctor_country This is  user doctor country
     * @param profiledata.doctor_phone This is  user doctor phone
     * @param profiledata.doctor_email This is  user doctor email
     * @param profiledata.injury_description This is  user injury description
     * @param response.CityByZipcode This is  city by zipcode
     * @param city.city_id This is  city  id
     * @param city.city_name This is  city  name
     * @param profiledata.r_city_id This is  city id
     * @param profiledata.profession_labortype This is  profession_labortype
     * @param profiledata.have_medication This is  have_medication
     * @param profiledata.have_stress_test This is  have_stress_test
     * @param profiledata.have_smoke This is  have_smoke
     * @param profiledata.feel_today This is  feel_today
     * @param profiledata.medication_description This is  medication description
     * @param profiledata.stress_test_on This is  stress test on
     * @param profiledata.smoke_amount This is  smoke amount
     * @param profiledata.entry_date This is  entry date
     */
    _getClientProfileInformationByID(memberId,function(data){
        profiledata = data[memberId];
        if(profiledata.t_ismedicalinfo==1){
            $('.isclubmedical').css("display","block");
        }
        $('.regusername').html(profiledata.first_name);
        $('#client_street').val(profiledata.address);
        $('#client_number').val(profiledata.doorno);
        $('#client_bus').val(profiledata.bus);
        $('#client_zipcode').val(profiledata.zipcode);
        $('#client_dob').val(profiledata.dob);
        $('#client_length').val(profiledata.height);
        $('#client_weight').val(profiledata.weight);
        $('#client_doctor_name').val(profiledata.doctor);
        $('#client_doctor_street').val(profiledata.doctor_address);
        $('#client_doctor_doorno').val(profiledata.doctor_doorno);
        $('#client_doctor_bus').val(profiledata.doctor_bus);
        $('#client_doctor_zipcode').val(profiledata.doctor_zipcode);
        var $doccityoption = $("#client_doctor_place");
        $doccityoption.val(profiledata.doctor_city);
        $('#client_doctor_country').val(profiledata.doctor_country);
        $('#client_doctor_phone').val(profiledata.doctor_phone);
        $('#client_doctor_email').val(profiledata.doctor_email);
        $('#injurytext').val(profiledata.injury_description);
        getValidImageUrl(PROFILEPATH + profiledata.userimage,function(url){
            $('#regusrimg').attr('src',url);
        });
        var $cityoption = $("#client_place");
            $cityoption.attr("disabled", "disabled");
            $cityoption.html('<option value="0">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
            getCityListByZipCode({zipCode:profiledata.zipcode},function(response){
                var optionhtml	=	'<option value="0">Select City</option>';//$cityoption.html(city.city_name);
                if(response.CityByZipcode) {
                    var cities 	=	(response.CityByZipcode.length>0) ? response.CityByZipcode:{};
                    $.each(cities,function(_index,city){
                        optionhtml+='<option value="' + city.city_id + '">' + city.city_name + '</option>';
                    });
                } else{
                    optionhtml	=	'<option value="0">Please enter valid zipcode</option>';
                }
                $cityoption.html(optionhtml);
                $('#client_place').val(profiledata.r_city_id);
                $('#client_placetext').text(profiledata.location);
                $cityoption.removeAttr("disabled");
            });
            $doccityoption.attr("disabled", "disabled");
            $doccityoption.html('<option value="0">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
            getCityListByZipCode({zipCode:profiledata.doctor_zipcode},function(response){
                var optionhtml	=	'<option value="0">Select City</option>';//$cityoption.html(city.city_name);
                if(response.CityByZipcode) {
                    var cities 	=	(response.CityByZipcode.length>0) ? response.CityByZipcode:{};
                    $.each(cities,function(_index,city){
                        optionhtml+='<option value="' + city.city_id + '">' + city.city_name + '</option>';
                    });
                } else{
                    optionhtml	=	'<option value="0">Please enter valid zipcode</option>';
                }
                $doccityoption.html(optionhtml);
                $('#client_doctor_place').val(data.doctor_city);
                $('#client_placetext').text(data.location);
                $doccityoption.removeAttr("disabled");
            });
            var $country	=	$("#client_country,#client_doctor_country");
            $country.html('<option selected="selected"">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
        /**
         * @param response.nationalities This is nationalities detail
         * @param row.nationality_id This is nationality id
         * @param row.nationality_name This is nationality name
         * @param data.r_nationality_id This is nationality id
         */
            loadMemberOptionList('getNationalityList',function(response){
                if(response.nationalities && (response.nationalities.length>0)){
                    var naionalityoptions	=	'';
                    $.each(response.nationalities,function(_index,row){
                        var selected =(row.nationality_id==23) ?'selected':'';
                        if(row.nationality_id==23){
                            $("#client_countrytext").text(row.nationality_name);
                        }
                        naionalityoptions+='<option value="'+row.nationality_id+'" '+selected+'>'+row.nationality_name+'</option>';
                    });
                    $country.html(naionalityoptions);
                    $country.val(data.r_nationality_id);
                    $country.removeAttr("placeholder");
                }
            });
            //var $activity	=	$(".client_activity");
        /**
         * @param response.activities This is activities
         * @param response.activity_id This is activity id
         * @param response.activity_name This is activity name
         * @param data.activity This is activity detail
         * @param row.frequency_type This is frequency type
         * @param row.activityid This is activity id
         * @param row.average_time This is average_time
         */
            loadMemberOptionList('getActivityList',function(response){
                var activity_options	=	'<option value="0">select activity</option>';
                if(response.activities && (response.activities.length>0)){
                    $.each(response.activities,function(_index,row){
                         activity_options	+=	'<option value="' + row.activity_id + '">' + row.activity_name + '</option>';
                    });
                    $('#client_activity_0').html(activity_options);
                }
                var activitydata = data.activity;
                var i =0;
                $.each(activitydata,function(key,row){
                    if(i!=0){
                        var rowcnt	=	$(".activity_row").length;
                        var selectoptions	=	$("#client_activity_0").html();
                        var HTML	=	'<div class="activity_row">'+
                                '<div class="inpt_bx">'+
                                    '<span id="client_activity_' + rowcnt + 'text" class="input1 selectchrbox client_ac_select_text ">select activity</span>'+
                                    '<select id="client_activity_' + rowcnt + '" class="client_ac_select client_activity selectbox select_opcity req-nonzero">'+
                                        selectoptions +
                                    '</select>' +
                                '</div>'+
                            '<div class="two_radio_box">' +
                                '<div class="single_radio_box">' +
                                    '<span class="radiobox">' +
                                        '<input class="training_freq'+ rowcnt +'" type="radio" name="training_freq" value="1" row="' + i + '"/>' +
                                    '</span>' +
                                    '<label>Montly</label>' +
                                '</div>' +
                                '<div class="single_radio_box">' +
                                    '<span class="radiobox">' +
                                        '<input class="training_freq'+ rowcnt +'" type="radio" name="training_freq" value="2" row="' + rowcnt + '"/>' +
                                    '</span>' +
                                    '<label>Weekly</label>' +
                                '</div>' +
                                '<input type="hidden" id="trainingfreq_' + rowcnt + '" value="1" />' +
                            '</div>' +
                            '<div class="field_box averbox">' +
                                '<label>Average time (Minutes)</label>' +
                                '<input type="text" id="avgtime_' + i + '" /> '+
                                '<div class="clear"></div>'+
                            '</div>'+
                        '</div>';
                        $(".activity_row:last").after(HTML);
                    }
                    var $trainig_freq = $('.training_freq'+i+'[value="'+row.frequency_type+'"');
                    $trainig_freq.attr('checked',true);
                    $trainig_freq.parent("span").addClass("radiobtn");
                    $('#client_activity_'+i+'text').text(row.activity_name);
                    $('#client_activity_'+i).val(row.activityid);
                    $('#avgtime_'+i).val(row.average_time);
                    i = i+1;
                });
            });
            var $profession	=	$("#client_profession");
            $profession	.html('<option selected="selected">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
        /**
         * @param response.professions This is professions
         * @param row.profession_id This is profession id
         * @param row.profession_name This is profession name
         * @param data.r_profession_id This is profession id
         */
            loadMemberOptionList('getProfessionList',function(response){
                var professionoptions	=	'<option value="0">Select Profession</option>';
                if(response.professions && (response.professions.length>0)){
                    $.each(response.professions,function(_index,row){
                         professionoptions	+=	'<option value="' + row.profession_id + '">' + row.profession_name + '</option>';
                    });
                    $profession.html(professionoptions);
                    $profession.val(data.r_profession_id);
                    $('#client_professiontext').text(data.profession_name);
                }
            });
            var $labortype = $('.labortype[value="'+profiledata.profession_labortype+'"');
            var $medication = $('.medication[value="'+profiledata.have_medication+'"');
            var $streettest = $('.streetest[value="'+profiledata.have_stress_test+'"');
            var $smoketest = $('.smoketest[value="'+profiledata.have_smoke+'"');
            var $curbodycondition = $('.curBodyCondition[value="'+profiledata.feel_today+'"');
            $labortype.attr("checked",true);
            $labortype.parent("span").addClass("radiobtn");
            $medication.attr("checked",true);
            $medication.parent("span").addClass("radiobtn");
            var $medicationDesc = $('#medicationDesc');
            var $smokeAmount = $('#smokeAmount');
            if($('.medication[value="0"]').is(":checked")){
                $medicationDesc.attr('disabled',true);
            }
            $medicationDesc.val(profiledata.medication_description);
            $streettest.attr("checked",true);
            $streettest.parent("span").addClass("radiobtn");
            $('#stressTestOn').val(dateFormatYear(profiledata.stress_test_on));
            $smoketest.attr("checked",true);
            $smoketest.parent("span").addClass("radiobtn");
            if($('.smoketest[value="0"]').is(":checked")){
                $smokeAmount.attr('disabled',true);
            }
            $smokeAmount.val(profiledata.smoke_amount);
            $curbodycondition.attr("checked",true);
            $curbodycondition.parent("span").addClass("radiobtn");
        //	var signature = document.getElementById("myCanvas");
            var sign = (profiledata.signature!='') ? JSON.parse(profiledata.signature):{};
            var $acceptDeclaration=$('#acceptDeclaration');
            if(sign.signature){
                $acceptDeclaration.attr("checked",true);
                $acceptDeclaration.parent("span").addClass("radiobtn");
                var imagedata = '<img src="'+sign.signature+'"/>';
                $("#signature_src").attr("src",imagedata);
            }



            $('#entrydate').val(dateFormatYear(profiledata.entry_date));

    });

        //	var $cityoption	=	($this.attr('id')=='client_zipcode')  ? $("#client_place"):$("#client_doctor_place");
});	
function dateFormatYear(datetime){

    var t = datetime.split(/[- :]/);
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    var date = d.getDate();
    date = date < 10 ? "0"+date : date;

   var  mon = d.getMonth()+1;
    mon = mon < 10 ? "0"+mon : mon;

   var  year = d.getFullYear();

    return (year+"-"+mon+"-"+date);
}
$(document).delegate("#client_zipcode, #client_doctor_zipcode","change",function(){
    logStatus("Event client_zipcode change", LOG_FUNCTIONS);
    var $this 	=	$(this);
    if($this.val()!=''){
        var $cityoption	=	($this.attr('id')=='client_zipcode')  ? $("#client_place"):$("#client_doctor_place");
        $cityoption.attr("disabled", "disabled");
        $cityoption.html('<option value="0">'+GetLanguageText(TXT_LOAD_WAITING_MSG)+'</option>');
        getCityListByZipCode({zipCode:$this.val()},function(response){
            var optionhtml	=	'<option value="0">Select City</option>';//$cityoption.html(city.city_name);
            if(response.CityByZipcode) {
                var cities 	=	(response.CityByZipcode.length>0) ? response.CityByZipcode:{};
                $.each(cities,function(_index,city){
                    optionhtml+='<option value="' + city.city_id + '">' + city.city_name + '</option>';
                });
            } else{
                optionhtml	=	'<option value="0">Please enter valid zipcode</option>';
            }
            $cityoption.html(optionhtml);
            $cityoption.removeAttr("disabled");
        })
    }
});
$(document).delegate("#client_place","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_placetext").text(selecttext);
});
$(document).delegate("#client_country","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_countrytext").text(selecttext);
});
$(document).delegate("#client_profession","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_professiontext").text(selecttext);
});
$(document).delegate(".client_ac_select","change",function(){
    var select_text = $(this).find("option:selected").text();
    $(this).parent().find(".client_ac_select_text").text(select_text);
});
$(document).delegate("#client_doctor_place","change",function(){
    var selecttext = $(this).find("option:selected" ).text();
    $("#client_doctor_placetext").text(selecttext);
});

function TransManageViewProfile(){
     TranslateText(".transloadingmsg", TEXTTRANS, TXT_LOAD_WAITING_MSG);
     TranslateText(".transstreet", PHTRANS, PH_STREET);
     TranslateText(".transnumber", PHTRANS, PH_NUMBER);
     TranslateText(".transbus", PHTRANS, PH_BUS);
     TranslateText(".transzipcode", PHTRANS, PH_ZIPCODE);
     TranslateText(".transplace", TEXTTRANS, TEX_PLACE);
     TranslateText(".transcountry", TEXTTRANS, TXT_COUNTRY);
}

$(document).ready(function(){
    //maincontent_leftimage();
    $(".main_left").show().removeClass("background1 background2 background4").addClass("background3");

    // $(window).resize(function(){
        // maincontent_leftimage();
    // });
});